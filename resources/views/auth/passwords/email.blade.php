    <!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Login</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{url('css/footer.css')}}">
        <link rel="stylesheet" href="{{url('css/register.css')}}">
        <link rel="stylesheet" href="{{url('css/register-responsive.css')}}">
                <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('css/font-and-global.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

      <style type="text/css">

      </style>
    </head>
    <body>
    <div id="loader" class="loader"></div>

        <section id="register-section">
            <div class="container br" id="register-container">
                <div class="row v-center">
                    <div class = "col-lg-12 col-md-12 col-sm-12 p-l-zero box-shadow-all">
                        
                        <div class = "col-lg-6 col-md-6 col-sm-6 bg-blue border-right">
                            <img src="{{url('icons/white-logo.svg')}}" class="logo">

                                    <div id="myCarousel" class="carousel slide col-lg-12 col-md-12 col-sm-12" data-ride="carousel">
                                      <!-- Indicators -->
                                      <ol class="carousel-indicators">
                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                      </ol>

                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner">
                                        <div class="item active">
                                          <h1>Zero <span>Fees</span></h1>
                                          <p>No hidden Costs. </p>
                                          <p>We are paid by the asset management Companies. You pay us nothing at all.</p>
                                        </div>

                                        <div class="item">
                                          <h1>Invest <span>Online</span></h1>
                                          <p>Easy Investing. </p>
                                          <p>Make and monitor your investments in just a few clicks. No more paper work.</p>
                                        </div>

                                        <div class="item">
                                          <h1>Secure<span> Transaction</span></h1>
                                          <p>Safely through BSE.</p>
                                          <p>Orders routed through Bombay Stock Exchange in realtime. Secure delivery and fulfilment.</p>
                                        </div>
                                      </div>
                                    </div> <!-- carousel ends -->
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="footer-container">
                                <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                                <p class="footer-info">&copy;2017,Rightfunds.com</p>
                            </div>
                        </div>
                        <div class = "col-lg-6 col-md-6 col-sm-6" id="second-col">
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="form-container"> 
                                <form id="login-form" method="POST" action="{{ route('password.email') }}">  
                                    {{ csrf_field() }}
                                    <p class="heading form-heading  active-form" id="login-form-head">Password Reset</p>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="text" name="email" value="{{ old('email') }}" required id="email" class="input-field" required>
                                            <label>Email</label>
                                            <span class="text-danger"></span>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                
                                    <p id="login-error" class = "text-danger"></p>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="form-group">
                                            <input type="submit" name = "submit" id="submit" value="Reset" class="btn btn-primary register-btn">
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="right-footer-container">
                                <p class="sub-heading">Follow us on</p>
                                <ul class="list-inline">
                                    <li class="footer-links"><a target = "_blank" href="{{url('/about-us')}}">About</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/terms')}}">Terms of Use</a></li>
                                    <li class="footer-links"><a target = "_blank" href="{{url('/contact-us')}}">Contact</a></li>
                                </ul>
                                <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                                <p class="footer-info">&copy;2017,Rightfunds.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


     <script src="js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     </script>
    </body>
</html>
