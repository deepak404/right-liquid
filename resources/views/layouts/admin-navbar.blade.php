<nav class="navbar navbar-default navbar-custom">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="{{url('icons/logo.svg')}}" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="{{ (\Request::route()->getName() == 'showDashboard') ? 'active-menu' : '' }}"><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
                <li><a href="{{url('/admin/customer_support')}}" class="{{ (\Request::route()->getName() == 'customerSupport') ? 'active-menu' : '' }}">Customer Details</a></li>
                <li><a href="{{url('/admin/pending_orders')}}" class="{{ (\Request::route()->getName() == 'pendingOrders') ? 'active-menu' : '' }}">Pending Orders</a></li>
                <li><a href="{{url('/admin/order_history')}}" class="{{ (\Request::route()->getName() == 'orderHistory') ? 'active-menu' : '' }}">Order history</a></li>
                <li><a href="{{url('/admin/manage_schemes')}}" class="{{ (\Request::route()->getName() == 'manageSchemes') ? 'active-menu' : '' }}">Schemes</a></li>
                <li><a href="{{url('/admin/client_detail')}}" class="{{ (\Request::route()->getName() == 'ClientDetails') ? 'active-menu' : '' }}">Client Details</a></li>
                
              </ul>
              <a href="/logout" class="pull-right logout" style="">Logout</a>
              
              <!-- <ul class="nav navbar-nav navbar-right">
                <li><button class="btn btn-primary" id="nav-invest-btn">Invest Now</button></li>
                <li><i class="material-icons" id="nav-notification">notifications</i></li>
                <li><p id="nav-initial">N</p></li>
                <li>
                    
                        <p class="nav-user-name">Naveen Kumar</p>
                        <p class="nav-user-details">nknaveen328@gmail.com</p>
                    
                </li>
                <li><i class="material-icons" id="nav-dropdown">keyboard_arrow_down</i></li>
              </ul> -->
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

        @yield('content')
        
        
<!--         <section id="footer-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline">
                            <li class="footer-links"><a href="{{url('/privacy_policy')}}">Privacy Policy</a></li>
                            <li class="footer-links"><a href="{{url('/terms')}}">Terms of Use</a></li>
                            <li class="footer-links"><a href="{{url('/contact-us')}}">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                        <p class="footer-info">&copy;2017,Rightfunds.com</p>
                    </div>
                </div>  
            </div>   -->
        </section>