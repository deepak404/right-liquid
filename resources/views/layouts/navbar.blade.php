    <?php 
                //$notifications = \Auth::user()->notifications()->orderBy('created_at','desc')->get();
    $pending_utr = \Auth::user()->investmentDetails->where('payment_type','rtgs')->where('investment_status',0)->where('utr_no',null)->count();

    //dd($notifications);
            ?>
        <nav class="navbar navbar-default navbar-custom">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/home')}}"><img src="icons/logo.svg" class="nav-logo"></a>
            </div>

            

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="{{ (\Request::route()->getName() == 'homePage') ? 'active-menu' : '' }}"><a href="{{url('/home')}}">Home</a></li>
                <li class="{{ (\Request::route()->getName() == 'PortfolioDetails') ? 'active-menu' : '' }}"><a href="{{url('/portfolio_details')}}">Account Statement</a></li>
                  <li class="{{ (\Request::route()->getName() == 'transactionHistory') ? 'active-menu' : '' }}"><a href="{{url('/transaction_history')}}">Transactions</a></li>
                    @if($pending_utr > 0)
                        <li class="{{ (\Request::route()->getName() == 'orderStatus') ? 'active-menu' : '' }}"><a href="{{url('/order_status')}}">Pending Orders<sup></sup></a></li>
                    @else
                      <li class="{{ (\Request::route()->getName() == 'orderStatus') ? 'active-menu' : '' }}"><a href="{{url('/order_status')}}">Pending Orders</a></li>

                  @endif
                      <li class="{{ (\Request::route()->getName() == 'showProfile') ? 'active-menu' : '' }}"><a href="{{url('/profile')}}">Settings</a></li>

              </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{url('/logout')}}">Logout</a></li>
                </ul>
              
              {{--<ul class="nav navbar-nav navbar-right">--}}

              {{--@if(\Request::route()->getName() == 'fundSelector')--}}
              {{--@else--}}
              {{--<li><button class="btn btn-primary" onclick="location.href = '/fund_selector';" id="nav-invest-btn">Invest Now</button></li>--}}
              {{--@endif--}}
                {{----}}
                {{--<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"><i class="material-icons" id="nav-notification">notifications</i></a>--}}
                    {{--<ul class="dropdown-menu notify-drop box-shadow-all">--}}
                        {{--<div class="notify-drop-title">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom"><i class="fa fa-dot-circle-o"></i></a></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- end notify title -->--}}
                        {{--<!-- notify content -->--}}
                        {{--<div class="drop-content">--}}
                        {{----}}
                            {{----}}
                        {{--</div>--}}
                      {{--</ul>--}}
                {{--</li>--}}
                {{--<li><p id="nav-initial">{{Auth::user()->name[0]}}</p></li>--}}
                {{--<li>--}}
                    {{----}}
                        {{--<p class="nav-user-name">{{Auth::user()->name}}</p>--}}
                        {{--<p class="nav-user-details">{{Auth::user()->email}}</p>--}}
                    {{----}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<div class="dropdown"><i class="material-icons dropdown-toggle" data-toggle = "dropdown" id="nav-dropdown">keyboard_arrow_down</i>--}}
                  {{--<ul class="dropdown-menu" id="logout-dropdown">--}}
                    {{--<li><a href="{{url('/logout')}}">Logout <i class="material-icons" id="logout-icon">exit_to_app</i></a></li>--}}
                  {{--</ul>--}}
                {{--</div></li>--}}
                {{--<!-- <div class="dropdown">--}}
  {{--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example--}}
  {{--<span class="caret"></span></button>--}}
  {{----}}
{{--</div> -->--}}
              {{--</ul>--}}
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

      @yield('content')      
        
        <section id="footer-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline">
                            <li class="footer-links"><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                            <li class="footer-links"><a href="{{url('/terms')}}">Terms of Use</a></li>
                            {{--<li class="footer-links"><a href="{{url('/contact-us')}}">Contact</a></li>--}}
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                        <p class="footer-info">&copy;<?php echo date('Y');?>, Liquid Plus</p>
                    </div>
                </div>  
            </div>  
        </section>