<!doctype html>
<html class="no-js" lang="">


<?php 
   $fmt = new \NumberFormatter( 'en_IN', \NumberFormatter::DECIMAL );
?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Liquid Plus | Investment History</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/investment-history.css">
        <link rel="stylesheet" href="css/acc-statement.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="css/modal.css">
        <link rel="stylesheet" href="css/pd-responsive.css">
        <link rel="stylesheet" href="css/navbar-responsive.css">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->



        <script
              src="https://code.jquery.com/jquery-3.2.1.js"
              integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
              crossorigin="anonymous"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/font-and-global.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <script src="js/investment-history.js"></script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-114917491-2');
        </script>


    </head>
    <body>

    @extends('layouts.navbar')
    @section('content')

        {{--<section id="acc-statement-header">--}}
            {{--<div class="container">--}}
                {{--<div class="row">   --}}
                    {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
                        {{--<ul class="list-inline text-center fl" id="acc-statement-header-ul">--}}
                            {{--<li><a href="{{url('/portfolio_details')}}">Portfolio Details</a></li>--}}
                            {{--<li><a href="{{url('/investment_history')}}">Investment History</a></li>--}}
                            {{--<li><a href="{{url('/withdraw_funds')}}">Withdraw Funds</a></li>--}}
                            {{--<li class="active-acc-header"><a href="{{url('/order_status')}}">Order Status</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>  --}}
            {{--</div><!-- container ends -->--}}
        {{--</section>--}}


        @if(count($investment_details) == 0)
            <h4 class="text-center">You haven't placed orders recently</h4>
        @else

        <!-- <section id="acct-info-section">    
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12" id="acct-info-container">
                        <div class="col-lg-10 col-md-10 col-sm-10 p-lr-zero">
                            <p id="acct-info-statement">Show Investments for
                            <select id="investment-duration">
                                <option>6 Months</option>
                                <option>3 Months</option>
                                <option>1 Year</option>
                            </select>
                            </p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                            <button class="btn btn-primary" id="export-investment-btn">Export</button>
                        </div>
                    </div>
                </div>  
            </div>  
        </section> -->
        
        @endif        

        <?php $count = 1;?>
        @foreach($investment_details as $key => $investments)

        @if($key == date('d-m-Y'))
        <section  class="portfolio-table-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero bg-white">
                        <p class="table-container-header"><span>Today</span><span class="table-detail-info">Total No of Investments : {{count($investments)}}</span><span class="pull-right" data-toggle = "collapse" href = "#one"><i class="material-icons" id="today">keyboard_arrow_up</i></span></p>
                        <div id="one" class="collapse in col-lg-12 col-md-12 col-sm-12 p-lr-zero" aria-expanded="true">
                            <div class="acc-table-wrapper">
                                <table class="table">
                                    <thead>
                                      <tr>
                                        <th class="fund-name-header"><p class="table-header">Invested Date and Time</p></th>
                                          <th class="fund-name-header"><p class="table-header">Investor Name</p></th>
                                          <th><p class="table-header">Amount Invested(Rs.)</p></th>
                                          <th><p class="table-header">Transaction Status</p></th>
                                          <th></th>
                                      </tr>
                                    </thead>
                                    @foreach($investments as $inv => $investment)
                                        <tbody>
                                          <tr class="p-tb-five table-row">
                                            <td class="fund-name-content"><p class="table-content">{{$investment['date']}}</p></td>
                                            <td><p class="table-content">{{$investment['acc_name']}}</p></td>
                                            <td><p class="table-content">{{$fmt->format($investment['inv_amount'])}}.00</p></td>
                                            <td>
                                                <p class="table-content inline">
                                                    {{$investment['order_status']}}
                                                    
                                                    <span class="inv-info" id="{{$investment['id']}}">i</span>
                                                </p>                                             
                                            </td>
                                              @if($investment['payment_type'] == 'rtgs')
                                                  @if($investment['utr_no'] == '')
                                                      <td><a href="#" class="add_utr" data-id="{{$investment['id']}}">Add UTR No</a></td>
                                                      @else
                                                      <td><p>{{$investment['utr_no']}}</p></td>
                                                  @endif
                                              @else
                                                  <td><p>Net Banking</p></td>
                                              @endif
                                          </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div> <!--      -->
                    </div><!-- container-border ends -->
                </div>  
            </div>  
        </section> 
        @else


        <section  class="portfolio-table-section">
            <div class="container" id="equity-funds-table-container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero">
                        <p class="table-container-header"><span>{{$key}}</span><span class="table-detail-info">Total No of Investments : {{count($investments)}}</span><span class="pull-right" data-toggle = "collapse" href = "#{{$count}}"><i class="material-icons" id="today">keyboard_arrow_down</i></span></p>
                        <div id="{{$count}}" class="collapse col-lg-12 col-md-12 col-sm-12 p-lr-zero" aria-expanded="true">
                            <div class="acc-table-wrapper">
                                <table class="table">
                                    <thead>
                                      <tr>
                                        <th class="fund-name-header"><p class="table-header">Invested Date and Time</p></th>
                                        <th><p class="table-header">Account Name</p></th>
                                        <th><p class="table-header">Order Amount(Rs.)</p></th>
                                        <th><p class="table-header">Transaction Status</p></th>
                                          <th></th>
                                      </tr>
                                    </thead>
                                    
                                    @foreach($investments as $inv => $investment)
                                        <tbody>
                                          <tr class="p-tb-five table-row">
                                            <td class="fund-name-content"><p class="table-content">{{$investment['date']}}</p></td>
                                            <td><p class="table-content">{{$investment['acc_name']}}</p></td>
                                            <td><p class="table-content">{{$fmt->format($investment['inv_amount'])}}.00</p></td>
                                            <td>
                                                <p class="table-content inline">
                                                    {{$investment['order_status']}}
                                                    
                                                    <span class="inv-info" id="{{$investment['id']}}">i</span>
                                                </p>                                             
                                            </td>
                                              @if($investment['payment_type'] == 'rtgs')
                                                  @if($investment['utr_no'] == '')
                                                      <td><a href="#" class="add_utr" data-id="{{$investment['id']}}">Add UTR No</a></td>
                                                  @else
                                                      <td><p>{{$investment['utr_no']}}</p></td>
                                                  @endif
                                              @else
                                                  <td><p>Net Banking</p></td>
                                              @endif
                                          </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </div> <!--      -->
                    </div><!-- container-border ends -->
                </div>  
            </div>  
        </section> 
        @endif
        <?php $count++; ?>
        @endforeach


        


        @endsection


        <div id="invDetailModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center" id="modal-header">Modal Header</h4>
              </div>
              <div class="modal-body" id="modal-body">
                    <div class="row">
                        <div class = "col-lg-12 col-md-12 col-sm-12">
                            <p class="modal-info-text text-center">Your Investment is cancelled Successfully.</p>
                            <img src="/icons/success-tick.png" class="modal-img center-block">
                            <a class="btn btn-primary" data-dismiss = "modal">OKAY</a>
                        </div>      
                    </div>
              </div>
            </div>

          </div>
        </div>


        <div id="utrModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center" id="modal-header">UTR No</h4>
                    </div>
                    <div class="modal-body" id="modal-body">
                        <div class="row">
                            <div class = "col-lg-12 col-md-12 col-sm-12">
                                <form action="#" name="utr-form" id="utr-form">
                                    <input type="text" id="utr_no" name="utr_no" placeholder="Enter UTR No" class="input-field" />
                                    <input type="hidden" id="inv_id" name="inv_id">
                                    <input type="submit" class="btn btn-primary grad-btn center-block" value="Add">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    <script>
        $(document).ready(function(){
            $(document).on('click','.material-icons',function(){
                if ($(this).text() == 'keyboard_arrow_down') {
                    console.log($(this).text());
                    $(this).text('keyboard_arrow_up');
                }else if($(this).text() == 'keyboard_arrow_up'){

                    $(this).text('keyboard_arrow_down');               
                }                  
            });
        });
    </script>

    </body>
</html>
