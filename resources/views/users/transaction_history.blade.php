<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Liquid Plus | Investment History</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css"> -->
    <link rel="stylesheet" href="css/portfolio-details.css">
    <link rel="stylesheet" href="css/acc-statement.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/navbar-responsive.css">
    <link rel="stylesheet" href="css/pd-responsive.css">

    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="css/font-and-global.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-114917491-2');
    </script>

</head>
<body>

@extends('layouts.navbar')
@section('content')

    {{--<section id="acc-statement-header">--}}
    {{--<div class="container">--}}
    {{--<div class="row">   --}}
    {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
    {{--<ul class="list-inline text-center fl" id="acc-statement-header-ul">--}}
    {{--<li class="active-acc-header"><a href="{{url('/portfolio_details')}}">Portfolio Details</a></li>--}}
    {{--<li><a href="{{url('/investment_history')}}">Investment History</a></li>--}}
    {{--<li><a href="{{url('/withdraw_funds')}}">Withdraw Funds</a></li>--}}
    {{--<li><a href="{{url('/order_status')}}">Order Status</a></li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>  --}}
    {{--</div><!-- container ends -->--}}
    {{--</section>--}}

    <section  class="portfolio-table-section">
        <div class="container" id="equity-funds-table-container">
            <div class="row">

                {{--@if($portfolios == null)--}}
                    {{--<div class="col-lg-12 col-md-12 col-sm-12 ">--}}
                        {{--<h4 id="no-investment" class="text-center">No portfolios yet to show.</h4>--}}
                    {{--</div>--}}
                {{--@else--}}
                    {{--<div class = "col-lg-12 col-md-12 col-sm-12">--}}

                        {{--<form action = "/get_user_portfolio_document" method = "post" class="pull-right">--}}
                            {{--{{csrf_field()}}--}}
                            {{--<input type="hidden" name="user" value="{{\Auth::user()->id}}">--}}
                            {{--<button type="submit" class="btn btn-edit" id="export-portfolio"><i class="material-icons">file_download</i>Export</button>--}}
                        {{--</form>--}}

                    {{--</div>--}}



                        <?php //dd(count($scheme)); ?>


                        <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero bg-white investor-port-holder">
                            <p class="table-container-header">Investments</p>
                            @if(count($investments) != 0)
                            <div class="acc-table-wrapper">
                                <table class="table ">
                                    <thead>
                                    <tr>

                                        <th class="fund-name-header"><p class="table-header">Investor Name</p></th>

                                        <th><p class="table-header">Investment Amount</p></th>
                                        <th><p class="table-header">Investment Date</p></th>
                                        <th><p class="table-header">Status</p></th>
                                        {{--<th><p class="table-header">Average Holding Days</p></th>--}}
                                        {{--<th><p class="table-header">Annualised Returns</p></th>--}}


                                        {{--<th class="folio-number-header"><p class="table-header">Folio Number</p></th>--}}

                                    </tr>
                                    </thead>
                                    <tbody>



                                        @foreach($investments as $inv)
                                            <tr class="p-tb-five table-row">
                                                <td class="fund-name-content"><p class="table-content">{{$inv['investor_name']}}</p></td>
                                                <td><p class="table-content">{{$inv['investment_amount']}}</p></td>
                                                <td><p class="table-content">{{$inv['investment_date']}}</p></td>
                                                <td><p class="table-content">{{$inv['status']}}</p></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                                <h4 class="text-center no-transaction">No Investment Transaction</h4>
                            @endif
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 container-border p-lr-zero bg-white investor-port-holder">
                            <p class="table-container-header">Withdraw</p>

                            @if(count($withdraws) != 0)
                            <div class="acc-table-wrapper">
                                <table class="table ">
                                    <thead>
                                    <tr>

                                        <th class="fund-name-header"><p class="table-header">Investor Name</p></th>

                                        <th><p class="table-header">Withdraw Amount</p></th>
                                        <th><p class="table-header">Withdraw Date</p></th>
                                        <th><p class="table-header">Status</p></th>
                                        {{--<th><p class="table-header">Average Holding Days</p></th>--}}
                                        {{--<th><p class="table-header">Annualised Returns</p></th>--}}


                                        {{--<th class="folio-number-header"><p class="table-header">Folio Number</p></th>--}}

                                    </tr>
                                    </thead>
                                    <tbody>



                                    @foreach($withdraws as $wd)
                                        <tr class="p-tb-five table-row">
                                            <td class="fund-name-content"><p class="table-content">{{$wd['investor_name']}}</p></td>
                                            <td><p class="table-content">{{$wd['withdraw_amount']}}</p></td>
                                            <td><p class="table-content">{{$wd['withdraw_date']}}</p></td>
                                            <td><p class="table-content">{{$wd['status']}}</p></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                                <h4 class="text-center no-transaction">No Withdraw Transaction</h4>
                            @endif
                        </div>

                {{--@endif--}}

            </div>
        </div>
    </section>

@endsection



</body>
</html>
