<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Liquid Plus | Fund Selector</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "csrf-token" content = "{{ csrf_token() }}">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="css/modal.css">
        <!-- <link rel="stylesheet" href="css/settings.css"> -->
        
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>


        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/font-and-global.css">
        
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="css/withdraw-details.css">
      <link rel="stylesheet" href="css/modal.css">
      <link rel="stylesheet" href="css/fund-selector.css">
      <link rel="stylesheet" href="css/fund-selector-responsive.css">
      <link rel="stylesheet" href="css/navbar-responsive.css">
      <link rel="stylesheet" href="css/fund-selector.css">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-114917491-2');
        </script>


        <style type="text/css">
          #infoModal .modal-body{
            padding: 0px !important;
          }
      </style>
      
    </head>
    <body>
        <div id="loader" class="loader"></div>
        @extends('layouts.navbar')
        @section('content')

        <form id="invest-form">
            <section id="fund-details-section">
                <div class="container box-shadow-all br">
                    <div class="row">
                        <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero bg-white">
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero p-30 border-bottom scheme-header">
                                <p id="portfolio-header">Investment < 30 Days</p>
                                <p id="portfolio-text">No Exit Load, Invest in Liquid Mutual Funds</p>
                            </div>
                            @foreach($schemes as $scheme)
                            @if($scheme['scheme_type'] == "liquid")
                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 scheme-container border-bottom" id="{{$scheme['scheme_code']}}">
                                    <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8 pl-10">
                                        <p class="inv-scheme-name">{{$scheme['scheme_name']}}</p>
                                    </div>
                                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <p class="inv-scheme-amount pull-right"><span>Rs. </span><span>
                                            <input type="text" name="" id="{{$scheme['scheme_code']}}" class="custom-amount" data-scheme="{{$scheme['scheme_type']}}">
                                        </span><span class="alert-low-amount"></span></p>
                                    </div>
                                </div>
                                <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero scheme-details-container">
                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom">
                                        <p class="inner-header">Fund Details</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Fund Manager</p>
                                            <p class = "detail-content" id="fund-manager-name-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Type</p>
                                            <p class = "detail-content" id="fund-type-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Exit Load</p>
                                            <p class = "detail-content" id="exit-load-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Benchmark</p>
                                            <p class = "detail-content" id="benchmark-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Asset Size(Cr)</p>
                                            <p class = "detail-content" id="asset-size-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Launch Date</p>
                                            <p class = "detail-content" id="launch-date-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot active-duration-holder">
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center active-duration" data-scode = "{{$scheme['scheme_code']}}">1 Week</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">1 Month</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">6 Month</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">1 Year</p>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero">
                                         <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">3 Years</p>
                                       </div>
                                    </div>
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">
                                       <div id="{{$scheme['scheme_code']}}-graph-container">
                                         
                                       </div>
                                     </div>
                                     <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 ">
                                        <p class="nav-det-container">
                                            <span class="inner-header">Nav</span>
                                            <span class="scheme-nav" id="scheme-nav-{{$scheme['scheme_code']}}"></span>
                                            <span class="green scheme-change"></span>
                                            <span class="scheme-perc green"></span>
                                        </p>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom border-top">
                                        <p class="inner-header">Annualised Returns</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">1 Week</p>
                                            <p class = "return-content" id="one-week-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">1 Month</p>
                                            <p class = "return-content" id="one-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">3 Month</p>
                                            <p class = "return-content" id="three-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">6 Month</p>
                                            <p class = "return-content" id="six-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">1 Year</p>
                                            <p class = "return-content" id="one-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">3 Year</p>
                                            <p class = "return-content" id="three-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @endforeach
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero p-30 border-bottom scheme-header">
                                <p id="portfolio-header">Investment > 30 Days</p>
                                <p id="portfolio-text">Exit Load - 0.5% for 30 Days. Invest in Arbitrage Funds. </p>
                            </div>
                            @foreach($schemes as $scheme)
                            @if($scheme['scheme_type'] == "arb")
                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 scheme-container border-bottom" id="{{$scheme['scheme_code']}}">
                                    <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8 pl-10">
                                        <p class="inv-scheme-name">{{$scheme['scheme_name']}}</p>
                                    </div>
                                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <p class="inv-scheme-amount pull-right"><span>Rs. </span><span>
                                            <input type="text" name="" id="{{$scheme['scheme_code']}}" class="custom-amount" data-scheme="{{$scheme['scheme_type']}}">
                                        </span><span class="alert-low-amount"></span></p>

                                    </div>
                                </div>
                                <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero scheme-details-container">
                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom">
                                        <p class="inner-header">Fund Details</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Fund Manager</p>
                                            <p class = "detail-content" id="fund-manager-name-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Type</p>
                                            <p class = "detail-content" id="fund-type-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Exit Load</p>
                                            <p class = "detail-content" id="exit-load-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Benchmark</p>
                                            <p class = "detail-content" id="benchmark-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Asset Size(Cr)</p>
                                            <p class = "detail-content" id="asset-size-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">Launch Date</p>
                                            <p class = "detail-content" id="launch-date-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot active-duration-holder">
                                        <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                            <p class="graph-duration text-center active-duration" data-scode = "{{$scheme['scheme_code']}}">1 Week</p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                            <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">1 Month</p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                            <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">6 Month</p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero border-right">
                                            <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">1 Year</p>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 w-20 p-lr-zero">
                                            <p class="graph-duration text-center" data-scode = "{{$scheme['scheme_code']}}">3 Years</p>
                                        </div>
                                    </div>
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">
                                       <div id="{{$scheme['scheme_code']}}-graph-container">
                                         
                                       </div>
                                     </div>
                                     <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 ">
                                        <p class="nav-det-container">
                                            <span class="inner-header">Nav</span>
                                            <span class="scheme-nav" id="scheme-nav-{{$scheme['scheme_code']}}"></span>
                                            <span class="green scheme-change"></span>
                                            <span class="scheme-perc green"></span>
                                        </p>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom border-top">
                                        <p class="inner-header">Annualised Returns</p>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">1 Week</p>
                                            <p class = "return-content" id="one-week-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">1 Month</p>
                                            <p class = "return-content" id="one-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">3 Month</p>
                                            <p class = "return-content" id="three-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">6 Month</p>
                                            <p class = "return-content" id="six-month-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">1 Year</p>
                                            <p class = "return-content" id="one-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                        <div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">
                                            <p class="detail-header">3 Year</p>
                                            <p class = "return-content" id="three-year-{{$scheme['scheme_code']}}"></p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @endforeach
                            <!-- <div class = "col-lg-12 col-md-12 col-sm-12 scheme-container border-bottom p-lr-zero">
                                <div class = "col-lg-8 col-md-8 col-sm-8">
                                    <p class="inv-scheme-name">Reliance Regular Savings Fund - Balanced Option  - Direct Plan Growth Plan</p>
                                </div>
                                <div class = "col-lg-4 col-md-4 col-sm-4">
                                    <p class="inv-scheme-amount pull-right"><span>Rs. </span>5000000</p>
                                </div>
                            </div> -->
                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 total-container border-bottom p-lr-zero">
                                <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <p class="total-text blue">Total Investment Amount</p>
                                </div>
                                <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <p class="inv-scheme-amount pull-right" id="total-inv-amount">Rs. <span>0</span></p>
                                </div>
                            </div>
                            <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 total-container border-bottom p-lr-zero">
                                <input type="submit" name="submit" value="Invest Now" id = "invest-btn" class="btn btn-primary grad-btn grad-btn center-block">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>

        @endsection


        <div id="chooseBankModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-body">
                <h4 class="text-center">Choose Your bank Account</h4>
                <div class="row text-center">
                    <div class="col-lg-12 col-md-12 col-sm-12" id="bank-account-list">
                        <form id="bank-account-form">
                            <!-- <div class="account-holder col-lg-12 col-md-12 col-sm-12">
                                <div class = "col-lg-3 col-md-3 col-sm-3">
                                    <input type="radio" name="1" id="bank1">
                                    <label for="bank1"><i class="material-icons">radio_button_unchecked</i></label>
                                </div>
                                <div class = "col-lg-9 col-md-9 col-sm-9 text-left p-l-zero">
                                    <p class="bank-name">Punjab National bank</p>
                                    <p class="bank-acc-no">8628101052147</p>
                                </div>
                            </div>

                            <div class="account-holder col-lg-12 col-md-12 col-sm-12">
                                <div class = "col-lg-3 col-md-3 col-sm-3">
                                    <input type="radio" name="1" id="bank2">
                                    <label for="bank2"><i class="material-icons">radio_button_unchecked</i></label>
                                </div>
                                <div class = "col-lg-9 col-md-9 col-sm-9 text-left p-l-zero">
                                    <p class="bank-name">Punjab National bank</p>
                                    <p class="bank-acc-no">8628101052147</p>
                                </div>
                            </div> -->
                        <div class="col-lg-12 col-md-12 col-sm-12" id="submit-btn-holder">
                            <input type="submit" name="submit-bank" class="btn btn-primary grad-btn grad-btn" id="submit-bank" value="Invest">
                        </div>
                        </form>
                    </div>
                </div>
              </div>
            </div>

          </div>
        </div>



        <div id="infoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-body">
                  <div class="modal-res-body">
                      <p class="modal-res-text"></p>
                  </div>

                    <div class="modal-res-header">
                        <img class="center-block response-image" src="/icons/success-tick.svg">
                    </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default grad-btn center-block success-btn" onclick="javascript:location.href='/home'" data-dismiss="modal">Done</button>
              </div>
            </div>

          </div>
        </div>

        @if(isset($updated_portfolio))
        <!-- Modal -->
        <div id="showPaymentInfo" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close close-pn">&times;</button>
                <h4 class="modal-title text-center blue-text" id="modal-header">Payment Summary</h4>
              </div>
              <div class="modal-body" id="modal-body">
                <table class="table payment-table">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Scheme Name</th>
                            <th>Payment Status</th>
                            <th>Amount</th>   
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 1; 
                            $scheme_name = App\SchemeDetails::pluck('scheme_name','scheme_code')->toArray();
                            // dd($updated_portfolio);
                            // echo "hello";
                        ?>
                        
                        @foreach($updated_portfolio as $portfolio)

                        <?php $name = $scheme_name[$portfolio['scheme_code']] ?>
                        <tr>
                            <td><p>{{$count}}.</p></td>
                            <td><p class="scheme-name">{{$name}}</p></td>
                            @if($portfolio['bse_payment_status'] == "apr")
                            <td><p class="payment-status">Payment Approved</p></td>
                            @elseif($portfolio['bse_payment_status'] == "rej")
                            <td><p class="payment-status">Payment Rejected</p></td>
                            @elseif($portfolio['bse_payment_status'] == "afc")
                            <td><p class="payment-status">Awaiting Funds Confirmation</p></td>
                            @elseif($portfolio['bse_payment_status'] == "pni")
                            <td><p class="payment-status">Payment Not Initiated Yet.</p></td>
                            @elseif($portfolio['bse_payment_status'] == "bdc")
                            <td><p class="payment-status">Awaiting Response from Billdesk.</p></td>
                            @else
                            <td><p class="payment-status">NA</p></td>
                            @endif
                            
                            <td><p class="order-amount">{{$portfolio['amount_invested']}}</p></td>
                        </tr>
                        <?php  $count++; ?>
                        @endforeach
                    </tbody>
                </table>
                <button type = "button" class = "btn btn-primary grad-btn grad-btn center-block popup-btn close-pn" id="close-pn">Okay</button>
              </div>
            </div>

          </div>
        </div>
        @endif

        <!-- Modal -->
        <div id="showInfo" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center blue-text" id="modal-header">Payment Link Info</h4>
              </div>
              <div class="modal-body" id="modal-body">
                <p class = "text-center general-info">Payment Link will be sent to your email. Kindly make the payment with in 24 Hours.</p>
                <button type = "button" class = "btn btn-primary grad-btn center-block popup-btn" id="mandate-pay" data-dismiss="modal">Okay</button>
              </div>
            </div>

          </div>
        </div>


        <?php        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);

//        dd($_SESSION);
        ?>
        @if(session('rtgs_success'))



        <div id="rtgsInfoModal" class="modal fade in" role="dialog" data-backdrop="static" data-keyboard="false" style="display: block;">
          <div class="modal-dialog" style="width: 600px; !important;">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                  @if(session('rtgs_success')['response_status'] == 'success')
                  <h4 class="modal-title text-center blue-text" id="modal-header">Order Placed Successfully</h4>
                  @else
                  <h4 class="modal-title text-center blue-text" id="modal-header">Order Placement Failed.</h4>
                  @endif

              </div>
              <div class="modal-body" id="modal-body">
                  @if(session('rtgs_success')['response_status'] == 'success')
                  <p class = "text-center general-info">Your Orders have been placed successfully. You can enter the UTR No of your respective investment in the Pending Orders tab.</p>
                      @else
                      <p class = "text-center general-info">Your Order placement failed. Kindly refresh and try again.</p>
                  @endif
                      <table class="table">
                          <thead>
                          <th>S.No</th>
                          <th>Scheme Name</th>
                          <th>Amount</th>
                          <th>Status</th>
                          </thead>
                          <tbody>
                            <?php $s_count = 1; //dd(session())?>
                            @foreach(session('rtgs_success')['response_portfolio_details'] as $portfolio)
                                <?php
                                $scheme_name = \App\SchemeDetails::where('scheme_code', $portfolio->scheme_code)->value('scheme_name');


                                ?>
                                <tr>
                                    <td>{{$s_count}}.</td>
                                    <td>{{$scheme_name}}</td>
                                    <td>{{$fmt->format($portfolio->amount_invested)}}</td>
                                    @if($portfolio->bse_order_status == 0)
                                        <td>Success</td>
                                    @else
                                        <td>Failed</td>
                                    @endif
                                </tr>
                                <?php $s_count++; ?>
                            @endforeach
                          </tbody>
                      </table>
                      @if(session('rtgs_success')['response_status'] == 'success')
                          <button type = "button" class = "btn btn-primary grad-btn center-block popup-btn" id="go-home" onclick="redirectPendingOrders()" data-dismiss="modal">Okay</button>
                          @else
                          <button type = "button" class = "btn btn-primary grad-btn center-block popup-btn hyx" onclick="javascript:location.href='/fund_selector'" id="go-home" data-dismiss="modal">Okay</button>

                  @endif
              </div>
            </div>

          </div>
        </div>
        @endif


        <!-- Modal -->
        <div id="utrModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center blue-text" id="modal-header">UTR No</h4>
              </div>
              <div class="modal-body" id="modal-body">
                <p class = "text-center general-info">Please enter the UTR No generated by the bank for the respective transaction.</p>
                <input type="text" name="utr-no" class="center-block" id="utr-no" placeholder="Enter UTR No" required>
                <span></span>
                <button type = "button" class = "btn btn-primary grad-btn center-block popup-btn" id="confirm-utr">Submit</button>
              </div>
            </div>

          </div>
        </div>

        @if(Session::has('error'))
        <div id="errorInfoModal" class="modal fade in" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="modal-res-header success-body">
                            <img class="center-block response-image" src="/icons/failed-tick.svg" style="top: 10%;">
                        </div>
                        <div class="modal-res-body">
                            <p class="modal-res-text">{{Session::get('error')}}</p>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default center-block success-btn" onclick="javascript:location.href='/home'" data-dismiss="modal">Done</button>
                    </div>
                </div>

            </div>
        </div>
        @endif



        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src = "js/fund-selector.js"></script>
        <script type="text/javascript" src = "js/fund-graph.js"></script>
        <script type="text/javascript" src = "js/custom-amount.js"></script>

        <script type="text/javascript">

            $(document).ready(function(){
                $('#showPaymentInfo').modal('show');
            });
        
                function redirect(id){
                    // window.location.href = '/make_payment/'+id+'';

                    // var newWindow = window.open('https://www.liquidplus.in/make_payment/'+id+'','_blank');

                    if (id != null) {
                            var newWindow = window.open('https://www.liquidplus.in/make_payment/'+id+'','_blank');
                            newWindow.location = 'https://www.liquidplus.in/make_payment/'+id+'';
                            // var win = window.open('https://www.liquidplus.in/make_payment/'+id+'','_blank');
                            // win.focus();
                    }else{
                        alert('Sorry. Something went wrong');
                    }
                
                }


                function redirectPendingOrders(){
                    window.location.href = '/order_status';

                }

                function redirectHome(){
                    window.location.href = '/';
                
                }



                function getMandatelink(bank_id){
                  $('#infoPopUp').modal('hide');
                  $.ajax({
                    type: "GET",
                    url: "/get_mandate_link",
                    data:{'bank_id':bank_id},
                    async:false,
                    success: function(data){
                      if (data.msg == 1) {
                          window.location.replace("https://www.liquidplus.in");
                          var win = window.open(data.mandate_url, '_blank');
                          win.focus();
                      }else if (data.msg == 2){
                        $('#showInfo').modal('show');
                      }
                    },
                    error: function(xhr,status,error){
                      alert(error);
                    }
                  });
                }
        </script>

    </body>
</html>
