<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Liquid Plus | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/withdraw-details.css">
        <link rel="stylesheet" href="css/index.css">
        <link rel="stylesheet" href="css/index-responsive.css">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="css/modal.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/font-and-global.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="css/index-responsive.css">
      <link rel="stylesheet" href="css/navbar-responsive.css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-2"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-114917491-2');
        </script>

    </head>
    <body>
    <div id="loader" class="loader"></div>

        @extends('layouts.navbar')
        @section('content')

        <?php  //dd($portfolio_total);

         ?>

        

        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 id="page-heading">{{\Auth::user()->name}}'s Consolidated Investment Summary </h3>
                        <p>Snapshot of your investments as of today, <?php echo date('j M Y');?></p>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 p-lr-zero box-shadow-all inv-summary br bg-white">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30">Investment Summary</p>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Investment</p>
                                    <p class="inv-sum-amount">Rs. {{$portfolio_total['amount_invested']}}</p>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Current Value</p>
                                    <p class="inv-sum-amount">Rs. {{$portfolio_total['current_value']}}</p>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    <p class="inv-sum-amount">Rs. {{$portfolio_total['net_returns']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Returns <span id="abs-return-type" class="return-type"></span><span id="ann-return-type" class="return-type"></span></p>
                                    <p class="inv-sum-amount green"><span class="abs-returns">{{round($portfolio_total['total_xirr'],2)}}%</span><span class="ann-returns">{{round($portfolio_total['total_xirr'],2)}}%</span></p>
                                    <!-- <p class="inv-sum-amount green">0%</p> -->
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->



                        <div class = "col-lg-3 col-md-3 col-sm-3 br inv-summary p-r-zero">
                            <div class = "col-lg-12 col-md-12 col-sm-12 br box-shadow-all p-lr-zero bg-white" id="withdraw-box">
                                {{--<div class = "col-lg-12 col-md-12 col-sm-12 header-pad border-bot p-l-zero">--}}
                                        {{--<p class="section-header pl-30">Withdraw</p>--}}
                                {{--</div>--}}
                                {{--<div class = "col-lg-12 col-md-12 col-sm-12">--}}
                                     {{--<p id="withdraw-info">Withdraw will be credit to your account in T+1 day.</p>--}}
                                     {{--<button class="btn btn-primary" id="home-withdraw">Withdraw</button>--}}
                                {{--</div>--}}

                                <p class="text-center" id="withdraw-info">Add more funds to your portfolio</p>
                                <a href="/fund_selector" id="invest-link" class="center-block btn">Invest Now</a>
                            </div>
                        </div>
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        <!-- @if($portfolios == null)
        <div class="container" id="no-inv-div" > 
            <div class="row">   

                  <div class = "col-lg-12 col-md-12 col-sm-12"> 
                        <p id="start-investing">Take your first step towards financial Independence !<span><a href="{{url('/fund_selector')}}">Start Investing now </a></span><span class="pull-right"><i class="material-icons">arrow_forward</i></span></p>
                  </div>   
            </div>  
        </div>  
        @endif -->

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3 id="page-heading">Accounts Held</h3>
                        {{--<p>Snapshot of your investments as of today</p>--}}



                        @foreach($portfolio as $port)

                            @if($port['current_value'] > 0)
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all br investor-card bg-white">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot header-pad" id="portfolio-header-cont">
                                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">

                                            <?php $acc_no = App\BankDetails::where('acc_name',$port['acc_name'])->pluck('acc_no'); //dd($acc_no); ?>
                                            <p class="section-header pl-30">{{$port['acc_name']}}</p>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                            <a href="#" class="pull-right withdraw-link" data-pan = "{{$port['pan']}}" data-accno = "{{$acc_no[0]}}">Withdraw</a>
                                        </div>
                                    </div>



                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 fund-container">
                                        <div class="col-lg-3 fund-info-container">
                                            <p class="fund-info-heading">Initial Investment</p>
                                            <p class="fund-info-detail">Rs. {{$port['total_investment_amount']}}</p>
                                        </div>
                                        <div class="col-lg-3 fund-info-container">
                                            <p class="fund-info-heading">Current Market value</p>
                                            <p class="fund-info-detail">Rs. {{$port['current_value']}}</p>
                                        </div>
                                        <div class="col-lg-3 fund-info-container">
                                            <p class="fund-info-heading">Profit/Loss</p>
                                            <p class="fund-info-detail">Rs. {{$port['p_or_loss']}}</p>
                                        </div>
                                        <div class="col-lg-3 fund-info-container">
                                            <p class="fund-info-heading">Returns</p>
                                            <p class="fund-info-detail green"><span>{{$port['xirr']}}%</span></p>
                                        </div>

                                    </div>  <!-- fund container ends -->
                                </div> <!-- box-shadow-all ends -->

                            @else

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all br investor-card bg-white">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot header-pad" id="portfolio-header-cont">
                                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                            <?php $acc_no = App\BankDetails::where('acc_name',$port['acc_name'])->pluck('acc_no'); //dd(substr($acc_no[0], -5)); ?>

                                            <p class="section-header pl-30">{{$port['acc_name']}}</p>
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                            {{--<a href="#" class="pull-right withdraw-link" data-pan = "{{$port['pan']}}">Withdraw</a>--}}
                                        </div>
                                    </div>



                                    <div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 fund-container">
                                        <div class="col-lg-3 fund-info-container">
                                            <p class="fund-info-heading">Initial Investment</p>
                                            <p class="fund-info-detail">Rs. {{$port['total_investment_amount']}}</p>
                                        </div>
                                        <div class="col-lg-3 fund-info-container">
                                            <p class="fund-info-heading">Current Market value</p>
                                            <p class="fund-info-detail">Rs. {{$port['current_value']}}</p>
                                        </div>
                                        <div class="col-lg-3 fund-info-container">
                                            <p class="fund-info-heading">Profit/Loss</p>
                                            <p class="fund-info-detail">Rs. {{$port['current_value']}}</p>
                                        </div>
                                        <div class="col-lg-3 fund-info-container">
                                            <p class="fund-info-heading">Returns</p>
                                            <p class="fund-info-detail green"><span>{{$port['xirr']}}%</span></p>
                                        </div>

                                    </div>  <!-- fund container ends -->
                                </div> <!-- box-shadow-all ends -->

                            @endif


                        @endforeach


                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>


        <div id="withdrawModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Withdraw</h4>
                    </div>
                    <form action="#" id="withdrawal-form">
                        <div class="modal-body">
                            <div class="acc-table-wrapper">
                                <table class="table withdraw-table">
                                    <tbody>
                                    <tr class="border-bot">
                                        <td>
                                            <p class="withdraw-fund-name">Reliance Liquid Fund - Cash Plan</p>
                                            <p class="current-invested">Current Value - Rs. <span id="100845">3816137.44</span></p>

                                            <p class="invested-bank current-invested">Invested through - <strong>IDBI Bank - Retail Net Banking</strong></p>
                                        </td>
                                        <td>

                                            <input type="text" name="100845" id="100845" data-pan="AABCG1276E" class="input-field custom-amount num-field" placeholder="Enter Amount">
                                        </td>
                                        <td>
                                            <input type="checkbox" class="full-amount" data-scheme="100845" id="checkbox-AABCG1276E-100845" name="">
                                            <label for="checkbox-AABCG1276E-100845"><i class="material-icons">check_box_outline_blank</i></label>
                                            <span>Full Amount</span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table> <!-- Equity Withdraw Table ends -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="pull-left">
                                <h4>Total Withdraw Amount</h4>
                                <p class="withdraw-info-amount">Rs. <span>0</span></p>
                            </div>
                           <div class="pull-right" id="withdraw-btn-holder">
                               <input type="submit" class="btn btn-primary grad-btn" value="Withdraw">
                           </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>


        <div id="infoModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="modal-res-body">
                            <p class="modal-res-text"></p>
                        </div>
                        <div class="modal-res-header">
                            <img class="center-block response-image" src="/icons/success-tick.svg">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary center-block grad-btn" data-dismiss="modal">Done</button>
                    </div>
                </div>

            </div>
        </div>


        <div id="withdrawResponseModal" class="modal fade in" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="width: 600px; !important;">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-center blue-text" id="modal-header">Withdraw Details</h4>
                    </div>
                    <div class="modal-body" id="modal-body">
                        <table class="table">
                            <thead>
                            <th>S.No</th>
                            <th>Scheme Name</th>
                            <th>Amount</th>
                            <th>Status</th>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary center-block grad-btn" data-dismiss="modal">Done</button>
                    </div>
                </div>

            </div>
        </div>

        @endsection

     <script src="js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="js/index.js"></script>
        <script src="js/withdraw-funds.js"></script>
        <script type="text/javascript" src="js/validation.js"></script>


        <script type="text/javascript">
         $(document).ready(function(){
            $('#abs-return-type').click(function(){
                $(this).hide();
                $('#ann-return-type').show();
                $('.ann-returns').show();
                $('.abs-returns').hide();
            });
            $('#ann-return-type').click(function(){
                $(this).hide();
                $('#abs-return-type').show();
                $('.ann-returns').hide();
                $('.abs-returns').show();
            });
         });
     </script>
    </body>
</html>
