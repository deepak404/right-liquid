<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Liquid Plus | Mutual Fund Investment</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/index.css">
        <link rel="stylesheet" href="css/footer.css">

        
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/font-and-global.css">
        <link rel="stylesheet" href="css/landing.css">
        <link rel="stylesheet" href="css/landing-responsive.css">
        <link rel="stylesheet" href="css/contact-us.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

        <style>
            button[type="submit"]{
                margin-top: 30px;
                margin-bottom: 30px;
            }


            .email-response {
                 width: 100% !important;
                margin-left: auto;
                margin-right: auto;
                text-align: left;
                color: #0091EA;
                margin-top: 20px;
            }

        </style>
    </head>
    <body>



    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="icons/logo.svg" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!--  <ul class="nav navbar-nav">
                   <li class="active-menu"><a href="#">Home</a></li>
                   <li><a href="#">Account Statement</a></li>
                   <li><a href="#">Settings</a></li>

                 </ul> -->

                <ul class="nav navbar-nav navbar-right" id="register-nav">
                    <li><a href="/login" class="blue-text" id="login">Login</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>


        <section id="contact-us">
            <div class="container box-shadow-all">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <h3 id="header">Contact Us</h3>

                        <div class = "col-lg-4 col-md-4 col-sm-4">
                            <img src="icons/location.png" class="contact-img">
                            <span class="contact-info">Location</span>
                            <p class="contact-inner-info">#1,1st 1st Floor, Balaji First Avenue,<br>
                            T.Nagar, Chennai<br>
                            Tamil Nadu - 600017</p>
                        </div>

                        <div class = "col-lg-4 col-md-4 col-sm-4">
                            <img src="icons/phone.png" class="contact-img">
                            <span class="contact-info">Phone</span>
                            <p class="contact-inner-info">+91 88258 88200</p>
                        </div>

                        <div class = "col-lg-4 col-md-4 col-sm-4">
                            <img src="icons/mail.png" class="contact-img">
                            <span class="contact-info">Mail</span>
                            <p class="contact-inner-info">contact@rightfunds.com</p>
                        </div>

                    </div>


                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <h3 id="header">Send us a Mail</h3>
                        <form action="#" id="contact-form" name="contact-form" class="col-lg-12 col-md-12 col-sm-12">
                            {{@csrf_field()}}
                            <div class = "col-lg-12 col-md-12 col-sm-12">
                                <div class = "col-lg-4 col-md-4 col-sm-4">
                                    <input type="text" name = "sender-name" id="sender-name" class="input-field" required>
                                    <label>Name</label>
                                    <span class="text-danger"></span>
                                </div>

                                <div class = "col-lg-4 col-md-4 col-sm-4">
                                     <input type="text" name = "email" id="email" class="input-field" required>
                                     <label>Email</label>
                                     <span class="text-danger"></span>
                                </div>

                                <div class = "col-lg-4 col-md-4 col-sm-4">
                                    <input type="text" name = "mobile" id="mobile" class="input-field" required>
                                    <label>Mobile</label>
                                    <span class="text-danger"></span>
                                </div>


                               <div class = "col-lg-12 col-md-12 col-sm-12" id="message-cont">
                                    <input type="text" name = "message" id="message" class="input-field" required>
                                    <label>Message</label>
                                    <span class="text-danger"></span>
                               </div>

                                <div class = "col-lg-12 col-md-12 col-sm-12">
                                    <p class="email-response"></p>

                                </div>

                                <button type="submit" name="submit" class="btn btn-primary grad-btn">Send</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </section>


    <section id="footer-section">
        <div class="container">
            <div class="row">
                <!-- <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="list-inline">
                        <li class="footer-links"><a href="#">Privacy Policy</a></li>
                        <li class="footer-links"><a href="#">Terms of Use</a></li>
                        <li class="footer-links"><a href="#">Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                    <p class="footer-info">&copy;2017,Rightfunds.com</p>
                </div> -->

                <div class = "col-lg-12 col-md-12 col-sm-12">
                    <div class = "col-lg-4 col-md-4 col-sm-4">
                        <img src="icons/logo.svg" id="footer-logo">
                        <p id="reg-company">&copy;Prosperity Technology Private Limited,2017</p>
                        <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
                    </div>

                    <div class = "col-lg-3 col-md-3 col-sm-3">
                        <p class="footer-info">Support</p>
                        <ul class="footer-list">
                            {{--<li><a href="#">FAQ</a></li>--}}
                            <li><a href="/terms">Terms of Use</a></li>
                            <li><a href="/privacy-policy">Privacy Policy</a></li>
                        </ul>
                    </div>

                    <div class = "col-lg-3 col-md-3 col-sm-3">
                        <p class="footer-info">Contact</p>
                        <ul class="footer-list">
                            <li> Level 1, No 1, Balaji First Avenue, T.Nagar, Chennai, Tamil Nadu 600017</li>
                            <li>+91 88258 88200</li>
                        </ul>
                    </div>

                    {{--<div class = "col-lg-2 col-md-2 col-sm-2">--}}
                    {{--<p class="footer-info">Follow us</p>--}}
                    {{--<ul class="list-inline" id="social-parent">--}}
                    {{--<li class="social-list"><a href="#"><img src="img/facebook-footer-logo.png" class="footer-social"></a></li>--}}
                    {{--<li class="social-list"><a href="#"><img src="img/twitter-footer-logo.png" class="footer-social"></a></li>--}}
                    {{--<li class="social-list"><a href="#"><img src="img/linkedin-footer-logo.png" class="footer-social"></a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                </div>
                <div class = "col-lg-12 col-md-12 col-sm-12" id="partners-container">
                    <p class="footer-info p-l-15">AMC Partners</p>
                    <p id="amc-names" class ="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                        <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                    <p class="p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><a
                                href="arn"><span id="arn">ARN : 116221</span></a></p>
                </div>
            </div>
        </div>
    </section>

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $('#contact-form').on('submit', function(e){
                e.preventDefault();
                console.log($(this).serialize());

                var data = $(this).serialize();
                console.log(data);

                $.ajax({
                    type: 'POST',
                    url: 'send_contact_enquiry',
                    data: data,
                    // async:false,
                    success:function(data){
                        if(data.msg == true){
                            $('.email-response').text('Message has been sent Successfully. We will reach you shortly.');
                        }else{
                            $('.email-response').text('Message not sent. Refresh and try again.');
                        }

                        $('#contact-form')[0].reset();
                    },
                    error:function(){

                    }
                });
            });


            $(document).ajaxStart(function(){////console.log('into loader');
                console.log('started');
                $('#contact-form button[type="submit"]').html("Sending <span class='mail-loader'></span>");

            });
            $(document).ajaxComplete(function(){////console.log('out from loader');
                console.log('Ended');

                $('#contact-form button[type="submit"]').html("Send");

            });
        })
    </script>

     <script src="js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
