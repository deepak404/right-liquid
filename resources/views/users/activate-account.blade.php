<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Portfolio Details</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/activate-account.css">
        <link rel="stylesheet" href="css/footer.css">
        
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/font-and-global.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    </head>
    <body>

        <nav class="navbar navbar-default navbar-custom">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="img/nav-logo.png" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class="active-menu"><a href="#">Home</a></li>
                <li><a href="#">Account Statement</a></li>
                <li><a href="#">Settings</a></li>
                
              </ul>
              
              <ul class="nav navbar-nav navbar-right">
                <li><button class="btn btn-primary" id="nav-invest-btn">Invest Now</button></li>
                <li><i class="material-icons" id="nav-notification">notifications</i></li>
                <li><p id="nav-initial">N</p></li>
                <li>
                    
                        <p class="nav-user-name">Naveen Kumar</p>
                        <p class="nav-user-details">nknaveen328@gmail.com</p>
                    
                </li>
                <li><i class="material-icons" id="nav-dropdown">keyboard_arrow_down</i></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

        <section id="settings">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 p-r-35 p-l-zero">
                            <div class="col-lg-12 col-md-12 col-sm-12 border-all br p-lr-zero" id="settings-sidebar">
                                <ul id="settings-nav">
                                    <li class="border-bottom active-settings-nav activate-acc-menu">
                                        <span><i class="material-icons activate-tick">done</i></span>
                                        <a href="#" class="blue-text">Personal Details</a>
                                    </li>
                                    <li class="border-bottom inactive-sidemenu activate-acc-menu">
                                        <span><i class="material-icons non-active-tick">done</i></span>
                                        <a href="#">Bank Details</a>
                                    </li>
                                    <li class="inactive-sidemenu activate-acc-menu">
                                        <span><i class="material-icons non-active-tick">done</i></span>
                                        <a href="#">EKYC and Documents</a>
                                        <p class="text-left blue-text" id="kyc-info-text">Will be Notified once the KYC verification is done.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 border-all br p-lr-zero">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero border-bottom header-pad">
                                <p class="section-header pl-30">Personal Details <button type="button" class="pull-right btn" id="add-nominee-btn">Add Nominee</button></p>
                                <!-- <p id="bank-details-info">To Edit Bank Details Please Contact <span class="blue-text">88258 88200</span> or <span class="blue-text">contact@rightfunds.com</span></p> -->
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="personal-details-tab">
                                <form id="personal-details-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "name" id="name" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pan" id="pan" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">PAN</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "dob" id="dob" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Date of Birth</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Father's/Spouse Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pin" id="pin" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Mother's Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <!-- <input type="text" name = "occ-type" id="occ-type" class="input-field" required> -->
                                                <select name="occ-type" id="occ-type">
                                                    <option>Private Sector</option>
                                                </select>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <!-- <label class="input-label">Occupation Type</label> -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Sex</p>
                                            <div class="sex-radio-divs">
                                              <div class = " male-radio-div padding-lr-zero radio-inline">
                                                <input type="radio"  name="gender" id="male" value = "male" >
                                                <label for="male" class="radio-label"><span class="radio left-label-span">Male</span></label>
                                              </div>

                                              <div class = " female-radio-div padding-lr-zero radio-inline">
                                                <input type="radio" name="gender" id="female" value = "female">
                                                <label for= "female" class="radio-label"><span class="radio right-label-span">Female</span></label>
                                              </div>                              
                                          </div>                                 
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Residence</p>
                                            <div class="resident-radio-divs">
                                              <div class = " indian-radio-div padding-lr-zero radio-inline">
                                                <input type="radio"  name="resident" id="indian" value = "indian" >
                                                <label for="indian" class="radio-label"><span class="radio left-label-span">Indian</span></label>
                                              </div>

                                              <div class = " nri-radio-div padding-lr-zero radio-inline">
                                                <input type="radio" name="resident" id="nri" value = "nri">
                                                <label for= "nri" class="radio-label"><span class="radio right-label-span">NRI</span></label>
                                              </div>                              
                                            </div>    
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <p class="form-helper">Marital Status</p>
                                            <div class="marital-radio-divs">
                                              <div class = " single-radio-div padding-lr-zero radio-inline">
                                                <input type="radio"  name="marital-status" id="single" value = "single" >
                                                <label for="single" class="radio-label"><span class="radio left-label-span">Single</span></label>
                                              </div>

                                              <div class = " married-radio-div padding-lr-zero radio-inline">
                                                <input type="radio" name="marital-status" id="married" value = "married">
                                                <label for= "married" class="radio-label"><span class="radio right-label-span">Married</span></label>
                                              </div>                              
                                            </div>    
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Communication Address</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pin" id="pin" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Pin Code</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pin" id="pin" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Nationality</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- Personal Details Tab ends -->


                             <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="bank-details-tab">
                                <form id="bank-details-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "acc-name" id="acc-name" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Account Holder Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="email" name = "acc-no" id="acc-no" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Account Number</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "ifsc-code" id="ifsc-code" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">IFSC Code</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <!-- <input type="text" name = "occ-type" id="occ-type" class="input-field" required> -->
                                                <select name="acc-type" id="acc-type">
                                                    <option>Savings</option>
                                                    <option>Current</option>
                                                </select>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <!-- <label class="input-label">Occupation Type</label> -->
                                            </div>
                                        </div>
                                        <!-- <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <p class="form-helper">Cancelled Cheque or Passbook Front Page</p>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="col-lg-2 col-md-2 col-sm-2 p-lr-zero">
                                                        
                                                    </div>
                                                    <div class="col-lg-10 col-md-10 col-sm-10 p-lr-zero">
                                                        <p class="mini-text">Upload</p>
                                                        <p class="mini-text">Type of Image - .jpg,.png</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- bank Details Tab ends -->

                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="ekyc-tab">
                                <form id="ekyc-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "current-password" id="current-password" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Current Password</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "new-password" id="new-password" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">New Password</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "re-enter-password" id="re-enter-password" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Re-Enter Password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- ekyc Tab ends -->

                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="nominee-details-tab">
                                <form id="nominee-details-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-name" id="nom1-name" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Nominee Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="email" name = "nom1-relationship" id="nom1-relationship" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Relationship</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-dob" id="nom1-dob" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Date of Birth</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-address" id="nom1-address" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Communication Address</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-pin" id="nom1-pin" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Pincode</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-perc" id="nom1-perc" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Percentage Holding</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- Nominee Details Tab ends -->

                        </div> 
                    </div>     
                </div>
            </div>
        </section>

        <section id="footer-section">
            <div class="container"> 
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="list-inline">
                            <li class="footer-links"><a href="#">Privacy Policy</a></li>
                            <li class="footer-links"><a href="#">Terms of Use</a></li>
                            <li class="footer-links"><a href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                        <p class="footer-info">&copy;2017,Rightfunds.com</p>
                    </div>
                </div>  
            </div>  
        </section>

     <script src="js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="js/jquery.donut.js"></script>

     <script type="text/javascript">
         $(document).ready(function(){
            $('#goal-chart').donut();

            $('#settings-nav').on('click','li',function(){

                $('#settings-nav').find('li').removeClass('active-settings-nav');
                $('#settings-nav').find('li').addClass('inactive-sidemenu');
                $(this).removeClass('inactive-sidemenu').addClass('active-settings-nav');
                

                switch($(this).index()){

                    case 0 :
                        $('.section-header').text('Personal Details');
                        $('#bank-details-info').hide();
                        $('#personal-details-tab').show();
                        $('#nominee-details-tab,#bank-details-tab,#change-password-tab').hide();
                        break;

                    case 1 :
                        $('.section-header').text('Bank Details');
                        $('#bank-details-info').show();
                        $('#bank-details-tab').show();
                        $('#nominee-details-tab,#change-password-tab,#personal-details-tab').hide();
                        break;

                    case 2 :

                        $('.section-header').text('EKYC and Documents');
                        $('#bank-details-info').hide();
                        $('#ekyc-tab').show();
                        $('#nominee-details-tab,#bank-details-tab,#personal-details-tab').hide();
                        break;
                        

                    case 3 :
                        $('.section-header').text('Nominee Details');
                        $('.section-header').append('<button type="button" class="pull-right btn" id="add-nominee-btn">Add Nominee</button>');
                        $('#add-nominee-btn').show();
                        $('#bank-details-info').hide();
                        $('#nominee-details-tab').show();
                        $('#bank-details-tab,#change-password-tab,#personal-details-tab').hide();

                        break;
                }
            });
         });
     </script>
     <script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
  crossorigin="anonymous"></script>
    </body>
</html>
