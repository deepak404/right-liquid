<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Liquid Plus | Personal Details</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <meta name = "csrf-token" content = "{{ csrf_token() }}">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css"> -->
        <link rel="stylesheet" href="css/settings.css">
        <link rel="stylesheet" href="css/activate-acc-responsive.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="css/jqueryui.css">
        <link rel="stylesheet" href="css/modal.css">

        <link rel="stylesheet" href="css/withdraw-details.css">
        
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/font-and-global.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="css/navbar-responsive.css">
    </head>
    <body>

        @extends('layouts.navbar')
        @section('content')

        <section id="settings">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 p-r-35 p-l-zero" id="sidebar-left">
                            <div class="col-lg-12 col-md-12 col-sm-12 border-all br p-lr-zero bg-white" id="settings-sidebar">
                                <ul id="settings-nav">
                                    <li class="border-bottom active-settings-nav"><a href="#">Contact Details</a></li>
                                    <li class="inactive-sidemenu border-bottom"><a href="#">Change Password</a></li>
                                    <li class="inactive-sidemenu"><a href="#">Bank Details</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 border-all br p-lr-zero bg-white">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero border-bottom header-pad">
                                <p class="section-header pl-30">Contact Details <button type="button" class="pull-right btn" id="add-nominee-btn">Add Nominee</button></p>
                                <p id="bank-details-info">To Edit Bank Details Please Contact <span class="blue-text">88258 88200</span> or <span class="blue-text">contact@rightfunds.com</span></p>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="personal-details-tab">
                                <form id="personal-details-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "name" id="name" value="{{\Auth::user()->name}}" class="input-field" required>
                                                <label>First Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="email" name = "email" id="email" value="{{\Auth::user()->email}}"" class="input-field" required>
                                                <label>E-mail ID</label>
                                                <span class="text-danger"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "mobile" id="mobile" value="{{\Auth::user()->mobile}}"" class="input-field" required>
                                                <label>Mobile Number</label>
                                                <span class="text-danger"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <!-- <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address" value="{{\Auth::user()->name}} class="input-field" required>
                                                <label>Communication Address</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pin" id="pin" class="input-field" required>
                                                <label>PIN Code</label>
                                            </div>
                                        </div> -->

                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary grad-btn btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- Personal Details Tab ends -->


                             <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="bank-details-tab">
                                <!-- <form id="bank-details-form"> -->

                                    <?php $count = 0; ?>
                                    @foreach($bank_details as $bank_detail)
                                    
                                    <?php $count++; ?>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <p class="bank-header">Bank {{$count}}</p>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "acc-name" id="acc-name" class="input-field" value="{{$bank_detail['acc_name']}}" required disabled>
                                                <label>Account Holder Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "acc-no" id="acc-no" class="input-field" value="{{$bank_detail['acc_no']}}" required disabled>

                                                <label>Account Number</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "ifsc-code" id="ifsc-code" class="input-field" value="{{$bank_detail['ifsc_code']}}" required disabled>
                                                <label>IFSC Code</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address" class="input-field" value="{{$bank_detail['acc_type']}}" required disabled>
                                                <label>Account Type</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "pan" id="pan" class="input-field" value="{{$bank_detail['pan']}}" required disabled>
                                                <label>PAN</label>
                                            </div>
                                        </div>
                                    </div>

                                    @endforeach


                                    <!-- <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <p class="bank-header">Bank 2</p>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "acc-name" id="acc-name" class="input-field" required>
                                                <label>Account Holder Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="email" name = "acc-no" id="acc-no" class="input-field" required>

                                                <label>Account Number</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "ifsc-code" id="ifsc-code" class="input-field" required>
                                                <label>IFSC Code</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "address" id="address" class="input-field" required>
                                                <label>Account Type</label>
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <input type="submit" class = "blue-btn btn-primary grad-btn btn" id="submit-personal" name="" value="SAVE"> -->
                                <!-- </form> -->
                            </div><!-- bank Details Tab ends -->

                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="change-password-tab">
                                <form id="change-password-form">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "current-password" id="current-password" class="input-field" required>
                                                <label>Current Password</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "new-password" id="new-password" class="input-field" required>
                                                <label>New Password</label>
                                            </div>
                                            <span class="text-danger" id="no-match"></span>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "confirm-password" id="confirm-password" class="input-field" required>
                                                <label>Re-Enter Password</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary grad-btn btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div><!-- bank Details Tab ends -->

                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="nominee-details-tab">
                                <form class="nnominee-details-form">
                                    <input type="hidden" name="nominee-type" value="1">
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-name" id="nom1-name" class="input-field" required>
                                                <label>Nominee Name</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="email" name = "nom1-relationship" id="nom1-relationship" class="input-field" required>
                                                <label>Relationship</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-dob" id="nom1-dob" class="input-field" required>
                                                <label>Date of Birth</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 p-tb-20">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-address" id="nom1-address" class="input-field" required>
                                                <label>Communication Address</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-pin" id="nom1-pin" class="input-field" required>
                                                <label>Pincode</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "nom1-perc" id="nom1-perc" class="input-field" required>
                                                <label>Percentage Holding</label>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class = "blue-btn btn-primary grad-btn btn" id="submit-personal" name="" value="SAVE">
                                </form>
                            </div> -->

                        </div> 
                    </div>     
                </div>
            </div>
        </section>

        @endsection()


    <div id="infoModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
              <div class="modal-res-body">
                  <p class="modal-res-text"></p>
              </div>
            <div class="modal-res-header">
                <img class="center-block response-image" src="/icons/success-tick.svg">
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default center-block success-btn grad-btn" data-dismiss="modal">Done</button>
          </div>
        </div>

      </div>
    </div>

     <script src="js/jquery.min.js"></script>
     <script src="js/personal-details.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

     <script type="text/javascript">
         $(document).ready(function(){

            $('#settings-nav').on('click','li',function(){

                $('#settings-nav').find('li').removeClass('active-settings-nav');
                $('#settings-nav').find('li').addClass('inactive-sidemenu');
                $(this).removeClass('inactive-sidemenu').addClass('active-settings-nav');
                

                switch($(this).index()){

                    case 0 :
                        $('.section-header').text('Personal Details');
                        $('#bank-details-info').hide();
                        $('#personal-details-tab').show();
                        $('#nominee-details-tab,#bank-details-tab,#change-password-tab').hide();
                        break;

                    case 1 :
                        $('.section-header').text('Change Password');
                        $('#bank-details-info').hide();
                        $('#bank-details-tab').hide();
                        $('#change-password-tab').show();
                        $('#nominee-details-tab,#bank-details-tab,#personal-details-tab').hide();
                        break;

                    case 2 :
                        $('.section-header').text('Bank Details');
                        $('#bank-details-info').show();
                        $('#bank-details-tab').show();
                        $('#nominee-details-tab,#change-password-tab,#personal-details-tab').hide();
                        break;

                    // case 3 :
                    //     $('.section-header').text('Nominee Details');
                    //     $('.section-header').append('<button type="button" class="pull-right btn" id="add-nominee-btn">Add Nominee</button>');
                    //     $('#add-nominee-btn').show();
                    //     $('#bank-details-info').hide();
                    //     $('#nominee-details-tab').show();
                    //     $('#bank-details-tab,#change-password-tab,#personal-details-tab').hide();

                    //     break;
                }
            });

         });
     </script>
     <script type="text/javascript" src="js/"></script>
     <script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
  crossorigin="anonymous"></script>

    </body>
</html>
