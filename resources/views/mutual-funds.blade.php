<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>SIP Calculator - Best SIP Plans to begin Investing</title>
        <meta name="description" content="Mutual fund SIP Plans to begin investing. SBI SIP | HDFC SIP | HSBC SIP | ICICI SIP | Franklin SIP | UTI SIP | Reliance SIP | MF SIP Calculator">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- <script src="js/vendor/modernizr-2.8.3.min.js"></script> -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="css/mutual-funds.css">
        <link rel="stylesheet" href="css/index.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111077749-1"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-111077749-1');
      </script>
    </head>
    <body>

        

<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
  <div class="container-fluid" id="navbar-container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="#"><img src="images/logo.svg" class="nav-logo"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li class="active-menu"><a href="/top_performing_mutual_funds_in_india">Top Performing Mutual Funds</a></li>
      </ul>
    </div>
  </div>
</nav>


    <section id="heading">
        <div class="container">
            <div class="row">
                <div class = "col-lg-12 col-md-12 col-sm-12">
                    <h1 class="text-center" id="topic-heading">Best SIP Plans to begin Investing</h1>
                    <p class="sub-info text-center">These funds are selected on the basis of past performance and current market scenarios.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="equity-mf" class="fund-class-section">
        <div class="container"> 
            <div class="row">   
                <div class = "col-lg-12 col-md-12 col-sm-12">   
                    <p class="color-heading">Equity Mutual Funds</p>
                    <div class="table-wrapper">
                        <table class="table"> 
                            <thead>
                                <tr>
                                    <th><p class="scheme-name">Scheme Name</p></th>
                                    <th>Value Research rating</th>
                                    <th>1 Yr(%)</th>
                                    <th>3 Yr(%)</th>
                                    <th>5 Yr(%)</th>
                                    <th>AUM(cr)</th>
                                    <th>Fund Class</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>    
                                    <td><p>Aditya Birla Sun Life Frontline Equity Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons no-star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>25.83</td>
                                    <td>11.55</td>
                                    <td>17.25</td>
                                    <td>19,936</td>
                                    <td>Large Cap</td>
                                </tr>
                                <tr>    
                                    <td><p>Kotak Select Focus</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>29.45</td>
                                    <td>14.54</td>
                                    <td>20.34</td>
                                    <td>15,935</td>
                                    <td>Large Cap</td>
                                </tr>
                                <tr>    
                                    <td><p>Franklin India Prima Plus Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons no-star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>25.74</td>
                                    <td>12.69</td>
                                    <td>18.66</td>
                                    <td>11,908</td>
                                    <td>Multicap</td>
                                </tr>
                                <tr>    
                                    <td><p>Aditya Birla Sun Life Pure Value Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>45.5</td>
                                    <td>19.55</td>
                                    <td>29.42</td>
                                    <td>2,300</td>
                                    <td>Midcap</td>
                                </tr>
                                <tr>    
                                    <td><p>Franklin India Smaller Companies Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>34.63</td>
                                    <td>19.66</td>
                                    <td>29.8</td>
                                    <td>7,705</td>
                                    <td>Smallcap</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>  
        </div>
    </section>

    <section id="balanced-mf" class="fund-class-section">
        <div class="container"> 
            <div class="row">   
                <div class = "col-lg-12 col-md-12 col-sm-12">   
                    <p class="color-heading">Balanced Mutual Funds</p>
                    <div class="table-wrapper">
                        <table class="table"> 
                            <thead>
                                <tr>
                                    <th><p class="scheme-name">Scheme Name</p></th>
                                    <th>Value Research rating</th>
                                    <th>1 Yr(%)</th>
                                    <th>3 Yr(%)</th>
                                    <th>5 Yr(%)</th>
                                    <th>AUM(cr)</th>
                                    <th>Fund Class</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>    
                                    <td><p>L&T Prudence Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>23.56</td>
                                    <td>13.19</td>
                                    <td>18.28</td>
                                    <td>8,313</td>
                                    <td>Balanced</td>
                                </tr>
                                <tr>    
                                    <td><p>HDFC Balanced Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>23.02</td>
                                    <td>12.96</td>
                                    <td>18.67</td>
                                    <td>18,027</td>
                                    <td>Balanced</td>
                                </tr>
                                <tr>    
                                    <td><p>Aditya Birla Balanced '95 Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons no-star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>21.28</td>
                                    <td>12.06</td>
                                    <td>17.28</td>
                                    <td>12,827</td>
                                    <td>Balanced</td>
                                </tr>
                                <tr>    
                                    <td><p>Principal Balanced Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>32.35</td>
                                    <td>15.3</td>
                                    <td>17.28</td>
                                    <td>705</td>
                                    <td>Balanced</td>
                                </tr>
                                <tr>    
                                    <td><p>DSP BR Balanced Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons no-star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>21.73</td>
                                    <td>12.69</td>
                                    <td>15.63</td>
                                    <td>6543</td>
                                    <td>Balanced</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>  
        </div>
    </section>


    <section id="debt-mf" class="fund-class-section">
        <div class="container"> 
            <div class="row">   
                <div class = "col-lg-12 col-md-12 col-sm-12">   
                    <p class="color-heading">Debt Mutual Funds</p>
                    <div class="table-wrapper">
                        <table class="table"> 
                            <thead>
                                <tr>
                                    <th><p class="scheme-name">Scheme Name</p></th>
                                    <th>Value Research rating</th>
                                    <th>1 Yr(%)</th>
                                    <th>3 Yr(%)</th>
                                    <th>5 Yr(%)</th>
                                    <th>AUM(cr)</th>
                                    <th>Fund Class</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>    
                                    <td><p>Kotak Income Opportunities Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons no-star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>6.64</td>
                                    <td>8.78</td>
                                    <td>9.02</td>
                                    <td>5,502</td>
                                    <td>Credit Oppurtunities</td>
                                </tr>
                                <tr>    
                                    <td><p>Franklin India Dynamic Accrual Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>8.93</td>
                                    <td>9.76</td>
                                    <td>9.21</td>
                                    <td>2,974</td>
                                    <td>Credit Oppurtunities</td>
                                </tr>
                                <tr>    
                                    <td><p>Aditya Birla Sun Life Medium Term Plan</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons no-star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>7.07</td>
                                    <td>9.27</td>
                                    <td>10.03</td>
                                    <td>11,749</td>
                                    <td>Credit Oppurtunities</td>
                                </tr>
                                <tr>    
                                    <td><p>Franklin India Ultra Short Bond Fund </p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>8.43</td>
                                    <td>9.29</td>
                                    <td>9.6</td>
                                    <td>12,177</td>
                                    <td>Ultra Short Term</td>
                                </tr>
                                <tr>    
                                    <td><p>L&T Short Term Income Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>8.46</td>
                                    <td>9.07</td>
                                    <td>9.06</td>
                                    <td>1,134</td>
                                    <td>Credit Oppurtunities</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>  
        </div>
    </section>


    <section id="elss-mf" class="fund-class-section">
        <div class="container"> 
            <div class="row">   
                <div class = "col-lg-12 col-md-12 col-sm-12">   
                    <p class="color-heading">ELSS Mutual Funds</p>
                    <div class="table-wrapper">
                        <table class="table"> 
                            <thead>
                                <tr>
                                    <th><p class="scheme-name">Scheme Name</p></th>
                                    <th>Value Research rating</th>
                                    <th>1 Yr(%)</th>
                                    <th>3 Yr(%)</th>
                                    <th>5 Yr(%)</th>
                                    <th>AUM(cr)</th>
                                    <th>Fund Class</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>    
                                    <td><p>Principal Tax Savings Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons no-star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>42.12</td>
                                    <td>16.79</td>
                                    <td>20.93</td>
                                    <td>383</td>
                                    <td>Equity - ELSS</td>
                                </tr>
                                <tr>    
                                    <td><p>Reliance Tax Saver(ELSS) Fund Growth</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>36.76</td>
                                    <td>13.05</td>
                                    <td>22.42</td>
                                    <td>10,157</td>
                                    <td>Equity - ELSS</td>
                                </tr>
                                <tr>    
                                    <td><p>Tata India Tax Savings Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>38.23</td>
                                    <td>18.02</td>
                                    <td>20.95</td>
                                    <td>981</td>
                                    <td>Equity - ELSS</td>
                                </tr>
                                <tr>    
                                    <td><p>Aditya Birla Sun Life Tax Relief 96</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>38.2</td>
                                    <td>16.72</td>
                                    <td>21.91</td>
                                    <td>4,349</td>
                                    <td>Equity - ELSS</td>
                                </tr>
                                <tr>    
                                    <td><p>L&T Tax Advantage Fund</p></td>
                                    <td>
                                        <p class="rating-holder center-block">
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                            <span><i class="material-icons star">star_rate</i></span>
                                        </p>
                                    </td>
                                    <td>34.72</td>
                                    <td>16</td>
                                    <td>18.79</td>
                                    <td>2,762</td>
                                    <td>Equity - ELSS</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>  
            </div>  
        </div>
    </section>




     <script src="js/jquery.min.js"></script>
     <script src="js/jquery.donut.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script>
         $(document).ready(function(){
            $('#chart').donut();
         });
     </script>
    </body>
</html>
