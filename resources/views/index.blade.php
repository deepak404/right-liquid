<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Liquid Plus | Mutual Fund Investment</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css"> -->
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/footer.css">

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->


    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="css/font-and-global.css">
    <link rel="stylesheet" href="css/landing.css">
    <link rel="stylesheet" href="css/landing-responsive.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114917491-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-114917491-2');
    </script>

</head>
<body>



<section id="introduction-tab">

    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid" id="navbar-container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="icons/logo.svg" class="nav-logo"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!--  <ul class="nav navbar-nav">
                   <li class="active-menu"><a href="#">Home</a></li>
                   <li><a href="#">Account Statement</a></li>
                   <li><a href="#">Settings</a></li>

                 </ul> -->

                <ul class="nav navbar-nav navbar-right" id="register-nav">
                    <li><a href="https://www.pwm-india.com" target="_blank">PWM</a></li>
                    {{--<li><a href="#">FAQ</a></li>--}}
                    <li><a href="/login" class ="blue-text">Login</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="container">
        <div class="row">
            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                <div class = "col-lg-7 col-md-7 col-sm-7 p-l-zero" id="intro-text-container">
                    <h1 class="fp">Put your idle money to
                        work with Liquid+</h1>

                    <p>Liquid plus platform provides an easy solution to invest your idle money into liquid schemes and arbitrage funds, even if its just for a few days.
                    </p>

                    <div id="powered-by">
                        <p>Powered by Prosperity Wealth Management</p>
                        <img src="icons/pro_logo.svg" alt="Prosperity Wealth Management Logo" id="p-logo">
                        {{--<img src="icons/wreath.svg" alt="Assets Under Management" id="wreath">--}}
                        <img src="images/partners.png" alt="Partners" id="partner-logo">
                    </div>

                </div>

                <div class="col-lg-5 col-md-5 col-sm-5">
                    <div id="login-card">
                        <h3 class="text-center">Signup for an account.</h3>

                        <form action="#" id="enquiry-form" name="enquiry-form">
                            {{csrf_field()}}
                            <div class="form-group">
                                <input type="text" name="company-name" id="company-name" placeholder="Company Name" required>
                            </div>

                            <div class="form-group">
                                <input type="email" name="email" id="email" placeholder="E-mail" required>
                            </div>

                            <div class="form-group">
                                <input type="text" name="mobile" id="mobile" placeholder="Mobile" required>
                            </div>

                            <div class="form-group">
                                <input type="text" name="name" id="name" placeholder="Name" required>
                            </div>

                            <p class="email-response text-center"></p>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary grad-btn">Sign Up</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="why-choose-us">
    <div class="container">
        <div class="row">
            <h2 class="text-center fp">6 Reasons to invest your money with Liquid + </h2>
            <div class = "col-lg-12 col-md-12 col-sm-12 feature-container">
                <div class = "col-lg-4 col-md-4 col-sm-4">
                    <img src="icons/icon1.svg" class="center-block choose-image">
                    <h4 class="text-center feature-heading">Gain upto 6.75% Returns
                        on your idle money. </h4>
                    <p class="feature-info text-center">Money lying idle in a current account accrues 0% interest everyday. With liquid funds gain upto 6.75% annualised returns on all days including Sundays.


                    </p>
                </div>
                <div class = "col-lg-4 col-md-4 col-sm-4">
                    <img src="icons/icon2.svg" class="center-block choose-image">
                    <h4 class="text-center feature-heading">Secure investments
                        via NetBanking & RTGS</h4>
                    <p class="feature-info text-center">Your funds are directly transmitted from your bank account to the respective asset management companies .
                    </p>
                </div>
                <div class = "col-lg-4 col-md-4 col-sm-4">
                    <img src="icons/icon3.svg" class="center-block choose-image">
                    <h4 class="text-center feature-heading">Quick Order
                        Realisation</h4>
                    <p class="feature-info text-center">Investments and Withdrawals are effected on the same day for orders placed before 1pm IST. </p>
                </div>
            </div>

            <div class = "col-lg-12 col-md-12 col-sm-12 feature-container">
                <div class = "col-lg-4 col-md-4 col-sm-4">
                    <img src="icons/icon4.svg" class="center-block choose-image">
                    <h4 class="text-center feature-heading">No Lock-in Periods,
                        Complete liquidity. </h4>
                    <p class="feature-info text-center">Unlike a bank fixed deposit there is no minimum investment period or penalty for premature withdrawals. You could invest on a Friday and redeem your funds on Monday.</p>
                </div>
                <div class = "col-lg-4 col-md-4 col-sm-4">
                    <img src = "icons/icon5.svg" class="center-block choose-image">
                    <h4 class="text-center feature-heading">Track your investments
                        daily via liquid+</h4>
                    <p class="feature-info text-center">Hassle free investment & withdrawal process with a click of a button. Account statements available on demand.
                    </p>
                </div>
                <div class = "col-lg-4 col-md-4 col-sm-4">
                    <img src="icons/icon6.svg" class="center-block choose-image">
                    <h4 class="text-center feature-heading">Short term Capital Gains Taxed
                        at 15% only on Arbitrage funds. </h4>
                    <p class="feature-info text-center">Save Taxes on investments in Arbitrage funds. These are zero volatility low risk funds that gain from the difference between the spreads in the futures and current market.</p>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="graph">
    <div class="container">
        <div class="row">
            <h2 class="text-center">Interest lost on Money lying idle in your current account.</h2>
            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="graph-holder">
                <div id="graph-nav">
                    <p>Money lying idle in a Current Account </p>
                    <p>
                        <span><a href="#" class="graph-duration" id="ten-lakhs">10 Lakhs</a></span>
                        <span><a href="#" class="graph-duration active" id="twenty-five-lakhs">25 Lakhs</a></span>
                        <span><a href="#" class="graph-duration" id="fifty-lakhs">50 Lakhs</a></span>
                        <span><a href="#" class="graph-duration" id="one-crore">1 Crore</a></span>
                        <span><a href="#" class="graph-duration" id="ten-crores">10 Crore</a></span>

                    </p>
                </div>

                <div id="graph-img-holder">
                    <img src="graph/10_lakhs.svg" style="display: none" alt="graph-img" data-dur="ten-lakhs" class="img img-responsive">
                    <img src="graph/25_lakhs.svg"  alt="graph-img" data-dur="twenty-five-lakhs" class="img img-responsive">
                    <img src="graph/50_lakhs.svg" style="display: none" alt="graph-img" data-dur="fifty-lakhs" class="img img-responsive">
                    <img src="graph/1_cr.svg" style="display: none" alt="graph-img" data-dur="one-crore" class="img img-responsive">
                    <img src="graph/10_cr.svg" style="display: none" alt="graph-img" data-dur="ten-crores" class="img img-responsive">

                </div>
                <p class="text-center" id="duration-indicator">
                    Duration
                </p>

                <p id="label-holder"><img src="icons/square.svg" id="label-image" alt="">Interest Lost</p>
            </div>
        </div>
    </div>
</section>


<section id="footer-section">
    <div class="container">
        <div class="row">
            <!-- <div class="col-lg-12 col-md-12 col-sm-12">
                <ul class="list-inline">
                    <li class="footer-links"><a href="#">Privacy Policy</a></li>
                    <li class="footer-links"><a href="#">Terms of Use</a></li>
                    <li class="footer-links"><a href="#">Contact</a></li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <p id="mf-disclaimer" class="footer-info">Mutual fund investments are subject to market risks. Please read the scheme information and other related documents before investing. Past performance is not indicative of future returns.</p>
                <p class="footer-info">&copy;2017,Rightfunds.com</p>
            </div> -->

            <div class = "col-lg-12 col-md-12 col-sm-12">
                <div class = "col-lg-4 col-md-4 col-sm-4">
                    <img src="icons/logo.svg" id="footer-logo">
                    <p id="reg-company">&copy;Prosperity Technology Private Limited,2017</p>
                    <p id="disclaimer">Disclaimer: Mutual Fund investment are subject to market risks, read all scheme related documents carefully before investing. Past Performance is not an indicator of future returns.</p>
                </div>

                <div class = "col-lg-3 col-md-3 col-sm-3">
                    <p class="footer-info">Support</p>
                    <ul class="footer-list">
                        <li><a href="/contact-us">Contact Us</a></li>
                        <li><a href="/terms">Terms of Use</a></li>
                        <li><a href="/privacy-policy">Privacy Policy</a></li>
                    </ul>
                </div>

                <div class = "col-lg-3 col-md-3 col-sm-3">
                    <p class="footer-info">Contact</p>
                    <ul class="footer-list">
                        <li> Level 1, No 1, Balaji First Avenue, T.Nagar, Chennai, Tamil Nadu 600017</li>
                        <li>+91 88258 88200</li>
                    </ul>
                </div>

                {{--<div class = "col-lg-2 col-md-2 col-sm-2">--}}
                    {{--<p class="footer-info">Follow us</p>--}}
                    {{--<ul class="list-inline" id="social-parent">--}}
                        {{--<li class="social-list"><a href="#"><img src="img/facebook-footer-logo.png" class="footer-social"></a></li>--}}
                        {{--<li class="social-list"><a href="#"><img src="img/twitter-footer-logo.png" class="footer-social"></a></li>--}}
                        {{--<li class="social-list"><a href="#"><img src="img/linkedin-footer-logo.png" class="footer-social"></a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            </div>
            <div class = "col-lg-12 col-md-12 col-sm-12" id="partners-container">
                <p class="footer-info p-l-15">AMC Partners</p>
                <p id="amc-names" class ="p-l-15"><span>SBI Mutual Fund</span> | <span>Reliance Mutual Fund</span> | <span>UTI Mutual Fund</span> | <span>TATA Mutual Fund</span> | <span>Sundaram Mutual Fund</span> | <span>L&T Mutual fund</span> | <span>Birla Sunlife Mutual fund</span> | <span>HDFC Mutual Fund</span>
                    <span>DSP BlackRock Mutual Fund</span> | <span>Kotak Mutual Fund</span> | <span>ICICI Mutual Fund</span> | <span>Franklin India Mutual Fund</span>.</p>
                <p class="p-l-15" id="cin-and-arn"><span id="cin">CIN Number: U72900TN2017PTC116593</span><a
                            href="arn"><span id="arn">ARN : 116221</span></a></p>
            </div>
        </div>
    </div>
</section>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<script>
    $(document).ready(function(){
        $('.graph-duration').on('click', function(e) {
            e.preventDefault();

            // console.log($(this).attr('id'));
            $('#graph-img-holder img').hide();
            $('#graph-img-holder img[data-dur = "'+$(this).attr('id')+'"]').show();
            $('.graph-duration').removeClass('active');
            $(this).addClass('active');

        })


        $('#enquiry-form').on('submit', function(e){
            e.preventDefault();

            var data = $(this).serialize();
            console.log(data);

            $.ajax({
                type: 'POST',
                url: 'send_enquiry',
                data: data,
                // async:false,
                success:function(data){
                    if(data.msg == true){
                        $('.email-response').text('Message has been sent Successfully. We will reach you shortly.');
                    }else{
                        $('.email-response').text('Message not sent. Refresh and try again.');
                    }

                    $('#enquiry-form')[0].reset();
                },
                error:function(){

                }
            });
        });


        $(document).ajaxStart(function(){////console.log('into loader');
            $('#enquiry-form button[type="submit"]').html("Sending <span class='mail-loader'></span>");

        });
        $(document).ajaxComplete(function(){////console.log('out from loader');

            $('#enquiry-form button[type="submit"]').html("Sign Up");

        });
    });
</script>
</body>
</html>
