<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css"> -->
        <!-- <link rel="stylesheet" href="{{url('/css/admin/index.css')}}"> -->
        <!-- <link rel="stylesheet" href="../css/index-responsive.css"> -->
        <link rel="stylesheet" href="{{url('/css/footer.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

        <link rel="stylesheet" href="{{url('/css/admin/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/suggestions.css')}}">
        <link rel="stylesheet" href="{{url('/css/modal.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

        <style type="text/css">
            input:focus ~ label.input-label, input:valid ~ label.input-label, input[type=email]:focus ~ label.input-label, input[type=email]:valid ~ label.input-label {
                top: -7px;
                font-size: 11px;
                color: #0091EA;
            }

            .bank-details-form{
                margin-top: 15px;
                padding-left: 0px;
            }

            select{
                padding-left: 0;
                font-size: 16px;
            }
            
            .worng-pass{
                color: #0091ea;
            }
            .delete-acc{
                float: right;
                color: #555555b0 !important;s
            }

        </style>
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Customer Details</p>
                        </div>

                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <div class="input-group">
                              <input type="text" class="form-control search" placeholder="Search Name / Pan / mobile ID" id="search-input" name="search-user" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="material-icons">search</i></span>
                            </div>
                        </div>

                        <div class = "col-lg-2 col-md-2 col-sm-2 p-r-zero">
                            <button class="btn btn-primary grad-btn" id="add-user-btn" onclick="javascript:location.href ='/admin/add_user'"><i class="material-icons">person_add</i><span>Add User</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @if(isset($user_details))

        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30"><span id="searched-user-name">{{$user_details['name']}}</span>|<span id="searched-user-email">{{$user_details['email']}}</span>|<span id="searched-user-phone">{{$user_details['mobile']}}</span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Current Value</p>
                                    <p class="inv-sum-amount" id="user-current-value">Rs.{{$portfolio_user_total['current_value']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Investment</p>
                                    <p class="inv-sum-amount" id="user-total-inv">Rs.{{$portfolio_user_total['amount_invested']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    <p class="inv-sum-amount" id="user-net-pl">Rs.{{$portfolio_user_total['net_returns']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Returns</p>
                                    <p class="inv-sum-amount green" id="user-returns">{{round($portfolio_user_total['abs_returns'],2)}}%</p>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>
        @else
        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30"><span id="searched-user-name">Name : </span>|<span id="searched-user-email">Email : </span>|<span id="searched-user-phone">Phone</span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Current Value</p>
                                    <p class="inv-sum-amount" id="user-current-value">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Investment</p>
                                    <p class="inv-sum-amount" id="user-total-inv">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    <p class="inv-sum-amount" id="user-net-pl">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Returns</p>
                                    <p class="inv-sum-amount green" id="user-returns">-%</p>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>
        @endif

        

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="con-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all br">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero header-pad" id="portfolio-header-cont">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <div class="dropdown">
                                      <button class="btn btn-primary dropdown-toggle dropdown-btn" type="button" data-toggle="dropdown">Kyc Details
                                      <span><i class="material-icons">keyboard_arrow_down</i></span></button>
                                      <ul class="dropdown-menu">
                                        <li><a href="#" id="user-inv-history" target="_blank">Investment History</a></li>
                                        <li><a href="#" id="user-port-details" target="_blank">Portfolio Details</a></li>
                                        <!-- <li><a href="#">JavaScript</a></li> -->
                                      </ul>
                                    </div>
                                </div>
<!--                                 <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <a href="#" class="btn btn-edit"><i class="material-icons">edit</i>Edit</a>
                                </div> -->
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                <ul class="list-inline border-bot" id="kyc-list">
                                    <li><a href="#" class="active details-toggle" id="company-details">Company Details</a></li>
                                    <li><a href="#" class="details-toggle" id="change-password">Change Password</a></li>
                                    <li><a href="#" id="bank-details" class="details-toggle">Bank Details</a></li>
                                    <li><a href="#" id="document-details" class="details-toggle">Documents</a></li>
                                </ul>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 m-t-30 details-tab" id="company-details-tab">
                                <form id="company-details-form" class="col-lg-12 col-md-12 col-sm-12">

                                    <?php //dd($user_details); ?>

                                    @if(isset($user_details))
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "name" id="name" class="input-field" value="{{$user_details['name']}}" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Name</label>
                                        </div>
                                    </div>

                                    <!-- <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "pan" id="pan" value="{{$user_details['pan']}}" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">PAN</label>
                                        </div>
                                    </div> -->

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "mobile" id="mobile" value="{{$user_details['mobile']}}" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Contact Number</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="email" name = "email" id="email" value="{{$user_details['email']}}" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Email</label>
                                        </div>
                                    </div>

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="submit-company" value="Edit">
                                    </div>
                                    @else
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "name" id="name" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Name</label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="user_id" id="user_id" value="">
                                    <!-- <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "pan" id="pan" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">PAN</label>
                                        </div>
                                    </div> -->

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "mobile" id="mobile" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Contact Number</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="email" name = "email" id="email" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Email</label>
                                        </div>
                                    </div>

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="submit-company" value="Save">
                                        <button type="button" class="btn btn-primary grad-btn" id="edit-company">Edit</button>
                                    </div>
                                    @endif
                                    
                                </form>
                            </div> <!-- company details tab ends -->

                            <div class="col-lg-12 col-mid-12 col-sm-12 m-t-30 details-tab" id="change-password-tab">
                                <form id="changepass" class="col-lg-12 col-md-12 col-sm-12">
                                    <input type="hidden" name="user_id" id="user_id_pass" value="">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="password" name ="old_pass" id="old_pass" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Old Password</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="password" name ="password" id="password" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">New Password</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="password" name ="c_password" id="c_password" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <span class="worng-pass"></span>
                                            <label class="input-label">Confirm Password</label>
                                        </div>
                                    </div>
                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="update-btn" value="Update">
                                    </div>
                                </form>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 m-t-30 details-tab" id="bank-details-tab">

                               @if(isset($bank_details))
                               @foreach($bank_details as $bank_detail)

                                <?php //dd($bank_detail); ?>
                                <p class="bank-count">Bank {{$bank_detail['acc_count']}}</p>
                                <form id="bank-details-form" class="col-lg-12 col-md-12 col-sm-12 bank-details-form p-lr-zero">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "acc-name" id="acc-name" value = "{{$bank_detail['acc_name']}}" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Name</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "acc-no" id="acc-no" value = "{{$bank_detail['acc_no']}}"  class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Account Number</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "ifsc-code" id="ifsc-code" value = "{{$bank_detail['ifsc_code']}}" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">IFSC Code</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "acc_type" id="acc_type" value = "{{$bank_detail['acc_type']}}" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Account type</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "payment_link" id="payment_link" value = "{{$bank_detail['payment_link']}}" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Payment Link</label>
                                        </div>
                                    </div>

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="submit-company" value="Edit">
                                    </div>
                                </form>
                               @endforeach
                               @else
                               @endif
                               
                            </div> <!-- bank details tab ends -->


                            <div class = "col-lg-12 col-md-12 col-sm-12 m-t-30 details-tab" id="document-details-tab">

                            @if(isset($document_details))
                             <form id="document-details-form">
                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">AOA</p>--}}
                                                {{--<a href = "../../docs/aoa/{{$document_details['aoa']}}" download class="file-name file-link" data-id="aoa">{{$document_details['aoa']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="aoa" id="aoa">--}}
                                                {{--<label for="aoa">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">MOA</p>--}}
                                                {{--<a href = "../../docs/moa/{{$document_details['moa']}}" download class="file-name file-link" data-id="moa">{{$document_details['moa']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="moa" id="moa">--}}
                                                {{--<label for="moa">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">BS</p>--}}
                                                {{--<a href = "../../docs/bs/{{$document_details['bs']}}" download class="file-name file-link" data-id="bs">{{$document_details['bs']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="bs" id="bs">--}}
                                                {{--<label for="bs">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">ASL</p>--}}
                                                {{--<a href = "../../docs/asl/{{$document_details['asl']}}" download class="file-name file-link" data-id="asl">{{$document_details['asl']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="asl" id="asl">--}}
                                                {{--<label for="asl">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">BR</p>--}}
                                                {{--<a href = "../../docs/br/{{$document_details['br']}}" download class="file-name file-link" data-id="br">{{$document_details['br']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="br" id="br">--}}
                                                {{--<label for="br">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">Address Proof</p>--}}
                                                {{--<a href = "../../docs/ap/{{$document_details['ap']}}" download class="file-name file-link" data-id="ap">{{$document_details['ap']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="ap" id="ap">--}}
                                                {{--<label for="ap">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">COI</p>--}}
                                                {{--<a href="../../docs/coi/{{$document_details['coi']}}" download class="file-name file-link" data-id="coi">{{$document_details['coi']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="coi" id="coi">--}}
                                                {{--<label for="coi">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-lg-4 col-md-4 col-sm-4 m-b-30">
                                        <div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">
                                            <div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">
                                                <p class="blue doc-name">KYC Status</p>
                                                <a href="../../docs/kyc/{{$document_details['kyc']}}" download class="file-name file-link" data-id="kyc">{{$document_details['kyc']}}</a>
                                            </div>
                                            <div class = "col-lg-3 col-md-3 col-sm-3">
                                                <input type="file" name="kyc" id="kyc">
                                                <label for="kyc">
                                                    <span><i class="material-icons upload-icon">file_upload</i></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                 <div class="col-lg-4 col-md-4 col-sm-4 m-b-30">
                                     <div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">
                                         <div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">
                                             <p class="blue doc-name">KYC Status</p>
                                             <a href="../../docs/cc/{{$document_details['cc']}}" download class="file-name file-link" data-id="cc">{{$document_details['cc']}}</a>
                                         </div>
                                         <div class = "col-lg-3 col-md-3 col-sm-3">
                                             <input type="file" name="cc" id="cc">
                                             <label for="cc">
                                                 <span><i class="material-icons upload-icon">file_upload</i></span>
                                             </label>
                                         </div>
                                     </div>
                                 </div>

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">PAN Card</p>--}}
                                                {{--<a href="../../docs/pan_card/{{$document_details['pan_card']}}" download class="file-name file-link" data-id="pan_card">{{$document_details['pan_card']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="pan_card" id="pan_card">--}}
                                                {{--<label for="pan_card">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">ID Proof</p>--}}
                                                {{--<a href = "../../docs/id_proof/{{$document_details['id_proof']}}" download class="file-name file-link" data-id="id_proof">{{$document_details['id_proof']}}</a>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="id_proof" id="id_proof">--}}
                                                {{--<label for="id_proof">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="submit-document" value="Edit">
                                    </div>
                                </form>
                            @else

                             <form id="document-details-form">
                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">AOA</p>--}}
                                                {{--<a href = "#" download class="file-name file-link" data-id="aoa"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="aoa" id="aoa" class="user-files">--}}
                                                {{--<label for="aoa">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">MOA</p>--}}
                                                {{--<a href = "#" download class="file-name file-link" data-id="moa"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="moa" id="moa" class="user-files">--}}
                                                {{--<label for="moa">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">BS</p>--}}
                                                {{--<a href = "#" download class="file-name file-link" data-id="bs"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="bs" id="bs" class="user-files">--}}
                                                {{--<label for="bs">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">ASL</p>--}}
                                                {{--<a href = "#" download class="file-name file-link" data-id="asl"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="asl" id="asl" class="user-files">--}}
                                                {{--<label for="asl">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">BR</p>--}}
                                                {{--<a href = "#" download class="file-name file-link" data-id="br"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="br" id="br" class="user-files">--}}
                                                {{--<label for="br">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">Address Proof</p>--}}
                                                {{--<a href = "#" download class="file-name file-link" data-id="ap"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="ap" id="ap" class="user-files">--}}
                                                {{--<label for="ap">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">COI</p>--}}
                                                {{--<a href="#" download class="file-name file-link" data-id="coi"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="coi" id="coi" class="user-files">--}}
                                                {{--<label for="coi">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-lg-4 col-md-4 col-sm-4 m-b-30">
                                        <div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">
                                            <div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">
                                                <p class="blue doc-name">KYC Status</p>
                                                <a href="#" download class="file-name file-link" data-id="kyc"></a>
                                                <span class="text-danger"></span>
                                            </div>
                                            <div class = "col-lg-3 col-md-3 col-sm-3">
                                                <input type="file" name="kyc" id="kyc" class="user-files">
                                                <label for="kyc">
                                                    <span><i class="material-icons upload-icon">file_upload</i></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                     <div class="col-lg-4 col-md-4 col-sm-4 m-b-30">
                                         <div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">
                                             <div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">
                                                 <p class="blue doc-name">Cancelled Cheque</p>
                                                 <a href="#" download class="file-name file-link" data-id="cc"></a>
                                                 <span class="text-danger"></span>
                                             </div>
                                             <div class = "col-lg-3 col-md-3 col-sm-3">
                                                 <input type="file" name="cc" id="cc" class="user-files">
                                                 <label for="cc">
                                                     <span><i class="material-icons upload-icon">file_upload</i></span>
                                                 </label>
                                             </div>
                                         </div>
                                     </div>

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">PAN Card</p>--}}
                                                {{--<a href="#" download class="file-name file-link" data-id="pan_card"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="pan_card" id="pan_card" class="user-files">--}}
                                                {{--<label for="pan_card">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">ID Proof</p>--}}
                                                {{--<a href = "#" download class="file-name file-link" data-id="id_proof"></a>--}}
                                                {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" name="id_proof" id="id_proof" class="user-files">--}}
                                                {{--<label for="id_proof">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="submit-document" value="Edit">
                                    </div>
                                </form>
                            @endif
                               
                            </div> <!-- bank details tab ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        @endsection

     <script src="{{url('js/jquery.min.js')}}"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="{{URL('js/admin/smart_search.js')}}"></script>
     <script src="{{URL('js/admin/admin-kyc.js?v=1.1')}}"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>

    <div id="infoModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
              <div class="modal-res-body">
                  <p class="modal-res-text"></p>
              </div>

            <div class="modal-res-header">
                <img class="center-block response-image" src="/icons/success-tick.svg">
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default center-block success-btn grad-btn" data-dismiss="modal">Done</button>
          </div>
        </div>

      </div>
    </div>

     <script type="text/javascript">
         $(document).ready(function(){
            $('.details-toggle').click(function(e){
                //alert($(this).attr('id'));
                e.preventDefault();
                $('#kyc-list').find('.active').removeClass('active');
                $(this).addClass('active');
                $('.details-tab').hide();
                var tab_name = $(this).attr('id');
                $('#'+tab_name+'-tab').show();
            });


            $('.search').focus(function(){
                $(this).parent().css({
                    'box-shadow': '0px 3px 5px 1px rgba(210, 210, 210, 0.3)',
                    'transition' : '0.3s'
                })
            });

            $('.search').blur(function(){
                $(this).parent().css({
                    'box-shadow': 'none',
                    'transition' : '0.3s'
                })
            });
         });
     </script>
    </body>
</html>
