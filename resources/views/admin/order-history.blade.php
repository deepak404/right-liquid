<!doctype html>

<?php 

use App\SchemeDetails;

class Schemes
{

  public $scheme_names; 


  function __construct(){
    $this->scheme_names = SchemeDetails::get()->pluck('scheme_name','scheme_code');
  }

}
?>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css"> -->
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link rel="stylesheet" href="{{url('/css/footer.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/admin/pending-orders.css')}}">
        <link rel="stylesheet" href="{{url('css/modal.css')}}">

        <style type="text/css">
          .scheme-det-holder{
              border-bottom: 1px solid #e6e6e6;
              padding: 0px 15px;
              padding-right: 0px;
            }
        </style>
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Orders <span id="total-orders"><strong>Total Orders: {{count($order_history)}}</strong></span></p>
                        </div>

                        <!-- <div class = "col-lg-5 col-md-5 col-sm-5">
                            <div class="input-group">
                              <input type="text" class="form-control search" placeholder="Search Name / Pan / mobile ID" id="search-user" name="search-user" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="material-icons">search</i></span>
                            </div>
                        </div>

                        <div class = "col-lg-2 col-md-2 col-sm-2 p-r-zero">
                            <button class="btn btn-primary" id="add-user-btn"><i class="material-icons">person_add</i><span>Add User</span></button>
                        </div> -->
                    </div>
                </div>
            </div>
        </section>

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">

                    @foreach($order_history as $orders)


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 po-container-holder">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero po-container">
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero pending-orders-holder">
                                <p class="po-details-holder"><a  data-toggle="collapse" data-target="#{{$orders['id']}}"><i class="material-icons">keyboard_arrow_down</i></a><span class="investor-name">{{$orders['user_name']}} </span> | <span class="investor-pan">Pan : {{$orders['pan']}} |</span><span class="order-type">{{$orders['investment_or_withdraw']}}</span><span class="green bse-status pull-right">BSE Pass</span></p>

                                <p class="order-details-holder">
                                    Order Date : <span> {{$orders['date']}}</span>
                                    Order Execution Date :  <span> {{$orders['date']}}</span>
                                    <span class="pull-right order-amount">Rs. {{$orders['amount']}}</span>
                                </p>
                            </div>

                            <?php //dd($user_portfolio_details); ?>

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero scheme-det-collapse collapse" id="{{$orders['id']}}">

                            @if($orders['investment_or_withdraw'] == 'Investment')
                              @foreach($user_portfolio_details as $portfolio_details)
                               @foreach($portfolio_details as $portfolio_detail)
                               @if($orders['id'] == $portfolio_detail['investment_id'])
                                <?php
                                $scheme = new Schemes();
                                $scheme_name = $scheme->scheme_names[$portfolio_detail['scheme_code']];

                                $current_status;

                                if($portfolio_detail['portfolio_status'] == 1){
                                    if($portfolio_detail['initial_units_held'] == $portfolio_detail['units_held']){
                                        $current_status = 'Active';
                                        $action = 'none';

                                    }else{
                                        $current_status = 'Partially Redeemed';
                                        $action = 'none';

                                    }
                                }elseif($portfolio_detail['portfolio_status'] == 2){
                                    $current_status = 'Redeemed';
                                    $action = 'none';
                                }elseif($portfolio_detail['portfolio_status'] == 3){
                                    $current_status = 'Cancelled';
                                    $action = 'none';
                                }
                                ?>
                                 <div class="scheme-det-holder" style="pointer-events:{{$action}}">
                                     <p class="pending-scheme-name">

                                         @if($current_status == "Partially Redeemed" || $current_status == "Redeemed" || $current_status == "Cancelled")
                                          {{--<span><input type="checkbox" id="{{$portfolio_detail['id']}}" name="check_{{$portfolio_detail['id']}}" checked>--}}
                                          {{--<label for="{{$portfolio_detail['id']}}"><i class="material-icons checkbox-icon green">check_box</i></label></span>--}}
                                         @else
                                             <span><input type="checkbox" id="{{$portfolio_detail['id']}}" name="check_{{$portfolio_detail['id']}}" checked>
                                              <label for="{{$portfolio_detail['id']}}"><i class="material-icons checkbox-icon green">check_box</i></label></span>
                                         @endif
                                         
                                         <!-- dd($portfolio_details); -->

                                         <span class="scheme-name">{{$scheme_name}} - ({{$current_status}})</span>

                                             @if($portfolio_detail['portfolio_status'] == "2")
                                             <span class="pull-right order-amount">Rs. {{$portfolio_detail['initial_amount_invested']}}</span>
                                             @else
                                             <span class="pull-right order-amount">Rs. {{$portfolio_detail['amount_invested']}}</span>
                                             @endif
                                     </p>
                                 </div>
                               @endif
                               @endforeach
                              @endforeach

                                <?php //dd($portfolio_detail); ?>
                                      <button type="button" class="btn btn-primary center-block revoke-investment-btn">Revoke Order</button>
                            @else
                                @foreach($total_withdraws as $withdraw)
                                        @if($orders['id'] == $withdraw['withdraw_group_id'])
                                            <?php
                                            $scheme = new Schemes();
                                            $scheme_name = $scheme->scheme_names[$withdraw['scheme_code']];

                                            $current_status;

                                            if($withdraw['withdraw_status'] == 1){

                                                $current_status = 'Redeemed';
                                                $action = 'none';

                                            }elseif($withdraw['withdraw_status'] == 0){

                                                $current_status = 'Order Placed (Not Redeemed Yet.)';
                                                $action = '';
                                            }
                                            ?>

                                            <div class="scheme-det-holder" style="pointer-events: {{$action}}">
                                                <p class="pending-scheme-name">

                                                    @if($withdraw['withdraw_status'] == "1")
                                                        {{--<span><input type="checkbox" id="{{$withdraw['id']}}" name="check_{{$withdraw['id']}}" checked>--}}
                                          {{--<label for="{{$withdraw['id']}}"><i class="material-icons checkbox-icon green">check_box</i></label></span>--}}
                                                    @else
                                                        <span><input type="checkbox" id="{{$withdraw['id']}}" name="check_{{$withdraw['id']}}">
                                          <label for="{{$withdraw['id']}}"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label></span>
                                                    @endif

                                                <!-- dd($portfolio_details); -->

                                                    <span class="scheme-name">{{$scheme_name}} - ({{$current_status}})</span>
                                                    <span class="pull-right order-amount">Rs. {{$withdraw['withdraw_amount']}}</span>
                                                </p>
                                            </div>
                                        @endif
                                    @endforeach
                                    @if($withdraw['withdraw_status'] == "1")
                                    <button type="button" class="btn btn-primary center-block revoke-withdraw-btn">Revoke Order</button>
                                    @else
                                        <button type="button" class="btn btn-primary center-block revoke-withdraw-btn" disabled>Revoke Order</button>
                                    @endif


                            @endif



                            </div>      
                        </div> <!-- box-shadow-all ends -->
                    </div> <!-- po-container-holder ends -->
                    @endforeach


                    <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 po-container-holder">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero po-container">
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero pending-orders-holder">
                                <p class="po-details-holder"><a href="#" data-toggle="collapse" data-target="#2"><i class="material-icons">keyboard_arrow_down</i></a><span class="investor-name">Vasudev fatehpuria </span> | <span class="investor-pan">Pan : BFRPN4910B |</span><span class="order-type">Investment</span><span class="green bse-status pull-right">BSE Pass</span></p>

                                <p class="order-details-holder">
                                    Order Date : <span> 10/11/2017</span>
                                    Order Execution Date :  <span> 10/11/2017</span>
                                    <span class="pull-right order-amount">Rs. 10,00,00,000</span>
                                </p>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 collapse p-lr-zero scheme-det-collapse" id="2">
                               <div class="scheme-det-holder">
                                   <p class="pending-scheme-name">
                                       <span><input type="checkbox" id="p-1" name="">
                                       <label for="p-1"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label></span>
                                       <span class="scheme-name">DSP BlackRock Tax Saver Fund</span>
                                       <span class="pull-right order-amount">Rs. 10,00,00,000</span>
                                   </p>
                               </div>


                               <div class="scheme-det-holder">
                                   <p class="pending-scheme-name">
                                       <span><input type="checkbox" id="p-1" name="">
                                       <label for="p-1"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label></span>
                                       <span class="scheme-name">DSP BlackRock Tax Saver Fund</span>
                                       <span class="pull-right order-amount">Rs. 10,00,00,000</span>
                                   </p>
                               </div>


                               <button type="button" class="btn btn-primary center-block revoke-order-btn">Revoke Order</button>
                            </div>
 -->




                           
                        </div>
                    </div>

                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        @endsection


        <div id="infoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-body">
                  <div class="modal-res-body">
                      <p class="modal-res-text"></p>
                  </div>

                <div class="modal-res-header">
                    <img class="center-block response-image" src="/icons/success-tick.svg">
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" id="done-btn" class="btn btn-primary center-block grad-btn" data-dismiss="modal">Done</button>
              </div>
            </div>

          </div>
        </div>

     <script src="../js/jquery.min.js"></script>
     <script src="{{url('js/admin/order-history.js')}}"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

     <script type="text/javascript">
         $(document).ready(function(){
            $('input[type="checkbox"]').on('change',function(){
                    if ($(this).prop('checked')) {
                        $(this).next().find('i').text('check_box').addClass('green');
                        //$count++;
                        ////console.log("Checked".$count);
                    }else{
                        $(this).next().find('i').text('check_box_outline_blank').removeClass('green');
                    }
                });
         })

     </script>
    </body>
</html>
