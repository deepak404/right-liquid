<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->


        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link rel="stylesheet" href="{{url('/css/footer.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/admin/schemes.css')}}">
        <link rel="stylesheet" href="{{url('/css/modal.css')}}">
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Scheme Management</p>
                        </div>

                        <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                            <button class="btn btn-primary grad-btn" id="add-scheme-btn"><i class="material-icons">person_add</i><span>Add Scheme</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="con-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all br">
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                <div class="table-wrapper">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><p class = "table-header">Scheme Name</p></th>
                                                <th><p class = "table-header">Scheme Type</p></th>
                                                <th><p class = "table-header">AMFII Code</p></th>
                                                <th><p class = "table-header">BSE Code</p></th>
                                                <th><p class = "table-header">Scheme Priority</p></th>
                                                <th><p class = "table-header">Scheme Status</p></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="scheme-list-body">


                                        @foreach($schemes as $scheme)
                                          <?php
                                            $scheme_type;
                                            if ($scheme->scheme_type == 'liquid') {
                                                $scheme_type = 'Liquid';
                                            }
                                            else if ($scheme->scheme_type == 'ust') {
                                                $scheme_type = 'UltraShort term';
                                            }
                                            else if($scheme->scheme_type == 'arb'){
                                              $scheme_type = 'Arbitrage';
                                            }
                                           ?>
                                            <tr class="border-bottom">
                                                <td><p class="scheme-name">{{$scheme['scheme_name']}}</p></td>
                                                <td><p class="scheme-name">{{$scheme_type}}</p></td>
                                                <td><p>{{$scheme['scheme_code']}}</p></td>
                                                <td><p>{{$scheme['bse_scheme_code']}}</p></td>
                                                <td><p>{{$scheme['scheme_priority']}}</p></td>
                                                @if($scheme['scheme_status'] == 1)
                                                <td><p class="active">Active</p></td>
                                                @else
                                                <td><p class="inactive">In Active</p></td>
                                                @endif
                                                <td class="edit-td" data-schemecode="{{$scheme['scheme_code']}}"><p><i class="material-icons">edit</i></p></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                           
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        @endsection



<!-- To add new scheme-->
<div id="addSchemeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <p class="text-center modal_header">Scheme Details</p>
      <div style="text-align:center;" id="add_scheme_status"></div>
      <div class="modal-body">

        <div class="row">
          <form id="add_scheme_form">
          {{csrf_field()}}
            <div class="col-lg-12 col-md-12 col-sm-12" id="form_container">

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="add_scheme_name" class="scheme_input input-field form-control center-block" id="add_scheme_name" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Scheme Name</label>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="add_fund_manager" class="scheme_input input-field form-control center-block" id="add_fund_manager" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Fund Manager</label>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="number" name="add_scheme_code" class="scheme_input input-field form-control center-block" id="add_scheme_code" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Scheme Code</label>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="add_asset_size" class="scheme_input input-field form-control center-block" id="add_asset_size" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Asset Size</label>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme type</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="add_scheme_type" class="scheme_select" name="add_scheme_type">
                  <option value="liquid">Liquid</option>
                  <option value="ust">Ultrashort Term</option>
                  <option value="arb">Arbitrage</option>
                  <!-- <option value="bal">Hybrid</option>
                  <option value="ts">ELSS</option> -->
                </select>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Fund type</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="add_fund_type" class="scheme_select" name="add_fund_type">
                  <option value="ultrashortterm">Ultra Short Term</option>
                  <option value="creditopportunities">Credit Opportunities</option>
                  <option value="longterm">Long Term</option>
                  <option value="largecap">Large Cap</option>
                  <option value="diversified">Diversified</option>
                  <option value="hybrid">Hybrid</option>
                  <option value="smallmidcap">Small & Mid Cap</option>
                  <option value="sector">Sector</option>
                  <option value="liquid">Liquid</option>
                  <option value="floatingrate">Floating Rate</option>
                  <option value="gilt">Gilt</option>
                  <option value="accrual">Accrual</option>
                </select>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="add_benchmark" class="scheme_input input-field form-control center-block" id="add_benchmark" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Benchmark</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="add_scheme_priority" class="scheme_input input-field form-control center-block" id="add_scheme_priority" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Scheme Priority</label>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="add_exit_load" class="scheme_input input-field form-control center-block" id="add_exit_load" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Exit Load</label>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="add_launch_date" class="scheme_input input-field form-control center-block" id="add_launch_date" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Launch Date</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="add_bse_scheme_code" class="scheme_input input-field form-control center-block" id="add_bse_scheme_code" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">BSE Scheme Code</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6" id="scheme-file-holder">
                <p class="scheme_file_helper" id="scheme-file-label">Scheme File</p>
                <input type="file" name="add_scheme_nav" class="scheme_input input-field form-control center-block" id="add_scheme_nav" required>
                <label for="add_scheme_nav" class="pull-right"><i class="material-icons" id="scheme-label">file_upload</i></label>
              </div>


            </div>
            <button type="submit" class="btn btn-primary grad-btn center-block" id="submit_scheme">Add Scheme</button>
          </form>
        </div>


      </div>
    </div>

  </div>
</div>

<!-- To Edit scheme-->
<div id="editSchemeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <p class="text-center modal_header">Scheme Details</p>
      <div style="text-align:center;" id="add_scheme_status"></div>
      <div class="modal-body">

        <div class="row">
          <form id="edit_scheme_form">
            <div class="col-lg-12 col-md-12 col-sm-12" id="edit-form">
              <input type="hidden" name="scheme_code" id="scheme_code_change">
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="edit_scheme_name" class="scheme_input input-field form-control center-block" id="edit_scheme_name" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Scheme Name</label>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="edit_fund_manager" class="scheme_input input-field form-control center-block" id="edit_fund_manager" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Fund Manager</label>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="number" name="edit_scheme_code" class="scheme_input input-field form-control center-block" id="edit_scheme_code" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Scheme Code</label>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="edit_asset_size" class="scheme_input input-field form-control center-block" id="edit_asset_size" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Asset Size</label>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Scheme type</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="edit_scheme_type" class="scheme_select" name="edit_scheme_type">
                  <option value="liquid">Liquid</option>
                  <option value="ust">Ultrashort Term</option>
                  <option value="arb">Arbitrage</option>
                  <!-- <option value="bal">Hybrid</option>
                  <option value="ts">ELSS</option> -->
                </select>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper">Fund type</p>
                <!--<input type="text" name="fund_type" class="scheme_input form-control center-block" id="fund_type" required>-->
                <select id="edit_fund_type" class="scheme_select" name="edit_fund_type">
                  <option value="ultrashortterm">Ultra Short Term</option>
                  <option value="creditopportunities">Credit Opportunities</option>
                  <option value="longterm">Long Term</option>
                  <option value="largecap">Large Cap</option>
                  <option value="diversified">Diversified</option>
                  <option value="hybrid">Hybrid</option>
                  <option value="smallmidcap">Small & Mid Cap</option>
                  <option value="sector">Sector</option>
                  <option value="liquid">Liquid</option>
                  <option value="floatingrate">Floating Rate</option>
                  <option value="gilt">Gilt</option>
                  <option value="accrual">Accrual</option>
                </select>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="edit_benchmark" class="scheme_input input-field form-control center-block" id="edit_benchmark" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Benchmark</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="edit_scheme_priority" class="scheme_input input-field form-control center-block" id="edit_scheme_priority" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Scheme Priority</label>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="edit_exit_load" class="scheme_input input-field form-control center-block" id="edit_exit_load" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Exit Load</label>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="edit_launch_date" class="scheme_input input-field form-control center-block" id="edit_launch_date" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">Launch Date</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="edit_bse_scheme_code" class="scheme_input input-field form-control center-block" id="edit_bse_scheme_code" required>
                <span class="highlight"></span>
                <span class="bar"></span>
                <label class="input-label">BSE Scheme Code</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <p class="scheme_input_helper" id="status-p">Scheme Status</p>
                <select id="edit_scheme_status" class="scheme_select" name="edit_scheme_status">
                  <option value="1">Active</option>
                  <option value="0">In Active</option>
                </select>
              </div>
<!-- 
              <div class="col-lg-6 col-md-6 col-sm-6" id="scheme-file-holder">
                <p class="scheme_file_helper" id="scheme-file-label">Scheme File</p>
                <input type="file" name="edit_scheme_nav" class="scheme_input input-field form-control center-block" id="edit_scheme_nav" required>
                <label for="edit_scheme_nav" class="pull-right"><i class="material-icons" id="scheme-label">file_upload</i></label>
              </div> -->


            </div>
            <button type="submit" class="btn btn-primary grad-btn center-block" id="edit-btn">Edit Scheme</button>
          </form>
        </div>


      </div>
    </div>

  </div>
</div>

<!-- End of add scheme-->

     <script src="../js/jquery.min.js"></script>
     <script src="../js/admin/add-scheme.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
