<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->


        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link rel="stylesheet" href="{{url('/css/footer.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/client-details.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/admin/schemes.css')}}">
        <link rel="stylesheet" href="{{url('/css/modal.css')}}">
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Client Details</p>
                        </div>

                        <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero">
                            <button class="btn btn-primary" id="add-user-btn" onclick="javascript:location.href ='/admin/add_user'"><i class="material-icons">person_add</i><span>Add User</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="portfolio-compensation">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-shadow-all p-lr-zero">
                  @foreach($info as $data)
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 border-bottom p-lr-zero">
                    <p class="index-tag-p" href="#{{$data['id']}}" data-toggle="collapse"><i class="material-icons arrow-icon">keyboard_arrow_down</i><span class="index-name">{{$data['name']}}</span> | <span class="index-info">{{$data['mobile']}}</span> | <span class="index-info">{{$data['email']}}</span>
                    </p>
                    <div class="dropdown pull-right">
                      <a class="btn dropdown-toggle more-icon" data-toggle="dropdown"><i class="material-icons">more_vert</i></a>
                      <ul class="dropdown-menu">
                        <li><a href="/admin/customer_support">KYC Details</a></li>
                        <li><a href="/admin/portfolio_details/{{$data['id']}}">Portfolio Details</a></li>
                        <li><a href="/admin/investment_history/{{$data['id']}}">Investment History</a></li>
                      </ul>
                    </div>
                    <div id="{{$data['id']}}" class="collapse">
                        <table class="info-table">
                          <thead>
                            <tr class="table-row">
                              <th>Name</th>
                              <th class="text-center">PAN</th>
                              <th class="text-center">Bank Name</th>
                              <th class="text-center">Acc No.</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($data['bank'] as $bank)
                            <tr class="table-row">
                              <td>{{$bank['acc_name']}}</td>
                              <td class="text-center">{{$bank['pan']}}</td>
                              <td class="text-center">{{$bank['bank_name']}}</td>
                              <td class="text-center">{{$bank['acc_no']}}</td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                  </div>
                  @endforeach
                </div>
            </div> <!-- Container ends -->
        </section>

        @endsection


<!-- End of add scheme-->

     <script src="../js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script src="/js/client-details.js"></script>
    </body>
</html>
