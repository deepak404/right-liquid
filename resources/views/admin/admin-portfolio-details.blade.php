<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css"> -->
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <!-- <link rel="stylesheet" href="../css/index-responsive.css"> -->
        <link rel="stylesheet" href="{{url('/css/footer.css')}}">
        <link rel="stylesheet" href="{{url('/css/jqueryui.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/admin/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        
        <link rel="stylesheet" href="{{url('/css/admin/schemes.css')}}">
        <link rel="stylesheet" href="{{url('/css/modal.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/portfolio-details.css')}}">


        <style>

            .select-form-helper{
                margin-left: 25px;
            }

            .select-form-helper ~.form-group>select{
                margin-left: 25px;
            }
        </style>

    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Portfolio Details</p>
                        </div>

                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <!-- <div class="input-group">
                              <input type="text" class="form-control search" placeholder="Search Name / Pan / mobile ID" id="search-user" name="search-user" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="material-icons">search</i></span>
                            </div> -->
                        </div>

                        <div class = "col-lg-2 col-md-2 col-sm-2 p-r-zero">
                            <button class="btn btn-primary" id="add-user-btn" onclick="javascript:location.href ='/admin/add_user'"><i class="material-icons">person_add</i><span>Add User</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

@if(isset($user_details))

        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30"><span id="searched-user-name">{{$user_details['name']}}</span>|<span id="searched-user-email">{{$user_details['email']}}</span>|<span id="searched-user-phone">{{$user_details['mobile']}}</span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Current Value</p>
                                    <p class="inv-sum-amount" id="user-current-value">Rs.{{$portfolio_user_total['current_value']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Investment</p>
                                    <p class="inv-sum-amount" id="user-total-inv">Rs.{{$portfolio_user_total['amount_invested']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    <p class="inv-sum-amount" id="user-net-pl">Rs.{{$portfolio_user_total['net_returns']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Returns</p>
                                    <p class="inv-sum-amount green" id="user-returns">{{round($portfolio_user_total['total_xirr'],2)}}%</p>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->
            </div> <!-- Container ends -->
        </section>
        @else
        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30"><span id="searched-user-name"></span>|<span id="searched-user-email"></span>|<span id="searched-user-phone"></span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Current Value</p>
                                    <p class="inv-sum-amount" id="user-current-value">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Investment</p>
                                    <p class="inv-sum-amount" id="user-total-inv">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    <p class="inv-sum-amount" id="user-net-pl">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Returns</p>
                                    <p class="inv-sum-amount green" id="user-returns">-%</p>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->
            </div> <!-- Container ends -->
        </section>
        @endif

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="con-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all br">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero header-pad" id="portfolio-header-cont">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero">
                                    <div class="dropdown">
                                      <button class="btn btn-primary dropdown-toggle dropdown-btn" type="button" data-toggle="dropdown">Portfolio Details
                                      <span><i class="material-icons">keyboard_arrow_down</i></span></button>
                                      <ul class="dropdown-menu">
                                        <li><a href="/admin/investment_history/{{$id}}" id="user-inv-history" target="_blank">Investment History</a></li>
                                        {{--<li><a href="#" id="user-port-details" target="_blank">KYC Details</a></li>--}}
                                        <!-- <li><a href="#">JavaScript</a></li> -->
                                      </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                                    <form action = "/admin/get_user_portfolio_document" method = "post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="user" value="{{$id}}">
                                        <button type="submit" class="btn btn-edit"><i class="material-icons">file_download</i>Export</button>
                                    </form>

                                    <a href="#" class="btn btn-edit" id="add-man-wd"><i class="material-icons">add_box</i>Add Withdrawal</a>
                                    <a href="#" id="add-man-inv" class="btn btn-edit"><i class="material-icons">library_add</i>Add Investment</a>

                                </div>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                <div class="table-wrapper">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><p class = "table-header">Mutual Fund scheme</p></th>
                                                <th><p class = "table-header">Investor Name</p></th>
                                                <th><p class = "table-header">Folio Number</p></th>
                                                <th><p class = "table-header">Amount Invested</p></th>
                                                <th><p class = "table-header">Scheme Type</p></th>
                                                <th><p class = "table-header">Units held</p></th>
                                                <th><p class = "table-header">Invested NAV</p></th>
                                                <th><p class = "table-header">Current NAV</p></th>
                                                <th><p class = "table-header">Current Value</p></th>
                                                <th><p class = "table-header">Net Returns</p></th>
                                                <!-- <th><p class = "table-header">Eligible as RTCG</p></th> -->
                                                <th><p class = "table-header">DOI/DOW</p></th>
                                                <th><p class = "table-header">In/wd</p></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($portfolios as $portfolio)
                                            <tr class="border-bottom">
                                                <td><p class="scheme-name">{{$portfolio['fund_name']}}</p></td>
                                                <td><p>{{$portfolio['investor_name']}}</p></td>
                                                <td><p class="folio-number">{{$portfolio['folio_number']}}</p></td>
                                                <td><p class="inv-amount">{{$portfolio['amount']}}</p></td>
                                                <td><p class="inv-amount">{{$portfolio['type']}}</p></td>
                                                <td><p class="units-held">{{$portfolio['units_held']}}</p></td>
                                                <td><p class="invested-nav">{{$portfolio['inv_nav']}}</p></td>
                                                <td><p class="current-nav">{{$portfolio['current_nav']}}</p></td>
                                                <td><p class="current-value">{{$portfolio['current_value']}}</p></td>
                                                <td><p class="net-returns">{{$portfolio['net_returns']}}</p></td>
                                                <!-- <td><p>0</p></td> -->
                                                <td><p class="purchase-date">{{$portfolio['date']}}</p></td>
                                                <td><p>{{$portfolio['transaction_type']}}</p></td>
                                                <td><p><i class="material-icons edit-portfolio" data-id = "{{$portfolio['id']}}">edit</i></p></td>
                                            </tr>
                                            @endforeach
                                            <!-- <tr class="border-bottom">
                                                <td><p class="scheme-name">DSP Blackrock Small and Midcap fund growth</p></td>
                                                <td><p>443490/32</p></td>
                                                <td><p>5000</p></td>
                                                <td><p>100.8988</p></td>
                                                <td><p>51.215</p></td>
                                                <td><p>5167.07</p></td>
                                                <td><p>167.07</p></td>
                                                <td><p>0</p></td>
                                                <td><p>14-11-2017</p></td>
                                                <td><p>Inv</p></td>
                                                <td><p><i class="material-icons">edit</i></p></td>
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><p class="scheme-name">DSP Blackrock Small and Midcap fund growth</p></td>
                                                <td><p>443490/32</p></td>
                                                <td><p>5000</p></td>
                                                <td><p>100.8988</p></td>
                                                <td><p>51.215</p></td>
                                                <td><p>5167.07</p></td>
                                                <td><p>167.07</p></td>
                                                <td><p>0</p></td>
                                                <td><p>14-11-2017</p></td>
                                                <td><p>Inv</p></td>
                                                <td><p><i class="material-icons">edit</i></p></td>
                                            </tr>

                                            <tr class="border-bottom total">
                                                <td><p>TOTAL</p></td>
                                                <td><p>443490/32</p></td>
                                                <td><p>5000</p></td>
                                                <td><p>100.8988</p></td>
                                                <td><p>51.215</p></td>
                                                <td><p>5167.07</p></td>
                                                <td><p>167.07</p></td>
                                                <td><p>0</p></td>
                                                <td><p>14-11-2017</p></td>
                                                <td><p>Inv</p></td>
                                                <td></td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->
            </div> <!-- Container ends -->
        </section>

        @endsection


<!-- To add new scheme-->
<div id="editPortfolioModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <p class="text-center modal_header">Edit Portfolio</p>
      <div style="text-align:center;" id="add_scheme_status"></div>
      <div class="modal-body">

        <div class="row">
          <form id="edit_portfolio_form">
          {{csrf_field()}}

            <input type="hidden" name="p_id" id="p_id" value="">
            <div class="col-lg-12 col-md-12 col-sm-12" id="form_container">

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="modal_scheme_name" class="scheme_input input-field form-control center-block" id="modal_scheme_name" required>
                <span class="bar"></span>
                <label class="input-label">Scheme Name</label>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="modal_folio_number" class="scheme_input input-field form-control center-block" id="modal_folio_number" required>

                <span class="bar"></span>
                <label class="input-label">Folio Number</label>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="number" name="modal_amount_invested" class="scheme_input input-field form-control center-block" id="modal_amount_invested" required>

                <span class="bar"></span>
                <label class="input-label">Amount Invested</label>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="modal_units_held" class="scheme_input input-field form-control center-block" id="modal_units_held" required>
                <span class="bar"></span>
                <label class="input-label">Units Held</label>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="modal_current_nav" class="scheme_input input-field form-control center-block" id="modal_current_nav" required>
                <span class="bar"></span>
                <label class="input-label">Current NAV</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="modal_invested_nav" class="scheme_input input-field form-control center-block" id="modal_invested_nav" required>
                <span class="bar"></span>
                <label class="input-label">Invested NAV</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="modal_current_value" class="scheme_input input-field form-control center-block" id="modal_current_value" required>
                <span class="bar"></span>
                <label class="input-label">Current value</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="modal_net_returns" class="scheme_input input-field form-control center-block" id="modal_net_returns" required>

                <span class="bar"></span>
                <label class="input-label">Net Returns</label>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <input type="text" name="dop" class="scheme_input input-field form-control center-block" id="dop" required>
                <span class="bar"></span>
                <label class="input-label">Date Of purchase/Redemption</label>
              </div>
            </div>
            <button type="submit" class="btn btn-primary center-block" id="submit_scheme">Save Portfolio</button>
          </form>
        </div>


      </div>
    </div>

  </div>
</div>


<div id="infoModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
          <div class="modal-res-body">
              <p class="modal-res-text"></p>
          </div>

        <div class="modal-res-header">
            <img class="center-block response-image" src="/icons/success-tick.svg">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default center-block success-btn grad-btn" onClick="window.location.reload()" data-dismiss="modal">Done</button>
      </div>
    </div>

  </div>
</div>


<div id="invModal" class="modal fade in" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Manual Investment</h4>
      </div>
      <div class="modal-body">

        <div class="row">

<form id="manual-investment" class="col-xs-12 col-md-12 col-sm-12" name="manual-investment">

              <div class="col-lg-12 col-md-12 col-sm-12">
            <input type="hidden" name="user_id" id="inv_user_id" value="{{$id}}">
          <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <input type="text" name="inv-amount" class="scheme_input input-field form-control center-block" id="inv-amount" required>
            <span class="bar"></span>
            <label class="input-label">Investment Amount</label>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <!-- <p class="form-helper">Investment Date</p> -->
              <div class="form-group">
                <input type="text" name="inv-date" class="scheme_input input-field form-control center-block" id="inv-date" required>
                <span class="bar"></span>
                <label class="input-label">Investment Date</label>
              </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <!-- <p class="form-helper">Investment Date</p> -->
              <div class="form-group">
                <input type="text" name="pan-number" class="scheme_input input-field form-control center-block" id="pan-number" required>
                <span class="bar"></span>
                <label class="input-label">PAN Number</label>
              </div>
          </div>


          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <!-- <p class="form-helper">Investment Date</p> -->
              <div class="form-group">
                  <input type="text" name="acc-number" class="scheme_input input-field form-control center-block" id="acc-number" required>
                  <span class="bar"></span>
                  <label class="input-label">Account Number</label>
              </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <!-- <p class="form-helper">Payment type</p> -->
              <div class="form-group">
                <select class="add-inv-select" name="payment-type" id="payment-type">
                  <option value="1">NEFT</option>
                  <option value="2">RTGS</option>
                </select>
              </div>
          </div>


          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <!-- <p class="form-helper">Investment Date</p> -->
              <div class="form-group">
                  <input type="text" name="utr-number" class="scheme_input input-field form-control center-block" id="utr-number">
                  <span class="bar"></span>
                  <label class="input-label">UTR Number</label>
              </div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12" id="inv-portfolio-div">
            <a href="#" class="btn btn-primary" id="add-portfolio">ADD PORTFOLIO</a>


            <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero portfolio-holder p-lr-zero">

            </div>

          </div>


          <div class="col-lg-12 col-md-12 col-sm-12">
            <input type="submit" name="inv-submit" id="inv-submit" class="center-block btn btn-primary">
          </div>

          </div>
        </form>
          
        </div>
      </div>
    </div>
  </div>
</div>



        <div id="wdModal" class="modal fade in" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Manual Withdraw</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">

                            <form id="manual-withdraw" class="col-xs-12 col-md-12 col-sm-12" name="manual-withdraw">

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <input type="hidden" name="user_id" id="wd_user_id" value="{{$id}}">
                                    <div class=" col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <input type="text" name="wd-amount" class="scheme_input input-field form-control center-block" id="wd-amount" required>
                                        <span class="bar"></span>
                                        <label class="input-label">Withdraw Amount</label>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <input type="text" name="scheme-code" class="scheme_input input-field form-control center-block" id="scheme-code">
                                            <span class="bar"></span>
                                            <label class="input-label">Scheme Code</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <input type="text" name="pan-number" class="scheme_input input-field form-control center-block" id="pan-number" required >
                                            <span class="bar"></span>
                                            <label class="input-label">PAN Number</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <!-- <p class="form-helper">Investment Date</p> -->
                                        <div class="form-group">
                                            <input type="text" name="acc-no" class="scheme_input input-field form-control center-block" id="acc-no" required>
                                            <span class="bar"></span>
                                            <label class="input-label">Account Number</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <p class="select-form-helper form-helper">Full Amount Redemptoion</p>
                                        <div class="form-group">
                                            <select class="add-inv-select" name="full-amount" id="full-amount">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <p class="select-form-helper form-helper">Portfolio Type</p>
                                        <div class="form-group">
                                            <select class="add-inv-select" name="portfolio-type" id="portfolio-type">
                                                <option value="liquid">Liquid</option>
                                                <option value="arb">Arbitrage</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <!-- <p class="form-helper">Investment Date</p> -->
                                        <div class="form-group">
                                            <input type="text" name="withdraw-date" class="scheme_input input-field form-control center-block" id="withdraw-date">
                                            <span class="bar"></span>
                                            <label class="input-label">Withdraw Date</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <!-- <p class="form-helper">Investment Date</p> -->
                                        <div class="form-group">
                                            <input type="text" name="withdraw-nav" class="scheme_input input-field form-control center-block" id="withdraw-nav">
                                            <span class="bar"></span>
                                            <label class="input-label">Withdraw Nav</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <!-- <p class="form-helper">Investment Date</p> -->
                                        <div class="form-group">
                                            <input type="text" name="withdraw-units" class="scheme_input input-field form-control center-block" id="withdraw-units">
                                            <span class="bar"></span>
                                            <label class="input-label">Withdraw Units</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="inv-submit" id="inv-submit" class="center-block btn btn-primary">
                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

     <script src="{{url('js/jquery.min.js')}}"></script>
     <script src="{{url('js/jquery-ui.js')}}"></script>
     <script src="{{url('js/admin/admin-portfolio.js')}}"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


     <script type="text/javascript">
         $(document).ready(function(){
            $('.details-toggle').click(function(e){
                //alert($(this).attr('id'));
                e.preventDefault();
                $('#kyc-list').find('.active').removeClass('active');
                $(this).addClass('active');
                $('.details-tab').hide();
                var tab_name = $(this).attr('id');
                $('#'+tab_name+'-tab').show();
            });


            $('.search').focus(function(){
                $(this).parent().css({
                    'box-shadow': '0px 3px 5px 1px rgba(210, 210, 210, 0.3)',
                    'transition' : '0.3s'
                })
            });

            $('.search').blur(function(){
                $(this).parent().css({
                    'box-shadow': 'none',
                    'transition' : '0.3s'
                })
            });
         });
     </script>
    </body>
</html>
