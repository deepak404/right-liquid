<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css"> -->
        <link rel="stylesheet" href="../css/admin/index.css">
        <!-- <link rel="stylesheet" href="../css/index-responsive.css"> -->
        <link rel="stylesheet" href="../css/footer.css">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="../css/font-and-global.css">
        <link rel="stylesheet" href="{{url('/css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="../css/admin/index.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="../css/admin/dashboard.css">
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Dashboard</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30">Investor's Summary</p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total No of Investors</p>
                                    <p class="inv-sum-amount">{{$total_users}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Avg. Assets Under Management</p>
                                    <p class="inv-sum-amount">Rs.{{$aum}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Schemes Offered</p>
                                    <p class="inv-sum-amount">{{$scheme_count}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Avg. Holding Period</p>
                                    <p class="inv-sum-amount green">{{round($avg_holding_period,2)}} days</p>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        
                   <!--  <div class = "col-lg-5 col-md-5 col-sm-5 box-shadow-all br">
                        <p class="topic">Fund Allocation</p>
                    </div> -->
                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                        <div class = "col-lg-12 col-md-12 col-sm-12 box-shadow-all br p-lr-zero">
                            <p class="topic pl-30">Fund Summary</p>
                            <table class="table">
                                <thead>
                                    <th>Funds</th>
                                    <th>1 Day</th>
                                    <th>7 Days</th>
                                    <th>1 Month</th>
                                    <th>3 Months</th>
                                    <th>1 Year</th>
                                    <th>Amount Invested</th>
                                    <th>Current value</th>
                                </thead>
                                <tbody>
                                    @foreach($schemes as $scheme)

                                        @if($scheme->scheme_type == 'liquid')
                                    <tr>
                                        <td><p class="scheme-name">{{$scheme['scheme_name']}}</p></td>
                                        @if($scheme['one_day'] > 0)
                                        <td><p class="green return-perc">{{$scheme['one_day']}}%</p></td>
                                        @else
                                            <td><p class="red return-perc">{{$scheme['one_day']}}%</p></td>
                                        @endif


                                        @if($scheme['seven_day'] > 0)
                                            <td><p class="green return-perc">{{$scheme['seven_day']}}%</p></td>
                                        @else
                                            <td><p class="red return-perc">{{$scheme['seven_day']}}%</p></td>
                                        @endif

                                        @if($scheme['thirty_day'] > 0)
                                            <td><p class="green return-perc">{{$scheme['thirty_day']}}%</p></td>
                                        @else
                                            <td><p class="red return-perc">{{$scheme['thirty_day']}}%</p></td>
                                        @endif

                                        @if($scheme['ninety_day'] > 0)
                                            <td><p class="green return-perc">{{$scheme['ninety_day']}}%</p></td>
                                        @else
                                            <td><p class="red return-perc">{{$scheme['ninety_day']}}%</p></td>
                                        @endif


                                        @if($scheme['ninety_day'] > 0)
                                            <td><p class="green return-perc">{{$scheme['one_year']}}%</p></td>
                                        @else
                                            <td><p class="red return-perc">{{$scheme['one_year']}}%</p></td>
                                        @endif

                                        <td><p class="return-perc">{{$scheme['amount_invested']}}</p></td>
                                        <td><p class="return-perc">{{$scheme['current_value']}}</p></td>
                                    </tr>

                                        @endif
                                    @endforeach


                                    @foreach($schemes as $scheme)

                                        @if($scheme->scheme_type == 'arb')
                                            <tr>
                                                <td><p class="scheme-name">{{$scheme['scheme_name']}}</p></td>
                                                @if($scheme['one_day'] > 0)
                                                    <td><p class="green return-perc">{{$scheme['one_day']}}%</p></td>
                                                @else
                                                    <td><p class="red return-perc">{{$scheme['one_day']}}%</p></td>
                                                @endif


                                                @if($scheme['seven_day'] > 0)
                                                    <td><p class="green return-perc">{{$scheme['seven_day']}}%</p></td>
                                                @else
                                                    <td><p class="red return-perc">{{$scheme['seven_day']}}%</p></td>
                                                @endif

                                                @if($scheme['thirty_day'] > 0)
                                                    <td><p class="green return-perc">{{$scheme['thirty_day']}}%</p></td>
                                                @else
                                                    <td><p class="red return-perc">{{$scheme['thirty_day']}}%</p></td>
                                                @endif

                                                @if($scheme['ninety_day'] > 0)
                                                    <td><p class="green return-perc">{{$scheme['ninety_day']}}%</p></td>
                                                @else
                                                    <td><p class="red return-perc">{{$scheme['ninety_day']}}%</p></td>
                                                @endif


                                                @if($scheme['ninety_day'] > 0)
                                                    <td><p class="green return-perc">{{$scheme['one_year']}}%</p></td>
                                                @else
                                                    <td><p class="red return-perc">{{$scheme['one_year']}}%</p></td>
                                                @endif

                                                <td><p class="return-perc">{{$scheme['amount_invested']}}</p></td>
                                                <td><p class="return-perc">{{$scheme['current_value']}}</p></td>
                                            </tr>
                                            @endif
                                    @endforeach
                                   <!--  <tr>
                                        <td><p class="scheme-name">Sundaram Long term Micro Cap tax Advantage Fund</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="red return-perc">0.2%</p></td>
                                    </tr>
                                    <tr>
                                        <td><p class="scheme-name">Sundaram Long term Micro Cap tax Advantage Fund</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="red return-perc">0.2%</p></td>
                                    </tr>
                                    <tr>
                                        <td><p class="scheme-name">Sundaram Long term Micro Cap tax Advantage Fund</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="red return-perc">0.2%</p></td>
                                    </tr>
                                    <tr>
                                        <td><p class="scheme-name">Sundaram Long term Micro Cap tax Advantage Fund</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="red return-perc">0.2%</p></td>
                                    </tr>
                                    <tr>
                                        <td><p class="scheme-name">Sundaram Long term Micro Cap tax Advantage Fund</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="red return-perc">0.2%</p></td>
                                    </tr>
                                    <tr>
                                        <td><p class="scheme-name">Sundaram Long term Micro Cap tax Advantage Fund</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="green return-perc">0.2%</p></td>
                                        <td><p class="red return-perc">0.2%</p></td>
                                    </tr> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                        
                        
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        @endsection

     <script src="../js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


     <script type="text/javascript">
         $(document).ready(function(){
            $('.details-toggle').click(function(e){
                //alert($(this).attr('id'));
                e.preventDefault();
                $('#kyc-list').find('.active').removeClass('active');
                $(this).addClass('active');
                $('.details-tab').hide();
                var tab_name = $(this).attr('id');
                $('#'+tab_name+'-tab').show();
            });


            $('.search').focus(function(){
                $(this).parent().css({
                    'box-shadow': '0px 3px 5px 1px rgba(210, 210, 210, 0.3)',
                    'transition' : '0.3s'
                })
            });

            $('.search').blur(function(){
                $(this).parent().css({
                    'box-shadow': 'none',
                    'transition' : '0.3s'
                })
            });
         });
     </script>
    </body>
</html>
