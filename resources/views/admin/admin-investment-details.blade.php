<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css"> -->
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <!-- <link rel="stylesheet" href="../css/index-responsive.css"> -->
        <link rel="stylesheet" href="{{url('/css/footer.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="{{url('css/admin/investment-details.css')}}">
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Investment Details</p>
                        </div>

                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <!-- <div class="input-group">
                              <input type="text" class="form-control search" placeholder="Search Name / Pan / mobile ID" id="search-user" name="search-user" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="material-icons">search</i></span>
                            </div> -->
                        </div>

                        <div class = "col-lg-2 col-md-2 col-sm-2 p-r-zero">
                            <button class="btn btn-primary" id="add-user-btn" onclick="javascript:location.href ='/admin/add_user'"><i class="material-icons">person_add</i><span>Add User</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @if(isset($user_details))

        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30"><span id="searched-user-name">{{$user_details['name']}}</span>|<span id="searched-user-email">{{$user_details['email']}}</span>|<span id="searched-user-phone">{{$user_details['mobile']}}</span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Current Value</p>
                                    <p class="inv-sum-amount" id="user-current-value">Rs.{{$portfolio_user_total['current_value']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Investment</p>
                                    <p class="inv-sum-amount" id="user-total-inv">Rs.{{$portfolio_user_total['amount_invested']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    <p class="inv-sum-amount" id="user-net-pl">Rs.{{$portfolio_user_total['net_returns']}}</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Returns</p>
                                    <p class="inv-sum-amount green" id="user-returns">{{round($portfolio_user_total['total_xirr'],2)}}%</p>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->
            </div> <!-- Container ends -->
        </section>
        @else
        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30"><span id="searched-user-name"></span>|<span id="searched-user-email"></span>|<span id="searched-user-phone"></span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Current Value</p>
                                    <p class="inv-sum-amount" id="user-current-value">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Investment</p>
                                    <p class="inv-sum-amount" id="user-total-inv">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    <p class="inv-sum-amount" id="user-net-pl">Rs.-</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Returns</p>
                                    <p class="inv-sum-amount green" id="user-returns">-%</p>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->
            </div> <!-- Container ends -->
        </section>
        @endif

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="con-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all br">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero header-pad" id="portfolio-header-cont">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero">
                                    <div class="dropdown">
                                      <button class="btn btn-primary dropdown-toggle dropdown-btn" type="button" data-toggle="dropdown">Investment History
                                      <span><i class="material-icons">keyboard_arrow_down</i></span></button>
                                      <ul class="dropdown-menu">
                                        {{--<li><a href="#" id="user-kyc-details" target="_blank">KYC Details</a></li>--}}
                                        <li><a href="/admin/portfolio_details/{{$id}}" id="user-port-details" target="_blank">Portfolio Details</a></li>
                                        <!-- <li><a href="#">JavaScript</a></li> -->
                                      </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    {{--<a href="#" class="btn btn-edit"><i class="material-icons">file_download</i>Export</a>               --}}
                                    
                                </div>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                <div class="table-wrapper">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><p class = "table-header">Date Invested</p></th>
                                                <th><p class = "table-header">Amount Invested</p></th>
                                                <th><p class = "table-header">Transaction</p></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($investment_details as $investment_detail)
                                            <tr class="border-bottom">
                                                <td><p class="investment-date">{{$investment_detail['investment_date']}}</p></td>
                                                <td><p>{{$investment_detail['investment_amount']}}</p></td>

                                                @if($investment_detail['investment_status'] == 0)
                                                <td><p class="inv-status">Processing</p></td>
                                                @elseif($investment_detail['investment_status'] == 1)
                                                <td><p class="inv-status success">Success</p></td>
                                                @else
                                                 <td><p class="inv-status failed">Failed</p></td>
                                                @endif
                                               
                                                
                                            </tr>
                                            @endforeach
                                            <!-- <tr class="border-bottom">
                                                <td><p class="investment-date">01-08-2017</p></td>
                                                <td><p>5000</p></td>
                                                <td><p class="inv-status">Processing</p></td>
                                                
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><p class="investment-date">01-08-2017</p></td>
                                                <td><p>5000</p></td>
                                                <td><p class="success inv-status">Success</p></td>
                                            </tr>

                                            <tr class="border-bottom total">
                                                <td><p>TOTAL</p></td>
                                                <td><p>15000</p></td>
                                                <td></td>
                                            </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                           
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        @endsection

     <script src="{{url('js/jquery.min.js')}}"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


     <script type="text/javascript">
         $(document).ready(function(){
            $('.details-toggle').click(function(e){
                //alert($(this).attr('id'));
                e.preventDefault();
                $('#kyc-list').find('.active').removeClass('active');
                $(this).addClass('active');
                $('.details-tab').hide();
                var tab_name = $(this).attr('id');
                $('#'+tab_name+'-tab').show();
            });


            $('.search').focus(function(){
                $(this).parent().css({
                    'box-shadow': '0px 3px 5px 1px rgba(210, 210, 210, 0.3)',
                    'transition' : '0.3s'
                })
            });

            $('.search').blur(function(){
                $(this).parent().css({
                    'box-shadow': 'none',
                    'transition' : '0.3s'
                })
            });
         });
     </script>
    </body>
</html>
