<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css"> -->
        <link rel="stylesheet" href="{{url('/css/footer.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="{{url('/css/admin/investment-details.css')}}">
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Customer Details</p>
                        </div>

                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <div class="input-group">
                              <input type="text" class="form-control search" placeholder="Search Name / Pan / mobile ID" id="search-user" name="search-user" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="material-icons">search</i></span>
                            </div>
                        </div>

                        <div class = "col-lg-2 col-md-2 col-sm-2 p-r-zero">
                            <button class="btn btn-primary" id="add-user-btn"><i class="material-icons">person_add</i><span>Add User</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br">
                            <div class="col-lg-12 col-md-12 col-sm-12 p-lr-zero header-pad" id="inv-summary-header">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 p-lr-zero">
                                    <p class="section-header pl-30"><span id="searched-user-name">Vasudev Fatehpuria</span>|<span id="searched-user-email">vgupta@rightfunds.com</span>|<span id="searched-user-phone">9843051487</span></p>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12" id="inv-sum-content-div">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Current Value</p>
                                    <p class="inv-sum-amount">Rs.1,00,000</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Total Investment</p>
                                    <p class="inv-sum-amount">Rs.1,00,000</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Net Profit/Loss</p>
                                    <p class="inv-sum-amount">Rs.1,00,000</p>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 inv-details-container">
                                    <p class="cont-header">Returns</p>
                                    <p class="inv-sum-amount green">7%</p>
                                </div>
                            </div> <!-- #inv-sum-content-div ends -->
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="con-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all br">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero header-pad" id="portfolio-header-cont">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-lr-zero">
                                    <div class="dropdown">
                                      <button class="btn btn-primary dropdown-toggle dropdown-btn" type="button" data-toggle="dropdown">Withdrawal Details
                                      <span><i class="material-icons">keyboard_arrow_down</i></span></button>
                                      <ul class="dropdown-menu">
                                        <li><a href="#">HTML</a></li>
                                        <li><a href="#">CSS</a></li>
                                        <li><a href="#">JavaScript</a></li>
                                      </ul>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <a href="#" class="btn btn-edit"><i class="material-icons">file_download</i>Export</a>               
                                    
                                </div>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                <div class="table-wrapper">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><p class = "table-header">Date of Withdrawal</p></th>
                                                <th><p class = "table-header">Withdrawal Amount</p></th>
                                                <th><p class = "table-header">Transaction</p></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="border-bottom">
                                                <td><p class="investment-date">01-08-2017</p></td>
                                                <td><p>5000</p></td>
                                                <td><p class="inv-status">Processing</p></td>
                                                
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><p class="investment-date">01-08-2017</p></td>
                                                <td><p>5000</p></td>
                                                <td><p class="inv-status">Processing</p></td>
                                                
                                            </tr>
                                            <tr class="border-bottom">
                                                <td><p class="investment-date">01-08-2017</p></td>
                                                <td><p>5000</p></td>
                                                <td><p class="success inv-status">Success</p></td>
                                            </tr>

                                            <tr class="border-bottom total">
                                                <td><p>TOTAL</p></td>
                                                <td><p>15000</p></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                           
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        @endsection

     <script src="../js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


     <script type="text/javascript">
         $(document).ready(function(){
            $('.details-toggle').click(function(e){
                //alert($(this).attr('id'));
                e.preventDefault();
                $('#kyc-list').find('.active').removeClass('active');
                $(this).addClass('active');
                $('.details-tab').hide();
                var tab_name = $(this).attr('id');
                $('#'+tab_name+'-tab').show();
            });


            $('.search').focus(function(){
                $(this).parent().css({
                    'box-shadow': '0px 3px 5px 1px rgba(210, 210, 210, 0.3)',
                    'transition' : '0.3s'
                })
            });

            $('.search').blur(function(){
                $(this).parent().css({
                    'box-shadow': 'none',
                    'transition' : '0.3s'
                })
            });
         });
     </script>
    </body>
</html>
