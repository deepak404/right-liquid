<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name = "csrf-token" content = "{{ csrf_token() }}">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link rel="stylesheet" href="{{url('/css/footer.css')}}">
        <link rel="stylesheet" href="{{url('/css/modal.css')}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('css/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('css/admin/add-user.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">

      <style type="text/css">
          .material-icons{
            vertical-align: middle !important;
          }

          .material-icons:hover{
            background-color: transparent !important;
          }

          label.input-label{
            top: 25px;
          }

          #infoModal .modal-content{
              padding: 20px;
          }
      </style>
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <p class="main-header">Add User</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="investment-summary">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br bg-white">
                            <p class="tab-header"><span><i class="material-icons" id="company-m-icon">business</i></span><span>Company Details</span><span class="pull-right"><a href="#" data-toggle="collapse" data-target = "#company-details"><i class="material-icons">keyboard_arrow_down</i></a></span></p>

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero collapse in" id="company-details">
                                <form id="company-details-form" class="col-md-12 add-user-forms">

                                    {{csrf_field()}}
                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "name" id="name" class="input-field text-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Name</label>
                                                <span class="text-danger"></span>
                                            </div>

                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="text" name = "mobile" id="mobile" class="input-field num-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Contact Number</label>
                                                <span class="text-danger"></span>
                                            </div>

                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="email" name = "email" id="email" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Email</label>
                                                <span class="text-danger"></span>
                                            </div>

                                        </div>
                                    </div>

                                    <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero">
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <input type="password" name = "password" id="password" class="input-field" required>
                                                <span class="highlight"></span>
                                                <span class="bar"></span>
                                                <label class="input-label">Password</label>
                                                <span class="text-danger"></span>
                                            </div>

                                        </div>
                                    </div>

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="submit-company" value="Add User">
                                    </div>
                                </form>
                            </div>
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>



        <section id="bank-details-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br bg-white">
                            <p class="tab-header"><span><i class="material-icons" id="bank-m-icon">account_balance</i></span><span>Bank Details</span><span class="pull-right"><a href="#" data-toggle="collapse" data-target = "#bank-details"><i class="material-icons">keyboard_arrow_down</i></a></span></p>

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero collapse in" id="bank-details">

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero" id="added-banks">
                                
                            </div>
                                <form id="bank-details-form" class="bank-details-form col-md-12 col-lg-12 col-sm-12 add-user-forms">
                                    {{csrf_field()}}
                                    <input type="hidden" name="user_id" id="user_id">
                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "acc_name" id="acc-name" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Name</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "acc_no" id="acc-no" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Account Number</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "ifsc_code" id="ifsc-code" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">IFSC Code</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <select name="bank_name" id="bank-name">
                                                @foreach($bank_info as $bank)
                                                <option value="{{$bank->bank_id}}">{{$bank->bank_name}}</option>
                                                @endforeach    
                                            </select>
                                            
                                            <!-- <input type="text" name = "bank_name" id="bank_name" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">Bank Name</label> -->
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <input type="text" name = "pan" id="pan" class="input-field" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label class="input-label">PAN</label>
                                            <span class="text-danger"></span>
                                        </div>

                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <select name="acc_type" id="acc-type">
                                                <option value="1">Savings</option>
                                                <option value="2">Current</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <select name="bank_type" id="bank-type">
                                                <option value="1">Retail Banking</option>
                                                <option value="2">Corportate Banking</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="submit-bank" value="Save Bank">
                                    </div>
                                </form>
                                <span><a href="#" id="add-bank"><i class="material-icons">add_box</i>Add Bank</a></span>
                            </div>
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>


        <section id="document-details-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero box-shadow-all inv-summary br bg-white">
                            <p class="tab-header"><span><i class="material-icons" id="document-m-icon">insert_drive_file</i></span><span>Document Details</span><span class="pull-right"><a href="#" data-toggle="collapse" data-target = "#document-details"><i class="material-icons">keyboard_arrow_down</i></a></span></p>

                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero collapse in" id="document-details">
                                <form id="document-details-form" class="col-md-12 add-user-forms">
                                {{csrf_field()}}

                                    <div class="col-lg-4 col-md-4 col-sm-4 m-b-30">
                                        <div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">
                                            <div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">
                                                <p class="blue doc-name">Cancelled Cheque</p>
                                                <p class="file-name"></p>
                                                <span class="text-danger"></span>
                                            </div>
                                            <div class = "col-lg-3 col-md-3 col-sm-3">
                                                <input type="file" class="user-files" name="cc" id="cc">
                                                <label for="cc">
                                                    <span><i class="material-icons upload-icon">file_upload</i></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">AOA</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="aoa" id="aoa">--}}
                                                {{--<label for="aoa">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">MOA</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="moa" id="moa">--}}
                                                {{--<label for="moa">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">BS</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="bs" id="bs">--}}
                                                {{--<label for="bs">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">ASL</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="asl" id="asl">--}}
                                                {{--<label for="asl">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">BR</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="br" id="br">--}}
                                                {{--<label for="br">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">Address Proof</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="ap" id="ap">--}}
                                                {{--<label for="ap">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">COI</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="coi" id="coi">--}}
                                                {{--<label for="coi">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-lg-4 col-md-4 col-sm-4 m-b-30">
                                        <div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">
                                            <div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">
                                                <p class="blue doc-name">KYC Status</p>
                                                  <p class="file-name"></p>
                                                  <span class="text-danger"></span>
                                            </div>
                                            <div class = "col-lg-3 col-md-3 col-sm-3">
                                                <input type="file" class="user-files" name="kyc" id="kyc">
                                                <label for="kyc">
                                                    <span><i class="material-icons upload-icon">file_upload</i></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">PAN Card</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="pan_card" id="pan_card">--}}
                                                {{--<label for="pan_card">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    {{--<div class="col-lg-4 col-md-4 col-sm-4 m-b-30">--}}
                                        {{--<div class = "col-lg-12 col-md-12 col-sm-12 border-all p-r-zero file-container br">--}}
                                            {{--<div class = "col-lg-9 col-md-9 col-sm-9 p-lr-zero">--}}
                                                {{--<p class="blue doc-name">ID Proof</p>--}}
                                                  {{--<p class="file-name"></p>--}}
                                                  {{--<span class="text-danger"></span>--}}
                                            {{--</div>--}}
                                            {{--<div class = "col-lg-3 col-md-3 col-sm-3">--}}
                                                {{--<input type="file" class="user-files" name="id_proof" id="id_proof">--}}
                                                {{--<label for="id_proof">--}}
                                                    {{--<span><i class="material-icons upload-icon">file_upload</i></span>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class = "col-lg-12 col-md-12 col-sm-12">
                                        <input type="submit" name="" class="btn btn-primary grad-btn" id="submit-documents" value="Save Documents">
                                    </div>
                                </form>
                            </div>
                        </div> <!-- box-shadow-all ends -->
                    </div>
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        @endsection


        <div id="infoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-body">
                <div class="modal-res-header">
                    <img class="center-block response-image" src="/icons/success-tick.svg">
                </div>
                <div class="modal-res-body">
                    <p class="modal-res-text"></p>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default center-block success-btn" data-dismiss="modal">Done</button>
              </div>
            </div>

          </div>
        </div>

     <script src="/js/jquery.min.js"></script>
     <script src="/js/validation.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script type="text/javascript" src = "{{url('/js/admin/add-user.js')}}"></script>


     <script type="text/javascript">
         $(document).ready(function(){

            //$('#infoModal').modal('show');
            $('.details-toggle').click(function(e){
                //alert($(this).attr('id'));
                e.preventDefault();
                $('#kyc-list').find('.active').removeClass('active');
                $(this).addClass('active');
                $('.details-tab').hide();
                var tab_name = $(this).attr('id');
                $('#'+tab_name+'-tab').show();
            });


            $('.search').focus(function(){
                $(this).parent().css({
                    'box-shadow': '0px 3px 5px 1px rgba(210, 210, 210, 0.3)',
                    'transition' : '0.3s'
                })
            });

            $('.search').blur(function(){
                $(this).parent().css({
                    'box-shadow': 'none',
                    'transition' : '0.3s'
                })
            });
         });
     </script>
    </body>
</html>
