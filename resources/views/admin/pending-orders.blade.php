<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rightfunds | Home Page</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" href="../css/normalize.css">
        <link rel="stylesheet" href="../css/main.css"> -->
        <link rel="stylesheet" href="{{url('/css/admin/index.css')}}">
        <link rel="stylesheet" href="{{url('/css/footer.css')}})}}">
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{url('css/font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('css/admin/admin-font-and-global.css')}}">
        <link rel="stylesheet" href="{{url('css/admin/index.css')}}">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
        <link rel="stylesheet" href="{{url('css/admin/pending-orders.css')}}">
        <link rel="stylesheet" href="{{url('css/modal.css')}}">
        <style type="text/css">
          .red{
            color: #ff5252 !important;
          }

          .orange{
            color: orange !important;
          }

            .material-icons:hover{
                background-color: transparent !important;
            }
        </style>
    </head>
    <body>

        @extends('layouts.admin-navbar')
        @section('content')

        <section id="header-section">
            <div class="container">
                <div class="row">
                    <div class = "col-lg-12 col-md-12 col-sm-12">
                        <div class = "col-lg-5 col-md-5 col-sm-5">
                            <form class="form-inline" id="allotment-status-form">
                               <input type="file" name="allotment-status" id="allotment-status">
                              <label for="allotment-status">
                                <span class="btn btn-primary grad-btn pending-btn">Allotment Status <i class="material-icons">file_upload</i></span>
                              </label>      
                            </form>

                            <form class="form-inline" id="redemption-status-form">
                               <input type="file" name="redemption-status" id="redemption-status">
                              <label for="redemption-status">
                                <span class="btn btn-primary grad-btn pending-btn">Redemption Status <i class="material-icons">file_upload</i></span>
                              </label>      
                            </form>
                            <p class="main-header">Orders <span id="total-orders"><strong>Pending Orders: {{count($investment_details) + count($withdraw_details)}}</strong></span></p>
                        </div>
                        <div class = "col-lg-7 col-md-7 col-sm-7 p-r-zero"> 
                            <a href="/admin/export_pending_orders" target="_blank" class="btn btn-primary grad-btn pending-btn pull-right" id="export-pending">Export</a> 
                            <a href="#" class="btn btn-primary grad-btn pending-btn pull-right" id="update-remarks">Update Remarks</a> 
                        </div>

                        <!-- <div class = "col-lg-5 col-md-5 col-sm-5">
                            <div class="input-group">
                              <input type="text" class="form-control search" placeholder="Search Name / Pan / mobile ID" id="search-user" name="search-user" aria-describedby="basic-addon2">
                              <span class="input-group-addon" id="basic-addon2"><i class="material-icons">search</i></span>
                            </div>
                        </div>

                        <div class = "col-lg-2 col-md-2 col-sm-2 p-r-zero">
                            <button class="btn btn-primary grad-btn" id="add-user-btn"><i class="material-icons">person_add</i><span>Add User</span></button>
                        </div> -->
                    </div>
                </div>
            </div>
        </section>

        <?php //dd($investment_details); ?>

        <section id="portfolio-compensation">
            <div class="container">
                <div class="row">


                    @foreach($investment_details as $investment_detail)
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 po-container-holder">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero po-container">
                              <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero pending-orders-holder">
                                  <p class="po-details-holder">
                                      <a data-toggle="collapse" data-target="#{{$investment_detail['investment_id']}}"><i class="material-icons">keyboard_arrow_down</i></a>
                                      <span class="investor-name">{{$investment_detail['user_name']}} </span> |
                                      <span class="investor-pan">Pan : {{$investment_detail['user_pan']}} |</span>
                                      <span class="order-type">{{$investment_detail['investment_or_withdraw']}}</span> |
                                      <span class="payment-type">{{$investment_detail['payment_type']}}

                                          @if($investment_detail['payment_type'] == 'RTGS')
                                              <span> - {{$investment_detail['utr_no']}}</span>
                                              @else
                                          @endif

                                      </span>

                                  <span class="green bse-status pull-right"></span></p>

                                  <p class="order-details-holder">
                                      Order Date : <span> {{$investment_detail['placed_date']}}</span>
                                      Order Execution Date :  <span> {{$investment_detail['execution_date']}}</span>
                                      <span class="pull-right order-amount">Rs. {{$investment_detail['amount']}}</span>
                                  </p>
                              </div>

                              <div class = "col-lg-12 col-md-12 col-sm-12 collapse p-lr-zero scheme-det-collapse" id="{{$investment_detail['investment_id']}}">

                                  @foreach($pending_investment_collection as $pending_orders)
                                  @foreach($pending_orders as $pending_order)
                                
                                    @if($pending_order['investment_id'] == $investment_detail['investment_id'])
                                        <div class="scheme-det-holder">

                                           @if($pending_order['portfolio_status'] == '1')
                                           <p class="pending-scheme-name">
                                               <span class="scheme-name" style="margin-left: 36px;">{{$pending_order['scheme_name']}}</span>
                                           </p>
                                           @else
                                           <p class="pending-scheme-name">
                                               <span><input type="checkbox" id="{{$pending_order['id']}}" class="portfolio_scheme" name="" data-pid = "{{$pending_order['id']}}">
                                               <label for = "{{$pending_order['id']}}"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label></span>
                                               <span class="scheme-name">{{$pending_order['scheme_name']}}</span>
                                               <span><i class="material-icons delete-icon" data-pid="{{$pending_order['id']}}" data-ptype = "Investment">delete</i></span>
                                           </p>
                                           @endif
                                           
                                           <p class="os-details-holder">

                                            @if($pending_order['folio_number'] == '')
                                              <span><strong>Folio Number : </strong>N/A</span>
                                            @else
                                              <span><strong>Folio Number : </strong>{{$pending_order['folio_number']}}</span>
                                            @endif

                                            @if($pending_order['portfolio_status'] == '1')
                                            <span class="bse-date">
                                                  BSE : </strong>{{$pending_order['bse_order_date']}}
                                              </span>
                                            @else

                                                @if($pending_order['bse_order_date'] != '')
                                                <span class="bse-date">
                                                    <strong><input type="checkbox" name="" id="{{$pending_order['investment_id']}}-{{$pending_order['id']}}" data-pid = "{{$pending_order['id']}}" class="bse_date_selector" data-ptype="i" data-bstatus = "{{$pending_order['bse_order_status']}}" checked>
                                                    <label for="{{$pending_order['investment_id']}}-{{$pending_order['id']}}"><i class="green material-icons checkbox-icon">check_box_</i></label>  
                                                    BSE : </strong><span class="bse-order-date">
                                                      {{$pending_order['bse_order_date']}}
                                                    </span>
                                                </span>
                                                @else
                                                <span class="bse-date">
                                                    <strong><input type="checkbox" class="bse_date_selector" name="" id="{{$pending_order['investment_id']}}-{{$pending_order['id']}}" data-pid = "{{$pending_order['id']}}" class="bse_date_selector" data-ptype="i" data-bstatus = "{{$pending_order['bse_order_status']}}">
                                                    <label for="{{$pending_order['investment_id']}}-{{$pending_order['id']}}"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label>  
                                                    BSE : </strong>
                                                    <span class="bse-order-date">
                                                      {{$pending_order['bse_order_date']}}
                                                    </span>
                                                </span>
                                                @endif
                                            @endif
                                                
                                               

                                               @if($pending_order['bse_order_no'] == '0' || $pending_order['bse_order_no'] == null)
                                               <span><strong>BSE Order No: </strong> - </span>
                                               @else
                                               <span><strong>BSE Order No: </strong>{{$pending_order['bse_order_no']}}</span>
                                               @endif

                                               <span class="scheme-amount pull-right"><strong>Rs.{{$pending_order->amount_invested}}</strong></span>
                                           </p>
                                           <p class="remarks" data-orderstatus = "{{$pending_order['bse_order_status']}}">
                                               <span><strong>Remarks : </strong>{{$pending_order['bse_remarks']}}</span>
                                           </p>
                                       </div>

                                    @endif
                                  @endforeach
                                  @endforeach
                                 
                                 <button type="button" class="btn btn-primary grad-btn center-block confirm-order-btn" data-type = "inv">Order</button>
                              </div>
                          </div> <!-- box-shadow-all ends -->
                      </div> <!-- po-container-holder ends -->
                    @endforeach


<!-- Pending Withdrawal Orders begin -->
                    
                    @foreach($withdraw_details as $withdraw_detail)
                    <?php //dd($withdraw_details); ?>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 po-container-holder">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero po-container">
                              <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero pending-orders-holder">
                                  <p class="po-details-holder"><a data-toggle="collapse" data-target="#{{$withdraw_detail['withdraw_group_id']}}"><i class="material-icons">keyboard_arrow_down</i></a><span class="investor-name">{{$withdraw_detail['user_name']}}</span> | <span class="investor-pan">Pan : {{$withdraw_detail['user_pan']}} |</span><span class="order-type">{{$withdraw_detail['investment_or_withdraw']}}</span>

                                  <span class="green bse-status pull-right"></span></p>

                                  <p class="order-details-holder">
                                      Order Date : <span> {{$withdraw_detail['placed_date']}}</span>
                                      Order Execution Date :  <span> {{$withdraw_detail['execution_date']}}</span>
                                      <span class="pull-right order-amount">Rs. {{$withdraw_detail['amount']}}</span>
                                  </p>
                              </div>

                              <div class = "col-lg-12 col-md-12 col-sm-12 collapse p-lr-zero scheme-det-collapse" id="{{$withdraw_detail['withdraw_group_id']}}">

                                  @foreach($pending_withdraw_collection as $pending_orders)
                                  @foreach($pending_orders as $pending_order)

                                          <?php //dd($pending_order); ?>


                                      @if($pending_order['withdraw_group_id'] == $withdraw_detail['withdraw_group_id'])
                                        <div class="scheme-det-holder">

                                           @if($pending_order['portfolio_status'] == '1')
                                           <p class="pending-scheme-name">
                                               <span class="scheme-name" style="margin-left: 36px;">{{$pending_order['scheme_name']}}</span>
                                           </p>
                                           @else
                                           <p class="pending-scheme-name">
                                               <span><input type="checkbox" id="{{$pending_order['id']}}" class="portfolio_scheme" name="" data-pid = "{{$pending_order['id']}}">
                                               <label for = "{{$pending_order['id']}}"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label></span>
                                               <span class="scheme-name">{{$pending_order['scheme_name']}}</span>
                                               <span><i class="material-icons delete-icon" data-pid="{{$pending_order['id']}}" data-ptype = "Withdrawal ">delete</i></span>
                                           </p>
                                           @endif
                                           
                                           <p class="os-details-holder">

                                            @if($pending_order['folio_number'] == '')
                                              <span><strong>Folio Number : </strong>N/A</span>
                                            @else
                                              <span><strong>Folio Number : </strong>{{$pending_order['folio_number']}}</span>
                                            @endif

                                            @if($pending_order['portfolio_status'] == '1')
                                            <span class="bse-date">
                                                  BSE : </strong>{{$pending_order['bse_order_date']}}
                                              </span>
                                            @else

                                                @if($pending_order['bse_order_date'] != '')
                                                <span class="bse-date">
                                                    <strong><input type="checkbox" name="" id="{{$pending_order['withdraw_group_id']}}-{{$pending_order['id']}}" data-pid = "{{$pending_order['id']}}" class="bse_date_selector" data-ptype="i" data-bstatus = "{{$pending_order['bse_order_status']}}" checked>
                                                    <label for="{{$pending_order['withdraw_group_id']}}-{{$pending_order['id']}}"><i class="green material-icons checkbox-icon">check_box_</i></label>  
                                                    BSE : </strong><span class="bse-order-date">
                                                      {{$pending_order['bse_order_date']}}
                                                    </span>
                                                </span>
                                                @else
                                                <span class="bse-date">
                                                    <strong><input type="checkbox" class="bse_date_selector" name="" id="{{$pending_order['withdraw_group_id']}}-{{$pending_order['id']}}" data-pid = "{{$pending_order['id']}}" class="bse_date_selector" data-ptype="i" data-bstatus = "{{$pending_order['bse_order_status']}}">
                                                    <label for="{{$pending_order['withdraw_group_id']}}-{{$pending_order['id']}}"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label>  
                                                    BSE : </strong>
                                                    <span class="bse-order-date">
                                                      {{$pending_order['bse_order_date']}}
                                                    </span>
                                                </span>
                                                @endif
                                            @endif
                                                
                                               

                                               @if($pending_order['bse_order_no'] == '0' || $pending_order['bse_order_no'] == null)
                                               <span><strong>BSE Order No: </strong> - </span>
                                               @else
                                               <span><strong>BSE Order No: </strong>{{$pending_order['bse_order_no']}}</span>
                                               @endif

                                               <span class="scheme-amount pull-right"><strong>Rs.{{$pending_order->withdraw_amount}}</strong></span>
                                           </p>
                                           <p class="remarks" data-orderstatus = "{{$pending_order['bse_order_status']}}">
                                               <span><strong>Remarks : </strong>{{$pending_order['bse_remarks']}}</span>
                                           </p>
                                       </div>

                                    @endif
                                  @endforeach
                                  @endforeach
                                 
                                 <button type="button" class="btn btn-primary grad-btn center-block confirm-order-btn" data-type = "wd">Withdraw</button>
                              </div>
                          </div> <!-- box-shadow-all ends -->
                      </div> <!-- po-container-holder ends -->
                    @endforeach




<!--                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 po-container-holder">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero po-container">
                            <div class = "col-lg-12 col-md-12 col-sm-12 p-lr-zero pending-orders-holder">
                                <p class="po-details-holder"><a href="#" data-toggle="collapse" data-target="#2"><i class="material-icons">keyboard_arrow_down</i></a><span class="investor-name">Vasudev fatehpuria </span> | <span class="investor-pan">Pan : BFRPN4910B |</span><span class="order-type">Investment</span><span class="green bse-status pull-right">BSE Pass</span></p>

                                <p class="order-details-holder">
                                    Order Date : <span> 10/11/2017</span>
                                    Order Execution Date :  <span> 10/11/2017</span>
                                    <span class="pull-right order-amount">Rs. 10,00,00,000</span>
                                </p>
                            </div>

                            <div class = "col-lg-12 col-md-12 col-sm-12 collapse p-lr-zero scheme-det-collapse" id="2">
                               <div class="scheme-det-holder">
                                   <p class="pending-scheme-name">
                                       <span><input type="checkbox" id="p-1" name="">
                                       <label for="p-1"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label></span>
                                       <span class="scheme-name">DSP BlackRock Tax Saver Fund</span>
                                       <span><i class="material-icons">delete</i></span>
                                   </p>
                                   <p class="os-details-holder">
                                       <span><strong>Folio Number : </strong>12564879/22</span>
                                       <span class="bse-date">
                                            <strong><input type="checkbox" name="" id="bse1">
                                            <label for="bse1"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label>  
                                            BSE : </strong>2017-11-12</span>
                                       <span><strong>BSE Order No: </strong>12564897</span>
                                       <span class="scheme-amount pull-right"><strong>Rs.1000000</strong></span>
                                   </p>
                                   <p class="remarks">
                                       <span><strong>Remarks : </strong>ORD CONF: Your Request for FRESH PURCHASE 5000.000 in SCHEME: K01-GR THRO : PHYSICAL is confirmed for CLIENT : Vasudev Fatehpuria (Code: ABEPF5833F) CONFIRMATION TIME: Nov 8 2017 9:27PM ENTRY BY: ORDER NO: 21683899 OFFLINE ORDER WILL BE TRIGGERED DUR</span>
                                   </p>
                               </div>


                               <div class="scheme-det-holder">
                                   <p class="pending-scheme-name">
                                       <span><input type="checkbox" id="p-1" name="">
                                       <label for="p-1"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label></span>
                                       <span class="scheme-name">DSP BlackRock Tax Saver Fund</span>
                                       <span><i class="material-icons">delete</i></span>
                                   </p>
                                   <p class="os-details-holder">
                                       <span><strong>Folio Number : </strong>12564879/22</span>
                                       <span class="bse-date">
                                            <strong><input type="checkbox" name="" id="bse1">
                                            <label for="bse1"><i class="material-icons checkbox-icon">check_box_outline_blank</i></label>  
                                            BSE : </strong>2017-11-12</span>
                                       <span><strong>BSE Order No: </strong>12564897</span>
                                       <span class="scheme-amount pull-right"><strong>Rs.1000000</strong></span>
                                   </p>
                                   <p class="remarks">
                                       <span><strong>Remarks : </strong>ORD CONF: Your Request for FRESH PURCHASE 5000.000 in SCHEME: K01-GR THRO : PHYSICAL is confirmed for CLIENT : Vasudev Fatehpuria (Code: ABEPF5833F) CONFIRMATION TIME: Nov 8 2017 9:27PM ENTRY BY: ORDER NO: 21683899 OFFLINE ORDER WILL BE TRIGGERED DUR</span>
                                   </p>
                               </div>


                               <button type="button" class="btn btn-primary grad-btn center-block confirm-order-btn">Order</button>
                            </div>





                           
                        </div>
                    </div> -->
                </div>  <!-- row ends -->        
            </div> <!-- Container ends -->
        </section>

        @endsection


        <div id="infoModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-body">
                  <div class="modal-res-body">
                      <p class="modal-res-text"></p>
                  </div>

                <div class="modal-res-header">
                    <img class="center-block response-image" src="/icons/success-tick.svg">
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" id="done-btn" class="btn btn-default center-block success-btn" data-dismiss="modal">Done</button>
              </div>
            </div>

          </div>
        </div>

        <div id="confirmAllotModal" class="modal fade" role="dialog">
          <div class="modal-dialog  schedule-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              
              <div class="modal-body schedule-inv-body">

                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="confirm-upload-container">
                    <p id="confirm-allot">Allotment Status</p>

                    <p class="text-center confirm-info">Are you Sure want to upload the allotment Status ?</p>
                    <button class="btn btn-primary grad-btn confirm-upload-btn" value="yes" id="confirm-allot-upload">Yes</button>
                    <button class="btn btn-primary grad-btn confirm-upload-btn" value="no" id="cancel-allot-upload">No</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div id="confirmRedemptionModal" class="modal fade" role="dialog">
          <div class="modal-dialog  schedule-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              
              <div class="modal-body schedule-inv-body">

                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="confirm-upload-container">
                    <p id="confirm-allot">Redemption Status</p>

                    <p class="text-center confirm-info">Are you Sure want to upload the redemption Status ?</p>
                    <button class="btn btn-primary grad-btn confirm-upload-btn" value="yes" id="confirm-redemption-upload">Yes</button>
                    <button class="btn btn-primary grad-btn confirm-upload-btn" value="no" id="cancel-redemption-upload">No</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

     <script src="../js/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     <script type="text/javascript" src="{{url('js/admin/pending-orders.js')}}"></script>
    </body>
</html>
