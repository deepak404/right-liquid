<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"  xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <title>Website Enquiry</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style>
        html,
        body,
        table,
        tbody,
        tr,
        td,
        div,
        p,
        ul,
        ol,
        li,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            margin: 0;
            padding: 0;
        }

        body {
            margin: 0;
            padding: 0;
            font-size: 0;
            line-height: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        table {
            border-spacing: 0;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        table td {
            border-collapse: collapse;
        }

        .ExternalClass {
            width: 100%;
        }

        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
            line-height: 100%;
        }
        /* Outermost container in Outlook.com */

        .ReadMsgBody {
            width: 100%;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            /*font-family: Arial;*/
        }

        h1 {
            font-size: 28px;
            font-weight:normal;
            line-height: 32px;
            padding-top: 10px;
            padding-bottom: 24px;
        }


        h2 {
            font-size: 24px;
            line-height: 28px;
            padding-top: 10px;
            padding-bottom: 20px;
        }

        h3 {
            font-size: 20px;
            line-height: 24px;
            padding-top: 10px;
            padding-bottom: 16px;
        }

        h4 {
            font-size: 18px;
            line-height: 21px;
            padding-top: 10px;
            padding-bottom: 12px;
        }

        h5 {
            font-size: 16px;
            line-height: 21px;
            padding-top: 10px;
            padding-bottom: 8px;
        }

        h6 {
            font-size: 14px;
            line-height: 21px;
            padding-top: 10px;
            padding-bottom: 8px;
        }

        p {
            font-size: 16px;
            line-height: 20px;
            /*font-family: Georgia, Arial, sans-serif;*/
        }

        @media all and (max-width: 599px) {
            .container600 {
                width: 100%;
            }
        }
    </style>
</head>
<body style="background-color:#F7F7F7;">


<table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;padding-top:40px; padding-bottom:40px;">
    <tr>
        <td width="100%" style="min-width:100%;background-color:#F7F7F7;padding:10px;">
            <center>
                <table class="container600" cellpadding="0" cellspacing="0" width="600" style="margin:0 auto;">
                    <tr>
                        <td width="100%" style="text-align:left;">
                            <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                                <tr>
                                    <td width="100%" style="min-width:100%;background-color:#FFFFFF;color:#000000;padding:30px;">
                                        <img alt="" src="https://preview.ibb.co/ffz9X8/Asset_1_4x.png" width="210" style="display: block;" />
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                                <tr>
                                    <td width="100%" style="min-width:100%;background-color:white;color:#333;padding:30px;">


                                        <p>Hello Vasudev,</p>
                                        <br>
                                        <p>{{$user_data['message']}}</p>

                                        <p style= "margin-top:20px">Name : {{$user_data['sender-name']}}</p>
                                        <p>E-mail : {{$user_data['email']}}</p>
                                        <p>Mobile : {{$user_data['mobile']}}</p>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellpadding="0" cellspacing="0" style="min-width:100%;">
                                <tr>
                                    <td width="100%" style="min-width:100%;background-color:#f3c34c;color:#FFFFFF;padding:30px;">
                                        <p style="font-size:12px;line-height:20px;text-align:center;">2018 Copyrights - Prosperity Wealth Management.</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
</body>
</html>