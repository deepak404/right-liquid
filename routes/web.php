<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $user = Auth::user();
    if (!$user) {
        return view('index');
    } else {
        if($user->role == 1) {
            return redirect('/admin/dashboard');
        } else {
            return redirect('/home');
        }
    }
});


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/login', function () {
    if (\Auth::user()) {
        return redirect('/home');
    }else{
        return view('login');
    }
});

Route::get('/contact-us', function () {
    return view('users.contact-us');

});

Route::get('/terms', function () {
        return view('users.terms');
});

Route::get('/arn', function () {
    return view('users.arn');
});

Route::get('/privacy-policy', function () {
        return view('users.privacy-policy');
});



Route::post('/send_enquiry', 'UserController@sendEnquiry')->name('sendEnquiry');



Route::post('/send_contact_enquiry', 'UserController@sendContactEnquiry')->name('sendContactEnquiry');




Auth::routes();


Route::middleware(['auth','user'])->group(function () {

    Route::get('/home', 'UserController@index')->name('homePage');

//    Route::get('/rtgs', 'UserController@getRtgsTransactions')->name('rtgs');

    Route::get('/portfolio_details', 'UserController@portfolioDetails')->name('PortfolioDetails');

    Route::get('/investment_history', 'UserController@investmentHistory')->name('InvestmentHistory');

    Route::get('/fund_selector', 'UserController@fundSelector')->name('FundSelector');

    Route::get('/profile', 'UserController@showProfile')->name('Profile');

    Route::post('/show_scheme_details', 'SchemeController@showSchemeDetails')->name('showSchemeDetails');

    Route::post('/get_scheme_performance_details','SchemeController@getSchemePerformanceDetails')->name('getSchemePerformanceDetails');

    Route::get('/get_bank_accounts', 'UserController@getBankAccounts')->name('getBankAccounts');

    Route::get('/profile', 'UserController@showProfile')->name('showProfile');

    Route::post('/invest_funds', 'FundController@investFunds')->name('investFunds');

    Route::get('/make_payment/{id}', 'FundController@makePayment')->name('makePayment');

    Route::post('/manage_payment', 'FundController@managePayment')->name('managePayment');

    Route::get('/payment_response/{id}','FundController@paymentResponse')->name('paymentResponse');

    Route::get('/handle_payment', 'FundController@handlePayment')->name('handlePayment') ;

    Route::get('/get_mandate_link', 'UserController@getMandateLink')->name('getMandateLink');

    Route::post('/get_investment_details', 'UserController@getInvestmentDetails')->name('getInvestmentDetails');

    Route::post('/change_password','userController@changePassword')->name('changePassword');

    Route::get('/get_max_inv_amount', 'UserController@getMaximumInvestmentAmount')->name('getMaximumInvestmentAmount');

    Route::get('/withdraw_funds', 'UserController@showWithdraw')->name('showWithdraw');

    Route::get('/order_status', 'UserController@orderStatus')->name('orderStatus');

    Route::post('/update_personal_details', 'UserController@updatePersonalDetails')->name('updatePersonalDetails');

    Route::post('/withdraw_user_funds', 'WithdrawController@withdrawFunds')->name('withdrawFunds');

    Route::post('/get_investment_history','UserController@getInvestmentHistory')->name('getInvestmentHistory');

    Route::post('/get_user_portfolio_document','UserController@getUserPortfolioDocument')->name('getUserPortfolioDocument');

    Route::post('/get_withdraw_details', 'UserController@getWithdrawDetails')->name('getWithdrawDetails');


    Route::post('/update_utr_no', 'UserController@updateUtrNo')->name('updateUtrNo');

    Route::get('/transaction_history', 'UserController@transactionHistory')->name('transactionHistory');

});



Route::middleware(['auth','admin'])->prefix('admin')->group(function () {

    Route::post('/get_user_portfolio_document','UserController@getUserPortfolioDocument')->name('getUserPortfolioDocument');


    Route::get('/client_detail', 'AdminController@clientDetail')->name('ClientDetails');

    Route::get('/add_user', 'AdminController@addUser')->name('AddUser');

    Route::post('/add_new_user', 'AdminController@addNewUser')->name('AddNewUser');

    Route::post('/add_user_bank', 'AdminController@addUserBank')->name('AddUserBank');

    Route::post('/update_user_bank', 'AdminController@updateUserBank')->name('UpdateUserBank');

    Route::post('/delete_bank', 'AdminController@deleteBank')->name('deleteBank');

    Route::post('/add_user_documents', 'AdminController@addUserDocuments')->name('addUserDocuments');

    Route::post('/edit_user_portfolio', 'AdminController@editUserPortfolio')->name('editUserPortfolio');

    Route::get('/manage_schemes', 'AdminController@manageSchemes')->name('manageSchemes');

    Route::post('/add_scheme', 'AdminController@addScheme')->name('addScheme');

    Route::get('/customer_support/{id?}', 'AdminController@customerSupport')->name('customerSupport');

    Route::get('/find','AdminController@userSearch')->name('userSearch');

    Route::get('/investment_history/{id}','AdminController@getUserInvestmentHistory')->name('getUserInvestmentHistory');

    Route::get('/dashboard','AdminController@showDashboard')->name('showDashboard');

    Route::get('/portfolio_details/{id}','AdminController@getUserPortfolioDetails')->name('getUserPortfolioDetails');

    Route::get('/pending_orders','AdminController@pendingOrders')->name('pendingOrders');

    Route::get('/order_history','AdminController@orderHistory')->name('orderHistory');

    Route::post('/complete_pending_orders','AdminController@completePendingOrders')->name('completePendingOrders');

    Route::post('/show_user_details', 'AdminController@showUserDetails')->name('showUserDetails');

    Route::post('/update_bse_date', 'AdminController@updateBseDate')->name('updateBseDate');

    Route::post('/modify_portfolio', 'AdminController@modifyPortfolio')->name('modifyPortfolio');

    Route::get('/export_pending_orders', 'AdminController@exportPendingOrders')->name('exportpendingOrders');

    Route::post('/update_allotment_status', 'AdminController@updateAllotmentStatus')->name('updateAllotmentStatus');

    Route::post('/update_redemption_status', 'AdminController@updateRedemptionStatus')->name('updateRedemptionStatus');

    Route::post('/add_manual_portfolio', 'AdminController@addManualPortfolio')->name('addManualPortfolio');

    Route::post('/add_manual_withdraw', 'AdminController@addManualWithdraw')->name('addManualWithdraw');

    Route::post('/cancel_pending_order','AdminController@cancelPendingOrder');

    Route::post('/update_user_details', 'AdminController@updateUserDetails')->name('updateUserDetails');
            
    Route::get('/manage_schemes', 'AdminController@manageSchemes')->name('ManageSchemes');

    Route::get('/get_nav', 'NavController@getNav')->name('getNav');

    Route::get('/delete_all_schemes', 'NavController@deleteSchemes')->name('deleteSchemes');

    Route::post('/get_scheme','AdminController@getSchemeInfo');

    Route::post('/update_scheme','AdminController@updateScheme');

    Route::post('/delete_bank','AdminController@deleteBank');

    Route::post('/update_pass','AdminController@updatePass');

    Route::get('/update_remark','AdminController@checkPayment');

});


        