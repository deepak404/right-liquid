var graph_scheme_code;
var graph_duration;

function create_scheme_graph() {
        //var scheme_code = schemeCode;
        var a = 2;
        google.charts.load('current', {
            packages: ['corechart', 'line']
        });
        google.charts.setOnLoadCallback(drawCurveTypes)
}

    function drawCurveTypes() {
        var data = new google.visualization.DataTable();
        data.addColumn('date', 'X');
        data.addColumn('number', 'NAV ');
        var chart_data = [];
        var formdata = 'duration=' + graph_duration + '&scheme_code='+graph_scheme_code;
        //console.log(formdata);
        $.ajax({
            type: "POST",
            url: "/get_scheme_performance_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            cache: !1,
            processData: !1,
            async: !1,
            data: formdata,
            success: function(response) {
                if (response.msg == 'success') {
                    var id = 1;
                    $.each(response.response, function(key, value) {
                        chart_data.push([new Date(value.year, value.month, value.date), value.amount])
                    })
                }
            },
            error: function(xhr, status, error) {},
        });
        data.addRows(chart_data);
        var options = {
            hAxis: {
                title: 'vertical',
                textPosition: 'none',
            },
            vAxis: {
                title: 'horizontal'
            },
            series: {
                1: {
                    curveType: 'function'
                }
            },
            hAxis: {
                title: 'Duration',
                baselineColor: "transparent",
                gridlines: {
                    color: "transparent"
                },
                //ticks: []
            },
            vAxis: {
                title: 'NAV',
                baselineColor: '#d7d7d7'
            },

            height : 400,
            legend: {
                position: "none"
            },
            colors: ["#0091EA"]
        };
        var chart = new google.visualization.LineChart(document.getElementById(''+graph_scheme_code+'-graph-container'));
        chart.draw(data, options)            
    }


    $(document).on('click','.graph-duration',function(){

        $(this).parent().parent().find('p.active-duration').removeClass('active-duration');
        $(this).addClass('active-duration');
        //console.log($(this).data('scode'));

        switch($(this).text()){

            case '1 Week':
                //console.log('3');
                graph_scheme_code = $(this).data('scode');
                graph_duration = "7days";
                create_scheme_graph();
                break;

            case '1 Month':
                //console.log('6');
                graph_scheme_code = $(this).data('scode');
                graph_duration = 1;
                create_scheme_graph();
                break;


            case '6 Month':
                //console.log('12');
                graph_scheme_code = $(this).data('scode');
                graph_duration = 6;
                create_scheme_graph();
                break;

            case '1 Year':
                //console.log('12');
                graph_scheme_code = $(this).data('scode');
                graph_duration = 12;
                create_scheme_graph();
                break;

            case '3 Years':
                //console.log('12');
                graph_scheme_code = $(this).data('scode');
                graph_duration = 36;
                create_scheme_graph();
                break;
        }

    });
