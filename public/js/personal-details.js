$(document).ready(function(){
	$('#change-password-form').on('submit',function(e){
		e.preventDefault();

		if ($('#new-password').val() != $('#confirm-password').val()) {
			$('#no-match').text('Password Doesn\'t match').show();
		}else{

			data = $(this).serialize();
			$.ajax({
		      type: 'POST',
		      url: '/change_password',
		      headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
		      data: data,
		      success:function(data){
		      	if (data.msg == "success") {
		      		$('.modal-res-text').text('Password has been Successfully Changed');
		      		$('.response-image').attr('src','/icons/success-tick.svg');
		      		$('.modal-res-header').removeClass('failure-body').addClass('success-body');
		      		$('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
		      		//console.log();
		      		$('.delete-bank[data-bankid='+data.response.bank_id+']').closest('.added-bank-container').remove();
                    $('#change-password-form')[0].reset();
		      	}else{
		      		$('.modal-res-text').text('Cannot Change the password right now.');
		      		$('.response-image').attr('src','/icons/failed-tick.svg');
		      		$('.modal-res-header').removeClass('success-body').addClass('failure-body');
		      		$('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');

		      	}
		      },
		      error:function(){ 
		          
		      }
		  });

			$('#infoModal').modal('show');
		}
	});

	$('#personal-details-form').on('submit',function(e){
		e.preventDefault();

			data = $(this).serialize();
			$.ajax({
		      type: 'POST',
		      url: '/update_personal_details',
		      headers: {
			      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
		      data: data,
		      success:function(data){
		      	if (data.msg == "success") {
		      		$('.modal-res-text').text('Password has been Successfully Changed');
		      		$('.response-image').attr('src','/icons/success-tick.svg');
		      		$('.modal-res-header').removeClass('failure-body').addClass('success-body');
		      		$('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
		      		//console.log();
		      		//$('.delete-bank[data-bankid='+data.response.bank_id+']').closest('.added-bank-container').remove();
		      		$('#infoModal').modal('show');

		      	}else{
		      		$('.modal-res-text').text('Cannot Change the password right now.');
		      		$('.response-image').attr('src','/icons/failed-tick.svg');
		      		$('.modal-res-header').removeClass('success-body').addClass('failure-body');
		      		$('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');
		      		$('#infoModal').modal('show');

		      	}
		      	$('#infoModal').modal('show');
		      },
		      error:function(xhr,text,error){ 
		          $errors = xhr.responseJSON.errors;
		          $.each($errors,function(key,value){
		          		// console.log(key,value[0]);
		          		$('#'+key).parent().find('span.text-danger').text(value[0]);
		          });
		      }
		  });

			//$('#infoModal').modal('show');
	});

	$('.input-field').keyup(function(){
		$(this).parent().find('span.text-danger').text(' ');
	})
});