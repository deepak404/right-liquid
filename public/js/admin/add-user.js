$(document).ready(function(){

	var created_user_id;

	$('#pan').keyup(function(){
		$(this).val($(this).val().toUpperCase());
	})

	$('#mobile').on('keydown',function(e){
        console.log(e.which);
        if ($(this).val().length == 15) {

            if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                return true;
            }
            return false;
        }
    });


    $('#acc-no').on('keydown',function(e){
	        if ($(this).val().length == 18) {

	            if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
	                return true;
	            }
	            return false;
	        }
	});

    $('#ifsc-code').on('keydown',function(e){

            // $(this).val($(this).val().toUpperCase());


            if (e.shiftKey) {
                e.preventDefault();
            }
            //console.log($(this).val().length);
        
            if ($(this).val().length == 11) {
                //console.log(e.which);
                if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                    return true;
                }else{
                    return false;
                }                            
            }

            if ((e.keyCode > 47 && e.keyCode <58) || (e.keyCode > 95 && e.keyCode <106) || (e.keyCode > 64 && e.keyCode <91) || e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                    //console.log('here');
                    return true;
            }else{
                return false;
            }
    });

   function checkBankForms(){
   	if ($('.bank-details-form').length == 0) {
    	$('#add-bank').show();
    }else{
    	$('#add-bank').hide();
    }
   }

    $('#add-bank').on('click',function(e){
    	e.preventDefault();
    	var token = $('meta[name="csrf-token"]').attr('content');
    	$('<form id="bank-details-form" class="bank-details-form col-md-12 col-lg-12 col-sm-12 add-user-forms">'+
                                    '<input type="hidden" name="_token" value="'+token+'">'+
                                    '<input type="hidden" name="user_id" id="user_id">'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<input type="text" name = "acc_name" id="acc-name" class="input-field" required>'+
                                            '<span class="highlight"></span>'+
                                            '<span class="bar"></span>'+
                                            '<label class="input-label">Name</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<input type="text" name = "acc_no" id="acc-no" class="input-field" required>'+
                                            '<span class="highlight"></span>'+
                                            '<span class="bar"></span>'+
                                            '<label class="input-label">Account Number</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<input type="text" name = "ifsc_code" id="ifsc-code" class="input-field" required>'+
                                            '<span class="highlight"></span>'+
                                            '<span class="bar"></span>'+
                                            '<label class="input-label">IFSC Code</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<select name="bank_name" id="bank-name">'+
	                                           '<option value="HDF">HDFC BANK</option>'+
	                                           '<option value="UTI">Axis Bank</option>'+
	                                           '<option value="ICI">ICICI Bank - Retail Net Banking</option>'+
	                                           '<option value="IDB">IDBI Bank - Retail Net Banking</option>'+
	                                           '<option value="162">Kotak Bank</option>'+
	                                           '<option value="SBI">State Bank of India</option>'+
	                                           '<option value="ALB">Allahabad Bank - Retail Net Banking</option>'+
	                                           '<option value="ADB">Andhra Bank</option>'+
	                                           '<option value="BBK">Bank of Bahrain and Kuwait</option>'+
	                                           '<option value="BBC">Bank of Baroda - Corporate Banking</option>'+
	                                           '<option value="BBR">Bank of Baroda - Retail Net Banking</option>'+
	                                           '<option value="BOM">Bank of Maharashtra</option>'+
	                                           '<option value="BMN">Bharatiya Mahila Bank</option>'+
	                                           '<option value="CNB">Canara Bank</option>'+
	                                           '<option value="CSB">Catholic Syrian Bank</option>'+
	                                           '<option value="CBI">Central Bank of India</option>'+
	                                           '<option value="CRP">Corporation Bank</option>'+
	                                           '<option value="COB">Cosmos Bank</option>'+
	                                           '<option value="DEN">Dena Bank</option>'+
	                                           '<option value="DBK">Deutsche Bank</option>'+
	                                           '<option value="DC2">Development Credit Bank - Corporate</option>'+
	                                           '<option value="DCB">Development Credit Bank - Retail</option>'+
	                                           '<option value="DLB">Dhanlakshmi Bank</option>'+
	                                           '<option value="FBK">Federal Bank</option>'+
	                                           '<option value="INB">Indian Bank</option>'+
	                                           '<option value="IOB">Indian Overseas Bank</option>'+
	                                           '<option value="IDS">IndusInd Bank</option>'+
	                                           '<option value="ING">ING Vysya Bank - Retail Net Banking</option>'+
	                                           '<option value="JKB">Jammu &amp; Kashmir Bank</option>'+
	                                           '<option value="JSB">Janata Sahakari Bank</option>'+
	                                           '<option value="KBL">Karnataka Bank Ltd</option>'+
	                                           '<option value="KVB">Karur Vysya Bank</option>'+
	                                           '<option value="LVC">Laxmi Vilas Bank - Corporate Net Banking</option>'+
	                                           '<option value="LVR">Laxmi Vilas Bank - Retail Net Banking</option>'+
	                                           '<option value="NKB">NKGSB BANK</option>'+
	                                           '<option value="OBC">Oriental Bank of Commerce</option>'+
	                                           '<option value="PMC">Punjab &amp; Maharastra Coop Bank</option>'+
	                                           '<option value="PSB">Punjab &amp; Sind Bank</option>'+
	                                           '<option value="CPN">Punjab National Bank - Corporate Banking</option>'+
	                                           '<option value="PNB">Punjab National Bank - Retail Net Banking</option>'+
	                                           '<option value="RTN">Ratnakar Bank - Retail Net Banking</option>'+
	                                           '<option value="RBS">RBS (The Royal Bank of Scotland)</option>'+
	                                           '<option value="SWB">Saraswat Bank</option>'+
	                                           '<option value="SVC">Shamrao Vitthal Co-operative Bank - Retail Net Banking</option>'+
	                                           '<option value="SIB">South Indian Bank</option>'+
	                                           '<option value="SCB">Standard Chartered Bank</option>'+
	                                           '<option value="SYD">Syndicate Bank</option>'+
	                                           '<option value="TMB">Tamilnad Mercantile Bank Ltd.</option>'+
	                                           '<option value="TNC">Tamilnadu State Coop Bank</option>'+
	                                           '<option value="TJB">TJSB Bank</option>'+
	                                           '<option value="UCO">UCO Bank</option>'+
	                                           '<option value="UBI">Union Bank of India</option>'+
	                                           '<option value="UNI">United Bank of India</option>'+
	                                           '<option value="VJB">Vijaya Bank</option>'+
	                                           '<option value="YBK">Yes Bank Ltd</option>'+
	                                           '<option value="SBM">STATE BANK OF MYSORE</option>'+
	                                           '<option value="CIU">CITY UNION BANK LIMITED</option>'+
	                                           '<option value="RBL">Ratnakar Bank - Retail Net Banking</option>'+
	                                           '<option value="ORT">Oriental bank of commerce</option>'+
	                                           '<option value="HSB">HSBC</option>'+
	                                           '<option value="CIT">CITI Bank</option>'+
                                            '</select>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<input type="text" name = "pan" id="pan" class="input-field" required>'+
                                            '<span class="highlight"></span>'+
                                            '<span class="bar"></span>'+
                                            '<label class="input-label">PAN</label>'+
                                            '<span class="text-danger"></span>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<select name="acc_type" id="acc-type">'+
                                                '<option value="1">Savings</option>'+
                                                '<option value="2">Current</option>'+
                                            '</select>'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<select name="bank_type" id="bank-type">'+
                                                '<option value="1">Retail Banking</option>'+
                                                '<option value="2">Corporate Banking</option>'+
                                            '</select>'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class = "col-lg-12 col-md-12 col-sm-12">'+
                                        '<input type="submit" name="" class="btn btn-primary grad-btn" id="submit-company" value="Save Bank">'+
                                    '</div>'+
                                '</form>').insertAfter('#added-banks');

    	checkBankForms();
    });


    $('.input-field').keyup(function(){
    	$(this).parent().find('.text-danger').text(' ');
    })









	/*General event handling and other function ends here*/




	$('#company-details-form').on('submit',function(e){

		e.preventDefault();

		var data = $(this).serialize();
		console.log(data);
			$.ajax({
		      type: 'POST',
		      url: '/admin/add_new_user',
		      data: data,
		      headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		      success:function(data){
		      	if (data.msg == "success") {
		      		$('.modal-res-text').text('User has been Successfully Added');
		      		$('.response-image').attr('src','/icons/success-tick.svg');
		      		$('.modal-res-header').toggleClass('success-body');
		      		$('#company-m-icon').text('check_circle').addClass('green');

		      		created_user_id = data.user_id;
		      		console.log(created_user_id);
		      	}else{
		      		$('.modal-res-text').text('User cannot be added right now');
		      		$('.response-image').attr('src','/icons/failure-tick.svg');
		      		$('.modal-res-header').toggleClass('failure-body');
		      	}

		      	$('#infoModal').modal('show');
		      },
		      error:function(x,a,e){ 
		          handleErrors(x.responseJSON);

		      }
		  });
	});


	$(document).on('submit','.bank-details-form',function(e){

		e.preventDefault();
		$(this).find('#user_id').val(created_user_id);
		data = $(this).serialize()
		// var data = 'acc_name='+$('#acc-name').val()+'&acc_no='+$('#acc-no').val()+'&ifsc_code='+$('#ifsc-code').val()+'&'
		console.log(data);
		// break;
			$.ajax({
		      type: 'POST',
		      url: '/admin/add_user_bank',
		      data: data,
		      headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		      success:function(data){
		      	if (data.msg == "success") {
		      		var acc_type;
		      		var bank_type;
		      		$('.modal-res-text').text('Bank Details has been Successfully Added.');
		      		$('.response-image').attr('src','/icons/success-tick.svg');
		      		$('.modal-res-header').removeClass('failure-body').addClass('success-body');
		      		$('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
		      		$('#bank-m-icon').text('check_circle').addClass('green');

		      		if (data.response.acc_type == 1) {
		      			acc_type = "Savings";
		      		}else{
		      			acc_type = 'Current';
		      		}

                    if (data.response.bank_type == 1) {
                        bank_type = "Retail Banking";
                    }else{
                        bank_type = 'Corporate Banking';
                    }

		      		$('#added-banks').append(
		      					'<div class=" added-bank-container border-bottom col-lg-12 col-md-12 col-sm-12 p-l-r-zero"> '+
                                    '<p class="bank-info"><span>Bank '+data.response.acc_count+'</span><span class="pull-right"><i class="delete-bank material-icons" data-bankid = "'+data.response.bank_id+'">delete</i></span></p>'+
                                    '<div class = "col-lg-3 col-md-3 col-sm-3">'+
                                        '<p class="bank-info-head">Name</p>'+
                                        '<p class="bank-info-details">'+data.response.acc_name+'</p>'+
                                    '</div>'+
                                    '<div class = "col-lg-3 col-md-3 col-sm-3">'+
                                        '<p class="bank-info-head">Account Number</p>'+
                                        '<p class="bank-info-details">'+data.response.acc_no+'</p>'+
                                    '</div>'+
                                    '<div class = "col-lg-3 col-md-3 col-sm-3">'+
                                        '<p class="bank-info-head">IFSC Code</p>'+
                                        '<p class="bank-info-details">'+data.response.ifsc_code+'</p>'+
                                    '</div>'+
                                    '<div class = "col-lg-3 col-md-3 col-sm-3">'+
                                        '<p class="bank-info-head">Account Type</p>'+
                                        '<p class="bank-info-details">'+acc_type+'</p>'+
                                    '</div>'+
                                    '<div class = "col-lg-3 col-md-3 col-sm-3">'+
                                        '<p class="bank-info-head">Bank Type</p>'+
                                        '<p class="bank-info-details">'+bank_type+'</p>'+
                                    '</div>'+
                                '</div>');

		      		$('.bank-details-form').remove();
		      		checkBankForms();

		      	}else{
		      		$('.modal-res-text').text('Cannot Update Bank Details Rightnow.');
		      		$('.response-image').attr('src','/icons/failed-tick.svg');
		      		$('.modal-res-header').removeClass('success-body').addClass('failure-body');
		      		$('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');
		      	}

		      	$('#infoModal').modal('show');
		      },
		      error:function(x,a,e){ 
		          handleErrors(x.responseJSON);

		      }
		  });
	});


	$(document).on('click','.delete-bank',function(){
			var bank_id = $(this).data('bankid');

			var data = 'bank_id='+bank_id;
			$.ajax({
		      type: 'POST',
		      url: '/admin/delete_bank',
		      data: data,
		      headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		      success:function(data){
		      	if (data.msg == "success") {
		      		$('.modal-res-text').text('Bank detail has been Successfully deleted');
		      		$('.response-image').attr('src','/icons/success-tick.svg');
		      		$('.modal-res-header').removeClass('failure-body').addClass('success-body');
		      		$('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
		      		//console.log();
		      		$('.delete-bank[data-bankid='+data.response.bank_id+']').closest('.added-bank-container').remove();

		      	}else{
		      		$('.modal-res-text').text('Cannot Delete the bank Details right now.');
		      		$('.response-image').attr('src','/icons/failure-tick.svg');
		      		$('.modal-res-header').removeClass('success-body').addClass('failure-body');
		      		$('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');

		      	}

		      	$('#infoModal').modal('show');
		      },
		      error:function(x,a,e){ 
		          handleErrors(x.responseJSON);

		      }
		  });

			checkBankForms();

			if($('.added-bank-container').length == 1){
				$('#bank-m-icon').text('account_balance').removeClass('green');
			}
			

	});

	$('.user-files').on('change',function(e){
		$(this).parent().prev().find('.file-name').text(e.target.files[0].name);
	});


	$(document).on('submit','#document-details-form',function(e){
		e.preventDefault();
		var formData = new FormData();
		var count = 0;
	    $('.user-files').each(function(){
	    	if($(this).get(0).files.length !== 0){
	    		
			    console.log($(this).attr('name'));

			    formData.append($(this).attr('name'),$(this).get(0).files[0]);
			    
	    	}else{
	    		$(this).parent().prev().find('span.text-danger').text('File Missing.').css({'display':'inline-block'});
	    		count++;
	    	}

	    	console.log(formData);
	    });
	    //created_user_id = 7;

	    formData.append('user_id',created_user_id);

	    if (count == 0) {
	    	$.ajax({
			      type: 'POST',
			      url: '/admin/add_user_documents',
			      headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				  },
			      data: formData,
			      contentType: false,
			      processData: false,
			      success:function(data){
			      	console.log(data.msg);
			        if(data.msg == "success"){
		      		$('.modal-res-text').text('User Documents have been Successfully uploaded');
		      		$('.response-image').attr('src','/icons/success-tick.svg');
		      		$('.modal-res-header').removeClass('failure-body').addClass('success-body');
		      		$('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
		      		//console.log();

			      	}else{
			      		$('.modal-res-text').text('Cannot update User Documents right now.');
			      		$('.response-image').attr('src','/icons/failure-tick.svg');
			      		$('.modal-res-header').removeClass('success-body').addClass('failure-body');
			      		$('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');

			      	}
			      	$('#document-m-icon').text('check_circle').addClass('green');
			      	$('#infoModal').modal('show');

			      },
			      error:function(x,a,e){ 
			          handleErrors(x.responseJSON);

			      }
			  });
	    }

	    

	})


	function handleAlerts(){

	}

	function handleErrors(error_response){
		console.log(error_response.message);
		
		$.each(error_response.errors,function(k,v){
			console.log(k+'-'+v);

			$('input[name='+k+']').parent().find('span.text-danger').text(v).css({'display':'inline-block'});
		});
	}
});