$(document).ready(function(){
        $('input[type="checkbox"]').on('change',function(){
                if ($(this).prop('checked')) {
                    $(this).next().find('i').text('check_box').addClass('green');
                    //$count++;
                    ////console.log("Checked".$count);
                }else{
                    $(this).next().find('i').text('check_box_outline_blank').removeClass('green');
                }
        });


        // $('.scheme-det-holder').each(function(){
        //   //console.log($(this).find('.bse_date_selector:checkbox:checked').length);

        //   var success_bse_length = $(this).find('.bse_date_selector[data-bstatus=0]:checkbox:checked').length;
        //   var failed_bse_length = $(this).find('.bse_date_selector[data-bstatus=1]:checkbox:checked').length;
        //   var available_bse_checkbox = $(this).find('.bse_date_selector:checkbox').length;
            

        //   console.log(success_bse_length,failed_bse_length,available_bse_checkbox);

        //   if (success_bse_length == available_bse_checkbox) {
        //         $(this).parent().prev().find('.po-details-holder').find('.bse-status').text('BSE Pass').addClass('green');
        //   }else if (failed_bse_length != 0 && success_bse_length != 0) {
        //         $(this).parent().prev().find('.po-details-holder').find('.bse-status').text('BSE Partial pass').addClass('green');
        //   }
        //   else{
        //     $(this).parent().prev().find('.po-details-holder').find('.bse-status').text('BSE Fail').addClass('red');
        //   }
        // })


        $('.scheme-det-collapse').each(function(){
            var remarks = $(this).find('.scheme-det-holder').find('.remarks').length;
            var success = $(this).find('.scheme-det-holder').find('.remarks[data-orderstatus = 0]').length;
            var failure =  $(this).find('.scheme-det-holder').find('.remarks[data-orderstatus = 1]').length;
            
            if(remarks == success){
              $(this).prev().find('.po-details-holder').find('.bse-status').text('BSE Pass').addClass('green');
            }else if(failure !=0 && success != 0){
              $(this).prev().find('.po-details-holder').find('.bse-status').text('BSE Partial pass').addClass('orange');
            }else{
              $(this).prev().find('.po-details-holder').find('.bse-status').text('BSE Fail').addClass('red');
            }
        });



        $('.bse_date_selector').on('change',function(){
          if ($(this).prop('checked') == true) {
            var p_id,p_type;
            p_id = $(this).data('pid');
            p_type = $(this).data('ptype');
            //console.log($(this).data('pid'));
            console.log(p_type,p_id);
            updateBseDate(p_id,p_type,'checked')
          }
         if ($(this).prop('checked') == false) {
            var p_id,p_type;
            //console.log($(this).data('pid'));
            var p_id = $(this).data('pid');
            p_type = $(this).data('ptype');
            console.log(p_type,p_id);
            updateBseDate(p_id,p_type,'not_checked')
          }
        })


        function updateBseDate(portfolio_id,pending_type,status,e){

            var dataString = 'portfolio_id=' + portfolio_id + '&status=' + status + '&pending_type=' + pending_type;
            console.log(dataString);
            $.ajax({
                type: "POST",
                url: "/admin/update_bse_date",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                beforeSend: function() {},
                success: function(data) {
                    console.log(data.msg);

                    if (data.msg == 'date_success') {


                        console.log(data.portfolio_id);
                        $('.bse_date_selector[data-pid="'+data.portfolio_id+'"]').prop('checked',true);
                        $('.bse_date_selector[data-pid="'+data.portfolio_id+'"]').parent().next().text(data.date);

                    }

                    if (data.msg == 'nodate_success') {
                        console.log(data.portfolio_id);
                        $('.bse_date_selector[data-pid="'+data.portfolio_id+'"]').prop('checked',false);
                        console.log(data.portfolio_id);
                        $('.bse_date_selector[data-pid="'+data.portfolio_id+'"]').parent().next().text('');
                    }

                },
                error: function(xhr, status, error) {},
            })
        }

        $(document).on('click','.confirm-order-btn',function(){
          var investment_container = $(this).closest('.scheme-det-collapse').find('.portfolio_scheme:checkbox:checked');

          var portfolio_ids = [];
          $.each(investment_container,function(){
              portfolio_ids.push($(this).data('pid'));
          })

          console.log(portfolio_ids);
          console.log($(this).text());
          var order_type = $(this).data('type');
          
          completePendingOrders(portfolio_ids,order_type);
          
        })




        function completePendingOrders(ids,order_type){

          console.log(order_type);
          var dataString = 'order_ids=' +ids+ '&order_type=' +order_type;
          console.log(dataString);

          $.ajax({
                type: "POST",
                url: "/admin/complete_pending_orders",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache: !1,
                processData: !1,
                data: dataString,
                beforeSend: function() {},
                success: function(data) {
                if (data.msg == "success") {
                  $('.modal-res-text').text('Orders have been successfully approved.');
                  $('.response-image').attr('src','/icons/success-tick.svg');
                  $('.modal-res-header').toggleClass('success-body');
                  $('#company-m-icon').text('check_circle').addClass('green');
                  $('#done-btn').attr('onclick', "javascript:location.href='/admin/pending_orders'");

                  //created_user_id = data.user_id;
                  //console.log(created_user_id);
                }else{
                  $('.modal-res-text').text('Orders cannot be approved right now. Kindly try again later.');
                  $('.response-image').attr('src','/icons/failure-tick.svg');
                  $('.modal-res-header').toggleClass('failure-body');
                }

            $('#infoModal').modal('show');
                },
                error: function(xhr, status, error) {
                    $('#notify-done').attr('onclick', "javascript:$('#scheduleInvModal').modal('hide')");
                    $('#great').html('Oops !');
                    $('#you-have').html('Something went wrong. Please try again');
                    $('#scheduleInvModal').modal('show')
                },
            })

        }


        $(document).on('change','#allotment-status',function(){
                 

                  if ($(this).val() == '') {
                    alert('No file Found');
                  }else{
                      var file = $(this).get(0).files[0];
                      var file_type = file['type'];

                      var ext = $(this).val().split('.').pop();
                      if (ext == 'txt') {
                          $('#confirmAllotModal').modal('show');

                      }else{
                        alert("Wrong File Format");
                      }
                  }
            });


            $(document).on('change','#redemption-status',function(){

                  if ($(this).val() == '') {
                    alert('No file Found');
                  }else{
                      var file = $(this).get(0).files[0];
                      var file_type = file['type'];

                      var ext = $(this).val().split('.').pop();
                      if (ext == 'txt') {
                          $('#confirmRedemptionModal').modal('show');

                      }else{
                        alert("Wrong File Format");
                      }
                  }
            });

            $('.delete-icon').on('click',function(e){
                e.preventDefault();
              var formData = 'id='+$(this).data('pid')+'&transaction_type='+$(this).data('ptype');
              $.ajax({
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'/admin/cancel_pending_order',
                cache: !1,
                processData: !1,
                data:formData,
                success:function(data){
                  console.log(data);
                  location.reload();
                },
                error:function(error){
                  console.log(error);
                }
              });
            });


            $(document).on('click','#confirm-allot-upload',function(){
                  $('#allotment-status-form').submit();
           });

           $(document).on('click','#confirm-redemption-upload',function(){
                  $('#redemption-status-form').submit();
           });

           $(document).on('submit','#allotment-status-form',function(e){
              e.preventDefault();

              var file = $('#allotment-status').get(0).files[0];

              var formData = new FormData();
              formData.append('allotment-file',file);


              $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });

                    $.ajax({
                        type: 'POST',
                        url: '/admin/update_allotment_status',
                        data: formData,
                        async : false,
                        contentType: false,
                        processData: false,
                        success:function(data){
                          if (data.msg == "success") {
                            $('#you-have').text(data.response);
                            $('#confirmAllotModal').modal('hide');
                            $('#scheduleInvModal').modal('show');
                          }else{
                            $('#you-have').text(data.response);
                            $('#confirmAllotModal').modal('hide');
                            $('#scheduleInvModal').modal('show');
                          }
                        },
                        error:function(){ 
                            
                        }
                    });

           });


          $(document).on('submit','#redemption-status-form',function(e){
              e.preventDefault();

              var file = $('#redemption-status').get(0).files[0];

              var formData = new FormData();
              formData.append('redemption-file',file);




                    $.ajax({
                        type: 'POST',
                        url: '/admin/update_redemption_status',
                        data: formData,
                        async : false,
                        contentType: false,
                        processData: false,
                        success:function(data){

                            $('#infoModal .modal-res-text').text(data.response);
                            $('#confirmRedemptionModal').modal('hide');
                            $('#infoModal').modal('show');

                        },
                        error:function(){ 
                            
                        }
                    });

           });


          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });



          $('#update-remarks').on('click',function(){

            $.ajax({
                type: 'GET',
                url: '/admin/update_remark',
                success:function(data){
                  if (data.msg == "success") {
                    $('#you-have').text(data.response);
                    $('#confirmRedemptionModal').modal('hide');
                    $('#scheduleInvModal').modal('show');
                    location.reload();
                  }
                },
                error:function(){ 
                    
                }
            });
          });


})