$(document).ready(function(){
	$('#add-scheme-btn').on('click',function(){
		$('#addSchemeModal').modal('show');
	});


	$(document).on('submit','#add_scheme_form',function(e){
	    e.preventDefault();
	    var scheme_status;
	    $('#add_scheme_submit').css({'display':'none'});

	    var scheme_name = $('#add_scheme_name').val();
	    var scheme_code = $('#add_scheme_code').val();
	    var bse_scheme_code = $('#add_bse_scheme_code').val();
	    var scheme_file = $('#add_scheme_nav').get(0).files[0];
	    var scheme_type = $('#add_scheme_type').val();
	    var scheme_priority = $('#add_scheme_priority').val();
	    var scheme_fund_manager = $('#add_fund_manager').val();
	    var scheme_launch_date = $('#add_launch_date').val();
	    var scheme_asset_size = $('#add_asset_size').val();
	    var scheme_benchmark = $('#add_benchmark').val();
	    var fund_type = $('#add_fund_type').val();
	    var exit_load = $('#add_exit_load').val();

	    var formData = new FormData();

	    formData.append('scheme_name',scheme_name);
	    formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
	    formData.append('scheme_code',scheme_code);
	    formData.append('bse_scheme_code',bse_scheme_code);
	    formData.append('scheme_file',scheme_file);
	    formData.append('scheme_type',scheme_type);
	    formData.append('fund_type',fund_type);
	    formData.append('scheme_priority', scheme_priority);
	    formData.append('fund_manager', scheme_fund_manager);
	    formData.append('launch_date', scheme_launch_date);
	    formData.append('asset_size', scheme_asset_size );
	    formData.append('benchmark', scheme_benchmark);
	    formData.append('exit_load',exit_load);
	    console.log(formData);

	    $.ajax({
	      type: "POST",
	      url: "/admin/add_scheme",
	      headers: {
	          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	      },

	      async : false,
	      data: formData,
	      cache: false,
	      processData: false,
	      contentType: false,
	      success: function(data) {
	        console.log(data);
	        $('#add_scheme_status').text('');
	          //addInvestment(data);
	          //$('#addNewSchemeModal').modal('hide');
	          if (data.msg == "1") {
	            $('#add_scheme_form')[0].reset();
	            if (data.scheme_data.scheme_status == 1) {
	                $scheme_status = "Active";
	            }else{
	                $scheme_status = "In-active";
	            }
	            var scheme_data = '<tr class="border-bottom">'+
	                '<td><p class="scheme-name">'+data.scheme_data.scheme_name+'</p></td>'+
	                '<td><p>'+data.scheme_data.scheme_code+'</p></td>'+
	                '<td><p>GROWTH</p></td>'+
	                '<td><p>'+data.scheme_data.priority+'</p></td>'+
	                '<td><p class="active">'+scheme_status+'</p></td>'+
	                '<td><p><i class="material-icons">edit</i></p></td>'+
            	'</tr>';


            	$('#scheme-list-body').append(scheme_data);

	            //var scheme_data ='<tr><td>'+data.scheme_data.scheme_name+'</td><td>'+data.scheme_data.scheme_code+'</td><td>'+data.scheme_data.fund_type+'</td><td>'+data.scheme_data.scheme_priority+'</td><td>'+data.scheme_data.bse_scheme_code+'</td><td><i data-code="'+data.scheme_data.scheme_code+'" class="remove-scheme material-icons delete_scheme_icon">delete</i></td><td><i data-code="'+data.scheme_data.scheme_code+'" class="edit-scheme material-icons edit_scheme_icon">mode_edit</i></td></tr>';
	            // var scheme_body = $('#scheme_details').html();console.log(scheme_body);
	            // $('#scheme_details').html(scheme_body+scheme_data);
	            $('#addSchemeModal').modal('hide');
	            
	          } else {
	            $('#add_scheme_status').css("color", "red");
	            var add_status = '';
	            $.each(data.msg, function(key, value) {
	            	add_status += value;
	            });
	            $('#add_scheme_status').text(add_status);
	          }
	      },
	      error: function(xhr, status, error) {
	          $('#add_scheme_status').css("color", "red");
	          $('#add_scheme_status').text('New Scheme creation failed');
	      },
	    });
	  });

	$('.edit-td').on('click',function(){
		var data = 'scheme_code='+$(this).data('schemecode');
		$.ajax({
			type:'POST',
			url:'/admin/get_scheme',
			data: data,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success:function(data){
				if (data.msg == 1) {
					$('#edit_scheme_type option').removeAttr("selected");
					$('#edit_fund_type option').removeAttr("selected");
					$('#edit_scheme_status option').removeAttr("selected");
					var val = data.info;
					console.log(val);
					$('#scheme_code_change').val(val.scheme_code);
					$('#edit_scheme_name').val(val.scheme_name);
					$('#edit_fund_manager').val(val.fund_manager);
					$('#edit_scheme_code').val(val.scheme_code);
					$('#edit_asset_size').val(val.asset_size);
					$('#edit_scheme_type option[value="'+val.scheme_type+'"]').attr("selected", "selected");
					$('#edit_fund_type option[value="'+val.fund_type+'"]').attr("selected", "selected");
					$('#edit_scheme_status option[value="'+val.scheme_status+'"]').attr("selected", "selected");
					$('#edit_benchmark').val(val.benchmark);
					$('#edit_scheme_priority').val(val.scheme_priority);
					$('#edit_exit_load').val(val.exit_load);
					$('#edit_launch_date').val(val.launch_date);
					$('#edit_bse_scheme_code').val(val.bse_scheme_code);
					$('#editSchemeModal').modal('show');
				}
			},
			error:function(error){
				console.log(error);
			}
		})
	});

	$('#edit_scheme_form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#edit_scheme_form').serialize();
		
		$.ajax({
			type:'POST',
			url:'/admin/update_scheme',
			data:formData,
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success:function(data){
				if (data.msg == 1) {
					location.reload();
				}
			},
			error:function(error){
				console.log();
			}
		})
	});


});