$(document).ready(function() {
    var imgType = '';
    var file = '';
    var userId = '';
    $(document).on('click', ".suggested-user", function() {
        console.log('clicked');
        $user_id = $(this).data('id');
        $('#submit-company').css('display','none');
        $('#edit-company').css('display','block');
        $('#company-details-form').find('#user_id').val($user_id);
        $('#user_id_pass').val($user_id);
        getUserDetails($user_id);
        
    });

    $('#c_password').on('keyup',function(){
        if ($('#c_password').val() != '') {
            if ($('#password').val() != $('#c_password').val()) {
                $('.worng-pass').text("Password doesn't match");
                $('#update-btn').prop('disabled','true');
            }else{       
                $('.worng-pass').text('');
                $('#update-btn').removeAttr('disabled');
            }
        }else{
                $('.worng-pass').text('');
        }
    });

    function getUserDetails(user_id){
        $user_id = user_id;
        var dataString = 'user_id=' + $user_id;
        $.ajax({
            type: "POST",
            url: "/admin/show_user_details",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: dataString,
            cache: !1,
            processData: !1,
            beforeSend: function() {},
            success: function(data) {
                $('#kyc_details').css({
                    display: 'block'
                });
                appendUserDetails(data,$user_id);
            },
            error: function(xhr, status, error) {},
        });
    }


        function appendUserDetails(data,user_id){
            if (data.msg == "success") {

                $('#user-inv-history').attr('href','investment_history/'+user_id);
                $('#user-port-details').attr('href','portfolio_details/'+user_id);
                console.log(data);
                $.each(data.user_details,function(k,v){
                    $('#company-details-tab').find('#'+k).val(v);
                });

                $('#bank-details-tab').empty();


                if (data.bank_details.length == 0) {
                   $('#bank-details-tab').append(
                        '<a class ="btn btn-primary grad-btn pull-right" data-userid='+user_id+' id="add-bank">Add Bank</a>'
                    );
                }else{

                    $('#bank-details-tab').append(
                        '<a class ="btn btn-primary grad-btn pull-right" data-userid='+user_id+' id="add-bank">Add Bank</a>'
                    );
                    console.log(data.bank_details);
                    $.each(data.bank_details,function(k,v){
                        appendBank(v);
                    });
                }

                
                if (data.document_details == null) {
                    $('.file-name').text('Not Uploaded Yet');
                    $('#submit-document').val('Save');
                }else{
                    $.each(data.document_details,function(k,v){

                        var file_name = $('.file-link[data-id="'+k+'"]').data('id');
                        $('.file-link[data-id="'+k+'"]').attr('href','../docs/'+file_name+'/'+v);
                        $('.file-link[data-id="'+k+'"]').text(v);
                    });
                }
                

                $('#searched-user-name').text(data.user_details.name);
                $('#searched-user-email').text(data.user_details.email);
                $('#searched-user-phone').text(data.user_details.mobile);


                $('#user-current-value').text("Rs. "+data.portfolio.current_value.toFixed(2));
                $('#user-total-inv').text("Rs. "+data.portfolio.amount_invested.toFixed(2));
                $('#user-net-pl').text("Rs. "+data.portfolio.net_returns);
                var xirr = data.portfolio.total_xirr;
                xirr = parseFloat(xirr).toFixed(2);
                console.log(parseFloat(xirr).toFixed(2));
                if (xirr == 'NaN') {
                  $('#user-returns').text("0%");
                }else{
                  $('#user-returns').text(xirr+"%");
                }


                $('#company-details-form input, .bank-details-form input, .bank-details-form select').attr('disabled', true);

            }
        }



        $('#edit-company').on('click', function(){
            $(this).closest('form').find('input').attr('disabled',false);
        });


        $('.checkbox-div input').on('change', function() {
            var parentDiv = $(this).parents(".checkbox-div:eq(0)");
            if ($(this).is(":checked")) {
                $(parentDiv).find("img").attr("src", "../icons/check_en.png")
            } else {
                $(parentDiv).find("img").attr("src", "../icons/check_dis.png")
            }
        })
    
    
    $(document).on('click','.file-link',function(){
        console.log($(this).data('id')+'/'+$(this).text());
    });


    $(document).on('click','#add-bank',function(e){
        e.preventDefault();
        var token = $('meta[name="csrf-token"]').attr('content');
        $('#bank-details-tab').append('<form id="bank-details-form" class="bank-details-form col-md-12 col-lg-12 col-sm-12 add-user-forms">'+
                                    '<input type="hidden" name="_token" value="'+token+'">'+
                                    '<input type="hidden" name="user_id" id="user_id">'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<input type="text" name = "acc_name" id="acc-name" class="input-field" required>'+
                                            '<span class="highlight"></span>'+
                                            '<span class="bar"></span>'+
                                            '<label class="input-label">Name</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<input type="text" name = "acc_no" id="acc-no" class="input-field" required>'+
                                            '<span class="highlight"></span>'+
                                            '<span class="bar"></span>'+
                                            '<label class="input-label">Account Number</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<input type="text" name = "ifsc_code" id="ifsc-code" class="input-field" required>'+
                                            '<span class="highlight"></span>'+
                                            '<span class="bar"></span>'+
                                            '<label class="input-label">IFSC Code</label>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<select name="bank_name" id="bank-name">'+
                                               '<option value="HDF">HDFC BANK</option>'+
                                               '<option value="UTI">Axis Bank</option>'+
                                               '<option value="ICI">ICICI Bank - Retail Net Banking</option>'+
                                               '<option value="IDB">IDBI Bank - Retail Net Banking</option>'+
                                               '<option value="162">Kotak Bank</option>'+
                                               '<option value="SBI">State Bank of India</option>'+
                                               '<option value="ALB">Allahabad Bank - Retail Net Banking</option>'+
                                               '<option value="ADB">Andhra Bank</option>'+
                                               '<option value="BBK">Bank of Bahrain and Kuwait</option>'+
                                               '<option value="BBC">Bank of Baroda - Corporate Banking</option>'+
                                               '<option value="BBR">Bank of Baroda - Retail Net Banking</option>'+
                                               '<option value="BOM">Bank of Maharashtra</option>'+
                                               '<option value="BMN">Bharatiya Mahila Bank</option>'+
                                               '<option value="CNB">Canara Bank</option>'+
                                               '<option value="CSB">Catholic Syrian Bank</option>'+
                                               '<option value="CBI">Central Bank of India</option>'+
                                               '<option value="CRP">Corporation Bank</option>'+
                                               '<option value="COB">Cosmos Bank</option>'+
                                               '<option value="DEN">Dena Bank</option>'+
                                               '<option value="DBK">Deutsche Bank</option>'+
                                               '<option value="DC2">Development Credit Bank - Corporate</option>'+
                                               '<option value="DCB">Development Credit Bank - Retail</option>'+
                                               '<option value="DLB">Dhanlakshmi Bank</option>'+
                                               '<option value="FBK">Federal Bank</option>'+
                                               '<option value="INB">Indian Bank</option>'+
                                               '<option value="IOB">Indian Overseas Bank</option>'+
                                               '<option value="IDS">IndusInd Bank</option>'+
                                               '<option value="ING">ING Vysya Bank - Retail Net Banking</option>'+
                                               '<option value="JKB">Jammu &amp; Kashmir Bank</option>'+
                                               '<option value="JSB">Janata Sahakari Bank</option>'+
                                               '<option value="KBL">Karnataka Bank Ltd</option>'+
                                               '<option value="KVB">Karur Vysya Bank</option>'+
                                               '<option value="LVC">Laxmi Vilas Bank - Corporate Net Banking</option>'+
                                               '<option value="LVR">Laxmi Vilas Bank - Retail Net Banking</option>'+
                                               '<option value="NKB">NKGSB BANK</option>'+
                                               '<option value="OBC">Oriental Bank of Commerce</option>'+
                                               '<option value="PMC">Punjab &amp; Maharastra Coop Bank</option>'+
                                               '<option value="PSB">Punjab &amp; Sind Bank</option>'+
                                               '<option value="CPN">Punjab National Bank - Corporate Banking</option>'+
                                               '<option value="PNB">Punjab National Bank - Retail Net Banking</option>'+
                                               '<option value="RTN">Ratnakar Bank - Retail Net Banking</option>'+
                                               '<option value="RBS">RBS (The Royal Bank of Scotland)</option>'+
                                               '<option value="SWB">Saraswat Bank</option>'+
                                               '<option value="SVC">Shamrao Vitthal Co-operative Bank - Retail Net Banking</option>'+
                                               '<option value="SIB">South Indian Bank</option>'+
                                               '<option value="SCB">Standard Chartered Bank</option>'+
                                               '<option value="SYD">Syndicate Bank</option>'+
                                               '<option value="TMB">Tamilnad Mercantile Bank Ltd.</option>'+
                                               '<option value="TNC">Tamilnadu State Coop Bank</option>'+
                                               '<option value="TJB">TJSB Bank</option>'+
                                               '<option value="UCO">UCO Bank</option>'+
                                               '<option value="UBI">Union Bank of India</option>'+
                                               '<option value="UNI">United Bank of India</option>'+
                                               '<option value="VJB">Vijaya Bank</option>'+
                                               '<option value="YBK">Yes Bank Ltd</option>'+
                                               '<option value="SBM">STATE BANK OF MYSORE</option>'+
                                               '<option value="CIU">CITY UNION BANK LIMITED</option>'+
                                               '<option value="RBL">Ratnakar Bank - Retail Net Banking</option>'+
                                               '<option value="ORT">Oriental bank of commerce</option>'+
                                               '<option value="HSB">HSBC</option>'+
                                               '<option value="CIT">CITI Bank</option>'+
                                            '</select>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<input type="text" name = "pan" id="pan" class="input-field" required>'+
                                            '<span class="highlight"></span>'+
                                            '<span class="bar"></span>'+
                                            '<label class="input-label">PAN</label>'+
                                            '<span class="text-danger"></span>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-4 col-sm-4">'+
                                        '<div class="form-group">'+
                                            '<select name="acc_type" id="acc-type">'+
                                                '<option value="1">Savings</option>'+
                                                '<option value="2">Current</option>'+
                                            '</select>'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class = "col-lg-12 col-md-12 col-sm-12">'+
                                        '<button type="submit" name="" class="btn btn-primary grad-btn" style="display:block !important;" id="submit-company" value="Save Bank">'+
                                    '</div>'+
                                '</form>');
    });


    $(document).on('submit','.bank-details-form',function(e){

        e.preventDefault();
        // break;

        var bank_id = $(this).attr('data-bankdata');
        var user_id = $('#add-bank').data('userid');
        $(this).find('#user_id').val(user_id);
        var data = $(this).serialize();
        var url;
        var op_type = $(this).find('.op_type').val();
        console.log(op_type);
        if (op_type == "update") {
          url = '/admin/update_user_bank';
        }else{
          url = '/admin/add_user_bank';
        }
        // break;
        console.log(data);
        console.log(url);
            $.ajax({
              type: 'POST',
              url: url,
              data: data,
              headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
              success:function(data){
                if (data.msg == "success") {
                    var acc_type;
                    $('.response-image').attr('src','/icons/success-tick.svg');
                    $('.modal-res-header').removeClass('failure-body').addClass('success-body');
                    $('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
                    $('#bank-m-icon').text('check_circle').addClass('green');

                    if (data.response.acc_type == 1) {
                        acc_type = "Savings";
                    }else{
                        acc_type = 'Current';
                    }

                    if (op_type == "update") {
                        $('.modal-res-text').text('Bank Details has been Successfully Updated.');
                    }else{
                        $('.modal-res-text').text('Bank Details has been Successfully Added.');
                        appendBank(data.response);
                    }


                    $('#bank-details-form').remove();

                    if(url == '/admin/update_user_bank'){
                        console.log($(this));
                    }


                    
                    //checkBankForms();

                }else{
                    $('.modal-res-text').text('Cannot Update Bank Details Rightnow.');
                    $('.response-image').attr('src','/icons/failed-tick.svg');
                    $('.modal-res-header').removeClass('success-body').addClass('failure-body');
                    $('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');
                }





                $('#infoModal').modal('show');
              },
              error:function(x,a,e){ 
                  handleErrors(x.responseJSON);

              }
          });


        $(this).find('input, select').attr('disabled', true);
        $(this).find('button').show();
        $(this).find('input[type="submit"]').hide();
    });




    function appendBank(v){
      console.log(v);
        $('#bank-details-tab').append(
                                
            '<form class="bank-details-form col-lg-12 col-md-12 col-sm-12" data-bankdata="'+v.id+'">'+
                '<input type = "hidden" name = "bank_id" class="bank_id" value = "'+v.id+'">'+
                '<input type = "hidden" name = "user_id" class="user_id" value = "'+v.user_id+'">'+
                '<input type = "hidden" name = "op_type" class="op_type" value = "update">'+
                '<p class="bank-count">Bank '+v.acc_count+'<i class="material-icons delete-acc" data-bankid="'+v.id+'">delete</i></p>'+
                '<div class="col-lg-4 col-md-4 col-sm-4">'+
                    '<div class="form-group">'+
                        '<input type="text" name = "acc_name" class="acc_name input-field" value="'+v.acc_name+'" required>'+
                        '<span class="highlight"></span>'+
                        '<span class="bar"></span>'+
                        '<label class="input-label">Name</label>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-4 col-md-4 col-sm-4">'+
                    '<div class="form-group">'+
                        '<input type="text" name = "acc_no" class="acc_no input-field" value="'+v.acc_no+'" required>'+
                        '<span class="highlight"></span>'+
                        '<span class="bar"></span>'+
                        '<label class="input-label">Account Number</label>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-4 col-md-4 col-sm-4">'+
                    '<div class="form-group">'+
                        '<input type="text" name = "ifsc_code" value="'+v.ifsc_code+'" class="ifsc_code input-field" required>'+
                        '<span class="highlight"></span>'+
                        '<span class="bar"></span>'+
                        '<label class="input-label">IFSC Code</label>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-4 col-md-4 col-sm-4">'+
                    '<div class="form-group">'+
                        '<select name="bank_name" class="bank-name" id="bank-selected'+v.id+'">'+
                           '<option value="HDF">HDFC BANK</option>'+
                           '<option value="UTI">Axis Bank</option>'+
                           '<option value="ICI">ICICI Bank - Retail Net Banking</option>'+
                           '<option value="IDB">IDBI Bank - Retail Net Banking</option>'+
                           '<option value="162">Kotak Bank</option>'+
                           '<option value="SBI">State Bank of India</option>'+
                           '<option value="ALB">Allahabad Bank - Retail Net Banking</option>'+
                           '<option value="ADB">Andhra Bank</option>'+
                           '<option value="BBK">Bank of Bahrain and Kuwait</option>'+
                           '<option value="BBC">Bank of Baroda - Corporate Banking</option>'+
                           '<option value="BBR">Bank of Baroda - Retail Net Banking</option>'+
                           '<option value="BOM">Bank of Maharashtra</option>'+
                           '<option value="BMN">Bharatiya Mahila Bank</option>'+
                           '<option value="CNB">Canara Bank</option>'+
                           '<option value="CSB">Catholic Syrian Bank</option>'+
                           '<option value="CBI">Central Bank of India</option>'+
                           '<option value="CRP">Corporation Bank</option>'+
                           '<option value="COB">Cosmos Bank</option>'+
                           '<option value="DEN">Dena Bank</option>'+
                           '<option value="DBK">Deutsche Bank</option>'+
                           '<option value="DC2">Development Credit Bank - Corporate</option>'+
                           '<option value="DCB">Development Credit Bank - Retail</option>'+
                           '<option value="DLB">Dhanlakshmi Bank</option>'+
                           '<option value="FBK">Federal Bank</option>'+
                           '<option value="INB">Indian Bank</option>'+
                           '<option value="IOB">Indian Overseas Bank</option>'+
                           '<option value="IDS">IndusInd Bank</option>'+
                           '<option value="ING">ING Vysya Bank - Retail Net Banking</option>'+
                           '<option value="JKB">Jammu &amp; Kashmir Bank</option>'+
                           '<option value="JSB">Janata Sahakari Bank</option>'+
                           '<option value="KBL">Karnataka Bank Ltd</option>'+
                           '<option value="KVB">Karur Vysya Bank</option>'+
                           '<option value="LVC">Laxmi Vilas Bank - Corporate Net Banking</option>'+
                           '<option value="LVR">Laxmi Vilas Bank - Retail Net Banking</option>'+
                           '<option value="NKB">NKGSB BANK</option>'+
                           '<option value="OBC">Oriental Bank of Commerce</option>'+
                           '<option value="PMC">Punjab &amp; Maharastra Coop Bank</option>'+
                           '<option value="PSB">Punjab &amp; Sind Bank</option>'+
                           '<option value="CPN">Punjab National Bank - Corporate Banking</option>'+
                           '<option value="PNB">Punjab National Bank - Retail Net Banking</option>'+
                           '<option value="RTN">Ratnakar Bank - Retail Net Banking</option>'+
                           '<option value="RBS">RBS (The Royal Bank of Scotland)</option>'+
                           '<option value="SWB">Saraswat Bank</option>'+
                           '<option value="SVC">Shamrao Vitthal Co-operative Bank - Retail Net Banking</option>'+
                           '<option value="SIB">South Indian Bank</option>'+
                           '<option value="SCB">Standard Chartered Bank</option>'+
                           '<option value="SYD">Syndicate Bank</option>'+
                           '<option value="TMB">Tamilnad Mercantile Bank Ltd.</option>'+
                           '<option value="TNC">Tamilnadu State Coop Bank</option>'+
                           '<option value="TJB">TJSB Bank</option>'+
                           '<option value="UCO">UCO Bank</option>'+
                           '<option value="UBI">Union Bank of India</option>'+
                           '<option value="UNI">United Bank of India</option>'+
                           '<option value="VJB">Vijaya Bank</option>'+
                           '<option value="YBK">Yes Bank Ltd</option>'+
                           '<option value="SBM">STATE BANK OF MYSORE</option>'+
                           '<option value="CIU">CITY UNION BANK LIMITED</option>'+
                           '<option value="RBL">Ratnakar Bank - Retail Net Banking</option>'+
                           '<option value="ORT">Oriental bank of commerce</option>'+
                           '<option value="HSB">HSBC</option>'+
                           '<option value="CIT">CITI Bank</option>'+
                        '</select>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-4 col-md-4 col-sm-4">'+
                    '<div class="form-group">'+
                        '<input type="text" name = "pan" class="input-field pan" value="'+v.pan+'" required>'+
                        '<span class="highlight"></span>'+
                        '<span class="bar"></span>'+
                        '<label class="input-label">PAN</label>'+
                        '<span class="text-danger"></span>'+
                    '</div>'+
                '</div>'+
                '<div class="col-lg-4 col-md-4 col-sm-4">'+
                    '<div class="form-group">'+
                        '<select class = "acc_type" name = "acc_type" id="acc_type'+v.id+'">'+
                            '<option value="savings">Savings</option>'+
                            '<option value = "current">Current</option>'+
                        '</select>'+
                    '</div>'+
                '</div>'+

                '<div class="col-lg-4 col-md-4 col-sm-4">'+
                    '<div class="form-group">'+
                        '<select class = "bank_type" name = "bank_type" id="bank_type'+v.id+'">'+
                            '<option value="1">Retail Banking</option>'+
                            '<option value = "2">Corporate Banking</option>'+
                        '</select>'+
                    '</div>'+
                '</div>'+


                // '<div class = "col-xs-12 p-lr-zero">'+
                //   '<div class="col-lg-12 col-md-12 col-sm-12">'+
                //     '<div class="form-group">'+
                //         '<input type="text" name = "payment_link"  value = "'+v.payment_link+'" class="input-field payment_link" required>'+
                //         '<span class="highlight"></span>'+
                //         '<span class="bar"></span>'+
                //         '<label class="input-label">Payment Link</label>'+
                //     '</div>'+
                //   '</div>'+
                // '</div>'+
                '<div class = "col-lg-12 col-md-12 col-sm-12">'+
                    '<input type="submit" name="" class="btn btn-primary grad-btn update-bank" style="display: none;" value="Save">'+
                    '<button type="button" name="" class="btn btn-primary grad-btn edit-bank">Edit</button>'+
                '</div>'+
            '</form>');

            // $('#bank-name').val(v.bank_name).text(v.bank_name)
            $('#bank-selected'+v.id+' option[value='+v.bank_code+']').prop('selected',true);
            $('#acc_type'+v.id+' option[value='+v.acc_type+']').prop('selected',true);
            $('#bank_type'+v.id+' option[value='+v.bank_type+']').prop('selected',true);
    }


    $(document).on('click', '.edit-bank', function(){
        $(this).closest('form').find('input, select').attr('disabled', false);
        $(this).hide();
        $(this).closest('form').find('input[type="submit"]').show();

    });


    $(document).on('submit','#document-details-form',function(e){
        e.preventDefault();
        var formData = new FormData();
        var count = 0;
        $('.user-files').each(function(){
            //console.log('hello');
            if($(this).get(0).files.length !== 0){
                
                console.log($(this).attr('name'));

                formData.append($(this).attr('name'),$(this).get(0).files[0]);
                console.log($(this).get(0).files[0]);
                
            }else{
                $(this).parent().prev().find('span.text-danger').text('File Missing.').css({'display':'inline-block'});
                count++;
                console.log(count);
            }

            console.log(formData);
        });
        //created_user_id = 7;

        console.log(count);
        // break;
        var user_id = $('#add-bank').data('userid');
        formData.append('user_id',user_id);

        if (count == 0) {
            $.ajax({
                  type: 'POST',
                  url: '/admin/add_user_documents',
                  headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  data: formData,
                  contentType: false,
                  processData: false,
                  success:function(data){
                    console.log(data.msg);
                    if(data.msg == "success"){
                    $('.modal-res-text').text('User Documents have been Successfully uploaded');
                    $('.response-image').attr('src','/icons/success-tick.svg');
                    $('.modal-res-header').removeClass('failure-body').addClass('success-body');
                    $('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
                    //console.log();
                    getUserDetails(user_id);

                    }else{
                        $('.modal-res-text').text('Cannot update User Documents right now.');
                        $('.response-image').attr('src','/icons/failure-tick.svg');
                        $('.modal-res-header').removeClass('success-body').addClass('failure-body');
                        $('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');

                    }
                    $('#document-m-icon').text('check_circle').addClass('green');
                    $('#infoModal').modal('show');

                  },
                  error:function(x,a,e){ 
                      handleErrors(x.responseJSON);

                  }
              });
        }

        

    })

    $('.user-files').on('change',function(e){
        //console.log('file');
        $(this).parent().prev().find('span.text-danger').hide();
        $(this).parent().prev().find('.file-name').text(e.target.files[0].name);
    })


    $('#company-details-form').on('submit',function(e){

        e.preventDefault();
        var data = $(this).serialize();

        $.ajax({
              type: 'POST',
              url: '/admin/update_user_details',
              headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              data: data,
              //contentType: false,
              //processData: false,
              success:function(data){
                console.log(data.msg);
                if(data.msg == "success"){
                $('#edit-company').css('display','block');
                $('#submit-company').css('display','none');
                $('.modal-res-text').text('User Details have been Successfully uploaded');
                $('.response-image').attr('src','/icons/success-tick.svg');
                $('.modal-res-header').removeClass('failure-body').addClass('success-body');
                $('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
                //console.log();
                    $('#company-details-form input').attr('disabled', true);


                }else{
                    $('.modal-res-text').text('Cannot update User details right now.');
                    $('.response-image').attr('src','/icons/failure-tick.svg');
                    $('.modal-res-header').removeClass('success-body').addClass('failure-body');
                    $('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');

                }
                $('#document-m-icon').text('check_circle').addClass('green');
                $('#infoModal').modal('show');

              },
              error:function(x,a,e){ 
                  handleErrors(x.responseJSON);

              }
          });
    });

    function handleErrors(error_response){
      console.log(error_response.message);
      
      $.each(error_response.errors,function(k,v){
        console.log(k+'-'+v);

        $('input[name='+k+']').parent().find('span.text-danger').text(v).css({'display':'inline-block'});
      });
    }

    // $('#update-bank').on('click',function(e){
    //   $(this).closest('.bank-details-form').submit(false);
    // })

    $(document).on('click','.delete-acc',function(){
      // console.log($(this).data('bankid'));
      var bank_id = $(this).data('bankid');
      var data = 'bank_id='+bank_id;
      $.ajax({
        type:'POST',
        url:'/admin/delete_bank',
        data:data,
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data){
          // location.reload();
          $("form").find(`[data-bankid='${bank_id}']`).parent().parent().empty();
        },
        error:function(error){
          console.log(error);
        }
      });
    });

    $(document).on('click','#edit-company',function(){
      $(this).css('display','none');
      $('#submit-company').css('display','block');
    });

    $('#changepass').on('submit',function(e){
      e.preventDefault();
      var formData = $('#changepass').serialize();
      $.ajax({
        type:'POST',
        url:'/admin/update_pass',
        data:formData,
        headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(data){
          console.log(data.msg);
          if (data.msg == 1) {
            $('.modal-res-text').text('User Password have been Successfully uploaded');
            $('.response-image').attr('src','/icons/success-tick.svg');
            $('.modal-res-header').removeClass('failure-body').addClass('success-body');
            $('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
            $('#infoModal').modal('show');
          }else if (data.msg == 2) {
            $('.modal-res-text').text(data.info);
            $('.response-image').attr('src','/icons/failed-tick.svg');
            $('.modal-res-header').removeClass('success-body').addClass('failure-body');
            $('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');
            $('#infoModal').modal('show');
          }
        },
        error:function(error){
          console.log(error);
        }
      })
    }); 

})