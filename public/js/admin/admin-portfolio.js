$(document).ready(function(){
	var p_id;
	$('.edit-portfolio').on('click',function(){
		p_id = $(this).data('id');

		var row = $(this).parent().parent().parent();

		console.log(row.find('p.scheme-name').text());
		$('#modal_scheme_name').val(row.find('p.scheme-name').text());
		$('#modal_folio_number').val(row.find('p.folio-number').text());
		$('#modal_amount_invested').val(row.find('p.inv-amount').text());
		$('#modal_units_held').val(row.find('p.units-held').text());
		$('#modal_invested_nav').val(row.find('p.invested-nav').text());
		$('#modal_current_nav').val(row.find('p.current-nav').text());
		$('#modal_current_value').val(row.find('p.current-value').text());
		$('#modal_net_returns').val(row.find('p.net-returns').text());
		$('#dop').val(row.find('p.purchase-date').text());
		$('#p_id').val(p_id);
		$('#editPortfolioModal').modal('show');

	});

	$('#add-man-inv').on('click',function(e){
		e.preventDefault();
		$('#invModal').modal('show');
	});

	$('#edit_portfolio_form').on('submit',function(e){
		e.preventDefault();
		var dataString = $(this).serialize();
		console.log(dataString);
		$.ajax({
		      type: 'POST',
		      url: '/admin/edit_user_portfolio',
		      data: dataString,
		      headers: {
				      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		      success:function(data){
		      	if (data.msg == "success") {
		      		$('.modal-res-text').text('User Portfolio has been updated Successfully.');
		      		$('.response-image').attr('src','/icons/success-tick.svg');
		      		$('.modal-res-header').toggleClass('success-body');
		      		$('#company-m-icon').text('check_circle').addClass('green');

		      		created_user_id = data.user_id;
		      		console.log(created_user_id);
		      	}else{
		      		$('.modal-res-text').text('User portfolio cannot be edited right now.');
		      		$('.response-image').attr('src','/icons/failure-tick.svg');
		      		$('.modal-res-header').toggleClass('failure-body');
		      	}

		      	$('#editPortfolioModal').modal('hide');
		      	$('#infoModal').modal('show');
		      },
		      error:function(x,a,e){ 
		          handleErrors(x.responseJSON);

		      }
		  });
		
	});

	$('#add-portfolio').on('click',function(){

        var port_children = $('.portfolio-holder').children().length;
        var append_next_value = port_children + 1;
        $('.portfolio-holder').append(
            '<div class = "col-lg-12 col-md-12 col-sm-12 padding-lr-zero port-child-holder" id = "port-child-'+append_next_value+'">'+
                    '<div class="col-xs-3 p-lr-zero">'+
                           '<div class="form-group">'+
                               '<input type="text" name="scheme-code" class="scheme_input input-field form-control center-block port-scheme-code" id="scheme-code" required>'+
                                '<span class="bar"></span>'+
                                '<label class="input-label">Scheme Code</label>'+
                           '</div>'+
                      '</div>'+
                      '<div class="col-xs-3 p-lr-zero">'+
                          '<div class="form-group">'+
                               '<input type="text" name="amount-inv" class="scheme_input port-amount-inv input-field form-control center-block" id="amount-inv" required>'+
                                '<span class="bar"></span>'+
                                '<label class="input-label">Amount Invested</label>'+
                           '</div>'+
                      '</div>'+
                      '<div class="col-xs-3 p-lr-zero">'+
                          '<div class="form-group">'+
                               '<input type="text" name="units-held" class="scheme_input port-units-held input-field form-control center-block" id="units-held" required>'+
                                '<span class="bar"></span>'+
                                '<label class="input-label">Units Held</label>'+
                           '</div>'+
                      '</div>'+
                      '<div class="col-xs-3 p-lr-zero">'+
                          '<div class="form-group">'+
                               '<input type="text" name="invested-nav" class="scheme_input port-invested-nav input-field form-control center-block" id="invested-nav" required>'+
                                '<span class="bar"></span>'+
                                '<label class="input-label">Invested NAV</label>'+
                           '</div>'+
                      '</div>'+
                      '<div class="col-xs-3 p-lr-zero">'+
                          '<div class="form-group">'+
                               '<input type="text" name="folio-number" class="scheme_input port-folio-number input-field form-control center-block" id="folio-number" required>'+
                                '<span class="bar"></span>'+
                                '<label class="input-label">Folio Number</label>'+
                           '</div>'+
                      '</div>'+
					'<div class="col-xs-3 p-lr-zero">'+
						'<div class="form-group">'+
							'<input type="text" name="investment-date" class="scheme_input port-investment-date input-field form-control center-block" id="investment-date" required>'+
							'<span class="bar"></span>'+
							'<label class="input-label">Investment Date</label>'+
						'</div>'+
					'</div>'+

					'<div class="col-xs-3 p-lr-zero">'+
						'<div class="form-group">'+
							'<select name="portfolio-type" class="port-portfolio-type">' +
								'<option value="liquid">Liquid</option>'+
					            '<option value="arb">Arbitrage</option>'+
							'</select>'+
						'</div>'+
					'</div>'+
                  '</div>'
              );
    });


    $('#manual-investment').on('submit',function(e){

        e.preventDefault();
        var user_id = $('#inv_user_id').val();
        var inv_amount = $('#inv-amount').val();
        var pan_number = $('#pan-number').val();
        var inv_date = $('#inv-date').val();
        var payment_type = $('#payment-type').val();
        var utr_no = $('#utr_number').val();
        var acc_no = $('#acc-number').val();

        //var dataString = 'inv_amount=' + inv_amount + '&pan_number=' + pan_number + '&user_id=' + user_id + '&inv_date=' + inv_date + '&port_type=' + port_type + '&payment_type=' + payment_type;
        
        var port_children = $('.portfolio-holder').children().length;
        
        if (port_children == 0) {
            alert('Kindly Add portfolio Details');
        }else{

            var port_details = [];
            var total_port_amount = 0;
            $('.port-child-holder').each(function(){
                total_port_amount += parseFloat($(this).find('.port-amount-inv').val());
                port_details.push({
                            'scheme_code': $(this).find('.port-scheme-code').val(),
                            'inv_amount': $(this).find('.port-amount-inv').val(),
                            'units_held': $(this).find('.port-units-held').val(),
                            'invested_nav' : $(this).find('.port-invested-nav').val(),
                            'folio_number' : $(this).find('.port-folio-number').val(),
							'investment_date' : $(this).find('.port-investment-date').val(),
							'portfolio_type' : $(this).find('.port-portfolio-type').val()
                        })

            });

            console.log(port_details);
            console.log(total_port_amount,inv_amount);

            if (total_port_amount != inv_amount) {
                alert('Investment Amount and Portfolio Sum doesn\'t match' );
            }else{

                var formData = new FormData();

                console.log(inv_amount,user_id,inv_date,payment_type);

                formData.append('inv_amount',inv_amount);
                formData.append('user_id',user_id);
                formData.append('pan_number',pan_number);
                formData.append('utr_no',utr_no);
                formData.append('inv_date',inv_date);
                formData.append('payment_type',payment_type);
                formData.append('acc_no',acc_no);
                myString = JSON.stringify(port_details);
                formData.append('port_details',myString);

                console.log(formData);
                console.log(total_port_amount,inv_amount);
                //var dataString = 'inv_amount=' + inv_amount + '&user_id=' + user_id + '&inv_date=' + inv_date + '&port_type=' + port_type + '&payment_type=' + payment_type + '&port_details=' + port_details;

                $.ajax({
                    type: "POST",
                    url: "/admin/add_manual_portfolio",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: formData,
                    cache: !1,
                    contentType: !1,
                    processData: !1,
                    beforeSend: function() {},
                    success: function(data) {
                        if (data.msg == "success") {
				      		$('.modal-res-text').text(data.response);
				      		$('.response-image').attr('src','/icons/success-tick.svg');
				      		$('.modal-res-header').toggleClass('success-body');
				      		$('#company-m-icon').text('check_circle').addClass('green');

				      		created_user_id = data.user_id;
				      		console.log(created_user_id);
				      		 $('#manual-withdraw').trigger('reset');
				      		 $('#wdModal').modal('hide');
				      	}else{
				      		$('.modal-res-text').text(data.response);
				      		$('.response-image').attr('src','/icons/failed-tick.svg');
				      		$('.modal-res-header').toggleClass('failure-body');
				      	}

				      	$('#infoModal').modal('show');
                    },
                    error: function(xhr, status, error) {},
                })

            }
        }
        
    });

    $('#inv-date, #withdraw-date').datepicker();

    $('body').on('focus',"#investment-date", function(){
        $(this).datepicker();
    });


    $('#add-man-wd').click(function(e){
    	e.preventDefault();

		$('#wdModal').modal('show');
	});


    $('#manual-withdraw').on('submit', function(e){
    	e.preventDefault();

    	var data = $(this).serialize();

        $.ajax({
            type: "POST",
            url: "/admin/add_manual_withdraw",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            // cache: !1,
            // contentType: !1,
            // processData: !1,
            beforeSend: function() {},
            success: function(data) {
                if (data.msg == "success") {
                    $('.modal-res-text').text('User Investments added Successfully.');
                    $('.response-image').attr('src','/icons/success-tick.svg');
                    $('.modal-res-header').toggleClass('success-body');
                    $('#company-m-icon').text('check_circle').addClass('green');

                    created_user_id = data.user_id;
                    console.log(created_user_id);
                    $('#manual-investment').trigger('reset');
                    $('.portfolio-holder').empty();
                }else{
                    $('.modal-res-text').text(data.response);
                    $('.response-image').attr('src','/icons/failure-tick.svg');
                    $('.modal-res-header').toggleClass('failure-body');
                }

                $('#infoModal').modal('show');
            },
            error: function(xhr, status, error) {},
        })
	});
});