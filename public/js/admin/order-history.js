$(document).ready(function(){
	$('.revoke-investment-btn').click(function(){
		var check_boxes = $(this).parent().find('input[type=checkbox]');
		var revoke_p_id = [];

		check_boxes.each(function(){
			if ($(this).prop('checked') == true) {
				console.log($(this).attr('id'));
				revoke_p_id.push($(this).attr('id'));
			}
		})

		console.log(revoke_p_id);
		if (revoke_p_id.length == 0 ) {
            $('.modal-res-text').text('No Valid Conditions to Revoke Orders.');
            $('.response-image').attr('src','/icons/failed-tick.svg');
            $('.modal-res-header').toggleClass('failure-body');
            $('#infoModal').modal('show');

        }else{

			var formdata = new FormData();
			formdata.append('p_ids',revoke_p_id);
			console.log(formdata);
			$.ajax({
            type: "POST",
            url: "/admin/modify_portfolio",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
            },
            data: formdata,
            cache: !1,
            processData: !1,
            contentType: !1,
            success: function(data) {
                if (data.msg == "success") {
		      		$('.modal-res-text').text('Investments have been Successfully Revoked');
		      		$('.response-image').attr('src','/icons/success-tick.svg');
		      		$('.modal-res-header').toggleClass('success-body');
		      		$('#company-m-icon').text('check_circle').addClass('green');
		      		$('#done-btn').attr('onclick', "javascript:location.href='/admin/order_history'");

		      		created_user_id = data.user_id;
		      		console.log(created_user_id);
		      	}else{
		      		$('.modal-res-text').text('Cannot Revoke your investment. Kindly try again later.');
		      		$('.response-image').attr('src','/icons/failed-tick.svg');
		      		$('.modal-res-header').toggleClass('failure-body');
		      	}

		      	$('#infoModal').modal('show');
            },
            error: function(xhr, error, status) {
                
            },
        })
		}
	});
});