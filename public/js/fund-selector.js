 $(document).ready(function(){

     // $('#infoModal').modal('show');
    $count = 0;
    var btn_disable = false;

    $(document).on('change','input[type="radio"]',function(){
        if ($(this).prop('checked')) {
            var grand_parent = $(this).parent().parent().parent().parent();
            var parent = $(this).parent().parent();
            grand_parent.find('i').text('radio_button_unchecked').removeClass('green');
            $(this).parent().find('i').text('check_circle').addClass('green');
            grand_parent.find('.inv-type-text').css({'font-weight':'400'});
            grand_parent.find('.investment-date').css({'color':'#999999','font-weight':'400'});
            parent.find('.inv-type-text').css({'font-weight':'500'});
            parent.find('.investment-date').css({'color':'#0091EA','font-weight':'500'});
        }
    });


            /*The below function is to stop the form submission when enter key is pressed*/

            $(window).keydown(function(e){
              if(e.keyCode == 13) {
                e.preventDefault();
                return false;
              }
            });


            /*
                The below Keydown event handler is to check if the amount exceeds 5000 and remove the red border bottom 
                and removes minimum alert text
            */

            // $('#inv-amount').on('keyup',function(e){
            //       ////console.log(e.keyCode);
            //       if($(this).val() > 4999){
            //         $(this).removeClass('red-bottom');
            //         $('#minimum-text').hide();
            //       }
            // });

            $('#inv-amount').on('keydown',function(e){
                  ////console.log(e.keyCode);
                  if (e.shiftKey) {
                        e.preventDefault();
                    }
                  if($(this).val() > 10000){
                    if (e.which == 8) {
                      return true;
                    }else{
                      e.preventDefault();
                    }
                  }
            });


            /*Below event handler will round off the Investment amount to next 1000 
            and passes it to showFunds()*/

            $('#inv-amount').on('blur', function(e) {

              if ($('#inv-amount').val() > 100000) {
                var p_type = $('#port-type').val()
                var inv_amount = 0;


                if ($('#inv-amount').val() != '') {
                    var amount = parseInt($('#inv-amount').val());
                    amount = Math.round(amount / 1000);
                    inv_amount = amount * 1000;
                    $('#inv-amount').val(inv_amount)
                }else{
                  $('.funds-container p').nextAll().empty();
                  $('#total-inv-amount span').text('0');
                }
                showFunds(p_type)
              }else{
                //$('#inv-amount').val()
                $('#minimum-text').text('Minimum Investment Amount : 5000').show();
              }
                
            });

            /*Below event handles will call showFunds() when portfolio type changes */

            function changePortType(port_type){

                    if (port_type == "own") {
                        $('#total-inv-amount>span').text('0');
                        $('#portfolio-header').text('Custom');
                        $('#portfolio-text').text('A custom portfolio is meant for more experienced investors who understand the differences between various asset classes. Build your own portfolio from a curated collection of 30 schemes that are ideal for long term wealth creation, parking short term capital or saving taxes. Rightfunds will dynamically re-balance your portfolio should you wish to change your choice later.');
                        ////console.log("custom Selected");

                        
                        $('#inv-amount').val('');
                        $('#inv-amount').prop('disabled',true);
                        showCustomFunds('own');

                    }else{
                        $('#total-inv-amount>span').text($('#inv-amount').val());
                        $('#inv-amount').prop('disabled',false);
                        //$('#inv-amount').val(temp_amount);
                        switch(port_type){
                        case "Conservative":
                            $('#portfolio-header').text('Conservative');
                            $('#portfolio-text').text('A conservative portfolio is best suited for short term investments. The portfolio comprises of debt mutual funds that offer investors steady returns which are unaffected by market volatility. They are usually seen as alternatives to fixed deposits and savings account. Holding investments for three years in a conservative portfolio provides the investor with indexation benefits (reduction of taxable interest to the extent of inflation).');
                            break;

                        case "Moderate":
                            $('#portfolio-header').text('Moderate');
                            $('#portfolio-text').text('A moderate portfolio comprises of balanced mutual funds that hold roughly 40% debt instruments and the rest equity. The debt holdings yield steady returns while the equity component provides capital appreciation. By holding investments in a moderate portfolio for a period of one year investors are completely exempt of paying tax on the capital gains.');
                        break;


                        case "Aggressive" :
                            $('#portfolio-header').text('Aggressive');
                            $('#portfolio-text').text('An aggressive portfolio is best suited for investments with a time horizon of 3 years and above. They comprise of equity mutual funds which provide the investors with capital appreciation. They are subject to market volatility, however have historically provided investors with superior returns when compared to the conservative and moderate portfolios. By holding investments in an aggressive portfolio for a period of one year investors are completely exempt of paying tax on the capital gains.');
                        break;


                        case "Tax Saver" :
                            $('#portfolio-header').text('Tax Saver');
                            $('#portfolio-text').text('An Equity Linked Savings Scheme comprises of equity mutual funds that provide an investor with tax benefits. Under section 80C of the Indian Income tax act investors can avail an exemption of up to Rupees One lakh and fifty thousand from their taxable income each year by investing in ELSS. The investments come with a lock in period of three years after which investors may withdraw their investments.');               
                        break;

                        }       

                        //console.log(port_type);
                        showFunds(port_type);
                    }
            }



            /*Below event will handle the change in the Investment type*/

            $('#onetime, #future, #monthly').on('change', function() {
              var inv_type = $(this).val();
              var p_type = $('#port-type').val();

              if ($('#inv-amount').val().length >= 0) {
                      if ($('#onetime').is(":checked")) {
                        $('#min-val').text('5000');
                        var response;

                        $.ajax({
                            type: 'GET',
                            url: '/get_onetime_investment_date',
                            data: '',
                            success:function(data){
                              response = data;
                              //console.log(data);
                            },
                            async:false,
                            error:function(){ 
                                
                            }
                        });

                        ////console.log(response.response);
                        resetDate($(this));
                        $(this).parent().next().find('input[type=text]').val(response.response).show();                
                    } 

                    else if ($('#future').is(":checked")) {
                        $('#min-val').text('5000');
                        resetDate($(this));
                        $(this).parent().next().find('input[type=text]').show();
                        $('#sche-sel-date').datepicker('show')
                    }

                    else if ($('#monthly').is(":checked")) {
                        $('#min-val').text('Min : 1000');
                        if ($(this).val() >= 1000) {
                            $('#min-val').css({
                                'color': '#797979'
                            });
                            var p_type = ''
                        }
                        resetDate($(this));
                        $('#to,.sip-duration-menu,.sip-date-menu').css({
                            'display': 'inline-block'
                        });
                        //$('#sip-date').datepicker()

                        //$('#sip-date').
                    }else{
                      showFunds(p_type)
                    }
                    // if (p_type == 'own') {
                    //     //showCustomFunds(p_type)
                    // } else {
                        
                    // }
              }else{
                $('#minimum-text').text('Enter Investment Amount').show();
              }
            });

            /*Investment type event handler ends*/


            function showFunds(p_type){
              var inv_amount,inv_type,response;
              if ($('#inv-amount').val() != '') {
                  inv_amount = parseInt($('#inv-amount').val())
              }
              if ($('input[name="investment-type"]').is(':checked')) {
                  inv_type = $('input[name="investment-type"]:checked').val()
              }
              if (p_type == '' || inv_amount == '') {
                  //console.log('nothing');
              } else {

                var dataString = 'p_type=' + p_type + '&inv_amount=' + inv_amount + '&inv_type=' + inv_type;
                var response;

                $.ajax({
                    type: 'POST',
                    url: '/showFunds',
                    data: dataString,
                    success:function(data){
                      response = data;
                      if (response.msg == "min_5000") {

                        $('#inv-amount').addClass('red-bottom');
                        $('#minimum-text').show();
                      }else{
                        suggestFunds(response);
                      }
                    },
                    error:function(){ 
                        
                    }
                });
              }
            }

            function showCustomFunds(p_type){
              var inv_type = '';
              var response;
              inv_type = $("input[name='investment-type']:checked").val()
              var p_type = p_type;
              var dataString = 'p_type=' + p_type + '&inv_type=' + inv_type;

              $.ajax({
                    type: 'POST',
                    url: '/showCustomFunds',
                    data: dataString,
                    success:function(data){
                      response = data;
                      suggestCustomFunds(response);
                    },
                    error:function(){ 
                        
                    }
                });

              
            }

            function suggestFunds(data){

              
                var total_amount = 0;
                //console.log(data);
                var container_name = data.msg;
                //console.log(container_name);
                $('#'+container_name+'-arrow').hide();
                var container = $('#'+container_name+'-funds-container');
                $('.funds-container').not(container).hide();
                container.show();
                //container.find('.fund-heading').nextAll().remove();
                $('.funds-container').find('.scheme-container,.scheme-details-container').remove();
                $('#eq-collapse-container,#bal-collapse-container,#debt-collapse-container,#ts-collapse-container').empty();
                $.each(JSON.parse(data.response),function(key,value){
                    
                    total_amount += value.amount;
                    //console.log(value.scheme_name);
                    //'#'+value.scheme_type+'-collapse-container'
                    $('#'+container_name+'-funds-container').append(

                          '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 scheme-container border-bottom"  href="#child-'+value.scheme_code+'" id="'+value.scheme_code+'">'+
                              '<div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8 pl-10">'+
                                  '<p class="inv-scheme-name">'+value.scheme_name+'</p>'+
                              '</div>'+
                              '<div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">'+
                                  '<p class="inv-scheme-amount pull-right"><span>Rs. </span>'+value.amount+'</p>'+
                              '</div>'+
                          '</div>'+

                          '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero scheme-details-container" id="child-'+value.scheme_code+'">'+
                              '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom">'+
                                  '<p class="inner-header"></p>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Fund Manager</p>'+
                                      '<p class = "detail-content" id="fund-manager-name-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Type</p>'+
                                      '<p class = "detail-content" id="fund-type-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Exit Load</p>'+
                                      '<p class = "detail-content" id="exit-load-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Benchmark</p>'+
                                      '<p class = "detail-content" id="benchmark-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Asset Size(Cr)</p>'+
                                      '<p class = "detail-content" id="asset-size-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Launch Date</p>'+
                                      '<p class = "detail-content" id="launch-date-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                              '</div>'+
                              '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot active-duration-holder">'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center active-duration" data-scode = "'+value.scheme_code+'">3 Months</p>'+
                                 '</div>'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center" data-scode = "'+value.scheme_code+'">6 Months</p>'+
                                 '</div>'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center" data-scode = "'+value.scheme_code+'">1 Year</p>'+
                                 '</div>'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center" data-scode = "'+value.scheme_code+'">3 Years</p>'+
                                 '</div>'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center" data-scode = "'+value.scheme_code+'">5 Years</p>'+
                                 '</div>'+
                              '</div>'+
                               '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">'+
                                 '<div id="'+value.scheme_code+'-graph-container">'+
                                   
                                 '</div>'+
                               '</div>'+
                               '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 ">'+
                                  '<p class="nav-det-container">'+
                                      '<span class="inner-header">Nav : </span>'+
                                      '<span class="scheme-nav" id="scheme-nav-'+value.scheme_code+'"></span>'+
                                      '<span class="green scheme-change"></span>'+
                                      '<span class="scheme-perc green"></span>'+
                                  '</p>'+
                              '</div>'+
                              '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom border-top">'+
                                  '<p class="inner-header">Absolute Returns</p>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">1 Month</p>'+
                                      '<p class = "return-content" id="one-month-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">3 Month</p>'+
                                      '<p class = "return-content" id="three-month-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">6 Month</p>'+
                                      '<p class = "return-content" id="six-month-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">1 Year</p>'+
                                      '<p class = "return-content" id="one-year-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">3 Year</p>'+
                                      '<p class = "return-content" id="three-year-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">5 Years</p>'+
                                      '<p class = "return-content" id="five-year-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                              '</div>'+
                          '</div>'

                      );
                });
                //console.log(total_amount);
                $('#total-inv-amount > span').text(total_amount);

            }

            function suggestCustomFunds(data){
                var total_amount = 0;
                //$('.funds-container').find('.fund-heading').nextAll().remove()
               $('#eq-collapse-container,#bal-collapse-container,#debt-collapse-container,#ts-collapse-container').empty();
               $('.funds-container').find('.scheme-container').remove();
               $.each(JSON.parse(data.response), function(key, value) {
                var container = $('#'+value.scheme_type+'-funds-container');
                //console.log(value.scheme_type);
                 $('#'+value.scheme_type+'-arrow').show();
                container.show();
                $('#'+value.scheme_type+'-collapse-container').append(

                          '<div class = "col-lg-12 col-md-12 col-xs-12 col-sm-12 scheme-container border-bottom" id="'+value.scheme_code+'">'+
                              '<div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8 pl-10">'+
                                  '<p class="inv-scheme-name">'+value.scheme_name+'</p>'+
                              '</div>'+
                              '<div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">'+
                                  '<p class="inv-scheme-amount pull-right"><span>Rs.'+ 
                                    '<input type="text" name="" class="custom-amount input-field" data-scheme="'+value.scheme_type+'" data-schemeid="'+value.scheme_code+'" id="'+value.scheme_code+'" placeholder>'+
                                  '</span></p>'+
                              '</div>'+
                          '</div>'+

                          '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero scheme-details-container">'+
                              '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom">'+
                                  '<p class="inner-header">Fund Details</p>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Fund Manager</p>'+
                                      '<p class = "detail-content" id="fund-manager-name-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Type</p>'+
                                      '<p class = "detail-content" id="fund-type-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Exit Load</p>'+
                                      '<p class = "detail-content" id="exit-load-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Benchmark</p>'+
                                      '<p class = "detail-content" id="benchmark-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Asset Size(Cr)</p>'+
                                      '<p class = "detail-content" id="asset-size-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">Launch Date</p>'+
                                      '<p class = "detail-content" id="launch-date-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                              '</div>'+
                              '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-zero border-bot active-duration-holder">'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center active-duration" data-scode = "'+value.scheme_code+'">3 Months</p>'+
                                 '</div>'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center" data-scode = "'+value.scheme_code+'">6 Months</p>'+
                                 '</div>'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center" data-scode = "'+value.scheme_code+'">1 Year</p>'+
                                 '</div>'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center" data-scode = "'+value.scheme_code+'">3 Years</p>'+
                                 '</div>'+
                                 '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero border-right w-20">'+
                                   '<p class="graph-duration text-center" data-scode = "'+value.scheme_code+'">5 Years</p>'+
                                 '</div>'+
                              '</div>'+
                               '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-lr-zero">'+
                                 '<div id="'+value.scheme_code+'-graph-container">'+
                                   
                                 '</div>'+
                               '</div>'+
                               '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 ">'+
                                  '<p class="nav-det-container">'+
                                      '<span class="inner-header">Nav</span>'+
                                      '<span class="scheme-nav" id="scheme-nav-'+value.scheme_code+'"></span>'+
                                      '<span class="green scheme-change"></span>'+
                                      '<span class="scheme-perc green"></span>'+
                                  '</p>'+
                              '</div>'+
                              '<div class = "col-lg-12 col-md-12 col-sm-12 col-xs-12 p-lr-30 border-bottom border-top">'+
                                  '<p class="inner-header"> Absolute Returns </p>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">1 Month</p>'+
                                      '<p class = "return-content" id="one-month-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">3 Month</p>'+
                                      '<p class = "return-content" id="three-month-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">6 Month</p>'+
                                      '<p class = "return-content" id="six-month-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">1 Year</p>'+
                                      '<p class = "return-content" id="one-year-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">3 Year</p>'+
                                      '<p class = "return-content" id="three-year-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                                  '<div class = "col-lg-2 col-md-2 col-sm-2 col-xs-2 p-lr-zero">'+
                                      '<p class="detail-header">5 Years</p>'+
                                      '<p class = "return-content" id="five-year-'+value.scheme_code+'"></p>'+
                                  '</div>'+
                              '</div>'+
                          '</div>'
                );
               });
            }

            $(document).on('click','.scheme-container',function(){

                var input = $(this).find('.custom-amount');
                if (input.is(':focus')) {

                }else{

                  // if ($(document).find($(this).next('.scheme-details-container')).css('display') == 'block') {
                  //   alert('visible')
                  //   $(this).toggleClass('card-selected').toggleClass('border-blue-left');
                  // }

                   // $(this).attr('href');
                   //console.log($(this).attr('id'));
                   //console.log($('fund-manager-name-'+$(this).attr('id')).text());
                   if ($('#fund-manager-name-'+$(this).attr('id')).text() == '') {
                    //console.log($('#fund-manager-name-'+$(this).attr('id')).text());
                    //console.log('empty text');
                    $(this).toggleClass('card-selected').toggleClass('border-blue-left');
                    $(this).next('.scheme-details-container').slideToggle(500).toggleClass('card-selected');
                    getSchemeDetails($(this).attr('id'));
                   }else{
                    //console.log('non empty text');
                    //console.log($('#fund-manager-name-'+$(this).attr('id')).text());
                    $(this).toggleClass('card-selected').toggleClass('border-blue-left');
                    //$(this).next('.scheme-details-container').addClass('set-height').slideToggle(500);
                    $(this).next('.scheme-details-container').slideToggle(500).css({'height':'auto'});
                   }

                   
                }
               
            });


            function getSchemeDetails(schemeCode){
              var dataString = 'scheme_code=' + schemeCode;
              var response;

              $.ajax({
                    type: 'POST',
                    url: '/show_scheme_details',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: dataString,
                    success:function(data){
                      data.msg == 'success';
                      if (data.msg == 'success') {
                        //console.log(data);
                        attachSchemeDetails(data,schemeCode)
                      }
                      
                    },
                    error:function(){ 
                        
                    }
                });
            }

            function attachSchemeDetails(response,schemeCode){

              $.each(response.response, function(key, value) {
                    var scheme_details = value;

                    $(document).find('#fund-manager-name-'+schemeCode).text(scheme_details.fund_manager);
                    $(document).find('#benchmark-'+schemeCode).text(scheme_details.benchmark);
                    $(document).find('#launch-date-'+schemeCode).text(scheme_details.launch_date);
                    $(document).find('#fund-type-'+schemeCode).text(scheme_details.fund_type);
                    $(document).find('#exit-load-'+schemeCode).text(scheme_details.exit_load);
                    $(document).find('#asset-size-'+schemeCode).text(scheme_details.asset_size);



                    //$(document).find('#one-day-'+schemeCode).text(scheme_details.one_day_nav);
                    //$(document).find('#five-day-'+schemeCode).text(scheme_details.five_day_nav);

                    if (parseFloat(scheme_details.one_week_nav) > 0) {
                      $(document).find('#one-week-'+schemeCode).text(scheme_details.one_week_nav+'%').addClass('green');
                    }else{
                      $(document).find('#one-week-'+schemeCode).text(scheme_details.one_week_nav+'%').addClass('red');
                    }

                    if (parseFloat(scheme_details.thirty_day_nav) > 0) {
                      $(document).find('#one-month-'+schemeCode).text(scheme_details.thirty_day_nav+'%').addClass('green');
                    }else{
                      $(document).find('#one-month-'+schemeCode).text(scheme_details.thirty_day_nav+'%').addClass('red');
                    }

                    if (parseFloat(scheme_details.ninety_day_nav) > 0) {
                      $(document).find('#three-month-'+schemeCode).text(scheme_details.ninety_day_nav+'%').addClass('green');
                    }else{
                      $(document).find('#three-month-'+schemeCode).text(scheme_details.ninety_day_nav+'%').addClass('red');
                    }

                    if (parseFloat(scheme_details.six_month_nav) > 0) {
                      $(document).find('#six-month-'+schemeCode).text(scheme_details.six_month_nav+'%').addClass('green');
                    }else{
                      $(document).find('#six-month-'+schemeCode).text(scheme_details.six_month_nav+'%').addClass('red');
                    }

                    if (parseFloat(scheme_details.one_year_nav) > 0) {
                      $(document).find('#one-year-'+schemeCode).text(scheme_details.one_year_nav+'%').addClass('green');
                    }else{
                      $(document).find('#one-year-'+schemeCode).text(scheme_details.one_year_nav+'%').addClass('red');
                    }

                    if (parseFloat(scheme_details.three_year_nav) > 0) {
                      $(document).find('#three-year-'+schemeCode).text(scheme_details.three_year_nav+'%').addClass('green');
                    }else{
                      $(document).find('#three-year-'+schemeCode).text(scheme_details.three_year_nav+'%').addClass('red');
                    }

                    if (parseFloat(scheme_details.five_year_nav) > 0) {
                      $(document).find('#five-year-'+schemeCode).text(scheme_details.five_year_nav+'%').addClass('green');
                    }else{
                      $(document).find('#five-year-'+schemeCode).text(scheme_details.five_year_nav+'%').addClass('red');
                    }

                    $(document).find('#scheme-nav-'+schemeCode).text(scheme_details.current_nav);


                });

                    graph_scheme_code = schemeCode;
                    graph_duration = "7days";

                    create_scheme_graph();

            }





            function resetDate(data){
              data.parent().parent().parent().find('input[type=text]').hide().val('');
            }






            /*Form submission Handling starts*/

            $('#invest-form').on('submit',function(e){

                  e.preventDefault();


                   var balance_amount = parseInt($('#total-inv-amount>span').html()) % 1000;
                   var total_amount = parseInt($('#total-inv-amount>span').html());

                   if (total_amount >= 100000) {
                      $('#chooseBankModal').modal('show');
                      getBankAccount();
                   }else{
                    alert('Investment Amount should be greater than 1 Lakh ');
                   }

                  
    
            });


            function getBankAccount(){

              var dataString = '';
                $.ajax({
                    type: 'GET',
                    url: '/get_bank_accounts',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: dataString,
                    success:function(data){
                      if (data.msg == "success") {
                        $('.account-holder').remove();
                        $(data.response).each(function(k,v){
                            $('#bank-account-form').prepend(

                                '<div class="col-lg-6 col-md-6 col-sm-12">'+
                                     '<div class="account-holder col-xs-12 p-lr-zero">'+
                                        '<div class="col-lg-3 col-md-3 col-sm-3">'+
                                            '<input type="radio" name="bank" id="'+v.id+'" value="'+v.id+'">'+
                                            '<label for="'+v.id+'"><i class="material-icons">radio_button_unchecked</i></label>'+
                                        '</div>'+
                                        '<div class="col-lg-9 col-md-9 col-sm-9 text-left p-l-zero">'+
                                            '<p class="bank-name">'+v.acc_name+'</p>'+
                                            '<p class="bank-acc-no">'+v.acc_no+'</p>'+
                                        '</div>'+
                                     '</div> '+
                                '</div>'
                                )


                        });
                      }
                    },
                    error:function(){ 
                        
                    }
                });
                //investNow();
            }


                // function showPaymentInfo(response){
                //   $('#modal-header').text('Make Investment Via');

                //   var mandate_body = '<form id="payment-type-form">'+
                //       '<div class="row">'+
                //           '<div class = "col-lg-12 col-md-12 col-sm-12 payment-container opa-5">'+
                //               '<div class = "col-lg-3 col-md-3 col-sm-3 p-r-zero">'+
                //                   '<label for="neft"><i class="material-icons">radio_button_unchecked</i></label>'+
                //                   '<input type="radio" name="payment" id="neft" value = "1" disabled>'+
                //               '</div>'+
                //               '<div class = "col-lg-9 col-md-9 col-sm-9 p-l-zero">'+
                //                   '<p class="payment-info">Internet Banking (NEFT)</p>'+
                //               '</div>'+
                //           '</div>    '+
                //           '<div class = "col-lg-12 col-md-12 col-sm-12 payment-container">'+
                //               '<div class = "col-lg-3 col-md-3 col-sm-3 p-r-zero">'+
                //                   '<label for="mandate"><i class="material-icons">radio_button_unchecked</i></label>'+
                //                   '<input type="radio" name="payment" id="mandate" value = "2">'+
                //               '</div>'+
                //               '<div class = "col-lg-9 col-md-9 col-sm-9 p-l-zero">'+
                //                   '<p class="payment-info">Bank Mandate</p>'+
                //                   '<p id="mandate-info">Payment via mandate can be enable only after submitting the mandate to Bombay Stock Exchange. It takes 30 working days to get approved.</p>'+
                //               '</div>'+
                //           '</div> '+
                //           '<input type="submit" id = "payment-submit" name="payment-submit" class="btn btn-primary payment-btn center-block" value="Invest" />'+
                //       '</div>'+
                //   '</form>';


                //   var neft_body = '<form id="payment-type-form">'+
                //       '<div class="row">'+
                //           '<div class = "col-lg-12 col-md-12 col-sm-12 payment-container">'+
                //               '<div class = "col-lg-3 col-md-3 col-sm-3 p-r-zero">'+
                //                   '<label for="neft"><i class="material-icons">radio_button_unchecked</i></label>'+
                //                   '<input type="radio" name="payment" id="neft" value = "1">'+
                //               '</div>'+
                //               '<div class = "col-lg-9 col-md-9 col-sm-9 p-l-zero">'+
                //                   '<p class="payment-info">Internet Banking (NEFT)</p>'+
                //               '</div>'+
                //           '</div>    '+
                //           '<div class = "col-lg-12 col-md-12 col-sm-12 payment-container opa-5">'+
                //               '<div class = "col-lg-3 col-md-3 col-sm-3 p-r-zero">'+
                //                   '<label for="mandate"><i class="material-icons">radio_button_unchecked</i></label>'+
                //                   '<input type="radio" name="payment" id="mandate" value = "2" disabled>'+
                //               '</div>'+
                //               '<div class = "col-lg-9 col-md-9 col-sm-9 p-l-zero">'+
                //                   '<p class="payment-info">Bank Mandate</p>'+
                //                   '<p id="mandate-info">Payment via mandate can be enable only after submitting the mandate to Bombay Stock Exchange. It takes 30 working days to get approved.</p>'+
                //               '</div>'+
                //           '</div> '+
                //           '<input type="submit" name="payment-submit" class="btn btn-primary payment-btn center-block" value="Invest" />'+
                //       '</div>'+
                //   '</form>';


                //   $('#modal-body').empty();

                //   if (response.msg == "approved") {
                //      $('#modal-body').append(mandate_body);
                //      $('#mandate-info').text('');
                //      $('#infoModal').css({'z-index' : '100000'});
                //      $('#infoModal').modal('show'); 

                //   }

                //   if (response.msg == "not_approved") {
                //      $('#modal-body').append(neft_body);
                //      $('#infoModal').css({'z-index' : '100000'});
                //      $('#infoModal').modal('show');

                //   }
                // }


            $(document).on('change','input[name=payment]',function(){
                //alert('chanhing');
                $(document).find('#payment-alert').hide();
            });


            $(document).on('submit','#bank-account-form',function(e){
                  //$('#infoModal').css({'z-index' : '100000'});
                  e.preventDefault();

                  //$('#payment-submit').prop('disabled',true);
                  //var portfolio = $("#port-type").val();
                  var radio = $(this).find('input[name=bank]:checked');
                  var checked_radio = radio.length;

                  if(checked_radio == 0){
                    //console.log(checked_radio);
                    $('#modal-body').append('<p id="payment-alert">Kindly Select the Payment Mode</p>')
                  }else{

                    var bank_id = radio.val();
                        customInvestNow(bank_id);
                  }
            });



            function handleErrors(errors){
              $('#modal-body').empty();
              $('#modal-header').html('');
              $.each(errors,function(key,value){
                  $('#modal-body').append(
                      '<img src="icons/failed-tick.png" class="center-block noti-image">'+
                      '<p class="text-center general-info">'+value+'</p>'
                    );
              });
              $('#infoModal').css({'z-index' : '100000'});
              $('#infoModal').modal('show');
            }


            function handleResponse(response){
              if (response.msg == "success") {

              }

              if (response.msg == "fail") {

              }
            }



                function customInvestNow(bank_id) {
                  //console.log("inside custom invest function");
                    var code_amount = [];
                    var myString = '';
                    $('.custom-amount').each(function() {
                        var amount = $(this).val();
                        var code = $(this).attr('id');
                        var scheme_type = $(this).data('scheme');
                        if (amount == 0) {
                            code_amount.push({
                                'code': code,
                                'amount': 0,
                                'scheme': scheme_type
                            })
                        } else {

                            if(amount<100000){
                              console.log('.custom-amount #'+code)

                              $('#'+code).find('.alert-low-amount').text('Min Investment Amount is 1 Lakhs').css({'display':'block'});
                              $('#chooseBankModal').modal('hide');
                              $('html, body').animate({
                                  scrollTop: $("#"+code).offset().top
                              }, 500);
                               throw new Error('This is not an error. This is just to abort javascript');
                            }else{
                              code_amount.push({
                                'code': code,
                                'amount': amount,
                                'scheme': scheme_type
                              })
                            }
                            
                        }
                        myString = JSON.stringify(code_amount)
                    });

                    balance_amount = parseInt($('#total-inv-amount>span').html()) % 1000;
                    var total_amount = parseInt($('#total-inv-amount>span').html());
                    if (balance_amount == 0 && total_amount > 0) {
                        var formData = new FormData();
                        formData.append('code_amount', myString);
                        formData.append('bank_id', bank_id);

                        console.log(formData);
                        $.ajax({
                            type: "POST",
                            url: "/invest_funds",
                            data: formData,
                            cache: !1,
                            contentType: !1,
                            processData: !1,
                            beforeSend: function() {},
                            success: function(data) {
                                $('#chooseBankModal').modal('hide');
                                if (data.msg == "success") {
                                  $('.modal-res-text').text('Your Investment has been successfully Scheduled');
                                  $('.response-image').attr('src','/icons/success-tick.svg');
                                  $('.modal-res-header').removeClass('failure-body').addClass('success-body');
                                  $('#infoModal .modal-body').find('.popup-btn').remove();

                                  if(data.bank_type == 1){
                                      //Retail Banking Where Both RTGS and NET Banking Should be shown.

                                      $('#infoModal .modal-body').append(
                                          '<form action="/manage_payment" class="payment-form" target="_blank" method="POST">'+
                                                '<input type = "hidden" name="p_type" value="nb">'+
                                                '<input type = "hidden" name="inv_id" value="'+data.id+'">'+
                                                '<input type="hidden" name="_token" value="'+$('meta[name="csrf-token"]').attr('content')+'"> '+
                                                '<input type="hidden" name="payment_type" value="net_banking">'+
                                                '<button type = "submit" class = "btn btn-primary center-block popup-btn grad-btn payment-btn" data-ptype = "nb" data-invid ="'+data.id+'"  id ="netbanking-btn">Pay Via Net Banking</button>'+
                                          '</form>'+


                                          '<form action="/manage_payment" class="payment-form" target="_blank" method="POST">'+
                                                '<input type = "hidden" name="p_type" value="rtgs">'+
                                                '<input type = "hidden" name="inv_id" value="'+data.id+'">'+
                                                '<input type="hidden" name="_token" value="'+$('meta[name="csrf-token"]').attr('content')+'"> '+
                                                '<input type="hidden" name="payment_type" value="rtgs">'+
                                                '<button type = "submit" class = "btn btn-primary center-block popup-btn grad-btn payment-btn" data-ptype = "nb" data-invid ="'+data.id+'"  id ="netbanking-btn">Pay Via RTGS/NEFT</button>'+
                                          '</form>'

                                          // '<button type = "button" class = "btn btn-primary center-block popup-btn grad-btn payment-btn" data-ptype = "rtgs" data-invid ="'+data.id+'" id ="neft-btn">Pay Via RTGS/NEFT</button>'

                                      )
                                  }else{
                                      //Coporate Banking Where only RTGS should be shown.

                                      $('#infoModal .modal-body').append(


                                          '<form action="/manage_payment" class="payment-form" target="_blank" method="POST">'+
                                                '<input type = "hidden" name="p_type" value="rtgs">'+
                                                '<input type = "hidden" name="inv_id" value="'+data.id+'">'+
                                                '<input type="hidden" name="_token" value="'+$('meta[name="csrf-token"]').attr('content')+'"> '+
                                                '<input type="hidden" name="payment_type" value="rtgs">'+
                                                '<button type = "submit" class = "btn btn-primary center-block popup-btn grad-btn payment-btn" data-ptype = "nb" data-invid ="'+data.id+'"  id ="netbanking-btn">Pay Via RTGS/NEFT</button>'+
                                          '</form>'

                                          // '<button type = "button" class = "btn btn-primary center-block popup-btn grad-btn payment-btn" data-ptype = "rtgs" data-invid ="'+data.id+'" id ="neft-btn">Pay Via RTGS/NEFT</button>'

                                      )

                                  }


                                  $('.response-image').css({'top':'10%'});
                                  
                                }else{
                                  $('.modal-res-text').text('Your Investment Cannot be scheduled Rightnow.');
                                  $('.response-image').attr('src','/icons/failed-tick.svg');
                                  $('.modal-res-header').removeClass('success-body').addClass('failure-body');
                                  $('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');
                                }

                                $('.modal-footer button').hide();
                                $('#infoModal').modal('show')
                            },
                            error: function(xhr, status, error) {},
                        })
                    } else {

                        //console.log(parseInt($('#total-inv-amount>span')));

                        $('#modal-body').empty();
                        $('#modal-header').text('Oops!');
                        
                        if ($('#total-inv-amount>span').text() == '0') {
                            // response += 'Investment amount is required' + '<br>'
                            //console.log('zero total');
                            $('#modal-body').append('<p class="text-center general-info">Investment Amount is Required</p>');

                        }
                        if (balance_amount != 0) {
                            // response += 'Investment amount should in multiples of thousands.' + '<br>'
                            $('#modal-body').append('<p class="text-center general-info">Investment amount should in multiples of thousands.</p>');
                        }

                        
                       
                        $('#infoModal').modal('show')

                    }
                }


                $(document).on('submit','.payment-form', function(){
                    $('#infoModal').modal('hide');
                    $('#invest-form')[0].reset();
                    $('#total-inv-amount span').text('0');
                });


                function handleOnetimeResponse(data){
                  var portfolio_type = $('#port-type').val();

                  if (data.msg == "success") {

                      // $('#modal-body').empty();
                      // $('#modal-header').html('Your Order Placed Successfully');
                      // $('#modal-body').append(
                      //   '<img class = "center-block modal-img" src = "icons/success-tick.png">'+
                      //   '<p class = "text-center general-info">'+data.status+' of '+
                      //     '<span></span>'+
                      //     '<span class="blue-text">Rs. ' + data.inv_amount + '</span> in '+
                      //     // '<span class="blue-text">' + portfolio_type + '</span> '+
                      //     '<span class="blue-text">'+portfolio_type+'</span> portfolio on <span class="blue-text">' + data.date + '</span>'+
                      //   '</p>'+
                      //   '<p class="general-info">Payment link will be sent to your email with in 24 hours.</p>'+
                      //   '<button type = "button" class = "btn btn-primary center-block popup-btn" onclick="redirect()">Okay</button>'
                        
                      //   );
                      
                      // //$('#you-have').html(data.status + ' of <span class="blue">Rs. ' + data.inv_amount + '</span> in <span class="blue">' + portfolio_type + '</span> portfolio on <span class="blue">' + data.date + '</span>');
                      // //$('#investment_id').val(data.id);
                      // $('#infoModal').css({'z-index' : '100000'});
                      // $('#infoModal').modal('show');

                      $('.modal-res-text').text('Your Investment has been successfully Scheduled');
                      $('.response-image').attr('src','/icons/success-tick.svg');
                      $('.modal-res-header').removeClass('failure-body').addClass('success-body');
                      $('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn');
                      //console.log();

                  }else{
                      $('.modal-res-text').text('Your Investment Cannot be scheduled Rightnow.');
                      $('.response-image').attr('src','/icons/failed-tick.svg');
                      $('.modal-res-header').removeClass('success-body').addClass('failure-body');
                      $('.modal-footer').find('button').removeClass('success-btn').addClass('failure-btn');
                  }
                }


                function handleCustomResponse(data){
                  var portfolio_type = $('#port-type').val();
                  if (portfolio_type == "own") {
                    portfolio_type = 'Custom';
                  }
                 
                  if (data.msg == 'success') {
                      $('#modal-body').empty();
                      $('#modal-header').html('Your Order Placed Successfully');
                      $('#modal-body').append(
                        '<img class = "center-block modal-img" src = "icons/success-tick.png">'+
                        '<p class = "text-center general-info">'+data.status+' of '+
                          '<span></span>'+
                          '<span class="blue-text">Rs. ' + data.inv_amount + '</span> in '+
                          // '<span class="blue-text">' + portfolio_type + '</span> '+
                          'Custom portfolio on <span class="blue-text">' + data.date + '</span>'+
                        '</p>'+
                        '<p class="general-info">Payment link will be sent to your email with in 24 hours.</p>'+
                        '<button type = "button" class = "btn btn-primary center-block popup-btn" onclick="redirect()">Okay</button>');
                      
                      //$('#you-have').html(data.status + ' of <span class="blue">Rs. ' + data.inv_amount + '</span> in <span class="blue">' + portfolio_type + '</span> portfolio on <span class="blue">' + data.date + '</span>');
                      $('#investment_id').val(data.id);
                      $('#infoModal').css({'z-index' : '100000'});
                      $('#infoModal').modal('show')
                  } else if(data.msg == "min_fail"){

                          var error_id;
                      $.each(data.response,function(key,value){
                          error_id = value.code;
                          //console.log(value.code);
                          //console.log(value.min_amt);
                        $('.custom-amount[data-schemeid='+value.code+']').css({'border-bottom' : '1px solid #FD353A'});
                        $('.custom-amount[data-schemeid='+value.code+']').parent().parent().append(
                          '<p class="min-danger">Min Amt - '+value.min_amt+'</p>').css({'width':'77%'});
                        //$('#'+value.code).css({'border-bottom' : '1px solid red'});
                      });

                      $('#infoModal').modal('hide');
                      $('html, body').animate({
                          scrollTop: $("#"+error_id).offset().top
                      }, 500);

                  }else {
                      $('#modal-body').empty();
                      $('#modal-header').html('');
                      //console.log(data);
                      if (data.hasOwnProperty('status')) {
                          //$('#you-have').html(data.status)
                          $('#modal-body').append('<p class="text-center general-info">'+data.status+'</p>');

                      }
                       else {
                          var response = '';
                          //console.log(data);
                          if (data.errors.hasOwnProperty('inv_date')) {
                              $('#modal-body').append('<img src="icons/failed-tick.png" class="center-block noti-image">'+
                                '<p class="text-center general-info">'+data.errors.inv_date+'</p>');
                          }
                          if (data.errors.hasOwnProperty('inactive')) {
                              $('#modal-body').append('<img src="icons/failed-tick.png" class="center-block noti-image">'+
                                '<p class="text-center general-info">'+data.errors.inactive+'</p>');
                          }
                          if (data.errors.hasOwnProperty('inv_type')) {                              
                              $('#modal-body').append('<img src="icons/failed-tick.png" class="center-block noti-image">'+
                                '<p class="text-center general-info">'+data.errors.inv_type+'</p>');

                          }
                          if (data.errors.hasOwnProperty('code_amount')) {                              
                              $('#modal-body').append('<img src="icons/failed-tick.png" class="center-block noti-image">'+
                                '<p class="text-center general-info">'+data.errors.code_amount+'</p>');
                          }
                          //$('#you-have').html(response)
                      }
                      $('#infoModal').css({'z-index' : '100000'});
                      $('#infoModal').modal('show')
                  }
                }


            $(document).on('click','.arrow-down',function(){
              if ($(this).hasClass('collapsed')) {
                $(this).text('keyboard_arrow_down');
              }
              else{
                $(this).text('keyboard_arrow_up');
              }
            })  


           $('.port-select').on('click',function(){
                    var val;
                    $('.portfolio-menu').html($(this).text()+'<span><i id="port-type-icon" class="material-icons">keyboard_arrow_down</i></span>');
                    if ($(this).text() == "Conservative") {
                        val = 'Conservative';
                    }
                    if ($(this).text() == "Moderate") {
                        val = 'Moderate';
                    }
                    if ($(this).text() == "Aggressive") {
                        val = 'Aggressive';
                    }
                    if ($(this).text() == "Tax Saver") {
                        val = 'Tax Saver';
                    }
                    if ($(this).text() == "Custom") {
                        val = 'own';
                    }
                    $('#port-type').val(val);
                    changePortType( $('#port-type').val());
                    //    alert($('#port-type').val());


                });


                $('.sip-dur-select').on('click',function(){
                    var val;
                    $('.sip-duration-menu').html($(this).text()+'<span><i id="port-type-icon" class="material-icons">keyboard_arrow_down</i></span>');
                    if ($(this).text() == "1 Year") {
                        val = '1';
                    }
                    if ($(this).text() == "3 Years") {
                        val = '3';
                    }
                    if ($(this).text() == "5 Years") {
                        val = '5';
                    }
                    if ($(this).text() == "10 Years") {
                        val = '10';
                    }
                    if ($(this).text() == "20 Years") {
                        val = '20';
                    }
                    if ($(this).text() == "25 Years") {
                        val = '25';
                    }
                    if ($(this).text() == "30 Years") {
                        val = '30';
                    }
                    $('#sip-duration').val(val);
                  

                });

                $('.sip-date-select').on('click',function(){
                    var val;
                    console.log($(this).text());
                    $('.sip-date-menu').html($(this).text()+'<span><i id="port-type-icon" class="material-icons">keyboard_arrow_down</i></span>');
                    if ($(this).text() == "5th") {
                        val = '05';
                    }
                    if ($(this).text() == "10th") {
                        val = '10';
                    }
                    if ($(this).text() == "15th") {
                        val = '15';
                    }
                    if ($(this).text() == "20th") {
                        val = '20';
                    }
                    if ($(this).text() == "25th") {
                        val = '25';
                    }
                    $('#sip-date').val(val);

                    //alert($('#sip-date').val());
                  

                });


           $('.scheme-container').on('blue',function(){
              $(this).removeClass('border-blue-left');
           })

           $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });


           $(document).ajaxStart(function(){//console.log('into loader');

              //$('html').css({'pointer-events':'none'});
              $('section,nav').css({'opacity' : '0.55'});
              $(".loader").css("display", "block");
          });
          $(document).ajaxComplete(function(){//console.log('out from loader');

              $('section,nav').css({'opacity' : '1'});
              $(".loader").css("display", "none");
              //$('html').css({'pointer-events':'all'});
          });



        $(document).on('click','.close-pn',function(e){
            e.preventDefault();
            $.ajax({
                type: "GET",
                url: "/handle_payment",
                data: null,
                cache: !1,
                async:false,
                contentType: !1,
                processData: !1,
                beforeSend: function() {},
                success: function(data) {
                    if (data.msg == "success") {
                      $('#showPaymentInfo').modal('hide');
                    }else{
                      $('#showPaymentInfo').modal('hide');
                    }
                },
                error: function(xhr, status, error) {},
            })

          });

          function infoPopUp(){
              $('#showInfo').modal('show');
          }

          $('#mandate-pay').on('click',function(){
              $('#showInfo').modal('hide');
              $('#infoModal').modal('hide');
              window.location.href = 'https://www.liquidplus.in/';
          });

        function getMandatelink(){
          $('#infoPopUp').modal('hide');
          $.ajax({
            type: "GET",
            url: "/get_mandate_link",
            async:false,
            success: function(data){
              if (data.msg == 1) {
                  if(data.mandate_url != null){
                    window.location.replace("http://www.liquidplus.in");
                    var win = window.open(data.mandate_url, '_blank');
                    win.focus();  
                  }else{
                    infoPopUp()
                  }
                  
              }else if (data.msg == 2){
                infoPopUp()
              }
            },
            error: function(xhr,status,error){
              alert(error);
            }
          });
        }


  // $(document).on('click','.payment-btn',function(){
  //     var inv_id = $(this).data('invid');
  //     var p_type = $(this).data('ptype');
  //
  //     // var dataString = 'inv_id='+inv_id+'&p_type=' +p_type;
  //
  //     if(p_type != 'rtgs'){
  //       triggerOrders(p_type, inv_id, 'net_banking')
  //     }else{
  //       // $('#utrModal').modal('show')
  //       triggerOrders(p_type, inv_id, 'rtgs')
  //     }
  //
  // })

  // $(document).on('click','#confirm-utr',function(){
  //     utr_no = $('#utr-no').val();
  //     if(utr_no.length <= 10){
  //       $('#utr-no').next().text('Please enter correct UTR No');
  //     }else{
  //       var neft_btn = $(document).find('#neft-btn');
  //       var inv_id = neft_btn.data('invid');
  //       var p_type = neft_btn.data('ptype');
  //       // var dataString = 'inv_id='+inv_id+'&p_type=' +p_type+'&utr_no='+utr_no;
  //
  //       triggerOrders(p_type, inv_id, utr_no)
  //     }
  // });

  function triggerOrders(p_type, inv_id, payment_type){
      // if (utr_no == null) {
        var dataString = 'inv_id='+inv_id+'&p_type=' +p_type+'&payment_type='+payment_type;
      // }else{
      //   var dataString = 'inv_id='+inv_id+'&p_type=' +p_type+'&utr_no='+utr_no;
      // }

      $.ajax({
          type: "POST",
          url: "/manage_payment",
          data: dataString,
          async:false,
          beforeSend: function() {},
          success: function(data) {
              if(p_type == 'nb'){
                if(data.msg == 1){
                  redirect(inv_id)
                }
              }else if (p_type == 'mandate') {
                if(data.msg == 1){
                  if(data.mandate_url != null){
                    window.location.replace("http://www.liquidplus.in");
                    var win = window.open(data.mandate_url, '_blank');
                    win.focus();  
                  }else{
                    infoPopUp()
                  }
                }else{
                  alert('Payment Link will be sent to your email with in 24hours. Kindly make the payment through the link');
                }
              }else if (p_type == 'rtgs') {
                if(data.msg == 1){
                  $('#utrModal,#infoModal').modal('hide');
                  $('#rtgsInfoModal').modal('show');
                  // alert('Order has been placed successfully and under process. you can check the status at the end of the day in Order status tab of Account statement section');
                }
              }
          },
          error: function(xhr, status, error) {},
      })
  }

  $('#utr-no').on('keyup',function(){
    $(this).next().text('');
  })


});

