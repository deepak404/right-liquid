            $(document).ready(function(){



                $('.withdraw-link').on('click', function(e){

                    console.log($(this).data('accno'));
                    e.preventDefault();
                    // alert($(this).data('pan'));


                    var currentTime = new Date();

                    var currentOffset = currentTime.getTimezoneOffset();

                    var ISTOffset = 330;   // IST offset UTC +5:30

                    var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);

                    // ISTTime now represents the time in IST coordinates

                    var hoursIST = ISTTime.getHours();
                    var minutesIST = ISTTime.getMinutes();

                    if(hoursIST > 13 && hoursIST < 15){

                        $('#infoModal .modal-res-text').text('Pending Orders will be executed between 1:00 PM and 3:00 PM. Kindly place your order after 3:00 PM');
                        // alert('Pending Order will be executed between 1:00 PM and 3:00 PM. Kindly place your order after 3:00 PM');
                        $('#infoModal').modal('show');
                    }else{
                        var data = 'pan='+$(this).data('pan')+'&acc_no='+$(this).data('accno');

                        $.ajax({
                            type: 'POST',
                            url: 'get_withdraw_details',
                            data: data,
                            // async:false,
                            success:function(data){
                                if(data.msg == true){
                                    showWithdrawDetails(data);
                                }
                            },
                            error:function(){

                            }
                        });
                    }


                });


                function showWithdrawDetails(data){

                    $('.withdraw-info-amount span').text('0');
                    $('.withdraw-table tbody').empty();


                    $.each(data.portfolio, function(k,v){
                        var tr = '<tr class="border-bot" id="'+k+'">'+
                            '<td>'+
                            '<p class="withdraw-fund-name">'+v.scheme_name+'</p>'+
                            '<p class="current-invested">Permitted Withdraw - Rs. <span id="'+k+'">'+formatIndianCurrency(v.current_value.toFixed(2))+'</span></p>'+
                            // '<p class="invested-bank current-invested">Invested through - <strong>'+v.bank_name+'</strong></p>'+
                            '<p class="exit-load-warning"></p>'+
                            '</td>'+
                            '<td>'+
                            '<input type="text" name="'+k+'" id="'+k+'" data-pan="'+v.pan+'" data-accno="'+v.acc_no+'" class="input-field custom-amount num-field" placeholder="Enter Amount">'+
                            '</td>'+
                            '<td>'+
                            '<input type="checkbox" class="full-amount" data-scheme="'+k+'" id="checkbox-'+v.pan+'-'+k+'" name="">'+
                            '<label for="checkbox-'+v.pan+'-'+k+'"><i class="material-icons">check_box_outline_blank</i></label>'+
                            '<span>Full Amount</span>'+
                            '</td>'+
                            '</tr>';

                        $('.withdraw-table tbody').append(tr);
                    });


                    $.each(data.withdraw, function(k,v){

                        var current_value = $(document).find('tr[id='+k+'] td:first-child').find('.current-invested span').text();
                        current_value = removeCurrencyFormat(current_value);
                        var permitted_withdraw = formatIndianCurrency((current_value - v.amount).toFixed(2));


                        $(document).find('tr[id='+k+'] td:first-child').find('.current-invested span').text(permitted_withdraw);
                        // $(document).find('tr[id='+k+'] td:first-child').append('<p class="pending-withdraw">Pending Withdraw - Rs. '+v.amount+'</p>');
                    });

                    $.each(data.arb_with_exit_load, function(k,v){
                        $(document).find('tr[id='+k+'] td:first-child').find('.exit-load-warning').text('Exit Load is applicable to Rs. '+formatIndianCurrency(v.amount.toFixed(2)));
                    });


                    $('#withdrawModal').modal('show');

                }


                function formatIndianCurrency(x){

                    x=x.toString();
                    var afterPoint = '';
                    if(x.indexOf('.') > 0)
                        afterPoint = x.substring(x.indexOf('.'),x.length);
                    x = Math.floor(x);
                    x=x.toString();
                    var lastThree = x.substring(x.length-3);
                    var otherNumbers = x.substring(0,x.length-3);
                    if(otherNumbers != '')
                        lastThree = ',' + lastThree;
                    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;

                    return res;
                }

                function removeCurrencyFormat(x){
                    x = x.split(",").join("")
                    return x;
                }



                $count = 0;
                var wd_amount;
                var current_value;

                $(document).on('change','input[type="checkbox"]',function(){
                    if ($(this).prop('checked')) {
                        $(this).parent().find('i').text('check_box').addClass('green');
                        //$count++;
                        //console.log("Checked".$count);
                    }else{
                        $(this).parent().find('i').text('check_box_outline_blank').removeClass('green');
                    }
                });


                $(document).on('change','.full-amount',function(){

                    if ($(this).prop('checked')) {
                        var scheme_code = $(this).data('scheme');
                        var current_inv = removeCurrencyFormat($(this).closest('tr').find('span#'+scheme_code+'').text());
                        $(this).closest('tr').find('.custom-amount').val(parseFloat(current_inv)).prop('disabled',true);
                        amountAddition()
                        makeEmpty();
                    }else{
                        $(this).closest('tr').find('.custom-amount').val('').prop('disabled',false);
                        amountAddition()
                        makeEmpty();
                    }
                });


                $(document).on('keyup', '.custom-amount', function() {
                    amountAddition()
                    makeEmpty();
                
                    
                });

                function amountAddition(){
                    var total = 0;
                    $('.custom-amount').each(function() {
                        var enter_amt = $(this).val();
                        if (enter_amt.length !== 0) {
                            total += parseFloat(enter_amt);
                            //console.log(total);
                            if (total == '') {
                                //console.log("empty");
                            }
                            $('.withdraw-info-amount > span').text(total.toFixed(2));
                        }
                    });
                }

                function makeEmpty(){
                    //console.log();
                    var total_input = $('.custom-amount').length;
                    var empty_amount = 0;
                    $('.custom-amount').each(function() {
                        var enter_amt = $(this).val();
                        if (enter_amt.length == 0) {
                            
                            empty_amount++;
                        }
                        if (total_input == empty_amount) {
                            $('.withdraw-info-amount > span').text('0');
                        }
                    });
                }


                $('#withdrawal-form').on('submit',function(e){
                    $('#withdraw-btn').prop('disabled',true);
                    var data = 'withdraw_funds=';
                    e.preventDefault();
                    var total = parseFloat(0);
                    var full_or_not = '';

                    $('.custom-amount').each(function() {
                        var wd_amount = removeCurrencyFormat($(this).val());
                        if (wd_amount != '') {
                            
                            var scheme_code = $(this).attr('id');
                            var pan = $(this).data('pan');
                            var acc_no = $(this).data('accno');

                            var current_value = removeCurrencyFormat($(this).closest('tr').find('span#'+scheme_code+'').text());


                            console.log(parseFloat(wd_amount),parseFloat(current_value));
                            if (parseFloat(wd_amount) > parseFloat(current_value)) {
                                //console.log(wd_amount);
                                handleErrors(error_type = "cv");
                            }
                            if(wd_amount < 999){
                                //console.log(wd_amount);
                                handleErrors(error_type = "1000");
                            }else{

                                if (parseFloat(current_value) == parseFloat(wd_amount)) {
                                    full_or_not = "f";
                                }else{
                                    full_or_not = "nf";
                                }
                                console.log(parseFloat(wd_amount), parseFloat(current_value));
                                total += parseFloat(wd_amount);
                                data += scheme_code +'/'+wd_amount+'/'+full_or_not+'/'+pan+'/'+acc_no+'|';
                                
                            }

                        }                        
                    });

                    //console.log(total);
                    if (total > 0 && total > 999) {

                        //console.log(wd_amount,current_value);
                        if (parseFloat(wd_amount) > parseFloat(current_value)) {
                            //console.log(wd_amount);
                                handleErrors(error_type = "cv");
                        }else{
                            //var response = ajaxCalls('POST','withdraw_user_funds',data);
                            ////console.log(response);
                            
                            //console.log(data);


                            $.ajax({
                                type: 'POST',
                                url: 'withdraw_user_funds',
                                data: data,
                                //async : false,
                                success:function(data){
                                  if (data.msg == "success") {
                                    // $('.modal-res-text').text('Your Withdraw has been Scheduled Successfully');
                                    // $('.response-image').attr('src','/icons/success-tick.svg');
                                    // $('.modal-res-header').removeClass('failure-body').addClass('success-body');
                                    // $('.modal-footer').find('button').removeClass('failure-btn').addClass('success-btn').attr('onclick',"javascript:location.href='/home'");
                                      var count = 1;
                                      $.each(data.response, function (k,v){
                                        $.each(v, function (key, value) {

                                            $('#withdrawResponseModal').find('tbody').append(
                                                '<tr>'+
                                                '<td>'+count+++'</td>'+
                                                '<td>'+value.scheme_name+'</td>'+
                                                '<td>'+value.amount+'</td>'+
                                                '<td>'+value.status+'</td>'+
                                                '</tr>'
                                            );
                                        });
                                      });
                                      $('#withdrawModal').modal('hide');
                                      $('#infoModal').modal('hide');
                                    $('#withdrawResponseModal').modal('show');

                                  }else{
                                    $('.modal-res-text').text('Cannot Withdraw Right now. Please Try again Later');
                                    $('.response-image').attr('src','/icons/failed-tick.svg');
                                    $('.modal-res-header').removeClass('success-body').addClass('failure-body');
                                    $('.modal-footer').find('button').attr('onclick',"javascript:location.href='/home'");
                                      $('#infoModal').modal('show');

                                    }

                                },
                                error:function(){ 
                                    
                                }
                            });
                        }
                        
                    }else{
                        //console.log('blah');
                        // console.log('hello');

                        handleErrors(error_type = "ngt1000");

                    }

                    
                    $('#withdraw-btn').prop('disabled',false);

                });

                    $('.custom-amount').on('keydown',function(e){
                            if (e.shiftKey) {
                                e.preventDefault();
                            }

                            var scheme_code = $(this).attr('id');

                            //console.log(parseFloat($(this).closest('tr').find('span#'+scheme_code+'').text()));
                            //console.log($(this).val());
                            if ($(this).val() > parseFloat($(this).closest('tr').find('span#'+scheme_code+'').text())) {
                                if (e.keyCode == 8 || e.ctrlKey === true || e.metaKey === true || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40) {
                                return true;
                            }else{
                                return false;
                            }                            
                            }
                    });


                function handleErrors(data){
                    $('#modal-body').empty();
                    $('#modal-header').text('');
                    //console.log(data);
                    if (data == "1000") {
                        $('#modal-body').append(
                            '<p class="text-center general-info">Withdrawal Amount should be greater than 1000<p>'
                            );
                    }
                    if (data == "cv") {
                        $('#modal-body').append(
                            '<p class="text-center general-info">Withdrawal Amount cannot be greater than the Current Value<p>'
                            );
                    }
                    if (data == "ngt1000") {
                        // console.log('hello ahaa');
                        $('#infoModal').find('.response-image').attr('src','/icons/failed-tick.svg');
                        $('#infoModal').find('.modal-res-text').text('Minimal Withdrawal Amount is 1000');
                        // $('#modal-body').append(
                        //     '<p class="text-center general-info">Minimal Withdrawal Amount is 100000<p>'
                        //     );
                    }

                    $('#infoModal').modal('show');
                }

                function handleResponse(data){
                    //console.log("inside Handle Response");
                    $('#modal-body').empty();
                    //console.log(data);
                    if (data.msg == "success") {
                        $('#modal-header').text('');
                         $('#modal-body').append(
                            '<img class = "center-block modal-img" src = "icons/success-tick.png">'+
                            '<p class="text-center general-info">'+data.status+' of Rs. <span class = "blue-text">'+data.amount+'</span> on <span class="blue-text">'+data.date+'</span><p>'+
                            '<button type = "button" class = "btn btn-primary center-block popup-btn" onclick="redirect()">Okay</button>');
                         $('#infoModal').modal('show');
                    }
                    if (data.msg == "failure") {
                        $('#modal-header').text('');
                         $('#modal-body').append(
                            '<img class = "center-block modal-img" src = "icons/failed-tick.png">'+
                            '<p class="text-center general-info">'+data.status+'<p>'+
                            '<button type = "button" class = "btn btn-primary center-block popup-btn" onclick="redirect()">Okay</button>');

                         $('#infoModal').modal('show');

                    }

                }

                // $('#infoModal').modal('show');

                $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                

                $(document).ajaxStart(function(){////console.log('into loader');

                      //$('html').css({'pointer-events':'none'});
                      $('section,nav').css({'opacity' : '0.55'});
                      $(".loader").css("display", "block");
                  });
                  $(document).ajaxComplete(function(){////console.log('out from loader');

                      $('section,nav').css({'opacity' : '1'});
                      $(".loader").css("display", "none");
                      //$('html').css({'pointer-events':'all'});
                  });


                function removeCurrencyFormat(x){
                    x = x.split(",").join("")
                    return x;
                }

            });