<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{

	protected $fillable = [
        'acc_name', 'acc_no', 'ifsc_code','acc_type','user_id','acc_count','bank_name','pan','bank_code', 'bank_type'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function investmentDetails(){
        return $this->hasMany('App\InvestmentDetails','pan');
    }
}
