<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{

//	protected $fillable = ['aoa','moa','bs','asl','br','ap','coi','kyc','pan_card','id_proof','user_id'];

    protected $fillable = ['kyc','cc','user_id'];

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
