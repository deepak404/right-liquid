<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile','role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];


    public function bankDetails(){
        return $this->hasMany('App\BankDetails');
    }

    public function documents(){
        return $this->hasOne('App\Documents');
    }


    public function portfolioDetails(){
        return $this->hasMany('App\PortfolioDetails');
    }

    public function investmentDetails(){
        return $this->hasMany('App\InvestmentDetails');
    }

    public function WithdrawDetails(){
        return $this->hasMany('App\WithdrawDetails');
    }
}
