<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemeHistory extends Model
{
    protected $table = 'scheme_history';
}
