<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioDetails extends Model
{

    protected $fillable = [
        'portfolio_status'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }


    public function investmentDetails(){
    	return $this->belongsTo('App\investmentDetails');
    }

    public function schemeDetails(){
    	return $this->belongsTo('App\SchemeDetails');
    }

    public function withdrawDetails(){
        return $this->belongsTo('App\PortfolioDetails', 'portfolio_id');
    }
}
