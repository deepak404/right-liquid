<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawDetails extends Model
{
    protected $table = "withdraw_details";

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function SchemeDetails(){

    	return $this->belongsTo('App\SchemeDetails');	
    }

    public function portfolioDetails(){
        return $this->belongsTo('App\PortfolioDetails');
    }
}
