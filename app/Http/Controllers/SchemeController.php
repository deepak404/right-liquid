<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HistoricNav;
use App\SchemeHistory;
use App\DailyNav;
use App\SchemeDetails;
use Carbon\Carbon;

class SchemeController extends Controller
{
    public function showSchemeDetails(Request $request){


    	$request->validate([

    		'scheme_code' => 'required',

    		]);

			//dd('hello');

			$scheme_code = $request['scheme_code'];
			$scheme_details_response = array();
			$scheme_details = SchemeHistory::where('scheme_code',$scheme_code)->get();

			foreach($scheme_details as $scheme_detail) {//Log::info($scheme_detail);



				$nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','desc')->first();

				$current_date = $nav->date;
				$current_nav = $nav->nav;

//				dd($current_date, $current_nav);



				$one_week_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(8)->toDateString())->orderBy('date','desc')->first();

				$one_week_nav  = $one_week_nav['nav'];

				$temp_nav  = $one_week_nav;
                $one_week_diff = date_diff(date_create($current_date), date_create(Carbon::now()->subDays(8)->toDateString()))->days;
                $one_week_nav = round((pow(($current_nav/$one_week_nav), (365/$one_week_diff)) - 1) * 100, 2);

//                dd($current_date, $current_nav, $temp_nav, Carbon::now()->subDays(8)->toDateString(), $one_week_diff, $one_week_nav);


				$thirty_day_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<' ,Carbon::now()->subDays(30)->toDateString())->orderBy('date','desc')->first();
				$thirty_day_nav  = $thirty_day_nav['nav'];
                $thirty_day_diff = date_diff(date_create($current_date), date_create(Carbon::now()->subDays(30)->toDateString()))->days;
                $thirty_day_nav = round((pow(($current_nav/$thirty_day_nav), (365/$thirty_day_diff)) - 1) * 100, 2);




				$ninety_day_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<' ,Carbon::now()->subDays(90)->toDateString())->orderBy('date','desc')->first();
				$ninety_day_nav  = $ninety_day_nav['nav'];
                $ninety_day_diff = date_diff(date_create($current_date), date_create(Carbon::now()->subDays(90)->toDateString()))->days;
                $ninety_day_nav = round((pow(($current_nav/$ninety_day_nav), (365/$ninety_day_diff)) - 1) * 100, 2);



				$six_month_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<' ,Carbon::now()->subDays(181)->toDateString())->orderBy('date','desc')->first();
				$six_month_nav  = $six_month_nav['nav'];
                $six_month_diff = date_diff(date_create($current_date), date_create(Carbon::now()->subDays(181)->toDateString()))->days;
                $six_month_nav = round((pow(($current_nav/$six_month_nav), (365/$six_month_diff)) - 1) * 100, 2);



				$one_year_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<' ,Carbon::now()->subDays(366)->toDateString())->orderBy('date','desc')->first();
				$one_year_nav  = $one_year_nav['nav'];
				$one_year_diff = date_diff(date_create($current_date), date_create(Carbon::now()->subDays(366)->toDateString()))->days;
                $one_year_nav = round((pow(($current_nav/$one_year_nav), (365/$one_year_diff)) - 1) * 100, 2);


				$three_year_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<' ,Carbon::now()->subDays(1096)->toDateString())->orderBy('date','desc')->first();
				$three_year_nav  = $three_year_nav['nav'];
                $three_year_diff = date_diff(date_create($current_date), date_create(Carbon::now()->subDays(1096)->toDateString()))->days;


                if ($three_year_nav == null) {
					$three_year_nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','aesc')->get()->last();
					$last_nav_date = $three_year_nav->date;
					$three_year_nav = $three_year_nav['nav'];
                    $three_year_diff = date_diff(date_create($current_date), date_create($last_nav_date))->days;

                }


                $three_year_nav = round((pow(($current_nav/$three_year_nav), (365/$three_year_diff)) - 1) * 100, 2);




				$five_year_nav = HistoricNav::where('scheme_code',$scheme_code)->where('date','<=' ,Carbon::now()->subDays(1826)->toDateString())->orderBy('date','aesc')->first();
				$five_year_nav  = $five_year_nav['nav'];



                $five_year_diff = date_diff(date_create($current_date), date_create(Carbon::now()->subDays(1826)->toDateString()))->days;

				if ($five_year_nav == null) {

					$five_year_nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','aesc')->get()->last();
					$last_nav_date = $five_year_nav->date;
					$five_year_nav = $five_year_nav['nav'];
                    $five_year_diff = date_diff(date_create($current_date), date_create($last_nav_date))->days;


                }

                $five_year_nav = round((pow(($current_nav/$five_year_nav), (365/$five_year_diff)) - 1) * 100, 2);


                //dd(($current_nav-$five_year_nav)/$five_year_nav);
//				$five_year_nav = round((($current_nav - $five_year_nav)/$five_year_nav)*100,2);



				$scheme_details_response['name'] = $scheme_detail->name;
	            $scheme_details_response['fund_manager'] = $scheme_detail->fund_manager;
	            $scheme_details_response['asset_size'] = number_format($scheme_detail->asset_size, 0,'',',');
	            $scheme_details_response['exit_load'] = $scheme_detail->exit_load;
	            $scheme_details_response['benchmark'] = $scheme_detail->benchmark;
	            $scheme_details_response['launch_date'] = date('d-m-Y', strtotime($scheme_detail->launch_date));
	            $scheme_details_response['investment_plan'] = $scheme_detail->investment_plan;
	            $scheme_details_response['fund_type'] = $scheme_detail->fund_type;

	            // $scheme_details_response['one_day_nav'] = $one_day_nav;
	            // $scheme_details_response['five_day_nav'] = $five_day_nav;
	            $scheme_details_response['one_week_nav'] = $one_week_nav;
	            $scheme_details_response['thirty_day_nav'] = $thirty_day_nav;
	            $scheme_details_response['ninety_day_nav'] = $ninety_day_nav;
	            $scheme_details_response['six_month_nav'] = $six_month_nav;
	            $scheme_details_response['one_year_nav'] = $one_year_nav;
	            $scheme_details_response['five_year_nav'] = $five_year_nav;
	            $scheme_details_response['three_year_nav'] = $three_year_nav;
	            $scheme_details_response['current_nav'] = $current_nav;


			}


			return response()->json(['msg'=>'success','response'=>array($scheme_details_response)]);

		}


		public function getSchemePerformanceDetails(Request $request){
			$months;
	        $scheme_code;
	        $request->validate([
	        	'duration' => 'required',
	            'scheme_code' => 'required'
	        	]);

	            $performance_data = array();
	            $months = $request['duration'];
	            $scheme_code = $request['scheme_code'];

	            //calculating the range date to prepare data

	            if ($request['duration'] == "7days") {
	            	$nav_range_date = Carbon::now()->subWeeks(1)->toDateString();	
	            }else{
	            	$nav_range_date = Carbon::now()->subMonths($months)->toDateString();	
	            }
	            

	            $nav_details = HistoricNav::where('date', '>', $nav_range_date)
	                ->where('scheme_code', $scheme_code)
	                ->orderBy('date', 'asc')
	                ->get();

	            //creating performance data
	            if(!empty($nav_details)) {
	                foreach($nav_details as $nav_detail) {
	                    $performance_data[] = array(
	                        'amount' => $nav_detail->nav,
	                        'year' => date('Y', strtotime($nav_detail->date)),
	                        'month' => date('m', strtotime($nav_detail->date)) - 1,
	                        'date' => date('d', strtotime($nav_detail->date)),
	                    );
	                }
	                return response()->json(['msg' => 'success', 'response'=> $performance_data]);
	            } else {
	                return response()->json(['msg' => 'failure']);
	            }
	        
		}
}
