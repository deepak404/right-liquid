<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SchemeDetails;
use App\HistoricNav;
use App\DailyNav;
use App\BankDetails;
use App\User;
use App\PortfolioDetails;
use App\InvestmentDetails;
use SoapClient;
use SoapHeader;
use App\BankInfo;
use Carbon\Carbon;

class FundController extends Controller
{

   	protected $pass_key = 'abcdef7895';
	protected $password = 'Rf@2017$';
    protected $response_key;

    public function investFunds(Request $request){

    	$user = \Auth::user();
		$save_status = false;
		$status_msg = '';
		$total_inv;
		$inv_date = '';
		$investment_id = '';
		$total_inv_amount;
		//$pan = \Auth::user()->pan;

		$bse_scheme_code = SchemeDetails::pluck('bse_scheme_code','scheme_code');

		$request->validate([
			'code_amount' => 'required',
			'bank_id' => 'required',
		]);


			$code_amount = $request['code_amount'];

			//dd($code_amount,$inv_type,$inv_date);

			/*
				$code_amount contains all the custom schemes with amount to be invested in the below format

				[{"code":"100520","amount":"10000","scheme":"eq"}],

			*/

			//var_dump($code_amount);

			$scheme_to_invest = json_decode($code_amount);
			$total_inv = 0;
			$empty_inv = array();
			$inv_scheme = array();

			//dd($inv_type);

            $bank_details = BankDetails::where('id',$request['bank_id'])->first();

			$bank_pan = $bank_details->pan;

			$bank_acc_no = $bank_details->acc_no;

			$bank_type = $bank_details->bank_type;


	 		//print_r($scheme_to_invest);
	 		//die();

			foreach ($scheme_to_invest as $scheme ) {
				$total_inv += $scheme->amount;
				$count = 0;



				/*
					Below is separating all the empty schemes and scheme to be invested
				*/

				if ($scheme->amount === 0) {

					
					$empty_inv[] = array('code' => $scheme->code,
					'amount' => $scheme->amount,
					'scheme_type' => $scheme->scheme,
					);
				}

				if ($scheme->amount !== 0) {

					//$total_inv_amount += $scheme->amount; 
					$inv_scheme[] = array('code' => $scheme->code,
					'amount' => $scheme->amount,
					'scheme_type' => $scheme->scheme,
					);
				}
			}

			$scheme_details = SchemeDetails::where('scheme_status',1)->get();

			$count = 0;

			//to check the scheme code received from user exist in scheme codes in scheme details

				$save_investment = new InvestmentDetails();
				$save_investment->investment_amount = $total_inv;
				$save_investment->investment_date = date('Y-m-d');
				$save_investment->investment_status = 0;
				$save_investment->pan = $bank_pan;

				
				$save_portfolio;

				$saved = $user->investmentDetails()->save($save_investment);
				

				if ($saved) {
					$investment_id = $saved->id;

					foreach ($inv_scheme as $portfolio) {
						$bse_date = date('Y-m-d');
						$bse_date = explode('-', $bse_date);
						$bse_date = $bse_date[0].$bse_date[1].$bse_date[2];
						$time = explode('.', microtime());
						$time = substr($time[1], 0 ,6);


						$unique_ref_no = $bse_date.substr($portfolio['code'], 0, 5).$time;
//                        $unique_ref_no = 2018081312456341692;
						$int_ref_no = $unique_ref_no;


//						dd($unique_ref_no, $time);
						//$this->getPassword();
						//dd($passed);
						 $user_folio = PortfolioDetails::where('pan',$bank_pan)->where('acc_no',$bank_acc_no)->where('scheme_code',$portfolio['code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

						 	if ($user_folio == null) {
						 		$folio_number = '';
						 	}
						    else if ($user_folio->count()  == 0) {
						    	$folio_number = '';	
						    }else{
						    	$folio_number = $user_folio['folio_number'];

						    }

						$portfolio_save = new PortfolioDetails();

						$portfolio_save->investment_id = $saved->id;
						$portfolio_save->scheme_code = $portfolio['code'];
						$portfolio_save->folio_number = $folio_number;
						$portfolio_save->bse_scheme_code = $bse_scheme_code[$portfolio['code']];
						$portfolio_save->acc_no = $bank_acc_no;
						$portfolio_save->int_ref_no = $int_ref_no;
						$portfolio_save->bse_order_date = $bse_date;
						$portfolio_save->unique_ref_no = $unique_ref_no;
						$portfolio_save->portfolio_status = 0;
						$portfolio_save->portfolio_type = $portfolio['scheme_type'];
						$portfolio_save->initial_amount_invested = $portfolio['amount'];
						$portfolio_save->amount_invested = $portfolio['amount'];
						$portfolio_save->investment_date = $saved->investment_date;
						$portfolio_save->units_held = 0;
						$portfolio_save->pan = $bank_pan;
						$save_portfolio = $user->PortfolioDetails()->save($portfolio_save);
						// die();

							if ($save_portfolio) {
								// $this->getPassword();
								// $transact_mode = 'NEW';
								// $buy_sell = 'P';
								// if ($folio_number == '') {
                                 //    	$buy_sell_type = 'FRESH'; //FRESH OR ADDITIONAL WILL COME HERE;
                                 //    }else{
                                 //    	$buy_sell_type = 'ADDITIONAL'; //FRESH OR ADDITIONAL WILL COME HERE;
                                 //    }

						  //   	$bse_order_response[] = $this->placeOrder($transact_mode,$unique_ref_no,$bank_pan[0],$bse_scheme_code[$portfolio['code']],$buy_sell,$buy_sell_type,$portfolio['amount'],$folio_number,$int_ref_no,$investment_id);


								// foreach ($bse_order_response as $key => $value) {
								// 		$date = date('d/m/Y h:i:s A', strtotime($saved->investment_date));
								// 		PortfolioDetails::where('investment_id',$value['inv_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => $bse_date]);
								// 	}

								$save_status = true;
								$status_msg = 'Your Order has been placed successfully';

							}
							// else{
							// 	InvestmentDetails::where('id',$investment_id)->delete();
							// }
					}
				}else{
					$save_status = false;
					$status_msg = 'Investment failed. Please check and try again';
				}

		if ($save_status) {
			return response()->json(['msg'=>'success', 'status'=>$status_msg,'inv_amount'=>$total_inv, 'date'=> date('d-m-Y',strtotime($inv_date)), 'id' => $investment_id, 'bank_type' => $bank_type]);
		}else {
				return response()->json(['msg'=>'failure', 'status'=>$status_msg]);
		}
    }


	public function getPassword(){


	    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
	    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
	       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
	       <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
	       <wsa:To>http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc</wsa:To>
	       </soap:Header>
	       
	       <soap:Body>
	          <bses:getPassword>
	             <!--Optional:-->
	             <bses:UserId>1245601</bses:UserId>
	             <!--Optional:-->
	             <bses:Password>'.$this->password.'</bses:Password>
	             <!--Optional:-->
	             <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
	          </bses:getPassword>
	       </soap:Body>
	    </soap:Envelope>';

	    //echo "hello";


	    $result = $this->makeSoapCall($soap_request);
	    // var_dump($result);
	    // die();
	    $result = explode('http://bsestarmf.in/MFOrderEntry/getPasswordResponse', $result);
	    $response = explode('|', $result[1]);

	    //var_dump($response);
	    //echo $response[1];

	    ///preg_match('/<getPasswordResult>-(.*?)-<getPasswordResult>/', $result, $pass_response);


	   //dd($response);

	    $pwd_response =  $response[0];
	    $this->response_key = $response[1];

	    //echo $this->response_key;

	  }


	  public function placeOrder($transact_mode,$unique_ref_no,$pan,$bse_scheme_code,$buy_sell,$buy_sell_type,$inv_amount,$folio_number,$int_ref_no,$investment_id){

	    //dd($this->response_key);



	    /*
	          Order Response will be in this order. Kindly don't go through the BSE web file documentation.
	          That will only confuse you. 

	          The order response will be in this format.

	          TRANSACTION_CODE|UNIQUE REFERENCE NUMBER|BSE ORDER NO|USER ID|MEMBER ID|CLIENT CODE|BSE REMARKS|SUCCESS FLAG

	          UNIQUE REFERENCE NUMBER = $unique_ref_no //this should be unique for each order. This will be tracking for the order.
	          BSE ORDER NO //This is generated by BSE
	          TRANSACTION CODE // NEW||MOD||CXL -- New order || Modification || Cancel.
	          SUCCESS FLAG // 0 == ORDER SUCCESS and 1 = ORDER FAILURE *What a Brilliant thing by BSE* 

	    */

	    $response_key = explode('</getPasswordResult></getPasswordResponse></s:Body></s:Envelope>', $this->response_key);



	    $soap_request = '<?xml version="1.0" encoding="utf-8"?>
	    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
	       <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
	       <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
	       <wsa:To>http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc</wsa:To>
	       </soap:Header>
	   <soap:Body>
	      <bses:orderEntryParam>
	         <!--Optional:-->
	         <bses:TransCode>'.$transact_mode.'</bses:TransCode>
	         <!--Optional:-->
	         <bses:TransNo>'.$unique_ref_no.'</bses:TransNo>
	         <!--Optional:-->
	         <bses:OrderId></bses:OrderId>
	         <!--Optional:-->
	         <bses:UserID>1245601</bses:UserID>
	         <!--Optional:-->
	         <bses:MemberId>12456</bses:MemberId>
	         <!--Optional:-->
	         <bses:ClientCode>'.$pan.'</bses:ClientCode>
	         <!--Optional:-->
	         <bses:SchemeCd>'.$bse_scheme_code.'</bses:SchemeCd>
	         <!--Optional:-->
	         <bses:BuySell>'.$buy_sell.'</bses:BuySell>
	         <!--Optional:-->
	         <bses:BuySellType>'.$buy_sell_type.'</bses:BuySellType>
	         <!--Optional:-->
	         <bses:DPTxn>P</bses:DPTxn>
	         <!--Optional:-->
	         <bses:OrderVal>'.$inv_amount.'</bses:OrderVal>
	         <!--Optional:-->
	         <bses:Qty></bses:Qty>
	         <!--Optional:-->
	         <bses:AllRedeem>N</bses:AllRedeem>
	         <!--Optional:-->
	         <bses:FolioNo>'.$folio_number.'</bses:FolioNo>
	         <!--Optional:-->
	         <bses:Remarks></bses:Remarks>
	         <!--Optional:-->
	         <bses:KYCStatus>Y</bses:KYCStatus>
	         <!--Optional:-->
	         <bses:RefNo>'.$int_ref_no.'</bses:RefNo>
	         <!--Optional:-->
	         <bses:SubBrCode></bses:SubBrCode>
	         <!--Optional:-->
	         <bses:EUIN>E173580</bses:EUIN>
	         <!--Optional:-->
	         <bses:EUINVal>N</bses:EUINVal>
	         <!--Optional:-->
	         <bses:MinRedeem>N</bses:MinRedeem>
	         <!--Optional:-->
	         <bses:DPC>N</bses:DPC>
	         <!--Optional:-->
	         <bses:IPAdd></bses:IPAdd>
	         <!--Optional:-->
	         <bses:Password>'.$response_key[0].'</bses:Password>
	         <!--Optional:-->
	         <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
	         <!--Optional:-->
	         <bses:Parma1></bses:Parma1>
	         <!--Optional:-->
	         <bses:Param2></bses:Param2>
	         <!--Optional:-->
	         <bses:Param3></bses:Param3>
	      </bses:orderEntryParam>
	   </soap:Body>
	</soap:Envelope>';

	//dd($soap_request);

	    $result = $this->makeSoapCall($soap_request);
	    //echo $result;



	    $response = explode('http://bsestarmf.in/MFOrderEntry/orderEntryParamResponse', $result);
	    $response = explode('|', $response[1]);

	    $trans_code = explode('</a:Action></s:Header><s:Body><orderEntryParamResponse xmlns="http://bsestarmf.in/"><orderEntryParamResult>', $response[0]);
	    $order_response = explode('</orderEntryParamResult></orderEntryParamResponse></s:Body></s:Envelope>', $response[7]);

	    $order_response = array(
	        'trans_code' => $trans_code[1],
	        'unique_ref_no' => $response[1],
	        'order_no' =>$response[2],
	        'client_code' => $response[5],
	        'remarks' =>$response[6],
	        'order_status' => $order_response[0],
	        'inv_id' => $investment_id,
	      );

	    //$this->place_order_count++;
	    return $order_response;

	    // print_r($order_response);
	    // die();
	    

	  }

	public function makeSoapCall($soap_request){

	    //dd($soap_request);
	    $header = array(
	        "Content-type: application/soap+xml;charset=\"utf-8\"",
	        "Accept: application/soap+xml",
	        "Cache-Control: no-cache",
	        "Pragma: no-cache",
	        "Content-length: ".strlen($soap_request),
	    );
	    $soap_do = curl_init();
	    curl_setopt($soap_do, CURLOPT_URL,"http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc?singleWsdl" );
	    curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt($soap_do, CURLOPT_POST,           true );
	    curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
	    curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
	    $result = curl_exec($soap_do);
	    //dd($result);
	    return $result;
	}

	public function paymentGatewayPassword(){
          $client = new SoapClient("https://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc?singleWsdl",array(
                  'soap_version' => SOAP_1_2, // !!!!!!!
                  'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
                  'trace' => 1,
                ));

          $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://tempuri.org/IStarMFPaymentGatewayService/GetPassword',true);
          $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic');

          $client->__setSoapHeaders([$actionHeader,$toHeader]);

              $login_params = array(
                
                'Param' => array(
                  'MemberId' => '12456',
                  'Password' => 'Rf@2017$',
                  'PassKey' => '1234569870',
                  'UserId' => '1245601',
                )

              );

            $result = $client->getPassword($login_params);
            //dd($result);
            $result = $result->GetPasswordResult->Status.'|'.$result->GetPasswordResult->ResponseString;
            //dd($result);

            $response = explode('|', $result);

            // dd($response);
            $pwd_response =  $response[0];
            $this->response_key = $response[1];

            // echo $this->response_key;

    }

	public function makePayment($id)
	  {
	  	  $this->paymentGatewayPassword();
	  	  $investment_id = $id;
	      $investment_details = InvestmentDetails::where('id',$investment_id)->get();
	      $pan = $investment_details->pluck('pan');
	      $pan = $pan[0];
	      $portfolio_details = PortfolioDetails::where('investment_id',$investment_id)->get();

	      $acc_no = $portfolio_details->first()->acc_no;
	      
	      $amount_invested = $portfolio_details->sum('amount_invested');
	      $order_nos = $portfolio_details->pluck('bse_order_no')->toArray();

	      // dd($order_nos);
	      

	      $client = new SoapClient("https://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc?singleWsdl",array(
	                  'soap_version' => SOAP_1_2, // !!!!!!!
	                  'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
	                  'trace' => 1,
	                ));

	      $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://tempuri.org/IStarMFPaymentGatewayService/PaymentGatewayAPI',true);
	      $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFPaymentGatewayService/StarMFPaymentGatewayService.svc/Basic');

	      $client->__setSoapHeaders([$actionHeader,$toHeader]);
	      $bank = \Auth::user()->bankDetails()->where('pan', $pan)->where('acc_no', $acc_no)->get()->toArray();
	      $bank = $bank[0];


	      // $bank['bank_id'] = stripslashes($bank['bank_id']);
	      $bank_id = $bank['bank_code'];
	      // dd($bank_id);
	      $payment_mode = BankInfo::where('bank_id',$bank['bank_code'])->pluck('mode');
	      // dd($payment_mode);

	      $bank_info = BankInfo::all();

	      $params = array(
	          'Param' => array(
	              'AccNo' => $bank['acc_no'],
	              'BankID' => $bank['bank_code'],
	              'ClientCode' => $pan,
	              'EncryptedPassword' => $this->response_key,
	              'IFSC' => $bank['ifsc_code'],
	              'LogOutURL' => 'https://www.liquidplus.in/payment_response/'.$id,
	              'MemberCode' => '12456',
	              'Mode' => $payment_mode[0],
	              'Orders' => array('string'=> $order_nos),
	              'TotalAmount' => $amount_invested,

	          )
	      );

	      // dd($params);

	       $result = $client->PaymentGatewayAPI($params);
	       echo $result->PaymentGatewayAPIResult->ResponseString;
	 }

	 public function paymentResponse($id){
          $this->paymentGatewayPassword();

           $inv_array = [];
           session_start();
           $_SESSION['inv_id'] = $id;
           
          $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
              'soap_version' => SOAP_1_2, // !!!!!!!
              'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
            ));

          $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI',true);
          $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

          $client->__setSoapHeaders([$actionHeader,$toHeader]);


          $portfolio_details = PortfolioDetails::where('investment_id',$id)->where('portfolio_status',0)->where('bse_order_status',0)->get();

          // dd($portfolio_details);
          foreach ($portfolio_details as $portfolio) {
          		// dd($portfolio->investment_id);
          		$pan = InvestmentDetails::where('id',$portfolio->investment_id)->pluck('pan');

              //echo $portfolio->bse_order_no."-".$portfolio->user->personalDetails->pan."<br>";
              // echo $portfolio->id."<br>";
              $params = array(
                'Flag' => '11',
                'UserId' => '1245601',
                'EncryptedPassword' => $this->response_key,
                'param' => $pan[0].'|'.$portfolio->bse_order_no.'|BSEMF',

              );

              // dd($params);

              $result = $client->MFAPI($params);
              $result = explode('|', $result->MFAPIResult);

              $payment_res = explode('(', $result[1]);
              // echo $portfolio->bse_order_no." -- ".trim($payment_res[0])."<br>";
              // die();

              if (trim($payment_res[0]) == "APPROVED") {
                  // echo "Inside Approved";
                  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'apr']);
              }else if (trim($payment_res[0]) == "REJECTED") {
                  // reject the order.
                  // echo "Inside rejected";
                  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'rej','portfolio_status'=>3]);
                  $inv_array[] = $portfolio->investment_id;
              }else if (trim($payment_res[0]) == "AWAITING FOR FUNDS CONFIRMATION") {
                  // Change the Payment Status column to AWC
                  // echo "inside afc";
                  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'afc']);
                  $inv_array[] = $portfolio->investment_id;
              }else if(trim($payment_res[0]) == "AWAITING FOR RESPONSE FROM BILLDESK"){
              	  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'bdc']);
                  $inv_array[] = $portfolio->investment_id;
              }else if (trim($payment_res[0]) == "PAYMENT NOT INITIATED FOR GIVEN ORDER") {
                  // Chnage the Payment STatus to PNI
                  // echo "Inside pni";

                  // if it stays "PNI" for more than 24 hours Cancel it off
                  PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'pni']);
                  $inv_array[] = $portfolio->investment_id;
              }else{

              }
              //echo ;
          }


          $update_pni_port = PortfolioDetails::where('created_at', '<', Carbon::now()->subDay())->where('bse_payment_status','pni')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);

          $update_ac = PortfolioDetails::where('created_at', '<', Carbon::now()->subDays(7))->where('bse_payment_status','afc')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);


          foreach ($inv_array as $inv) {

              $can_port_count = PortfolioDetails::where('investment_id',$inv)->where('portfolio_status','!=',3)->count();
              //echo $can_port_count."<br>";

              if ($can_port_count == 0) {
                  InvestmentDetails::where('id',$inv)->update(['investment_status'=>3]);
              }
          }


          // $updated_portfolio = PortfolioDetails::where('investment_id',$id)->get();
          
          return redirect('/fund_selector');
    }

    public function handlePayment(){

		// This function is to clear the session variable.
		session_start();
		unset($_SESSION);
		session_destroy();
		
		// print_r($_SESSION);
		return response()->json(['msg'=>'success']);
	}


	public function managePayment(Request $request){
		$request->validate([
			'inv_id' => 'required',
			'p_type' => 'required',
            'payment_type' => 'required',
		]);


		$bse_scheme_code = SchemeDetails::pluck('bse_scheme_code','scheme_code');

		$portfolio_details = PortfolioDetails::where('investment_id',$request['inv_id'])->get();
		$port_count = $portfolio_details->count();
		$pan_number = InvestmentDetails::where('id',$request['inv_id'])->value('pan');
		$order_count = 0;


		foreach ($portfolio_details as $portfolio){
			$bse_date = date('Y-m-d');
			$bse_date = explode('-', $bse_date);
			$bse_date = $bse_date[0].$bse_date[1].$bse_date[2];

			 $user_folio = PortfolioDetails::where('pan',$portfolio->pan)->where('acc_no',$portfolio->acc_no)->where('scheme_code',$portfolio->scheme_code)->whereIn('portfolio_status',[1,2])->whereNotNull('folio_number')->get()->last();
			 	if ($user_folio == null) {
			 		$folio_number = '';
			 	}
			    else if ($user_folio->count()  == 0) {
			    	$folio_number = '';
			    }else{
			    	$folio_number = $user_folio['folio_number'];

			    }
			$this->getPassword();
			$transact_mode = 'NEW';
			$buy_sell = 'P';
			if ($folio_number == '') {
		    	$buy_sell_type = 'FRESH'; //FRESH OR ADDITIONAL WILL COME HERE;
		    }else{
		    	$buy_sell_type = 'ADDITIONAL'; //FRESH OR ADDITIONAL WILL COME HERE;
		    }


//		    dd($folio_number, $portfolio->pan, $portfolio->scheme_code);


		    if($portfolio->scheme_code == '118058'){
		    	$bse_order_response[] = $this->placeOrder($transact_mode,$portfolio->unique_ref_no,$portfolio->pan,$bse_scheme_code[$portfolio->scheme_code]."-L0",$buy_sell,$buy_sell_type,$portfolio->amount_invested,$folio_number,$portfolio->int_ref_no,$request['inv_id']);
		    }else{
			    if($portfolio->amount_invested >= 200000){
                    $bse_order_response[] = $this->placeOrder($transact_mode,$portfolio->unique_ref_no,$portfolio->pan,$bse_scheme_code[$portfolio->scheme_code]."-L1",$buy_sell,$buy_sell_type,$portfolio->amount_invested,$folio_number,$portfolio->int_ref_no,$request['inv_id']);

                }else{
                    $bse_order_response[] = $this->placeOrder($transact_mode,$portfolio->unique_ref_no,$portfolio->pan,$bse_scheme_code[$portfolio->scheme_code],$buy_sell,$buy_sell_type,$portfolio->amount_invested,$folio_number,$portfolio->int_ref_no,$request['inv_id']);

                }
		    }





			foreach ($bse_order_response as $key => $value) {



                //Changing the portfolio status to 3 (Cancelled) if the order in the bse platform fails
                if($value['order_status'] == 1){
                    PortfolioDetails::where('investment_id',$value['inv_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => $bse_date, 'utr_no'=>$request['utr_no'], 'portfolio_status'=>3]);

                }else{
                    PortfolioDetails::where('investment_id',$value['inv_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => $bse_date, 'utr_no'=>$request['utr_no']]);

                }

					$order_count++;
		    }

		    //Cancelling the Investment if all the orders of the investments fail.

		    $failed_portfolio_count = PortfolioDetails::where('investment_id',$request['inv_id'])->where('portfolio_status',3)->count();
            $total_portfolio_count = PortfolioDetails::where('investment_id', $request['inv_id'])->count();

            if($failed_portfolio_count == $total_portfolio_count){
                InvestmentDetails::where('id', $request['inv_id'])->update(['investment_status'=>3]);
            }

		}

		if($request['p_type'] == 'nb'){
            InvestmentDetails::where('id',$request['inv_id'])->update(['payment_type'=>$request['payment_type']]);

            $failed_portfolio_count = PortfolioDetails::where('investment_id', $request['inv_id'])->where('bse_order_status',1)->count();
            $total_portfolio_count = PortfolioDetails::where('investment_id', $request['inv_id'])->count();

            if($failed_portfolio_count > 0){

                if($failed_portfolio_count == $total_portfolio_count){
                    InvestmentDetails::where('id',$request['inv_id'])->update(['investment_status'=>3]);

                }else{
                    PortfolioDetails::where('investment_id',$request['inv_id'])->update(['portfolio_status' => 3]);
                    InvestmentDetails::where('id',$request['inv_id'])->update(['investment_status'=>3]);

                }
                return redirect('/fund_selector')->with('error', 'Investment Order Failed. Please Refresh and try again.');

            }

            return redirect('https://www.liquidplus.in/make_payment/'.$request['inv_id']);

    	}elseif($request['p_type'] == 'rtgs'){
		    $get_inv_status = InvestmentDetails::where('id',$request['inv_id'])->first();
            //dd($get_inv_status, $request['inv_id'], gettype($get_inv_status->investment_status), ($get_inv_status->investment_status == 0));

            if($get_inv_status->investment_status == 0){
		        $response_status = 'success';
            }else{
                $response_status = 'failure';
            }


            $response_portfolio_details = PortfolioDetails::where('investment_id', $request['inv_id'])->get();

            InvestmentDetails::where('id',$request['inv_id'])->update(['payment_type'=>$request['payment_type']]);

            $pop_up = true;
            return redirect('/fund_selector')->with('rtgs_success', compact('pop_up', 'response_status', 'response_portfolio_details'));

//            return redirect('/fund_selector')->with(['rtgs_success'=>'Investment Order Passed', 'status' => $response_status, 'portfolio_response' => $response_portfolio_details]);

        }

		
	}
}
