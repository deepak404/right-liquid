<?php

namespace App\Http\Controllers;

use App\HistoricNav;
use App\Mail\Enquiry;
use App\Mail\ContactEnquiry;
use Illuminate\Http\Request;
use App\SchemeDetails;
use App\BankDetails;
use App\DailyNav;
use App\PortfolioDetails;
use App\InvestmentDetails;
use App\User;
use App\PersonalDetails; 
use App\GetUserPortfolioDetails;
use App\getPortfolioDetails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\WithdrawDetails;
use Excel;
use NumberFormatter;
use PHPExcel_Calculation_Financial;

class UserController extends Controller
{

    //use getPortfolioDetails;

	public function index(){

        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);

	    $portfolio = $this->getIndexCalculation(\Auth::user()->bankDetails->groupBy('pan'));

        $get_portfolio = new GetUserPortfolioDetails();

        $portfolio_details = $get_portfolio->userPortfolioDetails(\Auth::user()->id);


        if (array_key_exists('portfolios', $portfolio_details)) {
            $portfolios = $portfolio_details['portfolios'];
        }else{
            $portfolios = null;
        }

        $portfolio_total = $portfolio_details['portfolio_total'];

        $portfolio_total['total_xirr'] = 0;




        $average_xirr = 0;


        $port_count = 0;



        foreach ($portfolio as $key => $port){

            $user_portfolio = PortfolioDetails::where('pan', $port['pan'])->where('portfolio_status',1)->get();


            $portfolio[$key]['total_investment_amount'] = $fmt->format($port['total_investment_amount']);
            $portfolio[$key]['current_value'] = $fmt->format($port['current_value']);
            $portfolio[$key]['p_or_loss'] = $fmt->format($port['p_or_loss']);
            $portfolio[$key]['xirr'] = $port['xirr'];

            if($port['total_investment_amount'] > 0){
                $portfolio_total['total_xirr'] += ($port['total_investment_amount']/$portfolio_total['amount_invested']) * $port['xirr'];
            }





        }



        $portfolio_total['amount_invested'] = $fmt->format(round($portfolio_total['amount_invested'], 2));
        $portfolio_total['current_value'] = $fmt->format(round($portfolio_total['current_value'], 2));
        $portfolio_total['net_returns'] = $fmt->format($portfolio_total['net_returns']);


//        dd($portfolio);


        return view('users.index')->with(compact('portfolios','portfolio_total','portfolio'));
	}


	private function getIndexCalculation($bankDetails){
	    $response  = [];


	    foreach ($bankDetails as $pan => $banks){

	        foreach($banks as $bank){
	            $acc_name = $bank->acc_name;


                $investor_scheme_xirr = [];
                $portfolio_details = PortfolioDetails::where('pan',$pan)->where('acc_no', $bank->acc_no)->whereIn('portfolio_status',[1,2])->get();
                $investment_amount = 0;

                foreach ($portfolio_details as $portfolio){
                    $investment_amount += ($portfolio->units_held * $portfolio->invested_nav);
                }
                $current_value = 0;

                $amount = array();
                $date = array();

                foreach ($portfolio_details as $portfolio){
                    $nav = HistoricNav::where('scheme_code', $portfolio->scheme_code)->orderBy('date','desc')->first();

                    $temp = $portfolio->units_held * $nav->nav;

                    $current_value += $temp;

                }




                $xirr_portfolio = $portfolio_details->groupBy('scheme_code');
                $xirr_withdraw = WithdrawDetails::where('pan',$pan)->where('acc_no', $bank->acc_no)->where('withdraw_status', 1)->get()->groupBy('scheme_code')->toArray();

                $scheme_current_value = 0;



                //Looping through all the Portfolio of the User

                foreach ($xirr_portfolio as $scheme_code => $port){

//                    dd($scheme_code, array_key_exists($scheme_code, $xirr_withdraw));
                    $amount = [];
                    $date = [];

                    //checking whether there is a withdraw in the particular scheme code and adding it to the $amount and $date Variable.

                    if(array_key_exists($scheme_code, $xirr_withdraw)){

//                        dd('hello');
                        foreach ($xirr_withdraw[$scheme_code] as $withdraw){
                            $amount[] = $withdraw['withdraw_amount'];
                            $date[] = $withdraw['bse_redemption_date'];
                        }
                    }

//                    dd($amount, $date);


                    //Looping through all the portfolio details of the particular scheme and adding it to the variables $amount and $date
                    foreach ($port as $portfolio){
                        $scheme_nav = HistoricNav::where('scheme_code', $portfolio->scheme_code)->orderBy('date','desc')->first();

                        //to Check for Fully Withdrawn Details

                        if($portfolio->portfolio_status == 2){
                            $amount[] = -round($portfolio->initial_units_held * $portfolio->invested_nav, 3);
                        }else{
                            //this is for portfolio status 1.

                            if($portfolio->initial_units_held == $portfolio->units_held){
                                //If there is no Partial Redemption
                                $amount[] = -round($portfolio->initial_units_held * $portfolio->invested_nav, 3);

                            }else{
                                //if there is partial redemption.
                                // In partial Redemption Units will be reduced from 'units_held' field
                                $amount[] = -round($portfolio->initial_units_held * $portfolio->invested_nav, 3);

                            }
                        }

                        $date[] = $portfolio->bse_allotment_date;

                        $scheme_current_value += round($portfolio->units_held * $scheme_nav->nav, 3);



                    }




                    $amount[] = $scheme_current_value;
                    $date[] = $scheme_nav->date;

//                    dd($amount, $date, 'hello');


                    $scheme_current_value =0;


                    $sort_xirr = [];

                    for($i=0; $i<count($amount); $i++){
                        $sort_xirr[$date[$i]][] = $amount[$i];
                    }

                    ksort($sort_xirr);

                    $xirr_amount = [];
                    $xirr_date = [];
                    foreach ($sort_xirr as $x_date => $sort_amount){
                        foreach($sort_amount as $s_amount){
                            $xirr_date[] = $x_date;
                            $xirr_amount[] = $s_amount;
                        }
                    }



//                    dd($xirr_amount, $xirr_date, $scheme_code);

                    if(count($xirr_amount) == 2){
                        if($xirr_date[0] == $xirr_date[1]){
                            $scheme_xirr = 0;
                        }else{
                            $scheme_xirr = round(PHPExcel_Calculation_Financial::XIRR($xirr_amount,$xirr_date) * 100,2);
                        }
                    }else{

                        try{
                            $scheme_xirr = round(PHPExcel_Calculation_Financial::XIRR($xirr_amount,$xirr_date) * 100,2);
                        }catch(\Exception $e){
                            $scheme_xirr = 0;
                        }


                    }



                    $investor_scheme_xirr[$scheme_code] = $scheme_xirr;

                }




                //calculating the Weighted Average for XIRR.

                foreach($investor_scheme_xirr as $scheme_code => $xirr){
                    $investor_scheme_investment = PortfolioDetails::where('pan',$pan)->where('portfolio_status', 1)->where('scheme_code', $scheme_code)->get();
                    $investor_scheme_value = $investor_scheme_investment->sum('units_held') * HistoricNav::where('scheme_code', $scheme_code)->orderBy('date','desc')->value('nav');

                    if($investor_scheme_value == 0.0 || $current_value == 0.0){
//                        dd($investor_scheme_value, $current_value, $xirr);
                        $investor_scheme_xirr[$scheme_code] = $xirr;
                    }else{
                        $investor_scheme_xirr[$scheme_code] = $investor_scheme_value/$current_value * $xirr;

                    }
//                    dd($scheme_code, $xirr, $current_value);
                }

                if(count($investor_scheme_xirr) != 0){
                    $investor_xirr = array_sum($investor_scheme_xirr) / count($investor_scheme_xirr);
                }else{
                    $investor_xirr = 0;
                }



                $p_or_loss = $current_value - $investment_amount;


                $response[] = array(
                    'pan' => $pan,
                    'total_investment_amount' => round($investment_amount, 2),
                    'acc_name' => $acc_name,
                    'current_value' => round($current_value, 2),
                    'p_or_loss' => round($p_or_loss, 2),
                    'xirr' => round($investor_xirr, 2),
                );



            }



        }


        usort($response, function($a, $b) {
            return $a['acc_name'] <=> $b['acc_name'];
        });

//        dd($response);

        return $response;
    }


    private function sortDateArray(){

    }

    public function portfolioDetails()
    {

        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);


        $sub_members = \Auth::user()->bankDetails->pluck('pan','acc_no');

        $portfolios = [];


        foreach ($sub_members as $acc_no => $pan){

            $portfolios[$acc_no] = [];
            $portfolios[$acc_no]['total']['total_amount_invested'] = 0;
            $portfolios[$acc_no]['total']['current_value'] = 0;
            $total_amount_invested = 0;

            $portfolio_details = PortfolioDetails::where('acc_no', $acc_no)->whereIn('portfolio_status', [1,2])->get();

            foreach($portfolio_details as $portfolio){
                $total_amount_invested += $portfolio->units_held * $portfolio->invested_nav;
//
                $nav_code_value = DailyNav::where('scheme_code', $portfolio->scheme_code)->orderBy('date', 'desc')->first();

                $scheme_name = SchemeDetails::where('scheme_code', $portfolio->scheme_code)->pluck('scheme_name');

                $current_value = round($nav_code_value['nav_value'] * $portfolio['units_held'], 2);
                if (array_key_exists($portfolio->scheme_code, $portfolios[$acc_no])) {

                    $portfolios[$acc_no]['total']['total_amount_invested'] += $portfolio->units_held * $portfolio->invested_nav;
                    $portfolios[$acc_no]['total']['current_value'] += $current_value;


                    $net_returns = round($current_value - $portfolio->amount_invested, 2);

                    $portfolios[$acc_no][$portfolio->scheme_code]['scheme_name'] = $scheme_name[0];
                    $portfolios[$acc_no][$portfolio->scheme_code]['amount_invested'] += $portfolio->units_held * $portfolio->invested_nav;
                    $portfolios[$acc_no][$portfolio->scheme_code]['current_market_value'] += $current_value;
                    $portfolios[$acc_no][$portfolio->scheme_code]['folio_number'] = $portfolio->folio_number;
                    $portfolios[$acc_no][$portfolio->scheme_code]['net_returns'] += $net_returns;
//                   $portfolios[$pan][$portfolio->scheme_code]['bse_date'] = $portfolio->bse_allotment_date;

                    if ($total_amount_invested != 0) {
                        $holding_date = (date_diff(date_create(date('Y-m-d')), date_create($portfolio->bse_allotment_date))->format('%d')) * (($portfolio->units_held * $portfolio->invested_nav) / $total_amount_invested);
                        $portfolios[$acc_no][$portfolio->scheme_code]['holding_period'] += $holding_date;
                    } else {
                        $portfolios[$acc_no][$portfolio->scheme_code]['holding_period'] = 0;
                    }

                } else {

                    $portfolios[$acc_no]['total']['total_amount_invested'] += $portfolio->units_held * $portfolio->invested_nav;
                    $portfolios[$acc_no]['total']['current_value'] += $current_value;
                    $net_returns = round($current_value - ($portfolio->units_held * $portfolio->invested_nav), 2);

                    $portfolios[$acc_no][$portfolio->scheme_code]['scheme_name'] = $scheme_name[0];
                    $portfolios[$acc_no][$portfolio->scheme_code]['amount_invested'] = $portfolio->units_held * $portfolio->invested_nav;
                    $portfolios[$acc_no][$portfolio->scheme_code]['current_market_value'] = $current_value;
                    $portfolios[$acc_no][$portfolio->scheme_code]['folio_number'] = $portfolio->folio_number;
                    $portfolios[$acc_no][$portfolio->scheme_code]['net_returns'] = $net_returns;

                    if ($total_amount_invested != 0) {
                        $holding_date = (date_diff(date_create(date('Y-m-d')), date_create($portfolio->bse_allotment_date))->format('%d')) * (($portfolio->units_held * $portfolio->invested_nav) / $total_amount_invested);
                        $portfolios[$acc_no][$portfolio->scheme_code]['holding_period'] = $holding_date;
                    } else {
                        $portfolios[$acc_no][$portfolio->scheme_code]['holding_period'] = 0;
                    }

                }

                $portfolios[$acc_no]['total']['net_returns'] = $portfolios[$acc_no]['total']['current_value'] - $portfolios[$acc_no]['total']['total_amount_invested'];

            }
        }


        //Below is Calculation for XIRR Returns

        $xirr_amount = [];
        $xirr_date = [];
        foreach ($portfolios as $acc_no => $portfolio_details){



            unset($portfolio_details['total']);
            $scheme_codes = array_keys($portfolio_details);


            $xirr_withdraws = WithdrawDetails::where('acc_no', $acc_no)
                ->where('withdraw_status', 1)
                ->whereIn('scheme_code', $scheme_codes)
                ->get()
                ->groupBy('scheme_code');

            $xirr_portfolios = PortfolioDetails::where('acc_no', $acc_no)
                ->whereIn('portfolio_status',[1,2])
                ->whereIn('scheme_code', $scheme_codes)
                ->get()
                ->groupBy('scheme_code');


            foreach ($xirr_withdraws as $w_scheme_code => $xirr_withdraw){

                foreach ($xirr_withdraw as $w_xirr){
                    $xirr_amount[$acc_no][$w_scheme_code][] = $w_xirr->withdraw_amount;
                    $xirr_date[$acc_no][$w_scheme_code][] = $w_xirr->bse_redemption_date;
                }

            }



            foreach($xirr_portfolios as $scheme_code => $xirr_portfolio){

                $scheme_last_entry = DailyNav::where('scheme_code', $scheme_code)->orderBy('date', 'desc')->first();
                $scheme_current_value = 0;


                foreach ($xirr_portfolio as $x_portfolio){

                    $scheme_current_value += $x_portfolio->units_held * $scheme_last_entry->nav_value;
                    $xirr_amount[$acc_no][$scheme_code][] = -round($x_portfolio->initial_units_held * $x_portfolio->invested_nav, 3);
                    $xirr_date[$acc_no][$scheme_code][] = $x_portfolio->bse_allotment_date;
                }

                $xirr_amount[$acc_no][$scheme_code][] = $scheme_current_value;
                $xirr_date[$acc_no][$scheme_code][] = $scheme_last_entry->date;
            }

        }

        $sort_xirr = [];


        foreach(array_keys($xirr_amount) as $bank_acc_no){

            //Getting all the scheme codes of invested Bank account
            $bank_acc_schemes = array_keys($xirr_amount[$bank_acc_no]);

            foreach ($bank_acc_schemes as $scheme_code) {

                for ($i = 0; $i < count($xirr_amount[$bank_acc_no][$scheme_code]); $i++) {
                    $sort_xirr[$bank_acc_no][$scheme_code][$xirr_date[$bank_acc_no][$scheme_code][$i]] = $xirr_amount[$bank_acc_no][$scheme_code][$i];
                }


                ksort($sort_xirr[$bank_acc_no][$scheme_code]);

                if (count(array_values($sort_xirr[$bank_acc_no][$scheme_code])) == 2) {
                    if (array_keys($sort_xirr[$bank_acc_no][$scheme_code])[0] == array_keys($sort_xirr[$bank_acc_no][$scheme_code])[1]) {
                        $scheme_xirr = 0;
                    } else {

                        try {
                            $scheme_calc_xirr = PHPExcel_Calculation_Financial::XIRR(array_values($sort_xirr[$bank_acc_no][$scheme_code]), array_keys($sort_xirr[$bank_acc_no][$scheme_code])) * 100;

                        } catch (\Exception $e) {
                            $scheme_calc_xirr = 0;

                        }

                    }
                } else {

                    try {
                        $scheme_calc_xirr = PHPExcel_Calculation_Financial::XIRR(array_values($sort_xirr[$bank_acc_no][$scheme_code]), array_keys($sort_xirr[$bank_acc_no][$scheme_code])) * 100;

                    } catch (\Exception $e) {
                        $scheme_calc_xirr = 0;

                    }


                }

                $portfolios[$bank_acc_no][$scheme_code]['ann_returns'] = round($scheme_calc_xirr, 2);
            }

        }


            return view('users.portfolio-details')->with(compact('portfolios'));

    }

    public function showWithdraw(){
        $portfolio_investment_details = array();
        $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
        $current_date = new Carbon();
        $current_date->subDay();
        $current_date = $current_date->format('Y-m-d');
        $scheme_code_name = $scheme_details->toArray();
        //$nav_code_value = $nav_details->toArray();
        $user= \Auth::user();
        $portfolio_types = array('liquid','ust','arb');
        $total_investment_current_value = 0;
        $ts_year_eligibility = Carbon::now()->subDays(1095)->toDateString();
        $pending_withdrawls = [];
        $withdraw_details = WithdrawDetails::where('user_id',\Auth::user()->id)->where('withdraw_status','0')->get()->groupBy('pan');

        //dd($pending_withdraws);
        $pending_withdraws = array();

        foreach ($withdraw_details as $pan => $value) {
            foreach ($value as $withdraw) {

                if(array_key_exists($withdraw->scheme_code, $pending_withdraws)){
                    $pending_withdraws[$withdraw->scheme_code]['amount'] += $withdraw->withdraw_amount;
                    $pending_withdraws[$withdraw->scheme_code]['pan'] = $withdraw->pan;
                    $pending_withdraws[$withdraw->scheme_code]['scheme_code'] = $withdraw->scheme_code;
                }else{
                    $pending_withdraws[$withdraw->scheme_code]['amount'] = $withdraw->withdraw_amount;
                    $pending_withdraws[$withdraw->scheme_code]['pan'] = $withdraw->pan;
                    $pending_withdraws[$withdraw->scheme_code]['scheme_code'] = $withdraw->scheme_code;
                }
                
            }

            $pending_withdrawls[$pan] = $pending_withdraws;
            $pending_withdraws = [];
        }


        $portfolio_details = \Auth::User()->portfolioDetails()->where('portfolio_status',1)->get()->groupBy('pan');
        // dd(\Auth::User()->portfolioDetails()->where('portfolio_status',1)->sum('amount_invested'));
        // dd($portfolio_details);

        $calc_portfolio = [];

        $pan_array = [];


        $company_inv = [];

        foreach ($portfolio_details as $pan => $portfolio_detail) {
            // dd(count($portfolio_detail));
            foreach ($portfolio_detail as $portfolio) {
                // dd($portfolio_detail);
                $pan_array[] = $portfolio['pan'];
                $nav_code_value = DailyNav::where('scheme_code',$portfolio['scheme_code'])->orderBy('date', 'desc')->first();
                $units_held = $portfolio['units_held'];
                $nav_value = $nav_code_value->nav_value;
                $current_value = round(($units_held * $nav_value),2);

                $bank_name = BankDetails::where('user_id',\Auth::user()->id)->where('pan',$portfolio['pan'])->get();

                if (array_key_exists($portfolio['scheme_code'], $calc_portfolio)) {
                    $calc_portfolio[$portfolio['scheme_code']]['scheme_code'] = $portfolio['scheme_code'];
                    $calc_portfolio[$portfolio['scheme_code']]['current_value'] += $current_value;
                    $calc_portfolio[$portfolio['scheme_code']]['scheme_name'] = $scheme_code_name[$portfolio['scheme_code']];
                    $calc_portfolio[$portfolio['scheme_code']]['pan'] = $portfolio['pan'];
                    $calc_portfolio[$portfolio['scheme_code']]['scheme_type'] = $portfolio['portfolio_type'];
                    $calc_portfolio[$portfolio['scheme_code']]['bank_name'] = $bank_name[0]['bank_name'];
                    $calc_portfolio[$portfolio['scheme_code']]['acc_name'] = $bank_name[0]['acc_name'];

                }else{

                    $calc_portfolio[$portfolio['scheme_code']]['scheme_code'] = $portfolio['scheme_code'];
                    $calc_portfolio[$portfolio['scheme_code']]['current_value'] = $current_value;
                    $calc_portfolio[$portfolio['scheme_code']]['scheme_name'] = $scheme_code_name[$portfolio['scheme_code']];
                    $calc_portfolio[$portfolio['scheme_code']]['pan'] = $portfolio['pan'];
                    $calc_portfolio[$portfolio['scheme_code']]['bank_name'] = $bank_name[0]['bank_name'];
                    $calc_portfolio[$portfolio['scheme_code']]['scheme_type'] = $portfolio['portfolio_type'];
                    $calc_portfolio[$portfolio['scheme_code']]['acc_name'] = $bank_name[0]['acc_name'];

                }
            }

            $company_inv[$bank_name[0]['acc_name']] = $calc_portfolio;
            $calc_portfolio = [];
        }

        // dd($company_inv);


        // die();

        //dd($pending_withdraws);


        // $final_array = [];
        // foreach ($pan_array as $pan) {
        //     foreach ($calc_portfolio as $portfolio) {
        //         if ($portfolio['pan'] == $pan) {
        //             $bank_name = BankDetails::where('user_id',\Auth::user()->id)->where('pan',$pan)->pluck('bank_name');
        //             $portfolio['bank_name'] = $bank_name[0];
        //             $final_array[$pan] = $portfolio;
                    
                    
        //         }
        //     }
        // }

        // $pass_array = [];

        // $count = 0;
        // foreach ($calc_portfolio as $portfolio) {
        //     if (array_key_exists($portfolio['acc_name'], $pass_array)) {
        //         $pass_array[$portfolio['acc_name']][$count] = $portfolio;
        //     }else{
        //         $pass_array[$portfolio['acc_name']][$count] = $portfolio;
        //     }

        //     $count++;
        // }

        // dd($pass_array);

        $portfolio_details = $company_inv;
        $pending_withdraws = $pending_withdrawls;
        // dd($pending_withdraws);
        return view('users.withdraw-details')->with(compact('portfolio_details','pending_withdraws'));
    }

    public function investmentHistory(){

        $period = 6;
        $total_investment_details = \Auth::user()->investmentDetails()->get();

        $inv_date = array();
            foreach ($total_investment_details as $key => $value) {

                    if (array_key_exists(date('d-m-Y',strtotime($value['investment_date'])), $inv_date)) {

                        $inv_date[date('d-m-Y',strtotime($value['investment_date']))][] = $value;
                    }else{
                        
                        $inv_date[date('d-m-Y',strtotime($value['investment_date']))][] = $value;                      
                    }
            }


//            dd($inv_date);

            // dd($inv_date);
            krsort($inv_date);
            // dd($inv_date);


            // unset($inv_date[""]);
            // //dd($inv_date);
            //  $temp = array();
            //  foreach($inv_date as $key=>$value){
            //      $temp[strtotime($key)] = $value;
            //  }
            //  krsort($temp);

            //  $inv_date = array();
            //  foreach($temp as $key=>$value){
            //       $inv_date[date("d-m-Y",$key)] = $value;
            //  }

            //  dd($inv_date);

        return view('users.investment-history')->with('investment_details',$inv_date);
    }

    public function fundSelector(){

        date_default_timezone_set('Asia/Kolkata');

        if(date('H') > 13 && date('H') < 15){
            return redirect('/');
        }

        $schemes = SchemeDetails::where('scheme_status',1)->get();
        session_start();
        // session_destroy();
        // $_SESSION['inv_id'] = 228;
        // dd($_SESSION);
        if (isset($_SESSION['inv_id'])) {
            // dd($_SESSION);
            $updated_portfolio = PortfolioDetails::where('investment_id',$_SESSION['inv_id'])->get();
            // dd($updated_portfolio);
            return view('users.fund-selector')->with(compact('schemes','updated_portfolio'));
        }

        return view('users.fund-selector')->with(compact('schemes'));
    }

    public function getMandateLink(Request $request)
    {
        $url = BankDetails::where('id',$request['bank_id'])->value('payment_link');
        if ($url == NULL || $url == '') {
            return response()->json(['msg' => 2]);
        }else{
            return response()->json(['msg'=>1,'mandate_url'=>$url]);
        }
    }

    public function getBankAccounts(){

        try{
             $bank_accounts = BankDetails::where('user_id',\Auth::user()->id)->get();
             return response()->json(['msg'=>'success','response'=>$bank_accounts]);
        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public function getInvestmentDetails(Request $request)
    {


            $request->validate([
                'investment_id' => 'required',
                ]);


            $investment_details = array();
            $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
            $scheme_code_name = $scheme_details->toArray();
            $portfolio_details = PortfolioDetails::where('investment_id', $request['investment_id'])->get();

//            dd($portfolio_details);
            foreach($portfolio_details as $portfolio_detail) {
                
                if($portfolio_detail->portfolio_status == 1) {
                    $investment_details[] = array(
                        'investment_date' => date('d-m-Y', strtotime($portfolio_detail->investment_date)),
                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                        'purchase_nav' => round($portfolio_detail->invested_nav,2),
                        'amount_invested' => round($portfolio_detail->amount_invested,2),
                        'status' => 'Success'
                    );


                }else if ($portfolio_detail->portfolio_status == 3 || $portfolio_detail->bse_order_status == 1) {

                    $investment_details[] = array(
                        'investment_date' => date('d-m-Y', strtotime($portfolio_detail->investment_date)),
                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                        
                        'purchase_nav' => '-',
                        'amount_invested' => round($portfolio_detail->amount_invested,2),
                        'status' => 'Cancelled'
                    );

                }else if ($portfolio_detail->portfolio_status == 2) {

                    $investment_details[] = array(
                        'investment_date' => date('d-m-Y', strtotime($portfolio_detail->investment_date)),
                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],

                        'purchase_nav' => '-',
                        'amount_invested' => round($portfolio_detail->initial_amount_invested,2),
                        'status' => 'Withdrawn'
                    );

                } else if($portfolio_detail->portfolio_status == 0) {
                    $investment_details[] = array(
                        'investment_date' => date('d-m-Y', strtotime($portfolio_detail->investment_date)),
                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                        // 'purchase_nav' => round($portfolio_detail->invested_nav,2),  
                        'purchase_nav' => '-',
                        'amount_invested' => round($portfolio_detail->amount_invested,2),
                        'status' => 'Order Placed'
                    );
                }
            }

            
            return response()->json(['msg'=>'success','response'=> $investment_details]);
    }


    public function showProfile(){

        $bank_details = \Auth::user()->bankDetails;
        return view('users.personal-details')->with(compact('bank_details'));
    }

    public function changePassword(Request $request){

        $request->validate([
            'current-password'  => 'required',
            'new-password' => 'required',
            'confirm-password' => 'required',

        ]);

        $current_pass = $request['current-password'];
        $new_pass = $request['new-password'];
        $confirm_pass = $request['confirm-password'];

        $new_pass = bcrypt($new_pass);

        if(\Hash::check($current_pass,\Auth::user()->password)){

            //echo "Matching same";
            $changed = User::where('id',\Auth::user()->id)->update(['password'=>$new_pass]);

            if($changed){
                return response()->json(['msg'=>'success','response' => 'Password Changed Successfully']);
            }else{
                return response()->json(['msg'=>'failure','response' => 'Password Update Failed']);
            }
        }
        else{

            return response()->json(['msg'=>'failure','response' => 'Password Doesn\'t match the current Password']);
        }
    }


    public function getMaximumInvestmentAmount(){
        $file_contents = file(base_path() . '/public/docs/schemes.txt');

        unset($file_contents[0]);
        foreach ($file_contents as $line ) {
            $allot = explode('|', $line);

            //if ($allot[13] == 0.000) {
                echo $allot[1].' - '.$allot[8].' - Rs.'.$allot[13]."<br>";
            //}

        }
    }

    public function updatePersonalDetails(Request $request){

        $id = \Auth::user()->id;
        $request->validate([
            'name' => 'required',
            'email' => ['required',Rule::unique('users')->ignore($id)],
            'mobile' => ['required',Rule::unique('users')->ignore($id)],
        ]);

        $update = \Auth::user()->update($request->toArray());
        //print_r($update);
        //die();

        if ($update) {
            return response()->json(['msg'=>'success']);
        }else{
            return response()->json(['msg'=>'failure']);
        }
    }


    public function getInvestmentHistory(Request $request)
    {
        $response_type = 'document';
        $period = 6;
        $request = $request->all();
        if(array_key_exists('investment_period', $request)) {
            $period = $request['investment_period'];
        }
        if(array_key_exists('response_type', $request)) {
            $response_type = $request['response_type'];
        }

        // dd('hello');
        
        $total_investment_details = $this->getInvestmentHistoryDetails($period, $response_type);
        if($response_type == 'data') {
            return response()->json(['msg' => 'success', 'response' => $total_investment_details]);
        } else {
            $file_content = Excel::create('Investment Summary', function($excel) use ($total_investment_details) {
                $excel->sheet('Investment Summary', function($sheet) use ($total_investment_details)
                {
                    $sheet->fromArray($total_investment_details);
                });
            })->download('xlsx');
        }
        
    }


    public function getInvestmentHistoryDetails($period, $response_type)
    {
        $total_investment_details = array();
        $investment_period;


        //getting Investment period time as 12 months , 24 months and the default is 6 Months
        switch ($period) {
            case '1': {
                $investment_period = Carbon::now()->subMonths(12)->toDateString();
                break;
            }
            case '2': {
                $investment_period = Carbon::now()->subMonths(24)->toDateString();
                break;
            }
            default: {
                $investment_period = Carbon::now()->subMonths(6)->toDateString();
                break;
            }
        }\Log::info($investment_period);
        $user= \Auth::user();
        //$scheme_types = array('One Time', 'Future Investment', 'Monthly Investment');
        $scheme_status = array('Processing', 'Success', 'Failed','Cancelled');
        $total_investment_amount = 0;
        $total_units_held = 0;
        $toal_investment_current_value = 0;
        $investment_details = $user->investmentDetails()
            ->where('investment_date', '>', $investment_period)
            ->orderBy('investment_date','asc')
            ->get();

        //dd($investment_details);

        //dd($investment_details);
        $total_liquid_investment = 0;
        $total_ust_investment = 0;
        $total_arb_investment = 0;
        
        //$document_response =

        //adding investment details of the user
        foreach($investment_details as $investment_detail) {

            //getting portfolio details of the particular investment and storing it in $portfolio_details
            $portfolio_details = $investment_detail->portfolioDetails->all();
            $liquid_investment = 0;
            $ust_investment = 0;
            $arb_investment = 0;
            // $equity_investment = 0;
            // $balanced_investment = 0;
            // $ts_investment = 0;


            //below foreach is to find out the total amount invested in particular portfolio type and adding it to respective variable

            foreach($portfolio_details as $portfolio_detail) {
                if($portfolio_detail->portfolio_type == 'liquid'){
                    $liquid_investment += $portfolio_detail->amount_invested;
                } elseif($portfolio_detail->portfolio_type == 'ust') {
                    $ust_investment += $portfolio_detail->amount_invested;
                } elseif($portfolio_detail->portfolio_type == 'arb') {
                    $arb_investment += $portfolio_detail->amount_invested;
                 } //elseif($portfolio_detail->portfolio_type == 'ts') {
                //     $ts_investment += $portfolio_detail->amount_invested;
                // }

            } // $portfolio_details foreach ends


            $investment_date = '';

            //below if is to convert date format of the particular investment
            if($investment_detail->investment_date != NULL && $investment_detail->investment_date != '') {
                $investment_date = date('d-m-Y', strtotime($investment_detail->investment_date));
            }


            /*
                Getting all the investment if the investment amount in each portfolio is greater than 0 and storing it accordingly.
                
            */

            
            if($liquid_investment > 0){
                if($response_type == 'data') {
                    $total_investment_details['liquid'][] = array(
                        'investment_id' => $investment_detail->id,
                        'investment_date' => $investment_date,
                        'investment_amount' => $liquid_investment,
                        //'investment_type' => $scheme_types[$investment_detail->investment_type],
                        'investment_status' => $scheme_status[$investment_detail->investment_status],
                        'portfolio_type' => 'Liquid'
                    );
                } else {
                    $total_investment_details[] = array(
                        'Investment Date' => $investment_date,
                        'Invested Amount' => $liquid_investment,
                        'Scheme Class' => 'Liquid',
                        'Investment Status' => $scheme_status[$investment_detail->investment_status]
                    );
                }
            }


//            if($ust_investment > 0){
//                if($response_type == 'data') {
//
//                    $total_investment_details['ust'][] = array(
//                        'investment_id' => $investment_detail->id,
//                        'investment_date' => $investment_date,
//                        'investment_amount' => $equity_investment,
//                        'investment_type' => $scheme_types[$investment_detail->investment_type],
//                        'investment_status' => $scheme_status[$investment_detail->investment_status],
//                        'portfolio_type' => 'Ultrashort Term'
//                    );
//                } else {
//                    $total_investment_details[] = array(
//                        'Investment Date' => $investment_date,
//                        'Invested Amount' => $equity_investment,
//                        'Scheme Class' => 'UltraShort Term',
//                        'Investment Status' => $scheme_status[$investment_detail->investment_status]
//                    );
//                }
//            }

            if($arb_investment > 0){
                if($response_type == 'data') {

                    $total_investment_details['arb'][] = array(
                        'investment_id' => $investment_detail->id,
                        'investment_date' => $investment_date,
                        'investment_amount' => $arb_investment,
//                        'investment_type' => $scheme_types[$investment_detail->investment_type],
                        'investment_status' => $scheme_status[$investment_detail->investment_status],
                        'portfolio_type' => 'Arbitrage'
                    );
                } else {
                    $total_investment_details[] = array(
                        'Investment Date' => $investment_date,
                        'Invested Amount' => $arb_investment,
                        'Scheme Class' => 'Arbitrage',
                        'Investment Status' => $scheme_status[$investment_detail->investment_status]
                    );
                }
            }

            // if($balanced_investment > 0){
            //     if($response_type == 'data') {
            //         $total_investment_details['bal'][] = array(
            //             'investment_id' => $investment_detail->id,
            //             'investment_date' => $investment_date,
            //             'investment_amount' => $balanced_investment,
            //             'investment_type' => $scheme_types[$investment_detail->investment_type],
            //             'investment_status' => $scheme_status[$investment_detail->investment_status],
            //             'portfolio_type' => 'Balanced'
            //         );
            //     } else {
            //         $total_investment_details[] = array(
            //             'Investment Date' => $investment_date,
            //             'Invested Amount' => $balanced_investment,
            //             'Transaction Type' => $scheme_types[$investment_detail->investment_type],
            //             'Investment Status' => $scheme_status[$investment_detail->investment_status]
            //         );
            //     }
            // }
            // if($ts_investment > 0){
            //     if($response_type == 'data') {
            //         $total_investment_details['ts'][] = array(
            //             'investment_id' => $investment_detail->id,
            //             'investment_date' => $investment_date,
            //             'investment_amount' => $ts_investment,
            //             'investment_type' => $scheme_types[$investment_detail->investment_type],
            //             'investment_status' => $scheme_status[$investment_detail->investment_status],
            //             'portfolio_type' => 'Tax Saver'
            //         );
            //     } else {
            //         $total_investment_details[] = array(
            //             'Investment Date' => $investment_date,
            //             'Invested Amount' => $ts_investment,
            //             'Transaction Type' => $scheme_types[$investment_detail->investment_type],
            //             'Investment Status' => $scheme_status[$investment_detail->investment_status]
            //         );
            //     }
            // }


            // Getting Grand total of investments in each portfolio and storing it 
                
            $total_liquid_investment += $liquid_investment;
            $total_ust_investment += $ust_investment;
            $total_arb_investment += $arb_investment;
            // $total_balanced_investment += $balanced_investment;
            // $total_ts_investment += $ts_investment;

        //} // Investment details foreach ends

        //Getting all the withdraw details of the user and storing it in the variable

        // $withdraw_details = $user->withdrawDetails->where('withdraw_date', '>', $investment_period)->groupBy('withdraw_group_id')->all();Log::info(gettype($withdraw_details));
        // if(!(empty($withdraw_details))) {

        //     //processing each withdraw 
        //     foreach($withdraw_details as $withdraw_detail) {Log::info($withdraw_detail);
        //         $debt_investment = 0;
        //         $equity_investment = 0;
        //         $balanced_investment = 0;
        //         $ts_investment = 0;
        //         $withdraw_status = 1;
        //         $withdraw_date = '';
        //         foreach($withdraw_detail as $withdraw) {
        //             if($withdraw->status == 0) {
        //                 $withdraw_status = 0;
        //             }
        //             $withdraw_date = '';
        //             if($withdraw->withdraw_date != NULL && $withdraw->withdraw_date != '') {
        //                 $withdraw_date = date('d-m-Y',strtotime($withdraw->withdraw_date));
        //             }
        //         }

                // if($response_type == 'data') {
                //     if($withdraw->portfolio_type == 'debt'){
                //         $total_investment_details['debt'][] = array(
                //             'investment_date' => $withdraw_date,
                //             'investment_amount' => $withdraw_detail->sum('withdraw_amount'),
                //             'investment_type' => 'Withdraw',
                //             'investment_status' => $scheme_status[$withdraw_status]
                //         );
                //     }
                //     if($withdraw->portfolio_type == 'eq'){
                //         $total_investment_details['eq'][] = array(
                //             'investment_date' => $withdraw_date,
                //             'investment_amount' => $withdraw_detail->sum('withdraw_amount'),
                //             'investment_type' => 'Withdraw',
                //             'investment_status' => $scheme_status[$withdraw_status]
                //         );
                //     }
                //     if($balanced_investment > 0){
                //         $total_investment_details['bal'][] = array(
                //         'investment_date' => $withdraw_date,
                //         'investment_amount' => $withdraw_detail->sum('withdraw_amount'),
                //         'investment_type' => 'Withdraw',
                //         'investment_status' => $scheme_status[$withdraw_status]
                //         );
                //     }
                //     if($ts_investment > 0){
                //         $total_investment_details['ts'][] = array(
                //         'investment_date' => $withdraw_date,
                //         'investment_amount' => $withdraw_detail->sum('withdraw_amount'),
                //         'investment_type' => 'Withdraw',
                //         'investment_status' => $scheme_status[$withdraw_status]
                //         );
                //     }
                // } else {
                //     $total_investment_details[] = array(
                //         'Investment Date' => $withdraw_date,
                //         'Invested Amount' => $withdraw_detail->sum('withdraw_amount'),
                //         'Transaction Type' => 'Withdraw',
                //         'Investment Status' => $scheme_status[$withdraw_status]
                //     );
                // }

        //     } // Withdraw details foreach ends
        // } // !empty($withdraw_details) ends
        // if($response_type == 'data') {
        //     if($total_debt_investment > 0) {
        //         $total_investment_details['debt']['total_investment'] = $total_debt_investment;
        //     }
        //     if($total_equity_investment > 0) {
        //         $total_investment_details['eq']['total_investment'] = $total_equity_investment;
        //     }
        //     if($total_balanced_investment > 0) {
        //         $total_investment_details['bal']['total_investment'] = $total_balanced_investment;
        //     }
        //     if($total_ts_investment > 0) {
        //         $total_investment_details['ts']['total_investment'] = $total_ts_investment;
        //     }
        // }
    }
        return $total_investment_details;
    }

    public function getUserPortfolioDocument(Request $request)
    {


        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);
        $user = User::where('id', $request['user'])->get();

        $sub_members = $user[0]->bankDetails->pluck('pan','acc_no');


        $portfolios = [];


        foreach ($sub_members as $acc_no => $pan){

            $portfolios[$acc_no] = [];
            $portfolios[$acc_no]['total']['total_amount_invested'] = 0;
            $portfolios[$acc_no]['total']['current_value'] = 0;
            $total_amount_invested = 0;

            $portfolio_details = PortfolioDetails::where('acc_no', $acc_no)->whereIn('portfolio_status', [1,2])->get();

            foreach($portfolio_details as $portfolio){
                $total_amount_invested += $portfolio->units_held * $portfolio->invested_nav;
//
                $nav_code_value = DailyNav::where('scheme_code', $portfolio->scheme_code)->orderBy('date', 'desc')->first();

                $scheme_name = SchemeDetails::where('scheme_code', $portfolio->scheme_code)->pluck('scheme_name');

                $current_value = round($nav_code_value['nav_value'] * $portfolio['units_held'], 2);
                if (array_key_exists($portfolio->scheme_code, $portfolios[$acc_no])) {

                    $portfolios[$acc_no]['total']['total_amount_invested'] += $portfolio->units_held * $portfolio->invested_nav;
                    $portfolios[$acc_no]['total']['current_value'] += $current_value;


                    $net_returns = round($current_value - $portfolio->amount_invested, 2);

                    $portfolios[$acc_no][$portfolio->scheme_code]['scheme_name'] = $scheme_name[0];
                    $portfolios[$acc_no][$portfolio->scheme_code]['amount_invested'] += $portfolio->units_held * $portfolio->invested_nav;
                    $portfolios[$acc_no][$portfolio->scheme_code]['current_market_value'] += $current_value;
                    $portfolios[$acc_no][$portfolio->scheme_code]['folio_number'] = $portfolio->folio_number;
                    $portfolios[$acc_no][$portfolio->scheme_code]['net_returns'] += $net_returns;
//                   $portfolios[$pan][$portfolio->scheme_code]['bse_date'] = $portfolio->bse_allotment_date;

                    if ($total_amount_invested != 0) {
                        $holding_date = (date_diff(date_create(date('Y-m-d')), date_create($portfolio->bse_allotment_date))->format('%d')) * (($portfolio->units_held * $portfolio->invested_nav) / $total_amount_invested);
                        $portfolios[$acc_no][$portfolio->scheme_code]['holding_period'] += $holding_date;
                    } else {
                        $portfolios[$acc_no][$portfolio->scheme_code]['holding_period'] = 0;
                    }

                } else {

                    $portfolios[$acc_no]['total']['total_amount_invested'] += $portfolio->units_held * $portfolio->invested_nav;
                    $portfolios[$acc_no]['total']['current_value'] += $current_value;
                    $net_returns = round($current_value - ($portfolio->units_held * $portfolio->invested_nav), 2);

                    $portfolios[$acc_no][$portfolio->scheme_code]['scheme_name'] = $scheme_name[0];
                    $portfolios[$acc_no][$portfolio->scheme_code]['amount_invested'] = $portfolio->units_held * $portfolio->invested_nav;
                    $portfolios[$acc_no][$portfolio->scheme_code]['current_market_value'] = $current_value;
                    $portfolios[$acc_no][$portfolio->scheme_code]['folio_number'] = $portfolio->folio_number;
                    $portfolios[$acc_no][$portfolio->scheme_code]['net_returns'] = $net_returns;

                    if ($total_amount_invested != 0) {
                        $holding_date = (date_diff(date_create(date('Y-m-d')), date_create($portfolio->bse_allotment_date))->format('%d')) * (($portfolio->units_held * $portfolio->invested_nav) / $total_amount_invested);
                        $portfolios[$acc_no][$portfolio->scheme_code]['holding_period'] = $holding_date;
                    } else {
                        $portfolios[$acc_no][$portfolio->scheme_code]['holding_period'] = 0;
                    }

                }

                $portfolios[$acc_no]['total']['net_returns'] = $portfolios[$acc_no]['total']['current_value'] - $portfolios[$acc_no]['total']['total_amount_invested'];

            }
        }


        //Below is Calculation for XIRR Returns

        $xirr_amount = [];
        $xirr_date = [];
        foreach ($portfolios as $acc_no => $portfolio_details){



            unset($portfolio_details['total']);
            $scheme_codes = array_keys($portfolio_details);


            $xirr_withdraws = WithdrawDetails::where('acc_no', $acc_no)
                ->where('withdraw_status', 1)
                ->whereIn('scheme_code', $scheme_codes)
                ->get()
                ->groupBy('scheme_code');

            $xirr_portfolios = PortfolioDetails::where('acc_no', $acc_no)
                ->whereIn('portfolio_status',[1,2])
                ->whereIn('scheme_code', $scheme_codes)
                ->get()
                ->groupBy('scheme_code');


            foreach ($xirr_withdraws as $w_scheme_code => $xirr_withdraw){

                foreach ($xirr_withdraw as $w_xirr){
                    $xirr_amount[$acc_no][$w_scheme_code][] = $w_xirr->withdraw_amount;
                    $xirr_date[$acc_no][$w_scheme_code][] = $w_xirr->bse_redemption_date;
                }

            }



            foreach($xirr_portfolios as $scheme_code => $xirr_portfolio){

                $scheme_last_entry = DailyNav::where('scheme_code', $scheme_code)->orderBy('date', 'desc')->first();
                $scheme_current_value = 0;


                foreach ($xirr_portfolio as $x_portfolio){

                    $scheme_current_value += $x_portfolio->units_held * $scheme_last_entry->nav_value;
                    $xirr_amount[$acc_no][$scheme_code][] = -round($x_portfolio->initial_units_held * $x_portfolio->invested_nav, 3);
                    $xirr_date[$acc_no][$scheme_code][] = $x_portfolio->bse_allotment_date;
                }

                $xirr_amount[$acc_no][$scheme_code][] = $scheme_current_value;
                $xirr_date[$acc_no][$scheme_code][] = $scheme_last_entry->date;
            }

        }

        $sort_xirr = [];


        foreach(array_keys($xirr_amount) as $bank_acc_no){

            //Getting all the scheme codes of invested Bank account
            $bank_acc_schemes = array_keys($xirr_amount[$bank_acc_no]);

            foreach ($bank_acc_schemes as $scheme_code) {

                for ($i = 0; $i < count($xirr_amount[$bank_acc_no][$scheme_code]); $i++) {
                    $sort_xirr[$bank_acc_no][$scheme_code][$xirr_date[$bank_acc_no][$scheme_code][$i]] = $xirr_amount[$bank_acc_no][$scheme_code][$i];
                }


                ksort($sort_xirr[$bank_acc_no][$scheme_code]);

                if (count(array_values($sort_xirr[$bank_acc_no][$scheme_code])) == 2) {
                    if (array_keys($sort_xirr[$bank_acc_no][$scheme_code])[0] == array_keys($sort_xirr[$bank_acc_no][$scheme_code])[1]) {
                        $scheme_xirr = 0;
                    } else {

                        try {
                            $scheme_calc_xirr = PHPExcel_Calculation_Financial::XIRR(array_values($sort_xirr[$bank_acc_no][$scheme_code]), array_keys($sort_xirr[$bank_acc_no][$scheme_code])) * 100;

                        } catch (\Exception $e) {
                            $scheme_calc_xirr = 0;

                        }

                    }
                } else {

                    try {
                        $scheme_calc_xirr = PHPExcel_Calculation_Financial::XIRR(array_values($sort_xirr[$bank_acc_no][$scheme_code]), array_keys($sort_xirr[$bank_acc_no][$scheme_code])) * 100;

                    } catch (\Exception $e) {
                        $scheme_calc_xirr = 0;

                    }


                }

                $portfolios[$bank_acc_no][$scheme_code]['ann_returns'] = round($scheme_calc_xirr, 2);
            }

        }

        $count = 0;
        foreach($portfolios as $bank_acc_no => $portfolio_details){
            $investor_name = BankDetails::where('acc_no', $bank_acc_no)->value('acc_name');
            unset($portfolio_details['total']);
            foreach($portfolio_details as $portfolio){

                $user_portfolio[] = array(
                    'S.No' => $count,
                    'Investor Name' => $investor_name,
                    'Scheme Name' => $portfolio['scheme_name'],
                    'Amount Invested' => $portfolio['amount_invested'],
                    'Folio Number' => $portfolio['folio_number'],
                    'Current Value' => $portfolio['current_market_value'],
                    'Net Returns' => $portfolio['net_returns'],
                    'Annualised Returns' => $portfolio['ann_returns'].'%',

                );

                $count++;
            }

        }


        $file_content = Excel::create('Portfolio Summary', function($excel) use ($user_portfolio) {
                $excel->sheet('mySheet', function($sheet) use ($user_portfolio)
                {
                    $sheet->fromArray($user_portfolio);
                });
            })->download('xlsx');



    }


    public function orderStatus(){

        $date = Carbon::now()->subDays(2)->toDateString();
        $investment_details = InvestmentDetails::where('user_id',\Auth::user()->id)->where('investment_status',0)->where('investment_date','>',$date)->get();
        // dd($investment_details);
        $inv_details = [];

        $date_filter = [];
        $count = 1;
        $pan = BankDetails::all()->pluck('acc_name','pan');
        // dd($pan);
        foreach ($investment_details as $investment) {
            $inv_details[$count]['s_no'] = $count;
            $inv_details[$count]['date'] = date('d-m-Y',strtotime($investment->investment_date));
            $inv_details[$count]['id'] = $investment->id;
            $inv_details[$count]['acc_name'] = $pan[$investment->pan];
            $inv_details[$count]['inv_amount'] = $investment->investment_amount;
            $inv_details[$count]['payment_type'] = $investment->payment_type;
            $inv_details[$count]['utr_no'] = $investment->utr_no;

            $order_status = $investment->investment_status;
            if($order_status == 0){
                $status = 'Order Placed';
            }elseif($order_status == 1){
                $status = 'Success';
            }elseif ($order_status == 2) {
                $status = 'Failed';
            }elseif ($order_status == 3) {
                $status = 'Cancelled';
            }
            $inv_details[$count]['order_status'] = $status;

            if(array_key_exists(date('d-m-Y',strtotime($inv_details[$count]['date'])), $date_filter)){
                $date_filter[date('d-m-Y',strtotime($inv_details[$count]['date']))][] = $inv_details[$count];
            }else
            {
                $date_filter[date('d-m-Y',strtotime($inv_details[$count]['date']))][] = $inv_details[$count];
            }
            $count++;
            // dd($inv_details);
        }
        asort($date_filter);
        // dd($date_filter);
        $investment_details = $date_filter;
        return view('users.order_status')->with(compact('investment_details'));
    }



	public function getWithdrawDetails(Request $request){

        $request->validate([
            'pan'  => 'required',
            'acc_no' => 'required',
        ]);


//        dd($_POST);

        $withdraw_details = WithdrawDetails::where('user_id',\Auth::user()->id)->where('pan',$request['pan'])->where('acc_no', $request['acc_no'])->where('withdraw_status','0')->get();
        $portfolio_details = \Auth::User()->portfolioDetails()->where('pan',$request['pan'])->where('acc_no',$request['acc_no'])->where('portfolio_status',1)->get();

//        dd($request['acc_no'], $request['pan']);
        $pending_withdraws = [];
        $calc_portfolio = [];
        $arb_with_exit_load = [];
        $pan_array = [];

        $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
        $scheme_code_name = $scheme_details->toArray();

        $warning_arb_investments =  \Auth::User()->portfolioDetails()->where('pan',$request['pan'])->where('acc_no',$request['acc_no'])->where('portfolio_status',1)
            ->where('portfolio_type', 'arb')->where('bse_allotment_date','>=', Carbon::now()->subDays(30)->toDateString())->get();



        foreach ($warning_arb_investments as $w_inv) {
            $nav = DailyNav::where('scheme_code',$w_inv['scheme_code'])->orderBy('date', 'desc')->first();

            if(array_key_exists($w_inv->scheme_code, $arb_with_exit_load)){
                $arb_with_exit_load[$w_inv->scheme_code]['amount'] += $w_inv->units_held * $nav->nav_value;
            }else{
                $arb_with_exit_load[$w_inv->scheme_code]['amount'] = $w_inv->units_held * $nav->nav_value;
            }

        }



        $company_inv = [];

        foreach ($withdraw_details as $withdraw) {
            if(array_key_exists($withdraw->scheme_code, $pending_withdraws)){
                $pending_withdraws[$withdraw->scheme_code]['amount'] += $withdraw->withdraw_amount;
                $pending_withdraws[$withdraw->scheme_code]['pan'] = $withdraw->pan;
                $pending_withdraws[$withdraw->scheme_code]['scheme_code'] = $withdraw->scheme_code;
            }else{
                $pending_withdraws[$withdraw->scheme_code]['amount'] = $withdraw->withdraw_amount;
                $pending_withdraws[$withdraw->scheme_code]['pan'] = $withdraw->pan;
                $pending_withdraws[$withdraw->scheme_code]['scheme_code'] = $withdraw->scheme_code;
            }

        }

        foreach ($portfolio_details as $portfolio) {
            // dd($portfolio_detail);
            $pan_array[] = $portfolio['pan'];
            $nav_code_value = DailyNav::where('scheme_code',$portfolio['scheme_code'])->orderBy('date', 'desc')->first();
            $units_held = $portfolio['units_held'];
            $nav_value = $nav_code_value->nav_value;
            $current_value = round(($units_held * $nav_value),2);

            $bank_name = BankDetails::where('user_id',\Auth::user()->id)->where('pan',$portfolio['pan'])->where('acc_no', $portfolio['acc_no'])->get();
            $scheme_type = SchemeDetails::where('scheme_code', $portfolio['scheme_code'])->value('scheme_type');

            if (array_key_exists($portfolio['scheme_code'], $calc_portfolio)) {
                $calc_portfolio[$portfolio['scheme_code']]['scheme_code'] = $portfolio['scheme_code'];
                $calc_portfolio[$portfolio['scheme_code']]['current_value'] += $current_value;
                $calc_portfolio[$portfolio['scheme_code']]['scheme_name'] = $scheme_code_name[$portfolio['scheme_code']];
                $calc_portfolio[$portfolio['scheme_code']]['pan'] = $portfolio['pan'];
                $calc_portfolio[$portfolio['scheme_code']]['acc_no'] = $portfolio['acc_no'];
                $calc_portfolio[$portfolio['scheme_code']]['scheme_type'] = $portfolio['portfolio_type'];
                $calc_portfolio[$portfolio['scheme_code']]['bank_name'] = $bank_name[0]['bank_name'];
                $calc_portfolio[$portfolio['scheme_code']]['acc_name'] = $bank_name[0]['acc_name'];
                $calc_portfolio[$portfolio['scheme_code']]['scheme_type'] = $scheme_type;

            }else{

                $calc_portfolio[$portfolio['scheme_code']]['scheme_code'] = $portfolio['scheme_code'];
                $calc_portfolio[$portfolio['scheme_code']]['current_value'] = $current_value;
                $calc_portfolio[$portfolio['scheme_code']]['scheme_name'] = $scheme_code_name[$portfolio['scheme_code']];
                $calc_portfolio[$portfolio['scheme_code']]['acc_no'] = $portfolio['acc_no'];
                $calc_portfolio[$portfolio['scheme_code']]['pan'] = $portfolio['pan'];
                $calc_portfolio[$portfolio['scheme_code']]['bank_name'] = $bank_name[0]['bank_name'];
                $calc_portfolio[$portfolio['scheme_code']]['scheme_type'] = $portfolio['portfolio_type'];
                $calc_portfolio[$portfolio['scheme_code']]['acc_name'] = $bank_name[0]['acc_name'];
                $calc_portfolio[$portfolio['scheme_code']]['scheme_type'] = $scheme_type;

            }
        }

        return response()->json(['msg'=>true, 'portfolio'=>$calc_portfolio, 'withdraw'=>$pending_withdraws, 'arb_with_exit_load' => $arb_with_exit_load]);

    }


    public function updateUtrNo(Request $request){

        $request->validate([
            'utr_no'  => 'required',
            'inv_id' => 'required',
        ]);

        $update = InvestmentDetails::where('id', $request['inv_id'])->update(['utr_no'=>$request['utr_no']]);

        if($update){
            return response()->json(['msg'=>true]);
        }else{
            return response()->json(['msg'=>false]);
        }

    }


    public function transactionHistory(){
        $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);

//	    $response = [];
//	    $investment_details = InvestmentDetails::where('user_id', \Auth::user()->id)->where('investment_status', '!=',3)->get();
//
////	    dd($investment_details);
//
//	    foreach($investment_details as $inv){
//
//
//        }

        $investment_details = InvestmentDetails::where('user_id', \Auth::user()->id)->get();
        $investments = [];
        $withdraws = [];


        $inv_count = 1;

//        dd($investment_details);
        foreach($investment_details as $inv){

            $bank = BankDetails::where('pan', $inv->pan)->first();

            if($inv->investment_status == 0){
                $status = 'Order Placed';
            }elseif($inv->investment_status == 1){
                $status = 'Success';
            }elseif ($inv->investment_status == 2) {
                $status = 'Failed';
            }elseif ($inv->investment_status == 3) {
                $status = 'Cancelled';
            }

//            dd($bank->acc_name);

            $investments[] = array(
                'count' => $inv_count,
                'investor_name' => $bank->acc_name,
                'investment_amount' => $fmt->format($inv->investment_amount),
                'investment_date' => date('d-m-Y', strtotime($inv->investment_date)),
                'status' => $status,

            );

            $inv_count++;
        }

        $withdraw_details = WithdrawDetails::where('user_id', \Auth::user()->id)->get()->groupBy('withdraw_group_id');
        $withdraws = [];


        $wd_count = 1;
        foreach($withdraw_details as $w_id => $withdraw){
            $bank = BankDetails::where('pan', $withdraw->first()->pan)->first();

            $total_wd_count = $withdraw->count();
            $success_wd_count = 0;
            $failed_wd_count = 0;
            $wd_status = '';
            foreach ($withdraw as $wd){
                if($wd->withdraw_status == 1){
                    $success_wd_count++;
                }

                if($wd->withdraw_status == 2){
                    $failed_wd_count++;
                }

            }

            if($success_wd_count == $total_wd_count){
                $wd_status = 'Success';
            }elseif($success_wd_count > 0 && $total_wd_count > $success_wd_count){
                $wd_status = 'Partial Withdrawn';
            }elseif($failed_wd_count == $total_wd_count){
                $wd_status = 'Cancelled';
            }else{
                $wd_status = 'Order Placed';
            }

            $withdraws[] = array(
                'count' => $wd_count,
                'withdraw_amount' => $fmt->format($withdraw->sum('withdraw_amount')),
                'investor_name' => $bank->acc_name,
                'withdraw_date' => date('d-m-Y', strtotime($withdraw->first()->withdraw_date)),
                'status' => $wd_status,
            );

            $wd_count++;
        }

//        dd($investments, $withdraws);



        return view('users.transaction_history')->with(compact('investments','withdraws'));
    }


    public function sendEnquiry(Request $request){

        $request->validate([
            'company-name'  => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'name' => 'required',
        ]);


        try{
            $sendMail = Mail::to('vasudev@pwm-india.com')->send(new Enquiry($_POST));

            return response()->json(['msg'=>true]);
        }catch(\Exception $e){
            return response()->json(['msg'=>false]);
        }

    }



    public function getRtgsTransactions(){

//	    dd(Carbon::now()->subDays(2)->toDateString());
	    $investments = InvestmentDetails::where('investment_status','0')->where('payment_type', 'rtgs')->where('investment_date','<', Carbon::now()->subDays(0)->toDateString())->get();

	    foreach($investments as $inv){

	        foreach($inv->portfolioDetails as $portfolio){
	            $portfolio->portfolio_status = 3;
	            $portfolio->save();
            }

            $inv->investment_status = 3;
	        $inv->save();

        }
    }


    public function sendContactEnquiry(Request $request){
        $request->validate([
            'sender-name'  => 'required',
            'email' => 'required',
            'mobile' => 'required',
            'message' => 'required',
        ]);



        try{

            $sendMail = Mail::to('vgupta@rightfunds.com')->send(new ContactEnquiry($_POST));

            return response()->json(['msg'=>true]);
        }catch(\Exception $e){
            return response()->json(['msg'=>false]);
        }
    }


}
