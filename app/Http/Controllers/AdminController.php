<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\BankDetails;
use App\BankInfo;
use App\Documents;
use App\SchemeDetails;
use App\SchemeHistory;
use App\DailyNav;
use App\HistoricNav;
use App\GetUserPortfolioDetails;
use App\PortfolioDetails;
use App\WithdrawDetails;
use App\InvestmentDetails;
use Carbon\Carbon;
use SoapClient;
use SoapHeader;
use PHPExcel_Calculation_Financial;


class AdminController extends Controller
{
    public function addUser(){
        $bank_info = BankInfo::all();
    	return view('admin.add-user')->with(compact('bank_info'));
    }


    public function addNewUser(Request $request){
    	$request->validate([
    		'name' => 'required',
    		'email' => 'required|email|unique:users',
    		'mobile' => 'required|unique:users|max:10|min:10',
    		'password' => 'required',
    		]);

    	try{
    		$request = $request->toArray();
    		$request['role'] = 0;
    		$request['password'] = bcrypt($request['password']);
    		$insert = User::create($request);

    		if ($insert) {
    			return response()->json(['msg'=>'success','user_id'=>$insert->id]);
    		}else{
    			return response()->json(['msg'=>'failure']);
    		}
    	}catch(\Exception $e){
    		return response()->json(['msg'=>'failure']);
    	}
    }


    public function addUserBank(Request $request){
    	$request->validate([
    		'acc_name' => 'required',
    		'acc_no' => 'required',
    		'ifsc_code' => 'required',
    		'acc_type' => 'required',
    		'bank_type' => 'required',
    		'user_id' => 'required',
            'bank_name' => 'required',
            'pan' => ['required','regex:/^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/'],
    		]);

    	try{
    		$request = $request->toArray();
    		unset($request['_token']);

    		$acc_count = BankDetails::where('user_id',$request['user_id'])->count();
    		$acc_count++;
    		$request['acc_count'] = $acc_count;
            $bank_code = $request['bank_name'];
            $bank_name = BankInfo::where('bank_id',$request['bank_name'])->pluck('bank_name');
            $request['bank_name'] = $bank_name[0];
            $request['bank_code'] = $bank_code;
    		$insert = BankDetails::create($request);

    		$request['bank_id'] = $insert->id;

    		if ($insert) {
    			return response()->json(['msg'=>'success','response'=>$request]);
    		}else{
    			return response()->json(['msg'=>'failure']);
    		}
    	}catch(\Exception $e){
    		//dd('hello');
    		echo $e;
    		//return response()->json(['msg'=>'failure']);
    	}


    }

    public function updateUserBank(Request $request){
        $request->validate([
            'bank_id' => 'required',
            'acc_name' => 'required',
            'acc_no' => 'required',
            'ifsc_code' => 'required',
            'acc_type' => 'required',
            'bank_type' => 'required',
            'user_id' => 'required',
            'bank_name' => 'required',
            'pan' => ['required','regex:/^[A-Za-z]{5}\d{4}[A-Za-z]{1}$/'],
//            'payment_link' => 'required',
            ]);

        try{
            $bank_code = $request['bank_name'];
            $bank_name = BankInfo::where('bank_id',$request['bank_name'])->value('bank_name');

            $update = BankDetails::where('id',$request['bank_id'])->update([
                'pan'=>$request['pan'],
                'acc_name'=>$request['acc_name'],
                'bank_name' => $bank_name,
                'bank_code' => $bank_code,
//                'payment_link' => $request['payment_link'],
                'ifsc_code' => $request['ifsc_code'],
                'acc_no' => $request['acc_no'],
                'acc_type' => $request['acc_type'],
                'bank_type' => $request['bank_type'],

            ]);

            if ($update) {
                return response()->json(['msg'=>'success','response'=>$request]);
            }else{
                return response()->json(['msg'=>'failure']);
            }
        }catch(\Exception $e){
            //dd('hello');
            echo $e;
            //return response()->json(['msg'=>'failure']);
        }


    }

    public function deleteBank(Request $request){
    	$request->validate([

    		'bank_id' => 'required',
    		]);

    	try{
    		$delete = BankDetails::where('id',$request['bank_id'])->delete();


	    	if ($delete) {
				return response()->json(['msg'=>'success','response'=>$_POST]);
			}else{
				return response()->json(['msg'=>'failure']);
			}
    	}catch(\Exception $e){
    		return $e;
    	}
    }


    public function addUserDocuments(Request $request){

    	$request->validate([
//    		'aoa' => 'required',
//    		'moa' => 'required',
//    		'bs' => 'required',
//    		'asl' => 'required',
//    		'br' => 'required',
//    		'ap' => 'required',
//    		'coi' => 'required',
    		'kyc' => 'required',
            'cc' => 'required', //Cancelled Cheque
            //'pan_card' => 'required',
//    		'id_proof' => 'required',
    		'user_id' => 'required',
    		]);


//    	$files = ['aoa','moa','bs','asl','br','ap','coi','kyc','pan_card','id_proof'];
    	$files = ['kyc','cc'];

        $create_docs = array();

    	$create_docs['user_id'] = $request['user_id'];


    	foreach ($files as $file) {
    		if ($request->hasFile($file)) {
    			$folder = $file;
	    		$file = $request->file($file);
				$file_name = $request['user_id'].'-'.$folder.'-'.str_random().'.'. $file->getClientOriginalExtension();
	        	$new_file = $file->move(base_path() . '/public/docs/'.$folder.'/', $file_name);

	        	$create_docs[$folder] = $file_name;
	    	}
    	}


    	try{

            $document_count = Documents::where('user_id',$request['user_id'])->count();

            if ($document_count > 0) {
                $insert = Documents::where('user_id',$request['user_id'])->update($create_docs);
            }else{
                $insert = Documents::create($create_docs);
            }


    		if ($insert) {
    			return response()->json(['msg'=>'success']);
    		}else{
    			return response()->json(['msg'=>'failure']);
    		}
    	}catch(\Exception $e){
    		return $e;
    	}


    }

    public function showDashboard(){

        $total_users = User::where('role','!=','1')->count();
        $portfolio_details = PortfolioDetails::where('portfolio_status',1)->get();
        $aum = 0;


        //dd($last_nav);
        $holding_period = 0;
        $last_nav;


        if (count($portfolio_details) != 0) {
            foreach ($portfolio_details as $portfolio_detail) {

                $last_nav = DailyNav::where('scheme_code',$portfolio_detail['scheme_code'])->orderBy('date','aesc')->pluck('nav_value')->toArray();

                $aum += ($portfolio_detail->units_held) * $last_nav[0];
                $holding_date = date_diff(date_create(date('Y-m-d')),date_create($portfolio_detail['bse_allotment_date']));
                $holding_date = $holding_date->format('%d');
                $holding_period += $holding_date;
            }

            $aum = round($aum,2);
            $schemes = SchemeDetails::all();
            $scheme_count = $schemes->count();

            $avg_holding_period = $holding_period/($portfolio_details->count());
        }else{
            $avg_holding_period  = 0;
            $aum = 0;
        }





        $schemes = SchemeDetails::all();
        $scheme_count = $schemes->count();
        $count = 0;
        $scheme_aum = 0;
        foreach ($schemes as $scheme) {
            $amount_invested = 0;

                $current_nav = HistoricNav::where('scheme_code',$scheme['scheme_code'])->orderBy('date','desc')->first();


                $one_day_nav = HistoricNav::where('scheme_code',$scheme['scheme_code'])->where('date','<=' ,Carbon::now()->subDays(2)->toDateString())->orderBy('date','aesc')->first();

                $units_held = PortfolioDetails::where('scheme_code',$scheme['scheme_code'])->where('portfolio_status',1)->sum('units_held');

                //$amount_invested =  PortfolioDetails::where('scheme_code',$scheme['scheme_code'])->where('portfolio_status',1)->sum('amount_invested');

                $scheme_investment_amount = PortfolioDetails::where('scheme_code', $scheme['scheme_code'])->where('portfolio_status',1)->get();

                foreach ($scheme_investment_amount as $inv){
                    $amount_invested += $inv->units_held * $inv->invested_nav;
                }
//                $amount_invested = $units_held * $current_nav->nav;

                $scheme_aum = $units_held * $current_nav->nav;

//                if($scheme->scheme_code == '130446'){
//                    dd($current_nav, $one_day_nav);
//                }

                $one_day_nav = HistoricNav::where('scheme_code',$scheme['scheme_code'])->where('date','<' ,$current_nav->date)->orderBy('date','desc')->first();
                $one_day_nav = $this->calculateAbsoluteReturn($current_nav->nav, $one_day_nav->nav, $current_nav->date, $one_day_nav->date);



                $seven_day_nav = HistoricNav::where('scheme_code',$scheme['scheme_code'])->where('date','<' ,carbon::parse($current_nav->date)->subDays(8)->toDateString())->orderBy('date','aesc')->first();
                $seven_day_nav = $this->calculateAbsoluteReturn($current_nav->nav, $seven_day_nav->nav, $current_nav->date, $seven_day_nav->date);


                $thirty_day_nav = HistoricNav::where('scheme_code',$scheme['scheme_code'])->where('date','<' ,carbon::parse($current_nav->date)->subDays(31)->toDateString())->orderBy('date','aesc')->first();
                $thirty_day_nav = $this->calculateAbsoluteReturn($current_nav->nav, $thirty_day_nav->nav, $current_nav->date, $thirty_day_nav->date);

                $ninety_day_nav = HistoricNav::where('scheme_code',$scheme['scheme_code'])->where('date','<' ,carbon::parse($current_nav->date)->subDays(90)->toDateString())->orderBy('date','aesc')->first();
                $ninety_day_nav = $this->calculateAbsoluteReturn($current_nav->nav, $ninety_day_nav->nav, $current_nav->date, $ninety_day_nav->date);

                $one_year_nav = HistoricNav::where('scheme_code',$scheme['scheme_code'])->where('date','<' ,carbon::parse($current_nav->date)->subDays(90)->toDateString())->orderBy('date','aesc')->first();
                $one_year_nav = $this->calculateAbsoluteReturn($current_nav->nav, $one_year_nav->nav, $current_nav->date, $one_year_nav->date);

//                dd($one_year_nav);

//                $one_year_nav = HistoricNav::where('scheme_code',$scheme['scheme_code'])->where('date','<=' ,Carbon::now()->subDays(365)->toDateString())->orderBy('date','aesc')->first();
//                $one_year_nav  = $one_year_nav['nav'];
//                $one_year_nav = round((($last_nav[0] - $one_year_nav)/$one_year_nav)*100,2);
//                $one_year_nav = round($one_year_nav * (1),2);

                $schemes[$count]['scheme_name'] = $scheme['scheme_name'];
                $schemes[$count]['one_day'] = $one_day_nav;
                $schemes[$count]['seven_day'] = $seven_day_nav;
                $schemes[$count]['thirty_day'] = $thirty_day_nav;
                $schemes[$count]['ninety_day'] = $ninety_day_nav;
                $schemes[$count]['one_year'] = $one_year_nav;
                $schemes[$count]['amount_invested'] = round($amount_invested,2);
                $schemes[$count]['current_value'] = round($scheme_aum,2);

                $count++;
        }

//        dd($schemes);
        // dd($portfolio_details->count());

        $amount_invested  = $portfolio_details->sum('amount_invested');



        //dd($total_users);
        return view('admin.dashboard')->with(compact('aum','scheme_count','total_users','avg_holding_period','schemes'));
    }


    private function calculateAbsoluteReturn($current_nav, $last_nav, $current_date, $last_date){

        $date_diff = date_diff(date_create($current_date), date_create($last_date))->days;
        $returns = round((pow(($current_nav/$last_nav), (365/$date_diff)) - 1) * 100, 2);

        return $returns;
    }


    public function manageSchemes(){

        // SchemeHistory::whereNotIn('scheme_code',['106212','112423','114239','128053','133805'])->delete();
        // DailyNav::whereNotIn('scheme_code',['106212','112423','114239','128053','133805'])->delete();
        // HistoricNav::whereNotIn('scheme_code',['106212','112423','114239','128053','133805'])->delete();
        // SchemeDetails::whereNotIn('scheme_code',['106212','112423','114239','128053','133805'])->delete();

        $schemes = SchemeDetails::all();
        return view('admin.admin-scheme')->with(compact('schemes'));
    }


    public function addScheme(Request $request)
    {

        $request->validate([
            'scheme_name' => 'required',
            'scheme_code' => 'required|min:6|max:6|unique:scheme_details',
            'bse_scheme_code' => 'required|unique:scheme_details',
            'scheme_type' => 'required',
            'scheme_priority' => 'required',
            'scheme_file' => 'required',
            'fund_manager' => 'required',
            'launch_date' => 'required',
            'asset_size' => 'required',
            'benchmark' => 'required',
            'exit_load' => 'required',
            'fund_type' => 'required'
        ]);

        $this->revertScheme($request['scheme_code']);



            //dd($_POST);


            set_time_limit (400);
            $existing_fund_count = SchemeDetails::where('scheme_type', $request['scheme_type'])
                ->where('scheme_priority', $request['scheme_priority'])
                ->get();

            //Checking if the uploaded new scheme priority is already present in the same scheme type.

            if($existing_fund_count->count() == 0) {    //Log::info($request);//return $request;
                if ($request->hasFile('scheme_file')) {
                    $date = '';
                    $new_scheme = new SchemeDetails();
                    $new_scheme_details = new SchemeHistory();
                    $new_scheme->scheme_name = $request['scheme_name'];
                    $new_scheme->scheme_code = $request['scheme_code'];
                    $new_scheme->bse_scheme_code = $request['bse_scheme_code'];
                    $new_scheme->scheme_type = $request['scheme_type'];
                    $new_scheme->scheme_priority = $request['scheme_priority'];
                    $new_scheme->scheme_status = 1;

                    //$save_scheme = $new_scheme->save();

                    $new_scheme_details->scheme_code = $request['scheme_code'];
                    $new_scheme_details->name = $request['scheme_name'];
                    $new_scheme_details->investment_plan = 'Growth Option';
                    $new_scheme_details->launch_date = date('Y-m-d', strtotime($request['launch_date']));
                    $new_scheme_details->fund_type = $request['fund_type'];
                    $new_scheme_details->fund_manager = $request['fund_manager'];
                    $new_scheme_details->asset_size = $request['asset_size'];
                    $new_scheme_details->benchmark = $request['benchmark'];
                    $new_scheme_details->exit_load = $request['exit_load'];

                    //$save_scheme_details = $new_scheme_details->save();

                    //if ($save_scheme && $save_scheme_details) {
                        $file = $request->file('scheme_file');
                        $status = $file->move(base_path() . '/storage/csv/', $file->getClientOriginalName());

                        if ($status) {
                            if (($handle = fopen(base_path().'/storage/csv/'.$file->getClientOriginalName(),'r')) !== FALSE)
                            {

                                $no_of_lines = file(base_path().'/storage/csv/'.$file->getClientOriginalName());
                                $no_of_lines = count($no_of_lines);
                                $count = 0;
                                $is_scheme_saved = false;
                                $is_scheme_code_checked = false;
                                while (($data = fgetcsv($handle, 1000, ',')) !==FALSE)
                                {

                                    //dd($data[0]);

                                    $nav = new HistoricNav();
                                    if(!$is_scheme_code_checked) {
                                        if($data[0] == $request['scheme_code']) {
                                            $is_scheme_code_checked = true;
                                        } else {
                                            return response()->json(['msg'=>['status' => 'Scheme code provided does not match with the scheme code in the file.']]);
                                        }
                                    }

                                    if(!$is_scheme_saved) {
                                        if ($new_scheme->save() && $new_scheme_details->save()) {
                                            $is_scheme_saved =true;
                                        } else {
                                            return response()->json(['msg'=>['status' => 'Unable to create Scheme. Please check and try again.']]);
                                        }
                                    }

                                    $nav->scheme_code = $data[0];
                                    $nav->scheme_name = $data[1];
                                    $nav->nav = $data[2];
                                    $date = date('Y-m-d',strtotime($data[3]));
                                    $nav->date = $date;


                                    $nav->save();
                                    $count++;
                                    //echo $count;
                                }
                                    //echo $no_of_lines."<br>";
                                    //echo $count."<br>";
                                //die();

                                if ($no_of_lines == $count) {

                                    $store_daily_nav = $this->insertDailyNav($request['scheme_code']);

                                    if($store_daily_nav){
                                        $scheme_data = array(
                                            'scheme_code' => $request['scheme_code'],
                                            'scheme_name' => $request['scheme_name'],
                                            //'fund_type' => $this->fund_type[$request['scheme_type']],
                                            'priority' => $request['scheme_priority'],
                                            'bse_scheme_code' => $request['bse_scheme_code'],
                                            'scheme_status' => "1",
                                        );
                                        return response()->json(['msg'=>'1', 'scheme_data' => $scheme_data]);
                                    }else{
                                         return response()->json(['msg'=>['status' => 'Cannot upload rightnow. Try again later.']]);
                                    }

                                } else {//if the file lines are empty
                                    return response()->json(['msg'=>['status' => 'Invalid file data format. Please check file data format and try again.']]);
                                }
                            } else {//if file opening fails
                                return response()->json(['msg'=>['status' => 'Unable to open the file. Please recheck the file and try again.']]);
                            }

                        } else {//if unable to move file to handle it.
                            return response()->json(['msg'=>['status' => 'Unable to use the file. Please recheck the file and try again.']]);
                        }

                    // } else {//if scheme creation fails


                    // }

                   set_time_limit(60);
                } else { //if the nav doesn't exist
                    return response()->json(['msg'=>['status' => 'Invalid file. Please check and try again.']]);
                }
            } else {
                return response()->json(['msg'=>['status' => 'Scheme type with priority already exist.']]);
            }
    }

    function insertDailyNav($scheme_code){

        $historic_nav = HistoricNav::where('scheme_code',$scheme_code)->orderBy('date','desc')->limit(8)->get();
        $count = 0;
        //dd('inga varudhu');
        foreach ($historic_nav as $nav) {
            $store_nav = new DailyNav();
            $store_nav->scheme_code = $nav->scheme_code;
            $store_nav->nav_value = $nav->nav;
            $store_nav->date = $nav->date;
            if ($store_nav->save()) {
                $count++;
            }

        }

        if ($count == 8) {
            return true;
        }else{
            return false;
        }

    }

    function revertScheme($scheme_code){

        $scheme_history_count = count(SchemeHistory::where('scheme_code',$scheme_code)->get());
        $scheme_details_count = count(SchemeDetails::where('scheme_code',$scheme_code)->get());
        $historic_nav_count = count(HistoricNav::where('scheme_code',$scheme_code)->get());
        $daily_nav_count = count(DailyNav::where('scheme_code',$scheme_code)->get());


        //dd($scheme_history_count,$scheme_details_count,$historic_nav_count);

        if($scheme_history_count > 0){
            SchemeHistory::where('scheme_code',$scheme_code)->delete();
        }

        if($scheme_details_count > 0){
            SchemeDetails::where('scheme_code',$scheme_code)->delete();
        }

        if($historic_nav_count > 0){
            HistoricNav::where('scheme_code',$scheme_code)->delete();
        }

        if($daily_nav_count > 0){
            DailyNav::where('scheme_code',$scheme_code)->delete();
        }

            /*
                Uncomment below code to check if the deletion is successful. Successfull deletion results in count 0;

            */

        // $scheme_history_count = count(SchemeHistory::where('scheme_code',$scheme_code)->get());
        // $scheme_details_count = count(SchemeDetails::where('scheme_code',$scheme_code)->get());
        // $historic_nav_count = count(HistoricNav::where('scheme_code',$scheme_code)->get());


        // dd($scheme_history_count,$scheme_details_count,$historic_nav_count);



    }


    public function deleteSchemes(){
        SchemeHistory::whereNotIn('scheme_code',['106212','112423','114239','128053','133805']);
        DailyNav::whereNotIn('scheme_code',['106212','112423','114239','128053','133805']);
        HistoricNav::whereNotIn('scheme_code',['106212','112423','114239','128053','133805']);
        SchemeDetails::whereNotIn('scheme_code',['106212','112423','114239','128053','133805']);
    }


    public function customerSupport($id = null){

        if ($id == null) {
            return view('admin.admin-kyc-details');
        }else{
            $get_portfolio = new GetUserPortfolioDetails();
            $portfolio_details = $get_portfolio->userPortfolioDetails($id);
            $portfolio = $portfolio_details['portfolios'];
            $portfolio_user_total = $portfolio_details['portfolio_total'];
            $user_details = User::where('id',$id)->with('BankDetails','Documents')->get()->toArray();

            $bank_details = $user_details[0]['bank_details'];
            $document_details = $user_details[0]['documents'];
            $user_details = $user_details[0];
            unset($user_details['documents']);
            unset($user_details['bank_details']);
            unset($user_details['password']);

            // dd($document_details);
            // dd($bank_details);



            return view('admin.admin-kyc-details')->with(compact('portfolio_user_total','user_details','bank_details','document_details'));

        }

    }


    public function userSearch(Request $request){

       $request->validate([
            'search-input' => 'required',

        ]);

            $user_name = $request['search-input'];

            $users = User::where('name','LIKE',$user_name.'%')
                ->orwhere('mobile','LIKE',$user_name.'%')
                ->orwhere('email','LIKE',$user_name.'%')
                ->orwhere('pan','LIKE',$user_name.'%')
                ->distinct()
                ->limit(10)
                ->get();//Log::info(get_class($users));
                //$users = $users->where('p_check','1')->where('role', 0);
            /*foreach($users as $user) {
                $user_suggestions[$user->name.'/'.$user->mobile.'/'.$user->email] = $user->name.'/'.$user->mobile.'/'.$user->email;
            }*///Log::info($users);
            return json_encode($users);
    }

    public function showUserDetails(Request $request){
        $request->validate([
            'user_id' => 'required',
        ]);

        $user_details = User::where('id',$request['user_id'])->with('BankDetails','Documents')->get()->toArray();

        $bank_details = $user_details[0]['bank_details'];
        $document_details = $user_details[0]['documents'];
        $user_details = $user_details[0];
        unset($user_details['documents']);
        unset($user_details['bank_details']);
        unset($user_details['password']);


        $get_portfolio = new GetUserPortfolioDetails();
        $portfolio_details = $get_portfolio->userPortfolioDetails($request['user_id']);

        $portfolio_details = $portfolio_details['portfolio_total'];


        $portfolio = $this->getIndexCalculation(User::where('id', $request['user_id'])->first()->bankDetails->groupBy('pan'));

        $total_xirr = [];
        foreach ($portfolio as $port){
            if($port['xirr'] > 0){
                $total_xirr[] = $port['xirr'];
            }
        }

        if(count($total_xirr) > 0){
            $total_xirr_sum = array_sum($total_xirr)/count($total_xirr);

        }else{
            $total_xirr_sum = 0;
        }


        $portfolio_details['total_xirr'] = $total_xirr_sum;
        return response()->json(['msg'=>'success','user_details'=>$user_details,'bank_details'=>$bank_details,'document_details'=>$document_details,'portfolio'=>$portfolio_details]);
    }



    private function getIndexCalculation($bankDetails){
        $response  = [];


//        dd($bankDetails);
        foreach ($bankDetails as $pan => $banks){

            foreach($banks as $bank){
                $acc_name = $bank->acc_name;


                $investor_scheme_xirr = [];
                $portfolio_details = PortfolioDetails::where('pan',$pan)->where('acc_no', $bank->acc_no)->whereIn('portfolio_status',[1,2])->get();
                $investment_amount = 0;

                foreach ($portfolio_details as $portfolio){
                    $investment_amount += ($portfolio->units_held * $portfolio->invested_nav);
                }
                $current_value = 0;

                $amount = array();
                $date = array();

                foreach ($portfolio_details as $portfolio){
                    $nav = HistoricNav::where('scheme_code', $portfolio->scheme_code)->orderBy('date','desc')->first();

                    $temp = $portfolio->units_held * $nav->nav;

                    $current_value += $temp;

                }

//                dd($current_value);



                $xirr_portfolio = $portfolio_details->groupBy('scheme_code');
                $xirr_withdraw = WithdrawDetails::where('pan',$pan)->where('acc_no', $bank->acc_no)->where('withdraw_status', 1)->get()->groupBy('scheme_code')->toArray();

                $scheme_current_value = 0;

//                dd($xirr_withdraw, $xirr_portfolio);


                //Looping through all the Portfolio of the User

                foreach ($xirr_portfolio as $scheme_code => $port){

//                    dd($scheme_code, array_key_exists($scheme_code, $xirr_withdraw));
                    $amount = [];
                    $date = [];

                    //checking whether there is a withdraw in the particular scheme code and adding it to the $amount and $date Variable.

                    if(array_key_exists($scheme_code, $xirr_withdraw)){

//                        dd('hello');
                        foreach ($xirr_withdraw[$scheme_code] as $withdraw){
                            $amount[] = $withdraw['withdraw_amount'];
                            $date[] = $withdraw['bse_redemption_date'];
                        }
                    }

//                    dd($amount, $date);


                    //Looping through all the portfolio details of the particular scheme and adding it to the variables $amount and $date
                    foreach ($port as $portfolio){
                        $scheme_nav = HistoricNav::where('scheme_code', $portfolio->scheme_code)->orderBy('date','desc')->first();

                        //to Check for Fully Withdrawn Details

                        if($portfolio->portfolio_status == 2){
                            $amount[] = -round($portfolio->initial_units_held * $portfolio->invested_nav, 3);
                        }else{
                            //this is for portfolio status 1.

                            if($portfolio->initial_units_held == $portfolio->units_held){
                                //If there is no Partial Redemption
                                $amount[] = -round($portfolio->initial_units_held * $portfolio->invested_nav, 3);

                            }else{
                                //if there is partial redemption.
                                // In partial Redemption Units will be reduced from 'units_held' field
//                                dd($portfolio);
                                $amount[] = -round($portfolio->units_held * $portfolio->invested_nav, 3);

                            }
                        }
//                        $amount[] = -round($portfolio->initial_units_held * $scheme_nav->nav, 3);
                        $date[] = $portfolio->bse_allotment_date;

                        $scheme_current_value += round($portfolio->units_held * $scheme_nav->nav, 3);



                    }




                    $amount[] = $scheme_current_value;
                    $date[] = $scheme_nav->date;

//                    dd($amount, $date, 'hello');


                    $scheme_current_value =0;


                    $sort_xirr = [];

                    for($i=0; $i<count($amount); $i++){
                        $sort_xirr[$date[$i]][] = $amount[$i];
                    }

                    ksort($sort_xirr);

                    $xirr_amount = [];
                    $xirr_date = [];
                    foreach ($sort_xirr as $x_date => $sort_amount){
                        foreach($sort_amount as $s_amount){
                            $xirr_date[] = $x_date;
                            $xirr_amount[] = $s_amount;
                        }
                    }



//                    dd($xirr_amount, $xirr_date, $scheme_code);

                    if(count($xirr_amount) == 2){
                        if($xirr_date[0] == $xirr_date[1]){
                            $scheme_xirr = 0;
                        }else{
                            $scheme_xirr = round(PHPExcel_Calculation_Financial::XIRR($xirr_amount,$xirr_date) * 100,2);
                        }
                    }else{

                        try{
                            $scheme_xirr = round(PHPExcel_Calculation_Financial::XIRR($xirr_amount,$xirr_date) * 100,2);
                        }catch(\Exception $e){
                            $scheme_xirr = 0;
                        }


                    }



                    $investor_scheme_xirr[] = $scheme_xirr;

                }

//                dd($investor_scheme_xirr);

                if(count($investor_scheme_xirr) == 0){
                    $investor_xirr = 0;
                }else{
                    $investor_xirr = array_sum($investor_scheme_xirr)/count($investor_scheme_xirr);

                }



                $p_or_loss = $current_value - $investment_amount;
//                dd($p_or_loss);

                $response[] = array(
                    'pan' => $pan,
                    'total_investment_amount' => round($investment_amount, 2),
                    'acc_name' => $acc_name,
                    'current_value' => round($current_value, 2),
                    'p_or_loss' => round($p_or_loss, 2),
                    'xirr' => round($investor_xirr, 2),
                );

            }



        }


        usort($response, function($a, $b) {
            return $a['acc_name'] <=> $b['acc_name'];
        });

        return $response;
    }


    public function getUserInvestmentHistory($id){


        $get_portfolio = new GetUserPortfolioDetails();

        $portfolio_details = $get_portfolio->userPortfolioDetails($id);

        if ($portfolio_details != null) {
            $portfolio_user_total = $portfolio_details['portfolio_total'];

            $portfolio = $this->getIndexCalculation(User::where('id', $id)->first()->bankDetails->groupBy('pan'));

            $total_xirr = [];
            foreach ($portfolio as $port){
                if($port['xirr'] > 0){
                    $total_xirr[] = $port['xirr'];
                }
            }

            $total_xirr_sum = array_sum($total_xirr)/count($total_xirr);

            $portfolio_user_total['total_xirr'] = $total_xirr_sum;


            $user_details = User::where('id',$id)->get()->toArray();

            $user_details= $user_details[0];
            //$id = $id;

            $investment_details = InvestmentDetails::where('user_id',$id)->get()->toArray();
            //dd($investment_details);
            return view('admin.admin-investment-details')->with(compact('portfolio','portfolio_user_total','user_details','investment_details','id'));
        }else{
            return view('admin.admin-investment-details');
        }

    }

    public function getUserPortfolioDetails($id){

        $get_portfolio = new GetUserPortfolioDetails();
        $portfolio_details = $get_portfolio->userPortfolioDetails($id);
        if (!empty($portfolio_details['portfolios'])) {
            $portfolio = $portfolio_details['portfolios'];
        }else{
            $portfolio = '';
        }
        $portfolio_user_total = $portfolio_details['portfolio_total'];


        $portfolio = $this->getIndexCalculation(User::where('id', $id)->first()->bankDetails->groupBy('pan'));

        $total_xirr = [];
        foreach ($portfolio as $port){
            if($port['xirr'] > 0){
                $total_xirr[] = $port['xirr'];
            }
        }

        $total_xirr_sum = array_sum($total_xirr)/count($total_xirr);

        $portfolio_user_total['total_xirr'] = $total_xirr_sum;


        $user_details = User::where('id',$id)->get()->toArray();


        //dd($portfolio_total);

        $user_id = $id;
        $portfolio_success = PortfolioDetails::where('user_id', $id)->where('portfolio_status', 1)->get();
        //$investment_status = $this->getUserInvestmentPerformance($id);
        $portfolio_total = PortfolioDetails::where('user_id', $id)->where('portfolio_status', 1)->sum('amount_invested');
        $pass_portfolio = array();
        $portfolio = array();
        $portfolio_code_name = array();

        $current_value_total = 0;
        $nr_total = 0;

        $dateArray = [];
        $amountArray = [];

        foreach ($portfolio_success as $portfolio) {


            $scheme_name = SchemeDetails::where('scheme_code',$portfolio->scheme_code)->pluck('scheme_name');

            $current_nav = DailyNav::where('scheme_code',$portfolio->scheme_code)->orderBy('date', 'desc')->first();//where('date',Carbon::now()->subDays(1)->toDateString())->pluck('nav_value');print_r(Carbon::now()->subDays(1)->toDateString());
            $current_value = (($portfolio->units_held) * $current_nav->nav_value);
            $current_value_total += $current_value;
            $current_value_total = round($current_value_total,2);
            $amount_invested = $portfolio->amount_invested;
            $net_returns = round(($current_value - $amount_invested),2);
            $nr_total += $net_returns;

            $scheme_type;
            if ($portfolio->portfolio_type == 'liquid') {
                $scheme_type = 'Liquid';
            }
            else if ($portfolio->portfolio_type == 'ust') {
                $scheme_type = 'UltraShort term';
            }
            else if ($portfolio->portfolio_type == 'arb') {
                $scheme_type = 'Arbitrage';
            }

            $investor_name = BankDetails::where('pan', $portfolio->pan)->value('acc_name');
            $pass_portfolio[] = array(
                'id' => $portfolio->id,
                'scheme_code' => $portfolio->scheme_code,
                'investor_name' => $investor_name,
                'fund_name' => $scheme_name[0],
                'folio_number' => $portfolio->folio_number,
                'scheme_type' => $scheme_type,
                'type' => $portfolio->portfolio_type,
                'transaction_type' => 'Inv',
                'date'=> date('d-m-Y', strtotime($portfolio->investment_date)),
                'amount'=> $portfolio->amount_invested,
                'inv_nav' => $portfolio->invested_nav,
                'units_held' => round($portfolio->units_held,4),
                'current_nav' => $current_nav->nav_value,
                'current_value' => round(($portfolio->units_held) * ($current_nav->nav_value),2),
                'net_returns' => $net_returns,
            );

            if(!array_key_exists($portfolio->scheme_code, $portfolio_code_name)) {
                $portfolio_code_name[$portfolio->scheme_code] =  $scheme_name[0];
            }
        }


        $withdraw_details = WithdrawDetails::where('user_id', $id)->where('withdraw_status',1)->get();
        $scheme_code_name = SchemeDetails::pluck('scheme_name','scheme_code');
        $scheme_code_name = $scheme_code_name->toArray();


        //iterating withdraw deatils
        foreach($withdraw_details as $withdraw_detail) {


            $investor_name = BankDetails::where('acc_no', $withdraw_detail->acc_no)->value('acc_name');


            $pass_portfolio[] = array(
                'id' => $withdraw_detail->id,
                'scheme_code' => $withdraw_detail->scheme_code,
                'investor_name' => $investor_name,
                'fund_name' => $scheme_code_name[$withdraw_detail->scheme_code],
                'folio_number' => '',
                'transaction_type' => 'WD',
                'type' => $withdraw_detail->portfolio_type,
                'date'=> date('d-m-Y', strtotime($withdraw_detail->withdraw_date)),
                'amount'=> $withdraw_detail->withdraw_amount,
                'inv_nav' => $withdraw_detail->withdraw_nav,
                'units_held' => round($withdraw_detail->withdraw_units,4),
                'current_nav' => '',
                'current_value' => '',
                'net_returns' => '',
            );

        }

        $portfolios = $pass_portfolio;
        unset($user_details['password']);
        $user_details = $user_details[0];

//         dd($portfolios);
        return view('admin.admin-portfolio-details')->with(compact('portfolios','user_details','portfolio_user_total','id'));
    }

    public function pendingOrders($data = null){
        $user_id = '';
        $investment_details = array();
        $pending_investments = array();
        $pending_withdraw = array();
        $response_type = 'data';
        $user_folio;


        if($user_id == '') {
            $users = User::all();
        } else {
            $users = User::find($user_id)->get();
        }

        //dd($users);

        foreach ($users as $user) {

            //getting user's investment detail
           $user_investments = $user->investmentDetails()
                        ->where('investment_status', 0)
                        ->orderBy('investment_date', 'asc')
                        ->get();


            //dd($user_investments); works fine


            foreach($user_investments as $user_investment) {

                    $user_portfolios = $user_investment->portfolioDetails()
                    ->whereIn('portfolio_status',[0,1])
                    ->get();

                    // dd($user_portfolios);



                    foreach($user_portfolios as $user_portfolio)
                    {
                        $investment_date = '';
                        if(array_key_exists($user_portfolio->investment_id, $pending_investments)) {
                            if($response_type == 'data') {
                                $pending_investments[$user_portfolio->investment_id]['amount'] += $user_portfolio->amount_invested;
                            } else {
                                $pending_investments[$user_portfolio->investment_id]['Amount'] += $user_portfolio->amount_invested;
                                //dd($pending_investments);
                            }
                        } else {


                            //below if condition is to get the Investment placed date and order to be executed date

                            if($user_portfolio->investment_date != NULL && $user_portfolio->investment_date != '') {
                                $investment_date = date('d-m-Y',strtotime($user_portfolio->investment_date));
                                $order_placed_date = date('d-m-Y',strtotime($user_portfolio->created_at));

                            }
                            if($response_type == 'data') {


                                //echo $user_portfolio->scheme_code;
//                                dd($user_portfolio);

//                                if($user_portfolio->bse_order_no == 42016226){
//                                    $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$user_portfolio->scheme_code)->where('pan', $user_portfolio->pan)->where('acc_no', $user_portfolio->acc_no)->where('portfolio_status',['1,2'])->get()->last();
//                                    dd($user_folio);
////                                    dd($user_portfolio);
//                                }
                                $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$user_portfolio->scheme_code)->where('pan', $user_portfolio->pan)->where('acc_no', $user_portfolio->acc_no)->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                                //dd($user->bankDetails->pan);

//                                dd($user_folio);

                                $payment_type =   ($user_investment->payment_type == 'rtgs' ? 'RTGS' : 'Net Banking');
                                $investor_name = BankDetails::where('pan', $user_portfolio->pan)->value('acc_name');
                                $pending_investments[$user_portfolio->investment_id] = array(
                                    'investment_id' => $user_portfolio->investment_id,
                                    'user_id' => $user->id,
                                    'user_name' => $investor_name,
                                    'user_pan' => $user_portfolio->pan,
                                    'contact_no' => $user->mobile,
                                    //'portfolio_type' => $this->portfolio_types[$user_investment->portfolio_type],
                                    //'investment_type' => $this->investment_types[$user_investment->investment_type],
                                    'investment_or_withdraw' => 'Investment',
                                    'execution_date' => $investment_date,
                                    'placed_date' => $order_placed_date,
                                    'amount' => $user_portfolio->amount_invested,
                                    'folio_number' => $user_folio['folio_number'],
                                    'bse_scheme_code' => $user_folio['bse_scheme_code'],
                                    'payment_type' => $payment_type,
                                    'utr_no' => $user_investment->utr_no,
                                );


                            } else {
                                $pending_investments[$user_portfolio->investment_id] = array(
                                    'SNo' => $count,
                                    'User Name' => $user->name,
                                    'Contact Number' => $user->mobile,
                                    'Portfolio Type' => $this->portfolio_types[$user_investment->portfolio_type],
                                    'Investment Type' => $this->investment_types[$user_investment->investment_type],
                                    'Transaction Type' => 'Investment',
                                    'Date' => $investment_date,
                                    'Amount' => $user_portfolio->amount_invested,
                                    'bse_scheme_code' => $user_folio['bse_scheme_code'],
                                );
                                $count++;
                            }
                        }
                    }//foreach of User Portfolio ends here

            }//foreach of User Investments ends here



             /*
                    Getting Withdrawal Order starts below

            */

                    if ($user->WithdrawDetails != null) {
                        $user_withdraws = $user->WithdrawDetails
                        ->where('withdraw_status', 0)
                        ->groupBy('withdraw_group_id');


                        foreach ($user_withdraws as $user_withdraw => $withdraws) {
                           foreach ($withdraws as $withdraw) {


                            if($withdraw->withdraw_date != NULL && $withdraw->withdraw_date != '')
                            {
                                            $withdraw_date = date('d-m-Y',strtotime($withdraw->withdraw_date));
                                            $order_placed_date = date('d-m-Y',strtotime($withdraw->created_at));

                            }

                                //echo $user_portfolio->scheme_code;
                                $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$withdraw->scheme_code)->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();



                                if (array_key_exists($withdraw->withdraw_group_id, $pending_withdraw)) {

                                    //echo $withdraw->withdraw_group_id."<br>";



                                    $pending_withdraw[$withdraw->withdraw_group_id] = array(
                                                'withdraw_id' => $withdraw->id,
                                                'withdraw_group_id' => $withdraw->withdraw_group_id,
                                                'user_id' => $user->id,
                                                'user_name' => $user->name,
                                                'user_pan' => $withdraw->pan,
                                                'contact_no' => $user->mobile,
                                                'portfolio_type' => $withdraw->portfolio_type,
                                                //'investment_type' => $this->investment_types[$user_investment->investment_type],
                                                'investment_or_withdraw' => 'Withdraw',
                                                'execution_date' => $withdraw_date,
                                                'placed_date' => $order_placed_date,
                                                'amount' => $pending_withdraw[$withdraw->withdraw_group_id]['amount'] + $withdraw->withdraw_amount,
                                                'folio_numer' => $user_folio['folio_number'],
                                            );


                                }else{

                                    $pending_withdraw[$withdraw->withdraw_group_id] = array(
                                                'withdraw_id' => $withdraw->id,
                                                'withdraw_group_id' => $withdraw->withdraw_group_id,
                                                'user_id' => $user->id,
                                                'user_name' => $user->name,
                                                'user_pan' => $withdraw->pan,
                                                'contact_no' => $user->mobile,
                                                'portfolio_type' => $withdraw->portfolio_type,
                                                //'investment_type' => $this->investment_types[$user_investment->investment_type],
                                                'investment_or_withdraw' => 'Withdraw',
                                                'execution_date' => $withdraw_date,
                                                'placed_date' => $order_placed_date,
                                                'amount' => $withdraw->withdraw_amount,
                                                'folio_numer' => $user_folio['folio_number'],
                                            );
                                }


                           }
                        }
                    }else{
                        $user_withdraws = null;
                    }







       }//foreach of $users ends here


//        dd($pending_investments);
        $investment_details = $pending_investments;
        $withdraw_details = $pending_withdraw;

        $pending_withdraw_collection = [];

        //The below code is to loop through pending Investments and store it in the variable $pending_investment_collection

        foreach ($investment_details as $investment_detail) {
                //dd($investment_detail['investment_id']);
                $pending_investment_collection[] = PortfolioDetails::whereIn('portfolio_status',[0,1])->where('investment_id',$investment_detail['investment_id'])->get();
        }


        //The below code is to loop through pending Withdraws and store it in the variable $pending_withdraws_collection
        foreach ($withdraw_details as $withdraw_detail) {



                $pending_withdraw_collection[] = WithdrawDetails::where('withdraw_status',0)->where('withdraw_group_id',$withdraw_detail['withdraw_group_id'])->get();
        }

        //dd($pending_investment_collection);

        if (!empty($pending_investment_collection)) {
            foreach ($pending_investment_collection as $pending_orders) {

                foreach ($pending_orders as $pending_order) {
                    //dd($pending_order);
                    $scheme_name = SchemeDetails::where('scheme_code',$pending_order['scheme_code'])->pluck('scheme_name','bse_scheme_code')->toArray();
                    //dd($scheme_name);
                    $bse_scheme_code = array_keys($scheme_name);
                    $pending_order['scheme_name'] = $scheme_name[$bse_scheme_code[0]];



                    $pending_order['bse_scheme_code'] = $bse_scheme_code[0];

//                    dd($pending_order);

                    $user_folio = PortfolioDetails::where('user_id',$pending_order['user_id'])->where('pan', $pending_order['pan'])->where('acc_no', $pending_order['pan'])->where('scheme_code',$pending_order['scheme_code'])->where('portfolio_status',['1,2'])->get()->last();

                    $pending_order['folio_number'] = $user_folio['folio_number'];

                }
            }
        }


        if (!empty($pending_withdraw_collection)) {
            foreach ($pending_withdraw_collection as $pending_orders) {

                foreach ($pending_orders as $pending_order) {
                    # code...
                    $scheme_name = SchemeDetails::where('scheme_code',$pending_order['scheme_code'])->pluck('scheme_name');
                    $pending_order['scheme_name'] = $scheme_name[0];

                    $user_folio = PortfolioDetails::where('user_id',$pending_order['user_id'])->where('scheme_code',$pending_order['scheme_code'])->where('portfolio_status',['1,2'])->whereNotNull('folio_number')->get()->last();

                    $pending_order['folio_number'] = $user_folio['folio_number'];

                }
            }
        }




        /*Getting Pending Investments and Pending Withdrawals are Over.

                ***** About Investment *****

                Investment details are stored in variable $investment_details
                Portfolio details of the investments are stored in $pending_investment_collection


                ***** About Withdrawals ****

                Withdraw details are stored in variable $withdraw_details
                Details of the withdrawals are stored in $pending_withdraw_collection

        */


        // dd($pending_withdraw_collection);

        if($data == 'export'){

            $response_array = array('pending_investment' => $pending_investment_collection,'pending_withdraw'=>$pending_withdraw_collection);



            return $response_array;

        }else{
//             dd($pending_investment_collection);
            return view('admin.pending-orders')->with(compact('investment_details','pending_investment_collection','withdraw_details','pending_withdraw_collection'));
        }
    }

    public function editUserPortfolio(Request $request){
        $request->validate([
            'modal_folio_number' => 'required',
            'modal_invested_nav' => 'required',
            'dop' => 'required',
            'p_id' =>'required',
        ]);

        $dop = date('Y-m-d',strtotime($request['dop']));

        $update = PortfolioDetails::where('id',$request['p_id'])->update(['folio_number'=>$request['modal_folio_number'],'invested_nav'=>$request['modal_invested_nav'],'investment_date'=>$dop]);

        if ($update) {
            return response()->json(['msg'=>'success']);
        }else{
            return response()->json(['msg'=>'failure']);
        }
    }

    public function updateBseDate(Request $request){
        $request->validate([

            'portfolio_id' => 'required',
            'status' => 'required',
            'pending_type' => 'required',
            ]);


            $date = Carbon::now();

        if ($request['status'] == 'checked') {

            if ($request['pending_type'] == 'i') {
                //echo $request['portfolio_id'];
               $update =  PortfolioDetails::where('id',$request['portfolio_id'])->update(['bse_order_date'=>$date]);

                if ($update) {

                    //return "updated";

                   $date = date('d/m/Y', strtotime($date));
                   return response()->json(['msg'=>'date_success','portfolio_id'=>$request['portfolio_id'],'date'=>$date]);
                }
            }

            if ($request['pending_type'] == "w") {




                $update =  WithdrawDetails::where('id',$request['portfolio_id'])->update(['withdraw_bse_date'=>$date]);

                //return "hello";

                if ($update) {


                    //return "updated";

                   $date = date('d/m/Y', strtotime($date));
                   return response()->json(['msg'=>'date_success','portfolio_id'=>$request['portfolio_id'],'date'=>$date]);
                }
            }

        }



        if ($request['status'] == 'not_checked') {

            if ($request['pending_type'] == 'i') {

                $update =  PortfolioDetails::where('id',$request['portfolio_id'])->update(['bse_order_date'=> NULL]);

                if ($update) {
                    return response()->json(['msg'=>'nodate_success','portfolio_id'=>$request['portfolio_id'],'date'=>'']);
                }
            }

            if ($request['pending_type'] == 'w') {

                $update =  WithdrawDetails::where('id',$request['portfolio_id'])->update(['withdraw_bse_date'=> NULL]);

                if ($update) {
                    return response()->json(['msg'=>'nodate_success','portfolio_id'=>$request['portfolio_id'],'date'=>'']);
                }
            }
        }
    }

    public function completePendingOrders(Request $request){
        $request->validate([

            'order_ids' => 'required',
            'order_type' => 'required',

            ]);

            // echo "hello";

            $order_ids = $request['order_ids'];
            $order_type = $request['order_type'];


            $transaction_status_data = array();
            $nav_date = new Carbon();
            $current_date = $nav_date->format('Y-m-d');
            //$nav_date->subDay();
            $nav_date = $nav_date->format('Y-m-d');
            $investment_id = '';
            $save_status = false;
            $approved_amount = 0;
            $save_message = '';//Log::info($pending_portfolios);
            $scheme_details = SchemeDetails::get()->pluck('scheme_name','scheme_code');
            //dd($pending_portfolios);

            $scheme_code_name = $scheme_details->toArray();
            $notif_mode = 0;
            $user;


                //If order type is Investment it is processed below

                if ($order_type == 'inv') {

                    $portfolio_ids = preg_split('/,/', $order_ids);
                    $portfolio_orders = array();

                    foreach ($portfolio_ids as $id) {

                        $portfolio_orders[] = PortfolioDetails::where('id',$id)->get();
                    }

                    //print_r($portfolio_orders);


                    foreach ($portfolio_orders as $portfolio_order) {


                        foreach ($portfolio_order as $portfolio) {

                            $portfolio_detail = PortfolioDetails::where('id',$portfolio->id)
                            ->first();

                            // print_r($portfolio_detail);
                            // die();

                            if($portfolio_detail != NULL) {
                                if($portfolio_detail->portfolio_status == 0){



                                    //getting Investment detail of the particular portfolio details
                                    $investment_id = intval($portfolio_detail->investment_id);//Log::info('Portfolio id'.$portfolio_detail->id);
                                    $amount_invested = $portfolio_detail->amount_invested;
                                    $scheme_nav = DailyNav::where('date', '<', $nav_date)
                                    ->where('scheme_code', $portfolio_detail->scheme_code)
                                    ->orderBy('date', 'desc')
                                    ->first();//Log::info($nav_date);Log::info($scheme_nav);die();Log::info('get me if you can');

                                    $units_held = $amount_invested / $scheme_nav->nav_value;
                                    $portfolio_detail->invested_nav = $scheme_nav->nav_value;
                                    $portfolio_detail->units_held = $units_held;
                                    $portfolio_detail->initial_units_held = $units_held;
                                    $portfolio_detail->investment_date = $current_date;
                                    $portfolio_detail->portfolio_type = 'liquid';
                                    $portfolio_detail->portfolio_status = 1;
                                    $portfolio_detail->save();


                                    $transaction_status_data[] = array(
                                        'scheme_name' => $scheme_code_name[$portfolio_detail->scheme_code],
                                        'scheme_amount'=> $amount_invested,
                                        'status' => $portfolio_detail->portfolio_status,
                                    );
                                    $user = $portfolio_detail->User;
            //Log::info($transaction_status_data);
                                    $save_status = true;
                                    $save_message = "You have successfully approved an investment";
                                    $approved_amount += $amount_invested;
                                    //$notif_mode = $portfolio_detail->investmentDetails->realise_response;

                                    /*if($notif_mode == 1) {
                                        $user = $portfolio_detail->User;
                                        Mail::send('admin.investment_realise', array('user_name' => $user->name, 'investment_date' => date('d-m-Y', strtotime($current_date)), 'investment_type' => 'Investment', 'amount_invested' => $approved_amount, 'investment_details' => $transaction_status_data), function ($message) use ($user) {
                                            $message->from('test@rightfundspartners.com', 'Investment Summary');
                                            $message->to($user->email, $user->name);
                                            //$message->attach('profile_pic.jpg');
                                        });
                                    } elseif($notif_mode == 2) {
                                        $ch = curl_init();
                                        $user="naveen@rightfunds.com:Rightfunds20";
                                        $receipientno = ;
                                        $senderID="RFUNDS";
                                        $msgtxt="Your Investment of Rs. ".$approved_amount." approved successfully";
                                        curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                        curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
                                        $buffer = curl_exec($ch);
                                        curl_close($ch);
                                        //if(!empty ($buffer)) {
                                    }*/
                                }
                            } else {

                                $save_status = false;
                                $save_message = "Investment approve failed. Please check and try again";
                            }
                        }//foreach of portfolio id ends
                    }//foreach $portfolio_order ends

                }//Investment order type ends

                /*If the order type is Withdrawal it is process below*/

                if ($order_type == 'wd') {



                    $withdraw_ids = preg_split('/,/', $order_ids);
                    $withdraw_orders = array();
                    //dd($withdraw_ids);
                    foreach ($withdraw_ids as $id) {

                        $withdraw_orders[] = WithdrawDetails::where('id',$id)->get();
                    }

                    //print_r($portfolio_orders);

                    //dd($withdraw_orders);
                    foreach ($withdraw_orders as $withdraw_order) {
                        foreach ($withdraw_order as $withdraw) {
                            $withdraw_detail = WithdrawDetails::where('id',$withdraw->id)
                            ->first();

                            if($withdraw_detail != NULL) {

                                //echo $withdraw_detail->withdraw_date;
                                $current_value = 0; //current value of the portfolio
                                $scheme_code = $withdraw_detail->scheme_code; //scheme code of the portfolio to be processed

                                // echo $scheme_code;
                                $withdraw_amount = $withdraw_detail->withdraw_amount; //amount to be withdrawn
                                // echo $withdraw_amount;
                                //get the nav details related to the current processing withdraw id

                                //$daily_nav = DailyNav::all();
                                //dd($daily_nav);
                                //dd($nav_date);

                                //echo $withdraw->scheme_code;
                                $scheme_nav =  DailyNav::where('date', '<', $nav_date)
                                    ->where('scheme_code', $withdraw_detail->scheme_code)
                                    ->orderBy('date', 'desc')
                                    ->first();
                                //dd($scheme_nav->nav_);



                                //get the user related to the withdraw
                                $user = $withdraw_detail->User;



                                //get the portfolio's detail related to the withdraw
                                $portfolio_details = $user->portfolioDetails()
                                    ->where('scheme_code', $scheme_code)
                                    ->where('portfolio_status', 1)
                                    ->get();





                                //Getting current value just to check if the withdrawal amount is lesser than current amount;
                               $current_value = $portfolio_details->sum('units_held') * $scheme_nav->nav_value;



                                if($current_value >= $withdraw_amount) {
                                    $withdraw_units = $withdraw_amount / $scheme_nav->nav_value; //calculating the units to be withdrawn
                                    $withdraw_detail->withdraw_units = $withdraw_units;
                                    $withdraw_detail->withdraw_nav = $scheme_nav->nav_value;
                                    $withdraw_detail->withdraw_date = date('Y-m-d');
                                    $withdraw_detail->withdraw_status = 1;
                                    $portfolio_save_status;
                                    //loop through all active portfolio details of the scheme to reduce the withdrawn units
//                                    foreach($portfolio_details as $portfolio_detail) {
//                                        $user = $portfolio_detail->User;
//                                        //check if the withdraw units is greater than 0
//                                        if($withdraw_units > 0) {
//                                            //if the units held and withdraw units are same
//                                            if($portfolio_detail->units_held == $withdraw_units) {
//                                                $withdraw_units = 0;
//                                                $portfolio_detail->units_held = 0;
//                                                $portfolio_detail->amount_invested = 0;
//                                                $portfolio_detail->portfolio_status = 2;
//                                                $portfolio_save_status = $portfolio_detail->save();
//                                            } elseif($portfolio_detail->units_held < $withdraw_units) {
//                                                //if the units held less than  withdraw units
//                                                $withdraw_units -= $portfolio_detail->units_held;
//                                                $portfolio_detail->units_held = 0;
//                                                $portfolio_detail->amount_invested = 0;
//                                                $portfolio_detail->portfolio_status = 2;
//                                                $portfolio_save_status = $portfolio_detail->save();
//                                            } elseif($portfolio_detail->units_held > $withdraw_units) {
//                                                //if the units held is greater than the units to be withdrawn
//                                                $portfolio_detail->units_held -= $withdraw_units;
//                                                $portfolio_detail->amount_invested -= ($withdraw_units * $scheme_nav->nav_value);
//                                                $portfolio_detail->portfolio_status = 2;
//                                                $withdraw_units = 0;
//                                                $portfolio_save_status = $portfolio_detail->save();
//                                            }
//                                        }
//                                    }

                                    foreach ($portfolio_details as $portfolio_detail) {

//                                    dd($withdraw_units);
                                        if($withdraw_units != 0){


                                            if($portfolio_detail->units_held == $withdraw_units) {
//                                            dd('1');
                                                //Checking wthether the available portfolio units is equal to withdraw units. in case it is "YES". So changing the portfolio status to 2 (Fully withdrawn)
                                                $withdraw_units = 0;
                                                $portfolio_detail->units_held = 0;
                                                $portfolio_detail->amount_invested = 0;
                                                $portfolio_detail->portfolio_status = 2;
                                                $save_portfolio = $portfolio_detail->save();
                                            } elseif($portfolio_detail->units_held < $withdraw_units) {
//                                            dd($portfolio_detail->units_held, $withdraw_units);

                                                //if the units held less than  withdraw units
                                                $withdraw_units -= $portfolio_detail->units_held;
                                                $portfolio_detail->units_held = 0;
                                                $portfolio_detail->amount_invested = 0;
                                                $portfolio_detail->portfolio_status = 2;
                                                $save_portfolio = $portfolio_detail->save();
                                            } elseif($portfolio_detail->units_held > $withdraw_units) {
//                                            dd(3);
                                                //if the units held is greater than the units to be withdrawn
                                                $portfolio_detail->units_held -= $withdraw_units;
                                                $portfolio_detail->amount_invested -= ($withdraw->withdraw_units * $redeem[18]);
                                                $withdraw_units = 0;
                                                $portfolio_detail->portfolio_status = 1;
                                                $save_portfolio = $portfolio_detail->save();
                                            }
                                        }
                                    }

                                    if($save_portfolio) {
                                        if($withdraw_detail->save()) {
                                            $save_status = true;
                                            $save_message = "You have successfully approved an withdraw";
                                            $approved_amount += $withdraw_amount;
                                            $transaction_status_data[] = array(
                                                'scheme_name' => $scheme_code_name[$scheme_code],
                                                'scheme_amount'=> $withdraw_amount,
                                                'status' => 1,
                                            );
                                        } else {
                                            $save_status = false;
                                            $save_message = "Withdraw save failed. Please check and try again";
                                        }
                                    } else {
                                        $save_status = false;
                                        $save_message = "Portfolio Save Status failed. Please check and try again";
                                    }
                                } else {
                                    $save_status = false;
                                    $save_message = "Withdraw value is greater than current value. Please check and try again";
                                }
                            }
                        }//foreach of $withdraw ends
                    }//foreach of $withdraw_order ends
                }// Withdraw Order type ends

                if($investment_id !='' && $save_status) {
                    $portfolios = PortfolioDetails::where('investment_id', $investment_id)
                        ->where('portfolio_status',0);//Log::info(gettype($portfolios));
                    if($portfolios->count() == 0) {
                        $investment_detail = InvestmentDetails::where('id',$investment_id)->first();
                        $investment_detail->investment_status = 1;
                        $investment_detail->investment_date = $current_date;
                        $investment_detail->save();
                    }
                }

                //preparing response for approve withdraw and investment
                if($save_status) {
                    return response()->json(['msg' =>'success', 'status' => $save_message, 'date'=> date('d-m-Y',strtotime($current_date)), 'amount'=>$approved_amount]);
                } else {
                    return response()->json(['msg' =>'fail', 'status' => $save_message]);
                }
    }


    public function orderHistory(Request $request){
        $user_investments_withdraws = array();
        $start_date = '';
        $end_date = '';
        $response_type = 'data';
        $count =1;
        $user_id = '';
        $user_withdraw_details = null;
        $user_investment_details;
        if($user_id == '') {
            $users = User::all();
        } else {
            $users = User::where('id', $user_id)->get();//Log::info($users->id);
        }//Log::info($users->count());

        //iterating all users to get investments
        foreach ($users as $user) {//Log::info($user->id);

            //for user investments
            $user_investments = $user->investmentDetails()
                ->whereIn('investment_status', [1,2,3])
                ->orderBy('investment_date', 'asc')
                ->get();



            //iterating all investments of the user
            foreach($user_investments as $user_investment) {
                if($start_date != '' && $end_date != '') {
                    $start_date = date('Y-m-d', strtotime($start_date));
                    $end_date = date('Y-m-d', strtotime($end_date));
                    $user_portfolios = $user_investment->portfolioDetails()
                    ->whereBetween('investment_date', [$start_date, $end_date])
                    //->where('investment_date','>', $start_date)
                    //->where('investment_date','<', $end_date)
                    ->whereIn('portfolio_status',[0,1,2,3])
                    ->get();
                } else {
                     $user_portfolios = $user_investment->portfolioDetails()
                    ->whereIn('portfolio_status',[0,1,2,3])
                    ->get();
                }

                if($user_portfolios->count() > 0) {
                    $user_portfolio = $user_portfolios->first();
                    $investment_date = '';

                    if($user_portfolio->investment_date != NULL) {
                        $investment_date = date('d-m-Y',strtotime($user_portfolio->investment_date));
                    }


//                    if()

                    if($response_type == 'data') {
                        $investor_name = BankDetails::where('pan', $user_portfolio->pan)->value('acc_name');
                        $user_investments_withdraws[] = array(
                            'id' => $user_portfolio->investment_id,
                            'user_name' => $investor_name,
                            'contact_no' => $user->mobile,
                            'pan' => $user_portfolio->pan,
                            'investment_or_withdraw' => 'Investment',
                            'date' => $investment_date,
                            'amount' => $user_portfolios->sum('initial_amount_invested'),
                            'status' => $user_investment->investment_status,
                        );
                    } else {
                        $user_investments_withdraws[] = array(
                            'SNO' => $count,
                            'Name' => $user->name,
                            'Contact Number' => $user->mobile,
                            'Transaction Type' => 'Investment',
                            'Date' => $investment_date,
                            'Amount' => $user_portfolios->sum('amount_invested'),
                            'Status' => $user_investment->investment_status,
                        );
                        $count++;
                    }
                }
            }

            $user_investment_details = $user_investments_withdraws;

//            dd($user_investment_details);


            //for user withdrawls
            if($start_date != '' && $end_date != '') {


                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));

                $user_withdraws = $user->withdrawDetails()
                    ->whereBetween('withdraw_date', [$start_date, $end_date])
                    ->whereIn('withdraw_status', [0,1,2])
                    ->groupBy('withdraw_group_id');
            } else {

                if ($user->withdrawDetails != null) {

                    $user_withdraws = $user->WithdrawDetails
                    ->whereIn('withdraw_status', [0,1,2])
                    ->groupBy('withdraw_group_id');

//                    if($user->role != 1){
//                        dd($user_withdraws);
//                    }

                }

            }




            if($user_withdraws != null){
                if(count($user_withdraws) > 0) {
                    foreach($user_withdraws as $user_withdraw) {
                        $withdraw_data = $user_withdraw->first();

                        if($response_type == 'data') {

                            $user_investments_withdraws[] = array(
                                'id' => $withdraw_data->withdraw_group_id,
                                'user_name' => $user->name,
                                'contact_no' => $user->mobile,
                                'investment_or_withdraw' => 'Withdraw',
                                'pan' => $withdraw_data->pan,
                                'date' => date('d-m-Y',strtotime($withdraw_data->withdraw_date)),
                                'amount' => $user_withdraw->sum('withdraw_amount'),
                                'status' => $withdraw_data->withdraw_status,
                            );

                            $user_withdraw_details[] =  end($user_investments_withdraws);
                        } else {
                            $user_investments_withdraws[] = array(
                                'SNO' => $count,
                                'Name' => $user->name,
                                'Contact Number' => $user->mobile,
                                'Transaction Type' => 'Withdraw',
                                'Date' => date('d-m-Y',strtotime($withdraw_data->withdraw_date)),
                                'Amount' => $user_withdraw->sum('withdraw_amount'),
                                'Status' => $withdraw_data->withdraw_status,
                            );
                            $count++;
                        }
                    }
                }
            }
        }

//        dd($user_investments_withdraws);

        $order_history = $user_investments_withdraws;

//        dd($order_history);
        foreach ($user_investment_details as $inv) {
            $user_portfolio_details[] = PortfolioDetails::where('investment_id',$inv['id'])->with('SchemeDetails')->get()->toArray();
        }


//        dd($user_portfolio_details);

        $total_withdraws = WithdrawDetails::all();


        return view('admin.order-history')->with(compact('order_history','user_portfolio_details', 'total_withdraws'));
    }

    public function modifyPortfolio(Request $request){
        $request->validate([
            'p_ids' => 'required',
            ]);

//        $p = "[".$request['p_ids']."]";

        $p_ids = explode(',',$request['p_ids']);

//        dd($p_ids);
        $portfolio_update = PortfolioDetails::whereIn('id',$p_ids)->update([
            'folio_number' => null,
            'units_held' => 0,
            'initial_units_held' => null,
            'invested_nav' => null,
            'portfolio_status' => 0,
            ]);

        if ($portfolio_update) {

            $inv_id = PortfolioDetails::where('id',$p_ids[0])->pluck('investment_id');
            $inv = PortfolioDetails::where('id',$p_ids[0])->where('portfolio_status',1)->count();
            if ($inv == 0) {
                $update_inv = InvestmentDetails::where('id',$inv_id[0])->update(['investment_status'=>0]);
                if ($update_inv) {
                    return response()->json(['msg'=>'success']);
                }else{
                    return response()->json(['msg'=>'failure']);
                }
            }

        }else{
            return response()->json(['msg'=>'failure']);
        }

    }


    public function exportPendingOrders(){

        $response = $this->PendingOrders('export');

        //dd($response);

        $export_array = array();

        $pending_investment = $response['pending_investment'];
        // dd($pending_investment);
        $pending_withdraw = $response['pending_withdraw'];

        foreach ($pending_investment as $key => $value) {
            // dd($investment);
            foreach ($value as $investment) {

                if($investment['folio_number'] == ''){
                    $purchase_type = 'FRESH';
                    $folio_number = '';
                }else{
                    $purchase_type = 'ADDITIONAL';
                    $folio_number = trim($investment['folio_number']);
                }


                $export_array[] = trim($investment['bse_scheme_code']).'|'.'P'.'|'.$purchase_type.'|'.$investment['user_pan'].'|'.'P'.'|'.$investment['amount'].'|'.$folio_number.'|'.''.'|'.'Y'.'|'.''.'|'.'E173580'.'|'.'Y'.'|'.'N'.'|'.'N'.'|'.'N'.'|'.''.'|'.PHP_EOL;


            }
        }

        // $file_name = date('d-m-Y').'- Pending Orders.txt';
        // $myfile = fopen($file_name, "w");
        // fwrite($myfile, 'hello');
        // fclose($myfile);
        //  return \Response::download($myfile);
        $file_name = date('d-m-Y').'- Pending Orders.txt';
        //foreach ($export_array as $entry ) {
            //echo $entry."<br>";
            \File::put(public_path('/files/bulkupload/'.$file_name),$export_array);
        //}

         // $data = json_encode(['Example 1','Example 2','Example 3',]);
         //
         // \File::put(public_path('/files/upload'.$file_name),$data);
         return \Response::download(public_path('/files/bulkupload/'.$file_name));
    }



        public function updateAllotmentStatus(Request $request){
        $validator = \Validator::make($request->all(),[
            'allotment-file' => 'required',
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return response()->json(['msg' => 'Allotment File Not included']);
        } else {

            if($request->hasFile('allotment-file')){
                    $allotment_file = $request->file('allotment-file');
                    $allot_file_name = \Auth::user()->id.'-allot-'.date('d-m-Y').'-'.str_random().'.'. $allotment_file->getClientOriginalExtension();
                    $new_file = $request->file('allotment-file')->move(base_path() . '/public/files/AF/', $allot_file_name);
                    //$status = $cancelled_cheque->move(base_path() . '/files/cc/', $cancelled_cheque->getClientOriginalName());

                    $file_contents = file($new_file);
                    $allot_count = 0;
                    foreach ($file_contents as $line ) {
                        $allot = explode('|', $line);



                        $update_portfolio = PortfolioDetails::where('bse_order_no',$allot[1])->update(['folio_number'=>$allot[12],'initial_units_held'=>$allot[19],'units_held'=>$allot[19],'invested_nav'=>$allot[18],'portfolio_status'=>1,'bse_allotment_date'=>$allot[4]]);

                        $port_inv_id = PortfolioDetails::where('bse_order_no',$allot[1])->value('investment_id');
                        // return $port_inv_id;
                        $not_invested_port = PortfolioDetails::where('investment_id',$port_inv_id)->where('portfolio_status',0)->count();


                        if ($not_invested_port == 0) {
                            InvestmentDetails::where('id',$port_inv_id)->update(['investment_status'=>1]);
                        }

                        if ($update_portfolio) {
                            $allot_count++;
                        }


                    }

                    if (count($file_contents) == $allot_count) {
                        return response()->json(['msg'=>'success','response'=>'Allotment Status have been uploaded Successfully.']);
                    }else{
                        return response()->json(['msg'=>'success','response'=>'Allotment Status upload failed.']);
                    }



                }else{
                        return response()->json(['msg'=>'failure','response'=>'Cancelled Cheque Upload Failed. Check Your internet Connection.']);
                }

        }
    }


    public function updateRedemptionStatus(Request $request){



        /*
         * IMPORTANT NOTE :
         *
         * 1. Portfolio STATUS is set to '1' for both partial Withdrawn and full active entries. The difference can be found by comparing the `initial_amount_invested` and `amount_invested` columns
         * and also `units_held` and `initial_units_held`
         *
         * PORTFOLIO STATUS Definitions
         *
         * 1 - Active and partially redeemed;
         * 2 - Fully Withdrawn
         * 3 - Cancelled.
         *
         *
         * */


        $validator = \Validator::make($request->all(),[
            'redemption-file' => 'required',
        ]);

        if ($validator->fails()) {
            //return $validator->errors();
            return response()->json(['msg' => 'Redemption File Not included']);
        } else {

            if($request->hasFile('redemption-file')){
                    $redeem_file = $request->file('redemption-file');
                    $redeem_file_name = \Auth::user()->id.'-redeem-'.date('d-m-Y').'-'.str_random().'.'. $redeem_file->getClientOriginalExtension();
                    $new_file = $request->file('redemption-file')->move(base_path() . '/public/files/RF/', $redeem_file_name);
                    //$status = $cancelled_cheque->move(base_path() . '/files/cc/', $cancelled_cheque->getClientOriginalName());

                    $file_contents = file($new_file);


                    $orderNo = [];

                    foreach ($file_contents as $line ) {

                        $redeem = explode('|', $line);



                        $withdraw_entry = WithdrawDetails::where('bse_order_no', $redeem[1])->get();
                        $withdraw_count = 0;




                        foreach ($withdraw_entry as $withdraw) {
//                            dd($withdraw);
                            $withdraw_units = (float)$redeem[19];
                            $save_portfolio = 0;
                            if ($withdraw->full_amount == '0') {


                                $portfolio_details = PortfolioDetails::where('user_id',$withdraw->user_id)
                                ->where('scheme_code',$withdraw->scheme_code)
                                ->where('portfolio_status',1)
                                ->get();

//                                dd($portfolio_details);

                                foreach ($portfolio_details as $portfolio_detail) {

//                                    dd($withdraw_units);
                                    if($withdraw_units != 0){


                                        if($portfolio_detail->units_held == $withdraw_units) {
//                                            dd('1');
                                            //Checking wthether the available portfolio units is equal to withdraw units. in case it is "YES". So changing the portfolio status to 2 (Fully withdrawn)
                                            $withdraw_units = 0;
                                            $portfolio_detail->units_held = 0;
                                            $portfolio_detail->amount_invested = 0;
                                            $portfolio_detail->portfolio_status = 2;
                                            $save_portfolio = $portfolio_detail->save();
                                        } elseif($portfolio_detail->units_held < $withdraw_units) {
//                                            dd($portfolio_detail->units_held, $withdraw_units);

                                            //if the units held less than  withdraw units
                                            $withdraw_units -= $portfolio_detail->units_held;
                                            $portfolio_detail->units_held = 0;
                                            $portfolio_detail->amount_invested = 0;
                                            $portfolio_detail->portfolio_status = 2;
                                            $save_portfolio = $portfolio_detail->save();
                                        } elseif($portfolio_detail->units_held > $withdraw_units) {
//                                            dd(3);
                                            //if the units held is greater than the units to be withdrawn
                                            $portfolio_detail->units_held -= $withdraw_units;
                                            $portfolio_detail->amount_invested -= ($withdraw->withdraw_units * $redeem[18]);
                                            $withdraw_units = 0;
                                            $portfolio_detail->portfolio_status = 1;
                                            $save_portfolio = $portfolio_detail->save();
                                        }
                                    }
                                }

                                $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1, 'bse_redemption_date'=>$redeem[4]]);

                                if ($save_portfolio && $update_withdraw) {
                                    $withdraw_count++;
                                }

                            }

                            if ($withdraw->full_amount == '1') {
                                $update_portfolio = PortfolioDetails::where('user_id',$withdraw->user_id)
                                ->where('scheme_code', $withdraw->scheme_code)
                                ->where('portfolio_status', 1)
                                ->update(['units_held'=> 0,'amount_invested'=>0,'portfolio_status'=>2]);

                                $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1, 'bse_redemption_date'=>$redeem[4]]);

                                if ($update_portfolio && $update_withdraw) {
                                    $withdraw_count++;
                                }
                            }


                        }




                        //die();

                        //$redeem[18] ==> WIthdraw NAV
                        //$redeem[19] ==> WIthdraw unit
                        //$redeem[20] ==> WIthdraw amount



                    }

//                    dd($orderNo);

                    if (count($file_contents) == $withdraw_count) {
                        return response()->json(['msg'=>'success','response'=>'Redemption Status Updated Succesfully']);
                    }else if((count($file_contents) > $withdraw_count) && ($withdraw_count != 0)){
                        return response()->json(['msg'=>'success','response'=>'Redemption Status partially Updated']);
                    }else{
                        return response()->json(['msg'=>'failure','response'=>'Redemption Status update Failed']);

                    }

                }else{
                        return response()->json(['msg'=>'failure','response'=>'Cancelled Cheque Upload Failed. Check Your internet Connection.']);
                }

        }
    }


    public function addManualPortfolio(Request $request){
        $request->validate([
            'user_id' => 'required',
            'inv_amount' => 'required',
            'pan_number' => 'required',
            'utr_no' => 'required',
            'inv_date' => 'required',
            'payment_type' => 'required',
            'port_details' => 'required',
            'acc_no' => 'required',
        ]);



        $date = date('Y-m-d',strtotime($request['inv_date']));
        $scheme_details = SchemeDetails::pluck('scheme_status','scheme_code')->toArray();


        $save_inv = InvestmentDetails::create([
                'user_id' => $request['user_id'],
                'investment_amount' => $request['inv_amount'],
                'pan' => $request['pan_number'],
                'investment_status' => 1,
                'investment_date' => $date,
                'payment_type' => $request['payment_type'],
                'utr_no' => $request['utr_no'],
            ]);

        if ($save_inv) {
            $port_details = json_decode(stripslashes($request['port_details']));

            $count = 0;
            $port_count = count($port_details);

            foreach ($port_details as $portfolio) {

                $portfolio_details = new PortfolioDetails();
                $portfolio_details->user_id = $request['user_id'];
                $portfolio_details->investment_id = $save_inv->id;
                $portfolio_details->folio_number = $portfolio->folio_number;

                //dd(array_key_exists((int) $portfolio->scheme_code, $scheme_details),$portfolio->scheme_code, $scheme_details);
                if (array_key_exists((int) $portfolio->scheme_code, $scheme_details)) {
                    $portfolio_details->scheme_code = $portfolio->scheme_code;
                }else{

                    if ($port_count == 0) {
                        return response()->json(['msg' => 'success','response'=>'Cannot Add the Portfolio Details right noww.']);
                    }else{
                        $this->removeInvestments($save_inv->id);
                        return response()->json(['msg' => 'failed','response'=>'Scheme code doesn\'t match']);
                    }

                }

                $allotment_date = date('Y-m-d',strtotime($portfolio->investment_date));

                $portfolio_details->acc_no = $request['acc_no'];
                $portfolio_details->units_held = $portfolio->units_held;
                $portfolio_details->amount_invested = $portfolio->inv_amount;
                $portfolio_details->initial_amount_invested = $portfolio->inv_amount;
                $portfolio_details->initial_units_held = $portfolio->units_held;

                $portfolio_details->portfolio_type = $portfolio->portfolio_type;
                $portfolio_details->portfolio_status = 1;
                $portfolio_details->invested_nav = $portfolio->invested_nav;
                $portfolio_details->investment_date = $allotment_date;
                $portfolio_details->bse_allotment_date = $allotment_date;
                $portfolio_details->bse_order_status = 0;
                $portfolio_details->pan = $request['pan_number'];

                if ($portfolio_details->save()) {
                    $count++;
                }


            }

            if ($port_count == $count) {
                return response()->json(['msg' => 'success','response'=>'Portfolio Succesfully Added. Kindly reload the page and check the portfolio']);
            }else{

                $this->removeInvestments($save_inv->id);
                return response()->json(['msg' => 'failed','response'=>'Cannot Add the Portfolio Details right now.']);
            }
        }
    }


    public function addManualWithdraw(Request $request){
        $request->validate([
            'user_id' => 'required',
            'wd-amount' => 'required',
            'scheme-code' => 'required',
            'pan-number' => 'required',
            'full-amount' => 'required',
            'withdraw-date' => 'required',
            'withdraw-nav' => 'required',
            'withdraw-units' => 'required',
            'acc-no' => 'required',
            'portfolio-type' => 'required',
        ]);


        $total_available_units = PortfolioDetails::where('pan', $request['pan-number'])->where('portfolio_status', 1)->sum('units_held');

        if($total_available_units < $request['withdraw-units']){

            dd('he;;');
            return response()->json(['msg' => 'failed','response'=>'Withdraw Units are greater than the available units.']);

        }


        $last_group_id = WithdrawDetails::max('withdraw_group_id') + 1;

        $wd = new WithdrawDetails();

        $wd->user_id = $request['user_id'];
        $wd->withdraw_group_id = $last_group_id;
        $wd->pan = $request['pan-number'];
        $wd->acc_no = $request['acc-no'];
        $wd->scheme_code = $request['scheme-code'];
        $wd->withdraw_amount = $request['wd-amount'];
        $wd->withdraw_date = date('Y-m-d', strtotime($request['withdraw-date']));
        $wd->portfolio_type = $request['portfolio-type'];
        $wd->withdraw_status = 1;
        $wd->full_amount = $request['full-amount'];
        $wd->bse_order_status = 0;
        $wd->bse_order_date =  date('Y-m-d', strtotime($request['withdraw-date']));
        $wd->bse_redemption_date = date('Y-m-d', strtotime($request['withdraw-date']));
        $wd->withdraw_nav = $request['withdraw-nav'];
        $wd->withdraw_units = $request['withdraw-units'];

        $save_wd = $wd->save();


        if ($request['full-amount'] == '0') {


            $portfolio_details = PortfolioDetails::where('user_id',$request['user_id'])
                ->where('scheme_code',$request['scheme-code'])
                ->where('portfolio_status',1)
                ->get();

            if($portfolio_details->count() == 0){
                return response()->json(['msg' => 'failed','response'=>'No Investments Available in this scheme.']);
            }


            $withdraw_units = $request['withdraw-units'];

            foreach ($portfolio_details as $portfolio_detail) {

                if($withdraw_units != 0){


                    if($portfolio_detail->units_held == $withdraw_units) {
//                                            dd('1');
                        //Checking wthether the available portfolio units is equal to withdraw units. in case it is "YES". So changing the portfolio status to 2 (Fully withdrawn)
                        $withdraw_units = 0;
                        $portfolio_detail->units_held = 0;
                        $portfolio_detail->amount_invested = 0;
                        $portfolio_detail->portfolio_status = 2;
                        $save_portfolio = $portfolio_detail->save();
                    } elseif($portfolio_detail->units_held < $withdraw_units) {
//                                            dd($portfolio_detail->units_held, $withdraw_units);

                        //if the units held less than  withdraw units
                        $withdraw_units -= $portfolio_detail->units_held;
                        $portfolio_detail->units_held = 0;
                        $portfolio_detail->amount_invested = 0;
                        $portfolio_detail->portfolio_status = 2;
                        $save_portfolio = $portfolio_detail->save();
                    } elseif($portfolio_detail->units_held > $withdraw_units) {
//                                            dd(3);
                        //if the units held is greater than the units to be withdrawn
                        $portfolio_detail->units_held -= $withdraw_units;
                        $portfolio_detail->amount_invested -= ($withdraw_units * $request['withdraw-nav']);
                        $withdraw_units = 0;
                        $portfolio_detail->portfolio_status = 1;
                        $save_portfolio = $portfolio_detail->save();
                    }
                }
            }

//            $update_withdraw = WithdrawDetails::where('bse_order_no',$redeem[1])->update(['withdraw_nav'=>$redeem[18],'withdraw_units'=>$redeem[19],'withdraw_status'=>1, 'bse_redemption_date'=>$redeem[4]]);

            if ($save_portfolio && $save_wd) {

                return response()->json(['msg' => 'success','response'=>'Withdraw Details has been Added Successfully.']);

            }else{

                return response()->json(['msg' => 'failed','response'=>'Withdraw Details cannot be Added.']);

            }

        }



        if ($request['full-amount'] == '1') {

//            dd('hello');
            $update_portfolio = PortfolioDetails::where('user_id',$request['user_id'])
                ->where('scheme_code', $request['scheme-code'])
                ->where('portfolio_status', 1)
                ->update(['units_held'=> 0,'amount_invested'=>0,'portfolio_status'=>2]);


            if($save_wd && $update_portfolio){
                return response()->json(['msg' => 'success','response'=>'Withdraw Details has been Added Successfully.']);

            }
        }

    }

    public function removeInvestments($inv_id){
        PortfolioDetails::where('investment_id',$inv_id)->delete();
        InvestmentDetails::where('id',$inv_id)->delete();
    }


    public function updateUserDetails(Request $request)
    {
        $request->validate([
                'name'=>'required',
                'user_id'=>'required',
                'mobile'=>'required',
                'email' =>'required',
        ]);


        $update = User::where('id',$request['user_id'])->update(['name'=>$request['name'],'mobile'=>$request['mobile'],'email'=>$request['email']]);

        if ($update) {
            return response()->json(['msg'=>'success']);
        }else{
            return response()->json(['msg'=>'failure']);
        }

    }

    public function clientDetail()
    {
        $users = User::where('role','!=',1)->orderBy('name')->get()->toArray();
        $finalData = [];
        $count = 0;
        foreach ($users as $value) {
            $finalData[$count]['id'] = $value['id'];
            $finalData[$count]['name'] = $value['name'];
            $finalData[$count]['email'] = $value['email'];
            $finalData[$count]['mobile'] = $value['mobile'];
            $finalData[$count]['bank'] = BankDetails::where('user_id',$value['id'])->get()->toArray();
            $count += 1;
        }
        return view('admin.admin-client-details',['info'=>$finalData]);
    }

    //function to cancel pending order
    public function cancelPendingOrder(Request $request)
    {

        $validator = \Validator::make($request->all(),[

            'id' => 'required',
            'transaction_type' => 'required',
            ]);

        if ($validator->fails()) {

            return response()->json(['msg' => 'failure', 'status' => 'Something went wrong. Please refresh and try again']);
        } else {

//            dd($_POST, 'hello');

            $transaction_type = $request['transaction_type'];
            $id = intval($request['id']);

            //processing cancel order for withdraw
            if($transaction_type == 'Withdrawal') {
                $withdraw_detail = WithdrawDetails::find($id);
                if($withdraw_detail != NULL && $withdraw_detail->withdraw_status == 0) {
                    $withdraw_detail->withdraw_status = 2;
                    if($withdraw_detail->save()) {
                        return response()->json(['msg' => 'success', 'status' => 'You have successfully cancelled  withdraw', 'amount' => $withdraw_detail->withdraw_amount, 'date' => date('d-m-Y')]);
                    } else {
                        return response()->json(['msg' => 'failure', 'status' => 'Withdraw cancel failed. Please refresh and try again']);
                    }
                } else {
                    return response()->json(['msg' => 'failure', 'status' => 'Withdraw cancel failed. Please refresh and try again']);
                }
            } else if($transaction_type == 'Investment') {//processing cancel order for investment
                $portfolio_detail = PortfolioDetails::find($id);
                if($portfolio_detail != NULL && $portfolio_detail->portfolio_status == 0) {
                    $portfolio_detail->portfolio_status = 3;
                    if($portfolio_detail->save()) {

                        $cancelled_portfolio_count = PortfolioDetails::where('investment_id', $portfolio_detail->investment_id)
                            ->where('portfolio_status', 3)
                            ->count();

                        $total_portfolio_count = PortfolioDetails::where('investment_id', $portfolio_detail->investment_id)
                            ->count();


                        if($total_portfolio_count == $cancelled_portfolio_count){
                            InvestmentDetails::where('id', $portfolio_detail->investment_id)->update(['investment_status' => 3]);
                        }


                        return response()->json(['msg' => 'success', 'status' => 'You have successfully cancelled investment ', 'amount' => $portfolio_detail->amount_invested, 'date' => date('d-m-Y')]);
                    } else {
                        return response()->json(['msg' => 'failure', 'status' => 'Investment cancel failed. Please refresh and try again']);
                    }

                } else {
                    return response()->json(['msg' => 'failure', 'status' => 'Investment cancel failed. Please refresh and try again']);
                }
            } else {
                return response()->json(['msg' => 'failure', 'status' => 'Something went wrong. Please refresh and try again']);
            }
        }
    }

    public function getSchemeInfo(Request $request)
    {
        $scheme_details = SchemeDetails::where('scheme_code',$request['scheme_code'])->get()->toArray();
        $scheme_history = SchemeHistory::where('scheme_code',$request['scheme_code'])->get()->toArray();

        $finalData = ['scheme_name'=>$scheme_details[0]['scheme_name'],
                     'scheme_code'=>$scheme_details[0]['scheme_code'],
                     'fund_manager'=>$scheme_history[0]['fund_manager'],
                     'asset_size'=>$scheme_history[0]['asset_size'],
                     'scheme_type'=>$scheme_details[0]['scheme_type'],
                     'fund_type'=>$scheme_history[0]['fund_type'],
                     'benchmark'=>$scheme_history[0]['benchmark'],
                     'scheme_priority'=>$scheme_details[0]['scheme_priority'],
                     'exit_load'=>$scheme_history[0]['exit_load'],
                     'launch_date'=>$scheme_history[0]['launch_date'],
                     'bse_scheme_code'=>$scheme_details[0]['bse_scheme_code'],
                     'scheme_status'=>$scheme_details[0]['scheme_status']];

        return response()->json(['msg'=>1,'info'=>$finalData]);
    }

    public function updateScheme(Request $request)
    {
        $update = SchemeDetails::where('scheme_code',$request['scheme_code'])
                                ->update(['scheme_code'=>$request['edit_scheme_code'],
                                        'scheme_type'=>$request['edit_scheme_type'],
                                        'scheme_name'=>$request['edit_scheme_name'],
                                        'scheme_priority'=>$request['edit_scheme_priority'],
                                        'bse_scheme_code'=>$request['edit_bse_scheme_code'],
                                        'scheme_status'=>$request['edit_scheme_status']]);
        if ($update) {
            $SchemeHistory = SchemeHistory::where('scheme_code',$request['scheme_code'])
                                ->update(['scheme_code'=>$request['edit_scheme_code'],
                                        'fund_type'=>$request['edit_fund_type'],
                                        'name'=>$request['edit_scheme_name'],
                                        'fund_manager'=>$request['edit_fund_manager'],
                                        'asset_size'=>$request['edit_asset_size'],
                                        'benchmark'=>$request['edit_benchmark'],
                                        'exit_load'=>$request['edit_exit_load'],
                                        'launch_date'=>$request['edit_launch_date']]);
            if ($SchemeHistory) {
                return response()->json(['msg'=>1]);
            }else{
                return response()->json(['msg'=>0, 'info'=>'SchemeHistory not updated']);
            }
        }else{
            return response()->json(['msg'=>0,'info'=>'SchemeDetails not updated']);
        }
    }

    public function updatePass(Request $request)
    {
        // http_response_code(500);
        // dd($request->all());
        $validator = \Validator::make($request->all(),[
            'user_id' => 'required',
            'old_pass' => 'required',
            'password' => 'required',
            'c_password' => 'required',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }else{
            $old_pass = User::where('id',$request['user_id'])->value('password');
            if (\Hash::check($request['old_pass'], $old_pass)) {
                if ($request['password'] == $request['c_password']) {
                    $update = User::where('id',$request['user_id'])->update(['password'=>bcrypt($request['password'])]);
                    if ($update) {
                        return response()->json(['msg'=>1]);
                    }else{
                        return response()->json(['msg'=>2,'info'=>'Something went wrong Try again']);
                    }
                }else{
                    return response()->json(['msg'=>2, 'info'=>'Password Miss Matched.']);
                }
            }else{
                return response()->json(['msg'=>2,'info'=>'Incorrect Old Password.']);
            }
        }
    }

    public function checkPayment(){

        $this->mfPassword();
        $inv_array = [];

        $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
            'soap_version' => SOAP_1_2, // !!!!!!!
            'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
          ));

        $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/MFAPI',true);
        $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

        $client->__setSoapHeaders([$actionHeader,$toHeader]);


        $portfolio_details = PortfolioDetails::where('portfolio_status',0)->where('bse_order_status',0)->get();

        //****** Use the below commented code to get order status of the induvidual orders *******//

         // $params = array(
         //      'Flag' => '11',
         //      'UserId' => '1245601',
         //      'EncryptedPassword' => $this->response_key,
         //      'param' => 'ABEPF5833F|26184389|BSEMF',

         //    );

         // $result = $client->MFAPI($params);
         // $result = explode('|', $result->MFAPIResult);

         // $payment_res = explode('(', $result[1]);
         // dd($payment_res);



        foreach ($portfolio_details as $portfolio) {
            //echo $portfolio->bse_order_no."-".$portfolio->user->personalDetails->pan."<br>";
            $params = array(
              'Flag' => '11',
              'UserId' => '1245601',
              'EncryptedPassword' => $this->response_key,
              'param' => $portfolio->pan.'|'.$portfolio->bse_order_no.'|BSEMF',

            );

            $result = $client->MFAPI($params);
            $result = explode('|', $result->MFAPIResult);

            $payment_res = explode('(', $result[1]);
            //echo $portfolio->user->personalDetails->pan." -- ".$portfolio->bse_order_no." -- ".trim($payment_res[0])."<br>";

            // http_response_code(500);
            // dd($payment_res);

            if (trim($payment_res[0]) == "APPROVED") {
                // echo "Inside Approved";
                PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'apr']);
            }else if (trim($payment_res[0]) == "REJECTED") {
                // reject the order.
                // echo "Inside rejected";
                PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'rej','portfolio_status'=>3]);
                $inv_array[] = $portfolio->investment_id;
            }else if (trim($payment_res[0]) == "AWAITING FOR FUNDS CONFIRMATION") {
                // Change the Payment Status column to AWC
                // echo "inside afc";
                PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'afc']);
                $inv_array[] = $portfolio->investment_id;
            }else if (trim($payment_res[0]) == "PAYMENT NOT INITIATED FOR GIVEN ORDER") {
                // Chnage the Payment STatus to PNI
                // echo "Inside pni";

                // if it stays "PNI" for more than 24 hours Cancel it off
                PortfolioDetails::where('id',$portfolio->id)->update(['bse_payment_status'=>'pni']);
                $inv_array[] = $portfolio->investment_id;
            }else{

            }
            //echo ;
        }


        $update_pni_port = PortfolioDetails::where('created_at', '<', Carbon::now()->subDay())->where('bse_payment_status','pni')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);

        $update_ac = PortfolioDetails::where('created_at', '<', Carbon::now()->subDays(7))->where('bse_payment_status','ac')->where('portfolio_status','!=',3)->update(['portfolio_status'=>3]);

        foreach ($inv_array as $inv) {

            $can_port_count = PortfolioDetails::where('investment_id',$inv)->where('portfolio_status','!=',3)->count();
            // echo $can_port_count."<br>";

            if ($can_port_count == 0) {
                InvestmentDetails::where('id',$inv)->update(['investment_status'=>3]);
            }
        }
        // dd($update_ac);



        // $params = array(
        //       'Flag' => '11',
        //       'UserId' => '1245601',
        //       'EncryptedPassword' => $this->response_key,
        //       'param' => 'BFRPN4910B|24041305|BSEMF',

        //     );

        // $result = $client->MFAPI($params);

        // dd($result->MFAPIResult);
        // die();



        //dd($params);



          //dd($client->__getLastRequestHeaders());

          //dd($result);
        return response()->json(['msg'=>'success','response'=>'Status updated successfuly']);

    }

    public function mfPassword(){
        $client = new SoapClient("https://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc?singleWsdl",array(
                'soap_version' => SOAP_1_2, // !!!!!!!
                'Content-Type' => 'application/soap+xml; charset=utf-8[\r][\n]',
              ));

        $actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://www.bsestarmf.in/2016/01/IStarMFWebService/getPassword',true);
        $toHeader = new SoapHeader('http://www.w3.org/2005/08/addressing', 'To', 'http://www.bsestarmf.in/StarMFWebService/StarMFWebService.svc/Basic');

        $client->__setSoapHeaders([$actionHeader,$toHeader]);

            $login_params = array(
              'UserId' => '1245601',
              'MemberId' => '12456',
              'Password' => 'Rf@2017$',
              'PassKey' => '1234569870',

            );

          $result = $client->getPassword($login_params);
          $result = $result->getPasswordResult;
          // dd($result);

          $response = explode('|', $result);

          // dd($response);
          $pwd_response =  $response[0];
          $this->response_key = $response[1];
      }








}
