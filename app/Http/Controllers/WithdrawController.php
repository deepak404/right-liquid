<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\BankDetails;
use App\Documents;
use App\SchemeDetails;
use App\SchemeHistory;
use App\DailyNav;
use App\HistoricNav;
use App\GetUserPortfolioDetails;
use App\PortfolioDetails;
use App\WithdrawDetails;
use App\InvestmentDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use NumberFormatter;


class WithdrawController extends Controller
{

    protected $pass_key = 'abcdef7895';
    protected $password = 'Rf@2017$';
    protected $response_key;

    public function withdrawFunds(Request $request)
    {



        $validator = \Validator::make($request->all(),[
            'withdraw_funds' => 'required',
            
        ]);

        if ($validator->fails()) {
            return response()->json(['msg' => 'failure','status' => 'Withdraw failed. Please check and try again']);
        } else {
        	$pan;
            //$pan = \Auth::user()->PersonalDetails->pan;
            $bse_scheme_code = SchemeDetails::pluck('bse_scheme_code','scheme_code');

            $withdraw_request = $request['withdraw_funds'];
            $withdraw_funds = explode('|', $withdraw_request);
            $scheme_details = SchemeDetails::get()->pluck('scheme_type','scheme_code');
            $scheme_details = $scheme_details->toArray();
            $withdraw_status = array();
            $withdrawn_amount = 0;
            $withdraw_group_id = '';
            $withdraw_code_amount = array();

            // print_r($withdraw_funds);
            // die();

//            dd($withdraw_funds);

            foreach($withdraw_funds as $withdraw_fund) {
                $withdrawl_funds = explode('/', $withdraw_fund);
                if(array_key_exists(0, $withdrawl_funds) && array_key_exists(1, $withdrawl_funds)) {
                   if(strip_tags($withdrawl_funds[0]) != '' && strip_tags($withdrawl_funds[1]) != '' && strip_tags($withdrawl_funds[2]) != '') {
                        //$withdraw_code_amount[$withdrawl_funds[0]] = $withdrawl_funds[1];
                        $withdraw_code_amount[$withdrawl_funds[0]] = array(
                            'amount' => $withdrawl_funds[1],
                            'f_nf' => $withdrawl_funds[2],
                            'pan' => $withdrawl_funds[3],
                            'acc_no' => $withdrawl_funds[4]
                            );
                   }
                }   
            }

//            dd($withdraw_code_amount);
            // print_r($withdraw_code_amount);
            // die();


            $withdraw_details = WithdrawDetails::where('user_id',\Auth::user()->id)->where('withdraw_status','0')->get();
            //dd($pending_withdraws);
            $pending_withdraws = array();
            //dd(count($withdraw_details));

            //dd($withdraw_code_amount);


            /*Below foreach is to loop through all the current authenticated user's pending withdrawal and compare it with 
            the current value of the scheme*/


//            dd($withdraw_details);

            foreach ($withdraw_details as $withdraw) {
                /*
                    The below code is to check whether the withdrawal amount is greater than the available withdraw.
                    The available withdraw is calculated from pending withdraws and current value of the particular scheme.
                */

                if (array_key_exists($withdraw->scheme_code, $withdraw_code_amount)) {

                    if($withdraw->pan == $withdraw_code_amount[$withdraw->scheme_code]['pan']){

                        if($withdraw->acc_no == $withdraw_code_amount[$withdraw->scheme_code]['acc_no']){
                            //Below is to calulate the current value of the scheme.

                            $nav_code_value = DailyNav::where('scheme_code',$withdraw->scheme_code)->orderBy('date', 'desc')->first();
                            $units_held = \Auth::user()->portfolioDetails()->where('pan', $withdraw->pan)->where('acc_no', $withdraw->acc_no)->where('scheme_code',$withdraw->scheme_code)->where('portfolio_status','1')->sum('units_held');
                            $nav_value = $nav_code_value->nav_value;
                            $current_value = round(($units_held * $nav_value),2);


                            if (array_key_exists($withdraw->scheme_code, $pending_withdraws)) {
                                $pending_withdraws[$withdraw->scheme_code]['amount'] += $withdraw->withdraw_amount;
                            }else{
                                $pending_withdraws[$withdraw->scheme_code]['amount'] = $withdraw->withdraw_amount;
                            }

                            $pending_withdrawal_value = $pending_withdraws[$withdraw->scheme_code]['amount'];


                            $available_withdraw = $current_value - $pending_withdrawal_value;

                            //if withdrawal amount is greater than available withdraw then return with error.


                            if ($withdraw_code_amount[$withdraw->scheme_code]['amount'] > $available_withdraw) {

                                $scheme_name = SchemeDetails::where('scheme_code',$withdraw->scheme_code)->pluck('scheme_name');
                                return response()->json(['msg'=>'failure', 'status' => 'The withdrawal amount is greater than the available value in '.$scheme_name[0]]) ;
                            }else{
                                //echo "okay bhaai";
                            }
                        }
                    }
                }

//                dd('here');
            }


//            dd($pending_withdraws);

            $user = \Auth::user();
            $withdraw_group_id = WithdrawDetails::max('withdraw_group_id');
            //dd($withdraw_code_amount,$withdraw_group_id);
            $withdraw_group_id++;


//            dd($withdraw_code_amount);
            foreach($withdraw_code_amount as $code => $amount) {
//            	 print_r($amount);
//            	 die();
               	$pan = $amount['pan'];
               	$acc_no = $amount['acc_no'];

                $total_units_held = $user->portfolioDetails()
                    ->where('pan', $pan)
                    ->where('acc_no', $acc_no)
                    ->where('scheme_code', $code)
                    ->where('portfolio_status', 1)
                    ->sum('units_held');

                $nav_code_value = DailyNav::where('scheme_code',$code)->orderBy('date', 'desc')->first();
                $total_current_value = $nav_code_value->nav_value * $total_units_held;


                //dd($total_current_value,$amount);

                if(round($total_current_value, 2) < (int)$amount['amount']) {

                    $withdraw_status[$code] = 0;

                } else {

                    $date = date('Y-m-d');
                    $date = explode('-', $date);
                    $date = $date[0].$date[1].$date[2];
                    $time = explode('.', microtime());
                    $time = substr($time[1], 0 ,6);
                    $unique_ref_no = $date."12456".$time;
                    $int_ref_no = $unique_ref_no;


                    //dd($amount['amount'],round($total_current_value,0));
                    $withdraw_details = new WithdrawDetails();
                    $withdraw_details->scheme_code = $code;
                    $withdraw_details->unique_ref_no = $unique_ref_no;
                    $withdraw_details->int_ref_no = $unique_ref_no;
                    $withdraw_details->withdraw_amount = $amount['amount'];
                    $withdraw_details->pan = $amount['pan'];
                    $withdraw_details->acc_no = $amount['acc_no'];
                    $withdraw_details->withdraw_date = date('Y-m-d');
                    $withdraw_details->portfolio_type = $scheme_details[$code];
                    $withdraw_details->withdraw_group_id = $withdraw_group_id;
                    if ($amount['f_nf'] == "f") {
                        $withdraw_details->full_amount = 1;
                    }
                    if ($amount['f_nf'] == "nf") {
                        $withdraw_details->full_amount = 0;
                    }
                    $withdraw_details->withdraw_status = 0;


                    if($user->withdrawDetails()->save($withdraw_details)) {
                        $withdraw_status[$code] = 1;
                        $transact_mode = 'NEW';


                        $user_folio = PortfolioDetails::where('user_id',$user->id)->where('scheme_code',$code)->where('pan',$amount['pan'])->where('acc_no', $amount['acc_no'])->whereIn('portfolio_status',[1,2])->whereNotNull('folio_number')->get()->last();



                            if ($user_folio->count() == 0) {
                                $folio_number = '';
                            }else{
                                $folio_number = $user_folio['folio_number'];

                            }


                        $buy_sell = 'R';
                        $buy_sell_type = 'FRESH';

                        if ($amount['f_nf'] == "f") {
                            $all_redeem = 'Y';
                            $withdraw_amount = '';
                        }else{
                            $all_redeem = 'N';
                            $withdraw_amount = $amount['amount'];
                        }

                        $this->getPassword();

                        if($all_redeem == "Y"){

//                            dd('hello');
                            $bse_withdraw_response[] = $this->placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$code],$buy_sell,$buy_sell_type,'',$all_redeem,$folio_number,$int_ref_no,$withdraw_details->id);

                        }else{

                            $bse_withdraw_response[] = $this->placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$code],$buy_sell,$buy_sell_type,round($withdraw_amount, 0),$all_redeem,$folio_number,$int_ref_no,$withdraw_details->id);

                        }


//                        dd($bse_withdraw_response);
//                         if($code == '118058'){

//                        dd($all_redeem, $withdraw_amount, $amount);
//                             $bse_withdraw_response[] = $this->placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$code],$buy_sell,$buy_sell_type,round($withdraw_amount, 0),$all_redeem,$folio_number,$int_ref_no,$withdraw_details->id);
//                         }else{
//                             $bse_withdraw_response[] = $this->placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$code]."-L1",$buy_sell,$buy_sell_type,$withdraw_amount,$all_redeem,$folio_number,$int_ref_no,$withdraw_details->id);
//                         }

//                        $bse_withdraw_response[] = $this->placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code[$code],$buy_sell,$buy_sell_type,$withdraw_amount,$all_redeem,$folio_number,$int_ref_no,$withdraw_details->id);


                        //dd($bse_withdraw_response);

                        $withdrawn_amount += $amount['amount'];
                    } else {
                        $withdraw_status[$code] = 0;
                    }
                }
            }

//            dd($withdraw_status, $bse_withdraw_response);

//            if(!in_array(0,$withdraw_status)) {
                foreach ($bse_withdraw_response as $key => $value) {

                    $date = date('d/m/Y h:i:s A', strtotime($date));

                    //Cancelling the Withdraw instantly if the BSE Order is Failed.
                    if($value['order_status'] == 1){
                        WithdrawDetails::where('id',$value['wd_id'])->where('unique_ref_no',$value['unique_ref_no'])->update([
                            'bse_order_no' => $value['order_no'],
                            'bse_remarks' => $value['remarks'],
                            'bse_order_status' => $value['order_status'],'
                             bse_order_date' => date('Y-m-d'),
                            'withdraw_status' => 3]);

                    }else{
                        WithdrawDetails::where('id',$value['wd_id'])->where('unique_ref_no',$value['unique_ref_no'])->update(['bse_order_no' => $value['order_no'],'bse_remarks' => $value['remarks'],'bse_order_status' => $value['order_status'],'bse_order_date' => date('Y-m-d')]);

                    }
                }

                $withdraw_group = WithdrawDetails::where('withdraw_group_id', $withdraw_group_id)->get();

                $withdraw_response = [];
                $fmt = new NumberFormatter('en_IN', NumberFormatter::DECIMAL);

                foreach ($withdraw_group as $withdraw_entry){
                    $scheme_name = SchemeDetails::where('scheme_code', $withdraw_entry->scheme_code)->value('scheme_name');
                    if($withdraw_entry->bse_order_status == 0){
                        $status = 'Success';
                    }else{
                        $status = 'Failed';
                    }

                    $w_amount = $withdraw_entry->withdraw_amount;

                    $withdraw_response[$withdraw_entry->id][] = array(
                        'scheme_name' => $scheme_name,
                        'status' => $status,
                        'amount' => $fmt->format($w_amount)
                    );
                }
                return response()->json(['msg'=>'success', 'status' => 'You have successfuly scheduled an withdraw','amount' => $withdrawn_amount,'date' => date('d-m-Y'), 'response' => $withdraw_response ]);
//            } else {
//                //dd($withdraw_details);
//                return response()->json(['msg'=>'failure', 'status' => 'Withdraw failed. Please check and try again']) ;
//            }

        }
        
    }


    public function getPassword(){


        $soap_request = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
           <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
           <wsa:To>http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc</wsa:To>
           </soap:Header>
           
           <soap:Body>
              <bses:getPassword>
                 <!--Optional:-->
                 <bses:UserId>1245601</bses:UserId>
                 <!--Optional:-->
                 <bses:Password>'.$this->password.'</bses:Password>
                 <!--Optional:-->
                 <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
              </bses:getPassword>
           </soap:Body>
        </soap:Envelope>';

        //echo "hello";


        $result = $this->makeSoapCall($soap_request);
        // var_dump($result);
        // die();
        $result = explode('http://bsestarmf.in/MFOrderEntry/getPasswordResponse', $result);

//        dd($result);
        $response = explode('|', $result[1]);

        //var_dump($response);
        //echo $response[1];

        ///preg_match('/<getPasswordResult>-(.*?)-<getPasswordResult>/', $result, $pass_response);


       // dd($pass_response);

        $pwd_response =  $response[0];
        $this->response_key = $response[1];

        //echo $this->response_key;

      }

      

      public function placeWithdraw($transact_mode,$unique_ref_no,$pan,$bse_scheme_code,$buy_sell,$buy_sell_type,$withdraw_amount,$all_redeem,$folio_number,$int_ref_no,$withdraw_id){

        //dd($this->response_key);



        /*
              Order Response will be in this order. Kindly don't go through the BSE web file documentation.
              That will only confuse you. 

              The order response will be in this format.

              TRANSACTION_CODE|UNIQUE REFERENCE NUMBER|BSE ORDER NO|USER ID|MEMBER ID|CLIENT CODE|BSE REMARKS|SUCCESS FLAG

              UNIQUE REFERENCE NUMBER = $unique_ref_no //this should be unique for each order. This will be tracking for the order.
              BSE ORDER NO //This is generated by BSE
              TRANSACTION CODE // NEW||MOD||CXL -- New order || Modification || Cancel.
              SUCCESS FLAG // 0 == ORDER SUCCESS and 1 = ORDER FAILURE *What a Brilliant thing by BSE* 

        */


//        dd($all_redeem);

        $response_key = explode('</getPasswordResult></getPasswordResponse></s:Body></s:Envelope>', $this->response_key);



        $soap_request = '<?xml version="1.0" encoding="utf-8"?>
        <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
           <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
           <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
           <wsa:To>http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc</wsa:To>
           </soap:Header>
       <soap:Body>
          <bses:orderEntryParam>
             <!--Optional:-->
             <bses:TransCode>'.$transact_mode.'</bses:TransCode>
             <!--Optional:-->
             <bses:TransNo>'.$unique_ref_no.'</bses:TransNo>
             <!--Optional:-->
             <bses:OrderId></bses:OrderId>
             <!--Optional:-->
             <bses:UserID>1245601</bses:UserID>
             <!--Optional:-->
             <bses:MemberId>12456</bses:MemberId>
             <!--Optional:-->
             <bses:ClientCode>'.$pan.'</bses:ClientCode>
             <!--Optional:-->
             <bses:SchemeCd>'.$bse_scheme_code.'</bses:SchemeCd>
             <!--Optional:-->
             <bses:BuySell>'.$buy_sell.'</bses:BuySell>
             <!--Optional:-->
             <bses:BuySellType>'.$buy_sell_type.'</bses:BuySellType>
             <!--Optional:-->
             <bses:DPTxn>P</bses:DPTxn>
             <!--Optional:-->
             <bses:OrderVal>'.$withdraw_amount.'</bses:OrderVal>
             <!--Optional:-->
             <bses:Qty></bses:Qty>
             <!--Optional:-->
             <bses:AllRedeem>'.$all_redeem.'</bses:AllRedeem>
             <!--Optional:-->
             <bses:FolioNo>'.$folio_number.'</bses:FolioNo>
             <!--Optional:-->
             <bses:Remarks></bses:Remarks>
             <!--Optional:-->
             <bses:KYCStatus>Y</bses:KYCStatus>
             <!--Optional:-->
             <bses:RefNo>'.$int_ref_no.'</bses:RefNo>
             <!--Optional:-->
             <bses:SubBrCode></bses:SubBrCode>
             <!--Optional:-->
             <bses:EUIN>E173580</bses:EUIN>
             <!--Optional:-->
             <bses:EUINVal>N</bses:EUINVal>
             <!--Optional:-->
             <bses:MinRedeem>N</bses:MinRedeem>
             <!--Optional:-->
             <bses:DPC>N</bses:DPC>
             <!--Optional:-->
             <bses:IPAdd></bses:IPAdd>
             <!--Optional:-->
             <bses:Password>'.$response_key[0].'</bses:Password>
             <!--Optional:-->
             <bses:PassKey>'.$this->pass_key.'</bses:PassKey>
             <!--Optional:-->
             <bses:Parma1></bses:Parma1>
             <!--Optional:-->
             <bses:Param2></bses:Param2>
             <!--Optional:-->
             <bses:Param3></bses:Param3>
          </bses:orderEntryParam>
       </soap:Body>
    </soap:Envelope>';

//    dd($soap_request);

        $result = $this->makeSoapCall($soap_request);
        //echo $result;



        $response = explode('http://bsestarmf.in/MFOrderEntry/orderEntryParamResponse', $result);
        $response = explode('|', $response[1]);

        $trans_code = explode('</a:Action></s:Header><s:Body><orderEntryParamResponse xmlns="http://bsestarmf.in/"><orderEntryParamResult>', $response[0]);
        $order_response = explode('</orderEntryParamResult></orderEntryParamResponse></s:Body></s:Envelope>', $response[7]);

        $order_response = array(
            'trans_code' => $trans_code[1],
            'unique_ref_no' => $response[1],
            'order_no' =>$response[2],
            'client_code' => $response[5],
            'remarks' =>$response[6],
            'order_status' => $order_response[0],
            'wd_id' => $withdraw_id,
          );

        //$this->place_order_count++;
        return $order_response;
        

      }

      public function makeSoapCall($soap_request){

        //dd($soap_request);
        $header = array(
            "Content-type: application/soap+xml;charset=\"utf-8\"",
            "Accept: application/soap+xml",
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Content-length: ".strlen($soap_request),
        );
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL,"http://www.bsestarmf.in/MFOrderEntry/MFOrder.svc?singleWsdl" );
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($soap_do, CURLOPT_POST,           true );
        curl_setopt($soap_do, CURLOPT_POSTFIELDS,     $soap_request);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER,     $header);
        $result = curl_exec($soap_do);
        //dd($result);
        return $result;
      }
}
