<?php
namespace App;
use PHPExcel_Calculation_Financial;
class GetUserPortfolioDetails{

    function userPortfolioDetails($user_id){
        $return_array = array();


        $portfolio_details = PortfolioDetails::where('user_id',$user_id)->where('portfolio_status',1)->get()->groupBy('scheme_code');

        //dd($portfolio_details);

        $portfolios  = array();
        $count = 0;
        $portfolio_total['amount_invested'] = 0;
        $portfolio_total['units_held'] = 0;
        $portfolio_total['current_value'] = 0;
        $portfolio_total['net_returns'] = 0;
        $date_sum = 0;
        $total_xirr = 0;
        $net_returns_array = [];
        //dd($portfolio_details);



        if (count($portfolio_details) != 0) {


            foreach ($portfolio_details as $s_code => $portfolio_detail) {

                // dd($portfolio_detail);
//                $total_amount_invested = \Auth::user()->PortfolioDetails()->where('portfolio_status',1)->where('scheme_code',$s_code)->sum('amount_invested');
                $total_amount_invested = 0;
                foreach ($portfolio_detail as $portfolio) {
                    $total_amount_invested += $portfolio->units_held * $portfolio->invested_nav;
                    // dd($portfolio);
                    $nav_code_value = DailyNav::where('scheme_code',$s_code)->orderBy('date', 'desc')->first();
                    $scheme_name = SchemeDetails::where('scheme_code',$s_code)->pluck('scheme_name');

                    $units_per_scheme = \Auth::user()->PortfolioDetails()->where('scheme_code',$s_code)->where('portfolio_status',1)->sum('units_held');
//                    dd($units_per_scheme * $nav_code_value->nav_value);
//                    $amount_per_scheme = \Auth::user()->PortfolioDetails()->where('scheme_code',$s_code)->where('portfolio_status',1)->sum('amount_invested');
                    $amount_per_scheme = $units_per_scheme * $nav_code_value->nav_value;

//                    dd($amount_per_scheme, $s_code, \Auth::user(), $nav_code_value);

                    if (array_key_exists($s_code, $portfolios)) {

                        $current_value = round($nav_code_value['nav_value'] * $portfolio['units_held'],2);
                        $net_returns = round($current_value - $portfolio['amount_invested'],2);

                        $portfolios[$s_code]['current_value'] += $current_value;
                        $portfolios[$s_code]['scheme_name'] = $scheme_name[0];
                        $portfolios[$s_code]['scheme_type'] = $portfolio['portfolio_type'];
                        $portfolios[$s_code]['folio_number'] = $portfolio['folio_number'];
                        $portfolios[$s_code]['units_held'] += $portfolio['units_held'];
//                        $portfolios[$s_code]['amount_invested'] += $portfolio['amount_invested'];
                        $portfolios[$s_code]['amount_invested'] += $portfolio->units_held * $portfolio->invested_nav;
                        $portfolios[$s_code]['net_returns'] += round($net_returns,2);

                        // $this->getXirrReturns($s_code);
                        $portfolios[$s_code]['current_nav'] = round($nav_code_value['nav_value'],2);

                        if ($total_amount_invested != 0) {
                            $holding_date = (date_diff(date_create(date('Y-m-d')),date_create($portfolio['bse_allotment_date']))->format('%d')) * (($portfolio->units_held * $portfolio->invested_nav)/$total_amount_invested);
                            $portfolios[$s_code]['holding_period'] += $holding_date;
                        }else{
                            $portfolios[$s_code]['holding_period'] = 0;
                        }

                        // $holding_date = $holding_date;




                        $portfolio_total['amount_invested'] += $portfolio->units_held * $portfolio->invested_nav;

                        $portfolio_total['units_held'] += $portfolio['units_held'];
                        $portfolio_total['current_value'] += round($nav_code_value['nav_value'] * $portfolio['units_held'],2);

                        $net_returns_array[] = $net_returns;



                    }else{

                        // $this->getXirrReturns($s_code);

                        $current_value = round($nav_code_value['nav_value'] * $portfolio['units_held'],2);
//                         dd($current_value);
                        $net_returns = round($current_value - round($portfolio->units_held * $portfolio->invested_nav, 2),2);
                        // dd($portfolios);
                        // dd($net_returns);
                        $portfolios[$s_code]['id'] = $portfolio['id'];
                        $portfolios[$s_code]['current_value'] = $current_value;
                        $portfolios[$s_code]['scheme_name'] = $scheme_name[0];
                        $portfolios[$s_code]['folio_number'] = $portfolio['folio_number'];
                        $portfolios[$s_code]['scheme_type'] = $portfolio['portfolio_type'];
                        $portfolios[$s_code]['units_held'] = $portfolio['units_held'];
                        $portfolios[$s_code]['amount_invested'] = $portfolio->units_held * $portfolio->invested_nav;
                        $portfolios[$s_code]['net_returns'] = round($net_returns,2);
                        $portfolios[$s_code]['current_nav'] = round($nav_code_value['nav_value'],2);
                        // dd($portfolios);

                        if ($total_amount_invested != 0) {
                            $holding_date = (date_diff(date_create(date('Y-m-d')),date_create($portfolio['bse_allotment_date']))->format('%d')) * ($portfolio->units_held * $portfolio->invested_nav/$total_amount_invested);
                            $portfolios[$s_code]['holding_period'] = $holding_date;
                        }else{
                            $portfolios[$s_code]['holding_period'] = 0;
                        }

                        // $holding_date = $holding_date;
                        // $portfolios[$s_code]['holding_period'] = $holding_date;

                        $portfolio_total['amount_invested'] += round($portfolio->units_held * $portfolio->invested_nav, 2);
                        $portfolio_total['units_held'] += $portfolio['units_held'];
                        $portfolio_total['current_value'] += $current_value;
                        // $portfolio_total['holding_period'] = $holding_date;

                        // if($s_code == "100845"){
                        //     dd($portfolio['amount_invested'], $net_returns);
                        // }

                        $net_returns_array[] = $net_returns;

                    }

                    // dd($net_returns_array);




                    $portfolio_total['net_returns'] += $net_returns;
                    $count++;


                    $get_xirr = $this->getXirrReturns($s_code,$user_id);


                    $portfolios[$s_code]['xirr_returns'] = $get_xirr;
                    $total_xirr += $get_xirr;

                    $port_count = PortfolioDetails::where('user_id',\Auth::user()->id)->where('scheme_code',$s_code)->where('portfolio_status',1)->count();
                    // dd($port_count);
                    if($port_count != 0 ){
                        $portfolios[$s_code]['holding_period'] = $portfolios[$s_code]['holding_period']/$port_count;
                    }else{
                        $portfolios[$s_code]['holding_period'] = 0;
                    }

                    $portfolios[$s_code]['abs_returns'] = (($portfolios[$s_code]['current_value'] - $portfolios[$s_code]['amount_invested'])/$portfolios[$s_code]['amount_invested']) * 100;

                    // dd($portfolios);


                    if ($portfolios[$s_code]['holding_period'] != 0) {
                        $portfolios[$s_code]['ann_returns'] = $portfolios[$s_code]['abs_returns'] * (365/$portfolios[$s_code]['holding_period'])/$count;
                    }else{
                        $portfolios[$s_code]['ann_returns'] = 0;
                    }


                }


            }

            // dd(count($portfolios), $total_xirr);

            $portfolio_total['total_xirr'] = round($total_xirr/count($portfolios), 2);
            // dd($portfolio_total);
            //dd(($portfolio_total['current_value'] - $portfolio_total['amount_invested']),$portfolio_total['amount_invested']);
            // $portfolio_total['abs_returns'] = round((($portfolio_total['current_value'] - $portfolio_total['amount_invested'])/$portfolio_total['amount_invested']) * 100,2);

            $portfolio_total['net_returns'] = round($portfolio_total['current_value'] - $portfolio_total['amount_invested'],2);

            // // if ($portfolio_total['holding_period'] != 0) {
            //    $portfolio_total['ann_returns'] = round($portfolio_total['abs_returns'] * (365/$portfolio_total['holding_period']),2);
            // }else{
            //     $portfolio_total['ann_returns'] = 0;
            // }



            $return_array['portfolios'] = $portfolios;
            $return_array['portfolio_total'] = $portfolio_total;

            // dd($return_array['portfolio_total']);
        }else{

            // $portfolio_total['abs_returns'] = 0;

            // $portfolio_total['net_returns'] = 0;
            // $portfolio_total['ann_returns'] = 0;
            $portfolio_total['amount_invested'] = 0;
            $portfolio_total['units_held'] = 0;
            $portfolio_total['current_value'] = 0;
            $portfolio_total['total_xirr'] = 0;

            $portfolios = null;
            $return_array['portfolios'] = $portfolios;
            $return_array['portfolio_total'] = $portfolio_total;

        }


//         dd($return_array);
        return $return_array;
    }

    function getXirrReturns($scheme_code,$user_id){
        // dd($scheme_code);
        $portfolios = PortfolioDetails::where('user_id',$user_id)->where('scheme_code',$scheme_code)->whereIn('portfolio_status',[1,2])->get();
        $amount = [];
        $date = [];
        $total_units = $portfolios->sum('units_held');
        $last_nav = DailyNav::where('scheme_code',$scheme_code)->orderBy('date', 'desc')->get()->pluck('nav_value');
        $last_nav = $last_nav[0];
        $current_value = (float)$total_units * (float)$last_nav;

//        if($scheme_code == 118058){
//            dd($portfolios);
//        }

        foreach ($portfolios as $portfolio) {

            if($portfolio->status = 2){
                $amount[] = -$portfolio->initial_units_held * $last_nav;
                $date[] = $portfolio->bse_allotment_date;

            }else{
                $amount[] = -$portfolio->units_held * $last_nav;
                $date[] = $portfolio->bse_allotment_date;
            }


        }

        $withdraws = WithdrawDetails::where('user_id',$user_id)->where('scheme_code',$scheme_code)->where('withdraw_status',1)->get();


        foreach ($withdraws as $withdraw) {
            $amount[] = $withdraw->withdraw_amount;
            $date[] = $withdraw->withdraw_date;


        }

        $amount[] = round($current_value, 2);
        $date[] = date('Y-m-d');


//        if($scheme_code == 118058){
//            dd($amount, $date);
//        }

        try{
            $xirr = round(PHPExcel_Calculation_Financial::XIRR($amount,$date) * 100,2);

        }catch(\Exception $e){
//            dd($scheme_code, $user_id);
        }
        // dd($xirr);
        return $xirr;


    }
}


?>