<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemeDetails extends Model
{
    public function schemeDetails(){
    	return $this->hasMany('App\PortfolioDetails');
    }
}
