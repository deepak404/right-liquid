<?php

namespace App\Console\Commands;

use App\InvestmentDetails;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CancelRtgs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CancelRTGS';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This Commands Cancels all the RTGS Investments without UTR No after 2 Days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $investments = InvestmentDetails::where('investment_status','0')->where('payment_type', 'rtgs')->where('utr_no',null)->where('investment_date','<', Carbon::now()->subDays(2)->toDateString())->get();

        foreach($investments as $inv){

            foreach($inv->portfolioDetails as $portfolio){
                $portfolio->portfolio_status = 3;
                $portfolio->save();
            }

            $inv->investment_status = 3;
            $inv->save();

        }
    }
}
