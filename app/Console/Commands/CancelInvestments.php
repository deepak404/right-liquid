<?php

namespace App\Console\Commands;

use App\InvestmentDetails;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CancelInvestments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CancelInvestments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancels all the Pending Investments after 4 days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $investment_details = InvestmentDetails::where('investment_date','<', Carbon::now()->subDays(4)->toDateString())->where('investment_status',0)->get();


        foreach ($investment_details as $inv){
            $portfolio_details = $inv->portfolioDetails;

            foreach ($portfolio_details as $portfolio_detail){
                $portfolio_detail->portfolio_status = 3;
                $portfolio_detail->save();
            }

            $inv->investment_status = 3;
            $inv->save();


        }
//        dd($investment_details);
    }
}
