<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class CancelNEFT extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CancelNEFT';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the payment status of the pending orders and cancels PNI order after a day and AFC after 7 days.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        App::call('App\Http\Controllers\AdminController@checkPayment');

    }
}
