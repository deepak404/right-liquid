<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentDetails extends Model
{

	protected $fillable = [
        'user_id','pan','investment_amount','investment_status','investment_date','payment_type','utr_no'
    ];

    public function portfolioDetails(){
    	return $this->hasMany('App\PortfolioDetails','investment_id');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function bankDetails(){
        return $this->belongsTo('App\BankDetails', 'pan');
    }
}
